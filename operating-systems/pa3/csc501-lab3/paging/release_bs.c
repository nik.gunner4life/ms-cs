#include <conf.h>
#include <kernel.h>
#include <proc.h>
#include <paging.h>

SYSCALL release_bs(bsd_t bs_id) 
{
	bsTab[bs_id].status = 0;
	bsTab[bs_id].as_heap = 0;
	bsTab[bs_id].npages = 0;
	int j = 0;
	for(j = 0; j<NPROC; j++)
	{
		bsTab[bs_id].procList[j] = 0;
	}
}

