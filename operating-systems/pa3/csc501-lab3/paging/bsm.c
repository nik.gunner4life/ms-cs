/* bsm.c - manage the backing store mapping*/

#include <conf.h>
#include <kernel.h>
#include <paging.h>
#include <proc.h>

bs_t bsTab[MAX_ID + 1];
/*-------------------------------------------------------------------------
 * init_bsm- initialize bsm_tab
 *-------------------------------------------------------------------------
 */
SYSCALL init_bsm()
{
	//Initialize BS Table(bs_t)
	int i  = 0;
	for(i = 0; i<=MAX_ID; i++)
	{
		bsTab[i].status = 0;
		bsTab[i].as_heap = 0;
		bsTab[i].npages = 0;
		bsTab[i].baseAddr = i * BACKING_STORE_UNIT_SIZE	+ BACKING_STORE_BASE;		//Changes to these variables have been made in paging.h.
		int j = 0;
		for(j = 0; j<NPROC; j++)
		{
			bsTab[i].procList[j] = 0;
		}
	}
	return (OK);
}

/*-------------------------------------------------------------------------
 * get_bsm - get a free entry from bsm_tab 
 *-------------------------------------------------------------------------
 */
SYSCALL get_freeBSM()
{
	int i  = 0;
	struct	mblock	*bstoreptr;
	for(i = 0; i<=MAX_ID; i++)
	{
		if(bsTab[i].status == 0)
		{
			bsTab[i].as_heap = 1;
			bsTab[i].status = 1;
			nsrPrintf("\nReturning Backing Store with ID %d", i);
			return i;
		}
	}
	//nsrPrintf("\nNo Free BS was found\n");
	return SYSERR;
}


/*-------------------------------------------------------------------------
 * free_bsm - free an entry from bsm_tab 
 *-------------------------------------------------------------------------
 */
SYSCALL free_bsm(int i)
{
}

/*-------------------------------------------------------------------------
 * bsm_lookup - lookup bsm_tab and find the corresponding entry
 *-------------------------------------------------------------------------
 */
SYSCALL bsm_lookup(int pid, long vaddr, int* store, int* pageth)
{
}


/*-------------------------------------------------------------------------
 * bsm_map - add an mapping into bsm_tab 
 *-------------------------------------------------------------------------
 */
SYSCALL bsm_map(int pid, int vpno, int source, int npages)
{
}

/*-------------------------------------------------------------------------
 * bsm_unmap - delete an mapping from bsm_tab
 *-------------------------------------------------------------------------
 */
SYSCALL bsm_unmap(int pid, int vpno, int flag)
{
}

/*-------------------------------------------------------------------------
 * print_bs - printing the BS values
 *-------------------------------------------------------------------------
 */
int print_bs()
{
	int i = 0;
	for(i = 0; i<=MAX_ID; i++)
	{
		//nsrPrintf("\nBS %d has baseaddr %d, status %d", i, bsTab[i].baseAddr, bsTab[i].status);
	}
}