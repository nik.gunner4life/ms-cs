/* policy.c = srpolicy*/

#include <conf.h>
#include <kernel.h>
#include <paging.h>
nsrDebugPrintf = 0;
/*-------------------------------------------------------------------------
 * srpolicy - set page replace policy 
 *-------------------------------------------------------------------------
 */
SYSCALL srpolicy(int policy)
{
	nsrDebugPrintf = 1;
	if(policy == FIFO)
	{
		page_replace_policy = policy;
		nsrPrintf("\nThe Replacement policy is FIFO or %d\n", page_replace_policy);
	}
	else if(policy == AGING)
	{
		page_replace_policy = policy;
		nsrPrintf("\nThe Replacement policy is AGING or %d\n", page_replace_policy);
	}
	else
		nsrPrintf("\nSet the aging policy correctly\n");
}

/*-------------------------------------------------------------------------
 * grpolicy - get page replace policy 
 *-------------------------------------------------------------------------
 */
SYSCALL grpolicy()
{
  return page_replace_policy;
}
