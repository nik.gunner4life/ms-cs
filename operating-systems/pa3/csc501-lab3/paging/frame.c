/* frame.c - manage physical frames */
#include <conf.h>
#include <kernel.h>
#include <proc.h>
#include <paging.h>

fr_map_t frm_tab[NFRAMES];
struct _frame_t frameTab[NFRAMES];
/*-------------------------------------------------------------------------
 * init_frm - initialize frm_tab
 *-------------------------------------------------------------------------
 */
SYSCALL init_frm()
{
	int i  = 0;
	for(i = 0; i < NFRAMES; i++)
	{
		frameTab[i].status = FRM_FREE;		//Tells us what the status is
		frameTab[i].refcnt = 0;				//Still dunno how to use this. Will come to it later
		frameTab[i].bs = -1;					//Tells us the BS this frame is associated with.
		frameTab[i].bs_page = -1;			//Tells us which page in the backing store.
		//frameTab[i].pid = 0;				//Tells us the process holding this frame
		frameTab[i].age = 0;				//Used for aging Policy. Havent come to this yet. 
		int j = 0;
		for(j = 0; j < NPROC; j++)
		{
			frameTab[i].pid[j] = 0;				//Tells us the process holding this frame
		}
	}
	return OK;
}

/*-------------------------------------------------------------------------
 * get_freeFrame - get a free frame according page replacement policy
 *-------------------------------------------------------------------------
 */


SYSCALL get_freeFrame(int frameType)
{
	//Have still not implemented the page replacement Policy
	if(page_replace_policy == FIFO)
	{
		STATWORD ps;
		disable(ps);
		int i = 0;
		for(i = 0; i < NFRAMES; i++)
		{
			if(frameTab[i].status == FRM_FREE)
			{
				nsrPrintf("\nAllocating %d", FRAME0 + i);
				frameTab[i].status = FRM_USED;
				if(frameType == FR_PAGE)
					fifoPagingEnQ(FRAME0 + i);
				restore(ps);
				return FRAME0 + i;
			}
		}
		nsrPrintf("\nNo Free Frames found");
		int frameNo = fifoPagingDeQ();
		if(frameType == FR_PAGE)
			fifoPagingEnQ(frameNo);
		nsrPrintf("\nEvicting and Allocating Frame %d\n", frameNo);
		restore(ps);
		return frameNo;
	}
	else if(page_replace_policy == AGING)
	{
		STATWORD ps;
		disable(ps);
		
		int i = 0;
		for(i = 0; i < NFRAMES; i++)
		{
			if(frameTab[i].status == FRM_FREE)
			{
				nsrPrintf("\nAllocating Frame %d", FRAME0 + i);
				frameTab[i].status = FRM_USED;
				if(frameType == FR_PAGE)
				{
					fifoPagingEnQ(FRAME0 + i);
					//setAgeOfFrames();
				}
				restore(ps);
				return FRAME0 + i;
			}
		}
		nsrPrintf("\nNo Free Frames found");
		setAgeOfFrames();
		int frameNo = findFrameWithMinAge();
		if(frameType == FR_PAGE)
			fifoPagingEnQ(frameNo);
		nsrPrintf("\nEvicting and Allocating Frame %d\n", frameNo);

		restore(ps);
		return frameNo;
	}
}

/*-------------------------------------------------------------------------
 * free_frm - free a frame 
 *-------------------------------------------------------------------------
 */
SYSCALL free_frm(int i)
{

  kprintf("To be implemented!\n");
  return OK;
}

/*-------------------------------------------------------------------------
 * set_frm	- Setting the values of a frame. 
 *-------------------------------------------------------------------------
 */

int set_frm(int status, int pid, int bsID, int bsPage, int refcount, int age, int vpno, int frameNo)
{
	STATWORD ps;
	disable(ps);
	if(status != FR_IGNORE_ENTRY)
		frameTab[frameNo].status = status;
	if(pid != FR_IGNORE_ENTRY)
		frameTab[frameNo].pid[currpid] = 1;
	if(bsID != FR_IGNORE_ENTRY)
		frameTab[frameNo].bs = bsID;
	if(bsPage != FR_IGNORE_ENTRY)
		frameTab[frameNo].bs_page = bsPage;
	if(refcount != FR_IGNORE_ENTRY)
		frameTab[frameNo].refcnt = refcount;
	if(age != FR_IGNORE_ENTRY)
		frameTab[frameNo].age = age;
	if(vpno != FR_IGNORE_ENTRY)
		frameTab[frameNo].vpno[pid] = vpno;
	restore(ps);
	//kprintf("\nFrame %d has been set to %d, PID is %d, BSID is %d, bsPage is %d, vpno is %d", FRAME0 + frameNo, frameTab[frameNo].status, frameTab[frameNo].pid, frameTab[frameNo].bs, frameTab[frameNo].bs_page, frameTab[frameNo].vpno);
}

/*-------------------------------------------------------------------------
 * print_frm	- Printing the values of a frame. 
 *-------------------------------------------------------------------------
 */
int print_frm()
{
	int i = 0;
	for(i = 0; i < NFRAMES; i++)
	{
		if(frameTab[i].status != FRM_FREE)
		{
			//nsrPrintf("\n%d -> %d, %d, %d, %d, 0x%08X", FRAME0 + i, frameTab[i].status, frameTab[i].pid, frameTab[i].bs, frameTab[i].bs_page, frameTab[i].vpno);
		}
	}
}

