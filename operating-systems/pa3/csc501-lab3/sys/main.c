/* user.c - main */

#include <conf.h>
#include <kernel.h>
#include <proc.h>
#include <stdio.h>
#include <paging.h>

void halt();

/*------------------------------------------------------------------------
 *  main  --  user main program
 *------------------------------------------------------------------------
 */

int prA, prB, prC, prD, prE, prF, prG, pr1, pr2, pr3, pr4; 

int main() 
{
	kprintf("\n\nHello World. Xinu Lives!!\n\n");
	//test1();
	//test2();
	//test3();
	//test4();
	//test5();
	//test6();
	//test7();
	//test8();
	//test9();
	//testBS();
	return 0;
}

/* ----------------------------------------------------------------------------------------------------------------------------------------------- */
//Creating Multile Backing Stores for the same process (Main). Use this to test FIFO also
/* ----------------------------------------------------------------------------------------------------------------------------------------------- */
test1()
{
	kprintf("Mapping multiple backing stores for the same process ... Set NFRAMES to 20");

	int bs = 1;
	int bs2 = 2; 
	int bs1Limit = 9;
	int bs2Limit = 5;

	char *addr = (char*) 0x40000000; //1G
	int i = ((unsigned long) addr) >> 12;	// the ith page

	char *addr2 = (char*) 0x50000000; //1G
	int j = ((unsigned long) addr2) >> 12;

	get_bs(bs, 200);
	get_bs(bs2, 200);
	
	if (xmmap(i, bs, 200) == SYSERR) 
	{
		kprintf("xmmap call failed\n");
		return 0;
	}
	if (xmmap(j, bs2, 200) == SYSERR) 
	{
		kprintf("xmmap call failed\n");
		return 0;
	}

	kprintf("\n1st Store\n");
	for (i = 0; i < bs1Limit; i++) {
		*addr = 'A' + i;
		kprintf("\n%c", *addr);
		addr += NBPG;	//increment by one page each time
		//addr += 4;			  //Increment by 4 bytes
	}
	kprintf("\n2nd Store\n");
	for (i = 0; i < bs2Limit; i++) {
		*addr2 = 'P' + i;
		kprintf("\n%c", *addr2);
		addr2 += NBPG;	//increment by one page each time
		//addr += 4;			  //Increment by 4 bytes
	}

	addr = (char*) 0x40000000; //1G
	for (i = 0; i < bs1Limit; i++) 
	{
		kprintf("\n0x%08x: %c", addr, *addr);
		addr += NBPG;       //increment by one page each time
		//addr += 4;			  //Increment by 4 bytes
	}

	kprintf("\n2nd Store\n");
	addr2 = (char*) 0x50000000; //1G
	for (i = 0; i < bs2Limit; i++) 
	{
		kprintf("\n0x%08x: %c", addr2, *addr2);
		addr2 += NBPG;       //increment by one page each time
		//addr += 4;			  //Increment by 4 bytes
	}
	xmunmap(0x40000000 >> 12);

}

/* ----------------------------------------------------------------------------------------------------------------------------------------------- */
//Creating Multile Processes and mapping them to differet BS. Use this to test FIFO also
/* ----------------------------------------------------------------------------------------------------------------------------------------------- */
procATest2(char c) 
{
	int bs = 1;
	int bs1Limit = 14;

	char *addr = (char*) 0x40000000; //1G
	int i = ((unsigned long) addr) >> 12;	// the ith page

	get_bs(bs, 200);
	
	if (xmmap(i, bs, 200) == SYSERR) 
	{
		kprintf("xmmap call failed\n");
		return 0;
	}

	kprintf("\n1st Store A - P\n");
	for (i = 0; i < bs1Limit; i++) {
		*addr = 'A' + i;
		kprintf("\n%c", *addr);
		addr += NBPG;	//increment by one page each time
		//addr += 4;			  //Increment by 4 bytes
	}

	addr = (char*) 0x40000000; //1G
	for (i = 0; i < bs1Limit; i++) 
	{
		kprintf("\n0x%08x: %c", addr, *addr);
		addr += NBPG;       //increment by one page each time
		//addr += 4;			  //Increment by 4 bytes
	}

	xmunmap(0x40000000 >> 12);
}
procBTest2(char c) 
{
	int bs2 = 2; 
	int bs2Limit = 5;

	int i = 0;
	char *addr2 = (char*) 0x50000000; //1G
	int j = ((unsigned long) addr2) >> 12;

	get_bs(bs2, 200);
	
	if (xmmap(j, bs2, 200) == SYSERR) 
	{
		kprintf("xmmap call failed\n");
		return 0;
	}
	
	kprintf("\n2nd Store R to V\n");
	for (i = 0; i < bs2Limit; i++) {
		*addr2 = 'R' + i;
		kprintf("\n%c", *addr2);
		addr2 += NBPG;	//increment by one page each time
		//addr += 4;			  //Increment by 4 bytes
	}

	kprintf("\n2nd Store\n");
	addr2 = (char*) 0x50000000; //1G
	for (i = 0; i < bs2Limit; i++) 
	{
		kprintf("\n0x%08x: %c", addr2, *addr2);
		addr2 += NBPG;       //increment by one page each time
		//addr += 4;			  //Increment by 4 bytes
	}
	xmunmap(0x50000000 >> 12);
}
test2()
{
	kprintf("\nStarting Process A\n");
	resume(prA = create(procATest2, 1000, 20, "proc A", 1, 'A'));
	sleep(2);
	kprintf("\nStarting Process B\n");
	resume(prB = create(procBTest2, 1000, 20, "proc B", 1, 'B'));
}

/* ----------------------------------------------------------------------------------------------------------------------------------------------- */
//Creating A backing Store and Killing the process. And getting Another process to read the same backing store
/* ----------------------------------------------------------------------------------------------------------------------------------------------- */
procCTest3(char c) 
{
	int bs = 1;
	int bs1Limit = 5;

	char *addr = (char*) 0x40000000; //1G
	int i = ((unsigned long) addr) >> 12;	// the ith page

	get_bs(bs, 200);
	
	if (xmmap(i, bs, 200) == SYSERR) 
	{
		kprintf("xmmap call failed\n");
		return 0;
	}

	kprintf("\n1st Store\n");
	for (i = 0; i < bs1Limit; i++) {
		*addr = 'A' + i;
		kprintf("\n%c", *addr);
		addr += NBPG;	//increment by one page each time
		//addr += 4;			  //Increment by 4 bytes
	}

	addr = (char*) 0x40000000; //1G
	for (i = 0; i < bs1Limit; i++) 
	{
		kprintf("\n0x%08x: %c", addr, *addr);
		addr += NBPG;       //increment by one page each time
		//addr += 4;			  //Increment by 4 bytes
	}

	xmunmap(0x40000000 >> 12);
}
procDTest3(char c) 
{
	int bs2 = 1; 
	int bs2Limit = 5;

	int i = 0;
	char *addr2 = (char*) 0x50000000; //1G
	int j = ((unsigned long) addr2) >> 12;

	get_bs(bs2, 200);
	
	if (xmmap(j, bs2, 200) == SYSERR) 
	{
		kprintf("xmmap call failed\n");
		return 0;
	}
	
	kprintf("\n2nd Store\n");
	addr2 = (char*) 0x50000000; //1G
	for (i = 0; i < bs2Limit; i++) 
	{
		kprintf("\n0x%08x: %c", addr2, *addr2);
		addr2 += NBPG;       //increment by one page each time
		//addr += 4;			  //Increment by 4 bytes
	}
	xmunmap(0x50000000 >> 12);
}
test3()
{
	kprintf("\nStarting Process A\n");
	resume(pr1 = create(procCTest3, 1000, 20, "proc C", 1, 'A'));
	kprintf("\nMain is going to sleep\n");
	sleep(1);
	kprintf("\nStarting Process B\n");
	resume(pr2 = create(procDTest3, 1000, 20, "proc D", 1, 'B'));
}

/* ----------------------------------------------------------------------------------------------------------------------------------------------- */
//Accessing an unmapped Memory Block - Should throw an error and kill the process.
/* ----------------------------------------------------------------------------------------------------------------------------------------------- */
procATest4(char c) 
{
	int bs = 1;
	int bs1Limit = 6;

	char *addr = (char*) 0x40000000; //1G
	int i = ((unsigned long) addr) >> 12;	// the ith page

	get_bs(bs, 5);
	
	if (xmmap(i, bs, 5) == SYSERR) 
	{
		kprintf("xmmap call failed\n");
		return 0;
	}

	kprintf("\n1st Store\n");
	for (i = 0; i < bs1Limit; i++) {
		*addr = 'A' + i;
		kprintf("\n%c", *addr);
		addr += NBPG;	//increment by one page each time
		//addr += 4;			  //Increment by 4 bytes
	}

	addr = (char*) 0x40000000; //1G
	for (i = 0; i < bs1Limit; i++) 
	{
		kprintf("\n0x%08x: %c", addr, *addr);
		addr += NBPG;       //increment by one page each time
		//addr += 4;			  //Increment by 4 bytes
	}

	xmunmap(0x40000000 >> 12);
}
procBTest4(char c) 
{
	int bs2 = 1; 
	int bs2Limit = 5;

	int i = 0;
	char *addr2 = (char*) 0x50000000; //1G
	int j = ((unsigned long) addr2) >> 12;

	get_bs(bs2, 5);
	
	if (xmmap(j, bs2, 5) == SYSERR) 
	{
		kprintf("xmmap call failed\n");
		return 0;
	}
	
	kprintf("\n2nd Store\n");
	addr2 = (char*) 0x50000000; //1G
	for (i = 0; i < bs2Limit; i++) 
	{
		kprintf("\n0x%08x: %c", addr2, *addr2);
		addr2 += NBPG;       //increment by one page each time
		//addr += 4;			  //Increment by 4 bytes
	}
	/*xmunmap(0x40000000 >> 12);*/
}
test4()
{
	kprintf("\nStarting Process A\n");
	resume(pr1 = create(procATest4, 1000, 20, "proc A", 1, 'A'));
	kprintf("\nMain is going to sleep\n");
	sleep(2);
	kprintf("\nStarting Process B\n");
	resume(pr2 = create(procBTest4, 1000, 20, "proc B", 1, 'B'));
}

/* ----------------------------------------------------------------------------------------------------------------------------------------------- */
//Shared Memory Blocks - Ensure a new frame is not created for an already existing page in the physical Memory
/* ----------------------------------------------------------------------------------------------------------------------------------------------- */
procATest5(char c) 
{
	int bs = 1;
	int bs1Limit = 12;

	char *addr = (char*) 0x40000000; //1G
	int i = ((unsigned long) addr) >> 12;	// the ith page

	get_bs(bs, 5);
	
	if (xmmap(i, bs, 100) == SYSERR) 
	{
		kprintf("xmmap call failed\n");
		return 0;
	}

	kprintf("\n1st Store\n");
	for (i = 0; i < bs1Limit; i++) {
		*addr = 'A' + i;
		kprintf("\n%c", *addr);
		addr += NBPG;	//increment by one page each time
		//addr += 4;			  //Increment by 4 bytes
	}

	addr = (char*) 0x40000000; //1G
	for (i = 0; i < bs1Limit; i++) 
	{
		kprintf("\n0x%08x: %c", addr, *addr);
		addr += NBPG;       //increment by one page each time
		//addr += 4;			  //Increment by 4 bytes
	}
	kprintf("\nProc A is going to sleep for three seconds");
	sleep(3);
	kprintf("\nProc A has called unmap");
	xmunmap(0x40000000 >> 12);
}
procBTest5(char c) 
{
	int bs2 = 1; 
	int bs2Limit = 12;

	int i = 0;
	char *addr2 = (char*) 0x50000000; //1G
	int j = ((unsigned long) addr2) >> 12;

	get_bs(bs2, 5);
	
	if (xmmap(j, bs2, 100) == SYSERR) 
	{
		kprintf("xmmap call failed\n");
		return 0;
	}
	
	kprintf("\n2nd Store - Only Frames 1031 - 1036 should be used for the pages.\n");
	addr2 = (char*) 0x50000000; //1G
	for (i = 0; i < bs2Limit; i++) 
	{
		kprintf("\n0x%08x: %c", addr2, *addr2);
		addr2 += NBPG;       //increment by one page each time
		//addr += 4;			  //Increment by 4 bytes
	}
	kprintf("\nProc B is going to sleep for three seconds");
	sleep(3);
	kprintf("\nProc B has called unmap");
	xmunmap(0x50000000 >> 12);
}
test5()
{
	kprintf("\nStarting Process A\n");
	resume(pr1 = create(procATest5, 1000, 20, "proc A", 1, 'A'));
	kprintf("\nMain is going to sleep\n");
	sleep(2);
	kprintf("\nStarting Process B\n");
	resume(pr2 = create(procBTest5, 1000, 20, "proc B", 1, 'B'));
}

/* ----------------------------------------------------------------------------------------------------------------------------------------------- */
//Aging - Setting the policy and checking if aging works correctly
/* ----------------------------------------------------------------------------------------------------------------------------------------------- */

procATest6(char c) 
{
	int bs = 1;
	int bs1Limit = 16;

	char *addr = (char*) 0x40000000; //1G
	int i = ((unsigned long) addr) >> 12;	// the ith page
	char *addr2 = (char*) 0x40000000 + NBPG; //1G
	char *addr3 = (char*) 0x40000000 + (2 * NBPG); //1G

	get_bs(bs, 200);
	
	if (xmmap(i, bs, 200) == SYSERR) 
	{
		kprintf("xmmap call failed\n");
		return 0;
	}

	kprintf("\n1st Store\n");
	for (i = 0; i < bs1Limit; i++) 
	{
		*addr = 'A' + i;
		
		kprintf("\n%c", *addr);
		addr += NBPG;	//increment by one page each time
		//addr += 4;			  //Increment by 4 bytes
	}

	addr = (char*) 0x40000000; //1G
	for (i = 0; i < bs1Limit; i++) 
	{
		kprintf("\n0x%08x: %c", addr, *addr);
		if(i != 0 && i % 2 == 0)
		{
			kprintf("\nAging: %c", *addr2);
		}
		if(i != 0 && i % 4 == 0)
		{
			kprintf("\nAging: %c", *addr3);
		}
		addr += NBPG;       //increment by one page each time
		//addr += 4;			  //Increment by 4 bytes
	}
	sleep(3);
	kprintf("\nProc A has called unmap");
	xmunmap(0x40000000 >> 12);
}
procBTest6(char c) 
{
	int bs2 = 2; 
	int bs2Limit = 6;

	int i = 0;
	char *addr2 = (char*) 0x50000000; //1G
	int j = ((unsigned long) addr2) >> 12;

	get_bs(bs2, 200);
	
	if (xmmap(j, bs2, 200) == SYSERR) 
	{
		kprintf("xmmap call failed\n");
		return 0;
	}
	
	kprintf("\n2nd Store \n");
	for (i = 0; i < bs2Limit; i++) 
	{
		*addr2 = 'S' + i;
		kprintf("\n%c", *addr2);
		addr2 += NBPG;	//increment by one page each time
		//addr += 4;			  //Increment by 4 bytes
	}

	addr2 = (char*) 0x50000000; //1G
	for (i = 0; i < bs2Limit; i++) 
	{
		kprintf("\n0x%08x: %c", addr2, *addr2);
		addr2 += NBPG;       //increment by one page each time
		//addr += 4;			  //Increment by 4 bytes
	}
	sleep(3);
	kprintf("\nProc B has called xmunmap\n");
	xmunmap(0x50000000 >> 12);
}
test6()
{
	srpolicy(AGING);
	kprintf("\nStarting Process A\n");
	resume(pr1 = create(procATest6, 1000, 20, "proc A", 1, 'A'));
	kprintf("\nMain is going to sleep\n");
	sleep(2);
	kprintf("\nStarting Process B\n");
	resume(pr2 = create(procBTest6, 1000, 20, "proc B", 1, 'B'));
}

/* ----------------------------------------------------------------------------------------------------------------------------------------------- */
//vcreate, vgetmem and vfreemem
/* ----------------------------------------------------------------------------------------------------------------------------------------------- */
procATest7(char c) 
{
	int n = 10;
	char *x;
	char *temp;
	int i = 0;
	for(i = 0; i < n; i++)
	{
		x = vgetmem(NBPG);
		if(i == 0)
		{
			temp = x;
			kprintf("\n0x%08X, 0x%08X", temp, x);
		}
		*x = 'A' + i;
		kprintf("\n0x%08X: %c",x, *x);
	}
	x = temp;
	for(i = 0; i < n; i++)
	{
		kprintf("\nValue is 0x%08X: %c",x, *x);
		vfreemem(x, NBPG);
		x+=NBPG;
	}
}
procBTest7(char c) 
{
	int n = 10;
	char *x;
	char *temp;
	int i = 0;
	for(i = 0; i < n; i++)
	{
		x = vgetmem(NBPG);
		if(i == 0)
		{
			temp = x;
			kprintf("\n0x%08X, 0x%08X", temp, x);
		}
		*x = '1' + i;
		kprintf("\n0x%08X: %c",x, *x);
	}
	x = temp;
	for(i = 0; i < n; i++)
	{
		kprintf("\nValue is 0x%08X: %c",x, *x);
		vfreemem(x, NBPG);
		x+=NBPG;
	}
}
test7()
{
	//srpolicy(FIFO);
	kprintf("\nStarting Process A\n");
	resume(prA = vcreate(procATest7, 1000, 100, 20, "proc A", 1, 'A'));
	sleep(2);
	kprintf("\nStarting Process B\n");
	resume(prB = vcreate(procBTest7, 1000, 100, 20, "proc B", 1, 'B'));
}

/* ----------------------------------------------------------------------------------------------------------------------------------------------- */
//Shared Memory Blocks - Ensure a new frame is not created for an already existing page in the physical Memory
/* ----------------------------------------------------------------------------------------------------------------------------------------------- */
procATest8(char c) 
{
	int bs = 1;
	int bs1Limit = 12;

	char *addr = (char*) 0x40000000; //1G
	int i = ((unsigned long) addr) >> 12;	// the ith page

	get_bs(bs, 100);
	
	if (xmmap(i, bs, 100) == SYSERR) 
	{
		kprintf("xmmap call failed\n");
		return 0;
	}

	kprintf("\n1st Store\n");
	for (i = 0; i < bs1Limit; i++) {
		*addr = 'A' + i;
		kprintf("\n%c", *addr);
		addr += NBPG;	//increment by one page each time
		//addr += 4;			  //Increment by 4 bytes
	}

	addr = (char*) 0x40000000; //1G
	for (i = 0; i < bs1Limit; i++) 
	{
		kprintf("\n0x%08x: %c", addr, *addr);
		addr += NBPG;       //increment by one page each time
		//addr += 4;			  //Increment by 4 bytes
	}
	kprintf("\nProc A is going to sleep for three seconds");
	sleep(3);
	kprintf("\nProc A has called unmap");
	xmunmap(0x40000000 >> 12);
}
procBTest8(char c) 
{
	int bs2 = 1; 
	int bs2Limit = 12;

	int i = 0;
	char *addr2 = (char*) 0x50000000; //1G
	int j = ((unsigned long) addr2) >> 12;

	get_bs(bs2, 100);
	
	if (xmmap(j, bs2, 100) == SYSERR) 
	{
		kprintf("xmmap call failed\n");
		return 0;
	}
	
	kprintf("\n2nd Store\n");
	addr2 = (char*) 0x50000000; //1G
	for (i = 0; i < bs2Limit; i++) 
	{
		*addr2 = *addr2 + 10;
		kprintf("\n0x%08x: %c", addr2, *addr2);
		addr2 += NBPG;       //increment by one page each time
		//addr += 4;			  //Increment by 4 bytes
	}
	kprintf("\nProc B is going to sleep for three seconds");
	sleep(3);
	kprintf("\nProc B has called unmap");
	xmunmap(0x50000000 >> 12);
}
test8()
{
	kprintf("\nStarting Process A\n");
	resume(pr1 = create(procATest5, 1000, 20, "proc A", 1, 'A'));
	kprintf("\nMain is going to sleep\n");
	sleep(2);
	kprintf("\nStarting Process B\n");
	resume(pr2 = create(procBTest5, 1000, 20, "proc B", 1, 'B'));
}

/* ----------------------------------------------------------------------------------------------------------------------------------------------- */
//vcreate, vgetmem and vfreemem
/* ----------------------------------------------------------------------------------------------------------------------------------------------- */
procATest9(char c) 
{
	int n = 16;
	char *x;
	char *temp;
	int i = 0;
	for(i = 0; i < n; i++)
	{
		x = vgetmem(NBPG);
		if(i == 0)
		{
			temp = x;
			kprintf("\n0x%08X, 0x%08X", temp, x);
		}
		*x = 'A' + i;
		kprintf("\n0x%08X: %c",x, *x);
	}
	x = temp;
	for(i = 0; i < n; i++)
	{
		kprintf("\nValue is 0x%08X: %c",x, *x);
		vfreemem(x, NBPG);
		x+=NBPG;
	}
}
test9()
{
	srpolicy(AGING);
	kprintf("\nStarting Process A\n");
	resume(prA = vcreate(procATest9, 1000, 100, 20, "proc A", 1, 'A'));
	sleep(2);
}

/* ----------------------------------------------------------------------------------------------------------------------------------------------- */
//SYSERR Cases
/* ----------------------------------------------------------------------------------------------------------------------------------------------- */
procATestBS(char c) 
{
	kprintf("\nReleasing BS");
	release_bs(0);
}
procBTestBS(char c) 
{
	int npages = get_bs(0, 200);
	kprintf("\nNof of pages allocated is %d\n", npages);
	kprintf("\nChecking to see if backing store is allocated");
}
testBS()
{
	srpolicy(FIFO);
	kprintf("\nStarting Process A\n");
	resume(prA = vcreate(procATestBS, 1000, 100, 20, "proc A", 1, 'A'));
	sleep(2);
	kprintf("\nStarting Process B\n");
	resume(prA = create(procBTestBS, 1000, 100, 20, "proc A", 1, 'A'));
}

