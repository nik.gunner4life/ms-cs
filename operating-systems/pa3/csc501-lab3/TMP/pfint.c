/* pfint.c - pfint */

#include <conf.h>
#include <kernel.h>
#include <paging.h>
#include <proc.h>


/*-------------------------------------------------------------------------
 * pfint - paging fault ISR
 *-------------------------------------------------------------------------
 */
SYSCALL pfint()
{
	STATWORD ps;
	disable(ps);
	long faultAddress = read_cr2();
	long firstTenBits, middleTenBits, lastTwelveBits;
	struct pentry *pptr = &proctab[currpid];
	firstTenBits = faultAddress >> 22;
	middleTenBits = (faultAddress & 0x003ff000) >> 12;
	lastTwelveBits = faultAddress & 0x00000fff;
	nsrPrintf("\n#PAGE_FAULT in %s. The fault Address(cr2) is %08X. ", pptr->pname, faultAddress);

	//Check if it has an existing Page Table
	int i = 0;
	int currentPageTableFrame = 0;			
	
	pd_t *pageDirectoryValid;		//Tells us if the page table already exists or not.
	pt_t *pageTableValid;			//Tells us if the page has already there or not.
	
	pd_t pageDirectoryEntry;		//New Entry into the page directory table.
	pt_t pageTableEntry;			//New Entry into the page table.

	//Check if page directory and/or page table exists
	pageDirectoryValid = (pptr->pdbr * NBPG) + (firstTenBits * 4);
	if(pageDirectoryValid->pd_pres == 1)
	{
		//kprintf("\nPage Directory Entry is present");
		currentPageTableFrame = pageDirectoryValid->pd_base;
		pageTableValid = (currentPageTableFrame * NBPG) + (middleTenBits * 4);
		frameTab[currentPageTableFrame - FRAME0].refcnt++;
		//kprintf("\npfint Ref Cnt of %d is %d", currentPageTableFrame, frameTab[currentPageTableFrame - FRAME0].refcnt);
	}
	else
	{
		//kprintf("\nPage Directory Entry is NOT present");
		currentPageTableFrame = get_freeFrame(FR_TBL);
		//set_frm(int status, int pid, int bsID, int bsPage, int refcount, int age, int vpno, int frameNo)
		set_frm(FR_TBL, currpid, FR_IGNORE_ENTRY, FR_IGNORE_ENTRY, FR_IGNORE_ENTRY, FR_IGNORE_ENTRY, FR_IGNORE_ENTRY, currentPageTableFrame - FRAME0);
		pageDirectoryValid = (pptr->pdbr * NBPG) + (firstTenBits * 4);
		pageTableValid = (currentPageTableFrame * NBPG) + (middleTenBits * 4);
		frameTab[currentPageTableFrame - FRAME0].refcnt++;
		//kprintf("\npfint Ref Cnt of %d is %d", currentPageTableFrame, frameTab[currentPageTableFrame - FRAME0].refcnt);
	}

	//Mapping the Data from Backing Store to Free Frames (Current Assumption: There is eviction. FIFO is the Page Replacement Policy)
	int bsID = findBS(faultAddress, currpid);
	int pageToRead = findPage(faultAddress, currpid, bsID);
	//nsrPrintf("\nbsID is %d, Page to read is %d", bsID, pageToRead);
	
	//Memory Access Check
	if(pageToRead >= pptr-> bsList[bsID].bs_npages)
	{
		//nsrPrintf("\nAccessing a Page that has not been assigned .. Forcefully killing the process\n");
		kill(currpid);
		restore(ps);
		return SYSERR;
	}

	//Check if the page is already in the physical Memory
	int j = 0;
	for(j = 0; j < NFRAMES; j++)
	{
		if(frameTab[j].bs == bsID && frameTab[j].bs_page == pageToRead)
		{
			//nsrPrintf("\nPage already exists in the physical memory\n");
			fifoPagingPrintQ();
			//set_frm(int status, int pid, int bsID, int bsPage, int refcount, int age, int vpno, int frameNo)
			set_frm(FR_PAGE, currpid, FR_IGNORE_ENTRY, FR_IGNORE_ENTRY, FR_IGNORE_ENTRY, FR_IGNORE_ENTRY, faultAddress >> 12, j); //This is done because frameTab counts from 0 - 1023
			frameTab[j].refcnt++;

			pageDirectoryEntry.pd_pres = 1;
			pageDirectoryEntry.pd_write = 1;		/* page is writable?		*/
			pageDirectoryEntry.pd_user = 0;		/* is use level protection?	*/
			pageDirectoryEntry.pd_pwt = 0;		/* write through cachine for pt?*/
			pageDirectoryEntry.pd_pcd = 0;		/* cache disable for this pt?	*/
			pageDirectoryEntry.pd_acc = 0;		/* page table was accessed?	*/
			pageDirectoryEntry.pd_mbz = 0;		/* must be zero			*/
			pageDirectoryEntry.pd_fmb = 0;		/* four MB pages?		*/
			pageDirectoryEntry.pd_global = 0;		/* global (ignored)		*/
			pageDirectoryEntry.pd_avail = 0;	/* for programmer's use		*/
			pageDirectoryEntry.pd_base = currentPageTableFrame;		/* location of page table?	*/ 
			blkcopy(pageDirectoryValid, &pageDirectoryEntry, sizeof(pd_t));

			//Setting the Page Table Entry
			pageTableEntry.pt_pres = 1;
			pageTableEntry.pt_write = 1;		/* page is writable?		*/
			pageTableEntry.pt_user = 0;		/* is use level protection?	*/
			pageTableEntry.pt_pwt = 0;		/* write through cachine for pt?*/
			pageTableEntry.pt_pcd = 0;		/* cache disable for this pt?	*/
			pageTableEntry.pt_acc = 0;		/* page table was accessed?	*/
			pageTableEntry.pt_mbz = 0;		/* must be zero			*/
			pageTableEntry.pt_dirty = 0;		/* four MB pages?		*/
			pageTableEntry.pt_global = 0;		/* global (ignored)		*/
			pageTableEntry.pt_avail = 0;	/* for programmer's use		*/
			pageTableEntry.pt_base = FRAME0 + j;		/* location of page table?	*/
			blkcopy(pageTableValid, &pageTableEntry, sizeof(pt_t));	

			restore(ps);
			return OK;
		}
	}

	long newPage = get_freeFrame(FR_PAGE);
	//kprintf("... Frame Returned here is %d", newPage);
	fifoPagingPrintQ();
	//set_frm(int status, int pid, int bsID, int bsPage, int refcount, int age, int vpno, int frameNo)
	set_frm(FR_PAGE, currpid, bsID, pageToRead, FR_IGNORE_ENTRY, FR_IGNORE_ENTRY, faultAddress >> 12, newPage-FRAME0); //This is done because frameTab counts from 0 - 1023
	//kprintf("\nbsID is %d, Page to read is %d, Frame it is moved to is %d", bsID, pageToRead, newPage);
	read_bs(newPage, bsID, pageToRead);
	
	// Setting the PageDirectory Entry
	pageDirectoryEntry.pd_pres = 1;
	pageDirectoryEntry.pd_write = 1;		/* page is writable?		*/
	pageDirectoryEntry.pd_user = 0;		/* is use level protection?	*/
	pageDirectoryEntry.pd_pwt = 0;		/* write through cachine for pt?*/
	pageDirectoryEntry.pd_pcd = 0;		/* cache disable for this pt?	*/
	pageDirectoryEntry.pd_acc = 0;		/* page table was accessed?	*/
	pageDirectoryEntry.pd_mbz = 0;		/* must be zero			*/
	pageDirectoryEntry.pd_fmb = 0;		/* four MB pages?		*/
	pageDirectoryEntry.pd_global = 0;		/* global (ignored)		*/
	pageDirectoryEntry.pd_avail = 0;	/* for programmer's use		*/
	pageDirectoryEntry.pd_base = currentPageTableFrame;		/* location of page table?	*/ 
	blkcopy(pageDirectoryValid, &pageDirectoryEntry, sizeof(pd_t));

	//Setting the Page Table Entry
	pageTableEntry.pt_pres = 1;
	pageTableEntry.pt_write = 1;		/* page is writable?		*/
	pageTableEntry.pt_user = 0;		/* is use level protection?	*/
	pageTableEntry.pt_pwt = 0;		/* write through cachine for pt?*/
	pageTableEntry.pt_pcd = 0;		/* cache disable for this pt?	*/
	pageTableEntry.pt_acc = 0;		/* page table was accessed?	*/
	pageTableEntry.pt_mbz = 0;		/* must be zero			*/
	pageTableEntry.pt_dirty = 0;		/* four MB pages?		*/
	pageTableEntry.pt_global = 0;		/* global (ignored)		*/
	pageTableEntry.pt_avail = 0;	/* for programmer's use		*/
	pageTableEntry.pt_base = newPage;		/* location of page table?	*/
	blkcopy(pageTableValid, &pageTableEntry, sizeof(pt_t));	

	restore(ps);
	return OK;
}


