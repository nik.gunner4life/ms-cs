#include <conf.h>
#include <kernel.h>
#include <proc.h>
#include <paging.h>

int get_bs(bsd_t bs_id, unsigned int npages) 
{
	nsrPrintf("\nCalling Get BS: %d, for Pages: %d", bs_id, npages);
	if(bsTab[bs_id].as_heap == 1)
	{
		//nsrPrintf("\nIt is being used as a heap and hence cannot be allocated");
		return SYSERR;
	}
	if(npages > 200)
	{
		//nsrPrintf("\nYou have asked for too many pages.\n");
		return SYSERR;
	}
	bsTab[bs_id].status = 1;
	//As per forum post "Return Value for get_bs()", if npages has already been assigned before then the same number of pages should be used.
	if(bsTab[bs_id].npages > 0)
	{
		nsrPrintf("\n%d pages of %d BS has been allocated", npages, bs_id);
		return bsTab[bs_id].npages;
	}
	nsrPrintf("\n%d pages of %d BS has been allocated", npages, bs_id);
	bsTab[bs_id].npages = npages;
    return npages;
}


