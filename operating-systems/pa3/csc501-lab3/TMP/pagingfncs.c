/* Pagingfncs.c - manage page directories and page tables */
#include <conf.h>
#include <kernel.h>
#include <proc.h>
#include <paging.h>

/*----------------------------------------------
 * Initialize Page Directories and Page tables
 *----------------------------------------------
 */
SYSCALL init_pageDirectories()
{
	int i = 0;
	int frameNo  = get_freeFrame(FR_DIR);
	kprintf("\nNULL Proc Page Directory is %d", frameNo);
	//set_frm(int status, int pid, int bsID, int bsPage, int refcount, int age, int vpno, int frameNo)
	set_frm(FR_DIR, 0, FR_IGNORE_ENTRY, FR_IGNORE_ENTRY, FR_IGNORE_ENTRY, FR_IGNORE_ENTRY, FR_IGNORE_ENTRY, frameNo - FRAME0);
	pd_t *dirPtr = (pd_t *)(frameNo * NBPG);
	proctab[0].pdbr = frameNo;
	for(i = 0; i < 4; i++)
	{
		dirPtr->pd_pres = 1;
		dirPtr->pd_write = 1;		/* page is writable?		*/
		dirPtr->pd_user = 0;		/* is use level protection?	*/
		dirPtr->pd_pwt = 0;		/* write through cachine for pt?*/
		dirPtr->pd_pcd = 0;		/* cache disable for this pt?	*/
		dirPtr->pd_acc = 0;		/* page table was accessed?	*/
		dirPtr->pd_mbz = 0;		/* must be zero			*/
		dirPtr->pd_fmb = 0;		/* four MB pages?		*/
		dirPtr->pd_global = 0;		/* global (ignored)		*/
		dirPtr->pd_avail = 0;	/* for programmer's use		*/
		dirPtr->pd_base = FRAME0 + 1 + i;		/* location of page table?	*/
		//kprintf("\n%08X, %d", dirPtr->pd_base, dirPtr->pd_base);
		dirPtr++;
	}
	
	//This is for testing purposes (to check if read_bs works correctly). 

	//char *testAddr = (char *)(2304 * 4096);
	//for (i = 0; i < 16; i++) {
	//	*testAddr = 'A' + i;
	//	testAddr += ;	//increment by one page each time
	//	//addr += 4;			  //Increment by 4 bytes
	//}

	//testAddr = (char *)(2304 * 4096);
	//for (i = 0; i < 16; i++) {
	//	kprintf("0x%08x: %c\n", testAddr, *testAddr);
	//	testAddr += NBPG;	//increment by one page each time
	//	//addr += 4;			  //Increment by 4 bytes
	//}
}

SYSCALL init_pageTables()
{
	int i = 0;
	int loop = 0;
	pt_t *tablePtr;
	int frameNo;
	for(loop = 0; loop<4; loop++)
	{
		frameNo  = get_freeFrame(FR_TBL);
		kprintf("\nNULL Proc Page Table[%d] is %d", loop, frameNo);
		frameTab[frameNo].refcnt = PAGETABLE_ENTRIES;
		//set_frm(int status, int pid, int bsID, int bsPage, int refcount, int age, int vpno, int frameNo)
		set_frm(FR_TBL, GLOBAL_PID, FR_IGNORE_ENTRY, FR_IGNORE_ENTRY, FR_IGNORE_ENTRY, FR_IGNORE_ENTRY, FR_IGNORE_ENTRY, frameNo - FRAME0);
		tablePtr = (pt_t *)(frameNo * NBPG);
		int k;
		for(i = 0; i < PAGETABLE_ENTRIES; i++)
		{
			k = loop * PAGETABLE_ENTRIES + i;
			tablePtr->pt_pres = 1;
			tablePtr->pt_write = 1;		/* page is writable?		*/
			tablePtr->pt_user = 0;		/* is use level protection?	*/
			tablePtr->pt_pwt = 0;		/* write through cachine for pt?*/
			tablePtr->pt_pcd = 0;		/* cache disable for this pt?	*/
			tablePtr->pt_acc = 0;		/* page table was accessed?	*/
			tablePtr->pt_mbz = 0;		/* must be zero			*/
			tablePtr->pt_dirty = 0;		/* four MB pages?		*/
			tablePtr->pt_global = 0;		/* global (ignored)		*/
			tablePtr->pt_avail = 0;	/* for programmer's use		*/
			tablePtr->pt_base = k;		/* location of page table?	*/
			//kprintf("\n%08X, %d", tablePtr->pt_base, tablePtr->pt_base);
			tablePtr++;
		}
	}
}

SYSCALL init_processPageDirectory(int procPID)
{
	int i = 0;
	int frameNo  = get_freeFrame(FR_DIR);
	//set_frm(int status, int pid, int bsID, int bsPage, int refcount, int age, int vpno, int frameNo)
	set_frm(FR_DIR, procPID, FR_IGNORE_ENTRY, FR_IGNORE_ENTRY, FR_IGNORE_ENTRY, FR_IGNORE_ENTRY, FR_IGNORE_ENTRY, frameNo - FRAME0);
	//kprintf("\n%d", frameNo);
	pd_t *dirPtr = (pd_t *)(frameNo * NBPG);
	for(i = 0; i < 4; i++)
	{
		dirPtr->pd_pres = 1;
		dirPtr->pd_write = 1;		/* page is writable?		*/
		dirPtr->pd_user = 0;		/* is use level protection?	*/
		dirPtr->pd_pwt = 0;		/* write through cachine for pt?*/
		dirPtr->pd_pcd = 0;		/* cache disable for this pt?	*/
		dirPtr->pd_acc = 0;		/* page table was accessed?	*/
		dirPtr->pd_mbz = 0;		/* must be zero			*/
		dirPtr->pd_fmb = 0;		/* four MB pages?		*/
		dirPtr->pd_global = 0;		/* global (ignored)		*/
		dirPtr->pd_avail = 0;	/* for programmer's use		*/
		dirPtr->pd_base = FRAME0 + 1 + i;		/* location of page table?	*/
		dirPtr++;
	}
	return frameNo;
}

int findBS(long faultAddress, int procPID)
{
	STATWORD ps;
	disable(ps);
	faultAddress = faultAddress >> 12;
	int i  = 0;
	struct pentry *pptr = &proctab[procPID];
	long faultAddressInBytes = faultAddress * NBPG;
	long vpnoInBytes = 0;
	long vpnoAndNpagesInBytes = 0;
	for(i = 0; i <= MAX_ID; i++)
	{
		if(pptr->bsList[i].bs_status != 0)
		{
			vpnoInBytes = pptr->bsList[i].bs_vpno * NBPG;
			vpnoAndNpagesInBytes = pptr->bsList[i].bs_vpno * NBPG + pptr->bsList[i].bs_npages * NBPG;
			//kprintf("\n%d, %d, %d\n", faultAddress, vpnoInBytes, vpnoAndNpagesInBytes);
			if((faultAddressInBytes >= vpnoInBytes) && (faultAddressInBytes <= vpnoAndNpagesInBytes))
			{
				//kprintf("\n\Have found Address %d\n", i);
				restore(ps);
				return i;
			}
		}
	}
	restore(ps);
	return SYSERR;
}

int findPage(long faultAddress, int procPID, int bsID)
{
	STATWORD ps;
	disable(ps);
	faultAddress = faultAddress >> 12;
	int i  = 0;
	struct pentry *pptr = &proctab[procPID];
	if(pptr->bsList[bsID].bs_status != 0)
	{
		//kprintf("The page number we are looking for is %d", faultAddress - pptr->bsList[bsID].bs_vpno);
		restore(ps);
		return faultAddress - pptr->bsList[bsID].bs_vpno;
	}
	restore(ps);
	return SYSERR;
}

//FIFO Q For Paging
//Q has been initialized in initialize.c
int fifoPagingEnQ(int frameNo)
{
	STATWORD ps;
	disable(ps);
	int i = 0;
	for(i = 0; i<NFRAMES; i++)
	{
		if(fifoForPaging[i] == -1)
		{
			fifoForPaging[i] = frameNo;
			break;
		}
	}
	restore(ps);
	return (OK);
}

int fifoPagingDeQ()
{
	STATWORD ps;
	disable(ps);
	//This code can be changed for better performance (by breaking out of the loop when we see a -1). However for the time being I am going with this.
	int frameNo = fifoForPaging[0];
	int i = 0;
	//Moving every value to the previous index.
	for(i = 0; i < NFRAMES - 1; i++)
	{
		fifoForPaging[i] = fifoForPaging[i+1];
	}
	fifoForPaging[NFRAMES -1] = -1;
	int j = 0;
	for(j = 0; j < NPROC; j++)
	{
		if(frameTab[frameNo-FRAME0].pid[j] == 1)
		{
			int procPID = j;		//currpid is NOT used because we can be evicting the frame of any process.
			int procPDBR = proctab[procPID].pdbr;
			int virtualPageNo = frameTab[frameNo-FRAME0].vpno[procPID];
			//kprintf("\nProc PID is %d, Vir Page is %d", virtualPageNo);
			//Updating Page Table Entry to show that the page has been removed
			int firstTenBits = virtualPageNo >> 10;
			int middleTenBits = (virtualPageNo & 0x003ff);
			//kprintf("\nFirst Ten is 0x%08X, Last ten is 0x%08X", firstTenBits, middleTenBits);
			pd_t *pageDirectoryEntry = (procPDBR * NBPG) + (firstTenBits * 4);
			//kprintf("\n%d, %d", pageDirectoryEntry->pd_base, pageDirectoryEntry->pd_pres);
			pt_t *pageTableEntry = (pageDirectoryEntry->pd_base * NBPG) + (middleTenBits * 4);
			//kprintf("\nBefore Setting: %d, %d", pageTableEntry->pt_base, pageTableEntry->pt_pres);
			pageTableEntry->pt_pres = 0;
			frameTab[pageDirectoryEntry->pd_base-FRAME0].refcnt--;
			//kprintf("\ndequeue. Ref Cnt of %d is %d", pageDirectoryEntry->pd_base, frameTab[pageDirectoryEntry->pd_base - FRAME0].refcnt);
			frameTab[frameNo-FRAME0].pid[procPID] = 0;
			frameTab[frameNo-FRAME0].vpno[procPID] = 0;
			write_bs(frameNo);
			if(procPID == currpid)
				write_cr3((procPDBR) * NBPG);		//Invalidate all TLB Content as this will contain the page table values which are old. Refer Intel Volume III for invlpg. 
		}
	}
	frameTab[frameNo-FRAME0].bs = -1;
	frameTab[frameNo-FRAME0].bs_page = -1;
	frameTab[frameNo-FRAME0].status = FRM_USED;
	restore(ps);
	return frameNo;
}

int fifoPagingPrintQ()
{
	int i = 0;
	//nsrPrintf("\nFIFO Q\n");
	for(i = 0; i<NFRAMES; i++)
	{
		if(fifoForPaging[i] != -1)
		{
			//nsrPrintf("%d -> %d, %d, .. ", i, fifoForPaging[i], frameTab[fifoForPaging[i] - FRAME0].age);
		}
	}
	return (OK);
}

int testPaging()
{
	int procPDBR = proctab[currpid].pdbr;
	pd_t *pageDirectoryEntry = (procPDBR * NBPG);
	int i = 0;
	for(i = 0; i < PAGETABLE_ENTRIES; i++)
	{
		if(pageDirectoryEntry->pd_pres == 1)
		{
			//nsrPrintf("\n%d", pageDirectoryEntry->pd_base);
			int j = 0;
			pt_t *pageTableEntry = (pageDirectoryEntry->pd_base) * NBPG;
			for(j = 0; j<20; j++)
			{
				if(pageTableEntry->pt_pres == 1)
					//nsrPrintf("->%d ", pageTableEntry->pt_base);
				pageTableEntry++;
			}
		}
		pageDirectoryEntry++;
	}
}

int isPageTableFree(int frameNo)
{
	STATWORD ps;
	disable(ps);
	if(frameTab[frameNo].refcnt == 0)
	{
		restore(ps);
		return FRM_FREE;
	}
	else
	{
		restore(ps);
		return FR_TBL;
	}
}

/*	Aging Helper Functions	*/
int findFrameWithMinAge()
{
	/* We are going to use the FIFO Q here also as there might be multiple frames with the same age and we need to evict the one closer to the head. */
	STATWORD ps;
	disable(ps);
	int i = 0;
	int j = 0;
	int min = 255;
	int frameNo;
	//kprintf("\nEntered this function");
	for(i = 0; i < NFRAMES; i++)
	{
		if(frameTab[i].status == FR_PAGE)
		{
			if(frameTab[i].age < min)
			{
				min = frameTab[i].age;
				frameNo = FRAME0 + i;
			}
	
			else if(frameTab[i].age == min)
			{
				//Check which of these frameNo or i occurs first in the FIFOQ. 
				for(j = 0; j < NFRAMES; j++)
				{
					if(fifoForPaging[j] == FRAME0 + i || fifoForPaging[j] == frameNo)
					{
						frameNo = fifoForPaging[j];
						break;
					}
				}
			}
		}
	}
	nsrPrintf("\nFrame With least Age(%d) is %d", frameTab[frameNo - FRAME0].age, frameNo);
	//Remove that frame from the FIFO Q

	int fifoIndexToRemove = -1;
	for(j = 0; j<NFRAMES; j++)
	{
		if(fifoForPaging[j] == frameNo)
		{
			fifoIndexToRemove = j;
			break;
		}
	}
	if(fifoIndexToRemove != -1)
	{
		for(j = fifoIndexToRemove; j < NFRAMES - 1; j++)
			fifoForPaging[j] = fifoForPaging[j+1];
		fifoForPaging[NFRAMES - 1] = -1;
	}

	//Finding out all the processes that hold the frame and making the necessary changes in their page tables and page directories.
	for(j = 0; j < NPROC; j++)
	{
		if(frameTab[frameNo-FRAME0].pid[j] == 1)
		{
			int procPID = j;		//currpid is NOT used because we can be evicting the frame of any process.
			int procPDBR = proctab[procPID].pdbr;
			int virtualPageNo = frameTab[frameNo-FRAME0].vpno[procPID];
			//kprintf("\nProc PID is %d, Vir Page is %d", virtualPageNo);
			//Updating Page Table Entry to show that the page has been removed
			int firstTenBits = virtualPageNo >> 10;
			int middleTenBits = (virtualPageNo & 0x003ff);
			//kprintf("\nFirst Ten is 0x%08X, Last ten is 0x%08X", firstTenBits, middleTenBits);
			pd_t *pageDirectoryEntry = (procPDBR * NBPG) + (firstTenBits * 4);
			//kprintf("\n%d, %d", pageDirectoryEntry->pd_base, pageDirectoryEntry->pd_pres);
			pt_t *pageTableEntry = (pageDirectoryEntry->pd_base * NBPG) + (middleTenBits * 4);
			//kprintf("\nBefore Setting: %d, %d", pageTableEntry->pt_base, pageTableEntry->pt_pres);
			pageTableEntry->pt_pres = 0;
			frameTab[pageDirectoryEntry->pd_base-FRAME0].refcnt--;
			//kprintf("\ndequeue. Ref Cnt of %d is %d", pageDirectoryEntry->pd_base, frameTab[pageDirectoryEntry->pd_base - FRAME0].refcnt);
			frameTab[frameNo-FRAME0].pid[procPID] = 0;
			frameTab[frameNo-FRAME0].vpno[procPID] = 0;
			write_bs(frameNo);
			if(procPID == currpid)
				write_cr3((procPDBR) * NBPG);		//Invalidate all TLB Content as this will contain the page table values which are old. Refer Intel Volume III for invlpg. 
		}
	}
	frameTab[frameNo-FRAME0].bs = -1;
	frameTab[frameNo-FRAME0].bs_page = -1;
	frameTab[frameNo-FRAME0].status = FRM_USED;
	//nsrPrintf("\nFrame Has been freed and returning frame %d", frameNo);
	restore(ps);
	return frameNo;
}

int setAgeOfFrames()
{
	int i = 0;
	for(i = 0; i < NFRAMES; i++)
	{
		if(fifoForPaging[i] != -1)
		{
			int frameNo = fifoForPaging[i];
			int j = 0;
			for(j = 0; j < NPROC; j++)
			{
				if(frameTab[frameNo-FRAME0].pid[j] == 1)
				{
					int procPID = j;		//currpid is NOT used because we can be evicting the frame of any process.
					int procPDBR = proctab[procPID].pdbr;
					int virtualPageNo = frameTab[frameNo-FRAME0].vpno[procPID];
					//kprintf("\nProc PID is %d, Vir Page is %d", virtualPageNo);
					//Updating Page Table Entry to show that the page has been removed
					int firstTenBits = virtualPageNo >> 10;
					int middleTenBits = (virtualPageNo & 0x003ff);
					//kprintf("\nFirst Ten is 0x%08X, Last ten is 0x%08X", firstTenBits, middleTenBits);
					pd_t *pageDirectoryEntry = (procPDBR * NBPG) + (firstTenBits * 4);
					//kprintf("\n%d, %d", pageDirectoryEntry->pd_base, pageDirectoryEntry->pd_pres);
					pt_t *pageTableEntry = (pageDirectoryEntry->pd_base * NBPG) + (middleTenBits * 4);
					//kprintf("\nBefore Setting: %d, %d", pageTableEntry->pt_base, pageTableEntry->pt_pres);
					if(pageTableEntry->pt_acc == 1)
					{
						pageTableEntry->pt_acc = 0;
						frameTab[frameNo - FRAME0].age = (frameTab[frameNo - FRAME0].age >> 1) + 128;
						//This should never happen but a safety Condition. 
						if(frameTab[frameNo - FRAME0].age >= 255)
							frameTab[frameNo - FRAME0].age = 255;
					}
					else if(pageTableEntry->pt_acc == 0)
					{
						pageTableEntry->pt_acc = 0;			//For Safety Sake
						frameTab[frameNo - FRAME0].age = frameTab[frameNo - FRAME0].age >> 1;
						//This should never happen but still a safety Condition
						if(frameTab[frameNo - FRAME0].age >= 255)
							frameTab[frameNo - FRAME0].age = 255;
					}
					else
					{
					}
				}
			}
		}
	}
}