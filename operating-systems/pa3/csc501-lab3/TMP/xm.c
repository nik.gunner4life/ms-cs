/* xm.c = xmmap xmunmap */

#include <conf.h>
#include <kernel.h>
#include <proc.h>
#include <paging.h>


/*-------------------------------------------------------------------------
 * xmmap - xmmap
 *-------------------------------------------------------------------------
 */
SYSCALL xmmap(int virtpage, bsd_t source, int npages)
{
  /* sanity check ! */
	nsrPrintf("\nCalling xmMap for 0x%08X(%d), BS: %d, Pages: %d\n", virtpage, virtpage, source, npages);
	if ( (virtpage < 4096) || ( source < 0 ) || ( source > MAX_ID) ||(npages < 1) || ( npages >200))
	{
		nsrPrintf("xmmap call error: parameter error! \n");
		return SYSERR;
	}
	nsrPrintf("\nCalling xmMap for 0x%08X(%d), BS: %d, Pages: %d\n", virtpage, virtpage, source, npages);
	//Making an entry in the appropriate Backing Store about the process holding that backing store.
	bsTab[source].procList[currpid] = 1;
	bsTab[source].npages = npages;
	struct pentry *pptr = &proctab[currpid];
	  
	//Making an entry in proctab which contains the vpno, npages and so on
	pptr->bsList[source].bs_status = 1;
	pptr->bsList[source].bs_vpno = virtpage;
	pptr->bsList[source].bs_npages = npages;
	pptr->bsList[source].bs_pid = source;			//Not required. Will probably use it for something else. Index will give us the backing store ID.
	//nsrPrintf("\nThe Virtual Page number is: %d\n", pptr->bsList[source].bs_vpno);
	return OK;
}

/*-------------------------------------------------------------------------
 * xmunmap - xmunmap
 *-------------------------------------------------------------------------
 */
SYSCALL xmunmap(int virtpage)
{
	STATWORD ps;
	disable(ps);
	//nsrPrintf("\nxmUnmap has been called\n"); 
	/* sanity check ! */
	if ( (virtpage < 4096) )
	{ 
		//nsrPrintf("xmummap call error: virtpage (%d) invalid! \n", virtpage);
		restore(ps);
		return SYSERR;
	}
	
	struct pentry *pptr = &proctab[currpid];
	int i = 0;
	int bsID = -2;
	for(i = 0; i <= MAX_ID; i++)
	{
		if(pptr->bsList[i].bs_vpno == virtpage)
		{
			bsID = i;
			break;
		}
	}
	if(bsID == -2)
	{
		//nsrPrintf("\nSomething is wrong. Check Again\n");
		restore(ps);
		return SYSERR;
	}

	nsrPrintf("\nUnmap for Process %d has been called with Virtual Page %d\n", currpid, virtpage);
	//First Return all the frames to the Backing Store i.e return the values to the BS and free the frames for future use.
	for(i = 0; i < NFRAMES; i++)
	{
		if(frameTab[i].pid[currpid] == 1 && frameTab[i].status == FR_PAGE && frameTab[i].bs == bsID)
		{
			int firstTenBits = frameTab[i].vpno[currpid] >> 10;
			int middleTenBits = (frameTab[i].vpno[currpid] & 0x003ff);
			int procPDBR = pptr->pdbr;

			//Remove the page table entries for the frame. 
			pd_t *pageDirectoryEntry = (procPDBR * NBPG) + (firstTenBits * 4);
			pt_t *pageTableEntry = (pageDirectoryEntry->pd_base * NBPG) + (middleTenBits * 4);
			pageTableEntry->pt_pres = 0;
			pageTableEntry->pt_write = 1;
			pageTableEntry->pt_base = 0;
			frameTab[pageDirectoryEntry->pd_base - FRAME0].refcnt--;
			//nsrPrintf("\nxm Ref Cnt of %d is %d", pageDirectoryEntry->pd_base, frameTab[pageDirectoryEntry->pd_base - FRAME0].refcnt);

			//Ensuring that the frame is not held by any other process			
			int loopIndex = 0;
			int frameStatusForEviction = 1;
			for(loopIndex = 0; loopIndex < NPROC; loopIndex++)
			{
				if(frameTab[i].pid[loopIndex] == 1 && loopIndex != currpid)
				{
					frameStatusForEviction = 0;
					break;
				}
			}
			if(frameStatusForEviction == 0)
			{
				//Frame cannot be evicted. It is held by another process.
				//nsrPrintf("\nDon't evict frame. It is being held by process %d (at least)\n", loopIndex);
				frameTab[i].pid[currpid] = 0;
				frameTab[i].vpno[currpid] = 0;
			}
			else
			{
				//------------- Frame can be evicted ... Remove it from the FIFO Q --------------//
				
				int j = 0;
				int fifoIndexToRemove = -1;
				//Find the index to remove
				for(j = 0; j<NFRAMES; j++)
				{
					if(fifoForPaging[j] == FRAME0 + i)
					{
						fifoIndexToRemove = j;
						break;
					}
				}
				//Remove that index value
				if(fifoIndexToRemove != -1)
				{
					for(j = fifoIndexToRemove; j < NFRAMES - 1; j++)
						fifoForPaging[j] = fifoForPaging[j+1];
					fifoForPaging[NFRAMES - 1] = -1;
				}
				nsrPrintf("\nEvicting frame %d.", FRAME0 + i);
				write_bs(FRAME0 + i);		//This is done because write_bs by default subtracts FRAME0
				frameTab[i].pid[currpid] = 0;
				frameTab[i].vpno[currpid] = 0;
				set_frm(FRM_FREE, FR_IGNORE_ENTRY, -1, -1, FR_IGNORE_ENTRY, FR_IGNORE_ENTRY, FR_IGNORE_ENTRY, i);		//Here it need not be done because set_frm by default acts on the frame Index and not the actual frame number. 
			}
			//Check if the page table can be evicted.
			int pageTBLStatus = isPageTableFree(pageDirectoryEntry->pd_base - FRAME0);			//The function is in the pagingfncs.c
			if(pageTBLStatus == FRM_FREE)
			{
				int frameID = pageDirectoryEntry->pd_base - FRAME0;
				nsrPrintf("\nEvicting Page Table %d", frameID + FRAME0);
				frameTab[frameID].pid[currpid] = 0;
				frameTab[frameID].vpno[currpid] = 0;
				frameTab[frameID].status = FRM_FREE;
				frameTab[frameID].bs = -1;
				frameTab[frameID].bs_page = -1;
				frameTab[frameID].refcnt = 0;
				frameTab[frameID].age = 0;
				long *addr;
			}
			write_cr3((procPDBR) * NBPG);		//Invalidate the TLB content. 
		}
	}

	//Updating the Backing Store Map in the Proctab and the backing store itself. 
	bsTab[bsID].procList[currpid] = 0;
	pptr->bsList[bsID].bs_status = 0;
	pptr->bsList[bsID].bs_vpno = 0;
	pptr->bsList[bsID].bs_npages = 0;

	restore(ps);
	return OK;
}


SYSCALL xmhmap(int virtpage, bsd_t source, int npages)
{
  /* sanity check ! */

  if ( (virtpage < 4096) || ( source < 0 ) || ( source > MAX_ID) ||(npages < 1) || ( npages >200)){
	nsrPrintf("xmmap call error: parameter error! \n");
	return SYSERR;
  }
  //Making an entry in the appropriate Backing Store about the process holding that backing store.
  bsTab[source].procList[currpid] = 1;
  struct pentry *pptr = &proctab[currpid];
  
  //Making an entry in proctab which contains the vpno, npages and so on
  pptr->vmemlist->mnext = 4096 * NBPG;
  pptr->bsList[source].bs_status = 1;
  pptr->bsList[source].bs_vpno = virtpage;
  pptr->bsList[source].bs_npages = npages;
  pptr->bsList[source].bs_pid = source;			
  
  //Not required. Will probably use it for something else. Index will give us the backing store ID.
  //nsrPrintf("\nThe Virtual Page number is: %d\n", pptr->bsList[source].bs_vpno);

  return OK;
}

SYSCALL xmhunmap(int virtpage)
{
	STATWORD ps;
	disable(ps);
	/* sanity check ! */
	if ( (virtpage < 4096) )
	{ 
		//nsrPrintf("xmummap call error: virtpage (%d) invalid! \n", virtpage);
		restore(ps);
		return SYSERR;
	}
	
	struct pentry *pptr = &proctab[currpid];
	int i = 0;
	int bsID = -2;
	for(i = 0; i <= MAX_ID; i++)
	{
		if(pptr->bsList[i].bs_vpno == virtpage)
		{
			bsID = i;
			break;
		}
	}
	if(bsID == -2)
	{
		//nsrPrintf("\nSomething is wrong. Check Again\n");
		restore(ps);
		return SYSERR;
	}
	//First Return all the frames to the Backing Store i.e return the values to the BS and free the frames for future use.
	for(i = 0; i < NFRAMES; i++)
	{
		if(frameTab[i].pid[currpid] == 1 && frameTab[i].status == FR_PAGE && frameTab[i].bs == bsID)
		{
			int firstTenBits = frameTab[i].vpno[currpid] >> 10;
			int middleTenBits = (frameTab[i].vpno[currpid] & 0x003ff);
			int procPDBR = pptr->pdbr;

			//Remove the page table entries for the frame. 
			pd_t *pageDirectoryEntry = (procPDBR * NBPG) + (firstTenBits * 4);
			pt_t *pageTableEntry = (pageDirectoryEntry->pd_base * NBPG) + (middleTenBits * 4);
			pageTableEntry->pt_pres = 0;
			pageTableEntry->pt_write = 1;
			pageTableEntry->pt_base = 0;
			frameTab[pageDirectoryEntry->pd_base - FRAME0].refcnt--;
			//nsrPrintf("\nxm Ref Cnt of %d is %d", pageDirectoryEntry->pd_base, frameTab[pageDirectoryEntry->pd_base - FRAME0].refcnt);

			//Ensuring that the frame is not held by any other process			
			int loopIndex = 0;
			int frameStatusForEviction = 1;
			for(loopIndex = 0; loopIndex < NPROC; loopIndex++)
			{
				if(frameTab[i].pid[loopIndex] == 1 && loopIndex != currpid)
				{
					frameStatusForEviction = 0;
					break;
				}
			}
			if(frameStatusForEviction == 0)
			{
				//Frame cannot be evicted. It is held by another process.
				//nsrPrintf("\nDon't evict frame %d. It is being held by process %d (at least)\n", i, loopIndex);
				frameTab[i].pid[currpid] = 0;
				frameTab[i].vpno[currpid] = 0;
			}
			else
			{
				//------------- Frame can be evicted ... Remove it from the FIFO Q --------------//
				
				int j = 0;
				int fifoIndexToRemove = -1;
				//Find the index to remove
				for(j = 0; j<NFRAMES; j++)
				{
					if(fifoForPaging[j] == FRAME0 + i)
					{
						fifoIndexToRemove = j;
						break;
					}
				}
				//Remove that index value
				if(fifoIndexToRemove != -1)
				{
					for(j = fifoIndexToRemove; j < NFRAMES - 1; j++)
						fifoForPaging[j] = fifoForPaging[j+1];
					fifoForPaging[NFRAMES - 1] = -1;
				}
				nsrPrintf("\nEvicting frame %d.", FRAME0 + i);
				write_bs(FRAME0 + i);		//This is done because write_bs by default subtracts FRAME0
				frameTab[i].pid[currpid] = 0;
				frameTab[i].vpno[currpid] = 0;
				frameTab[i].status = FRM_FREE;
				frameTab[i].bs = -1;
				frameTab[i].bs_page = -1;
				frameTab[i].refcnt = 0;
				frameTab[i].age = 0;
				
				//Here it need not be done because set_frm by default acts on the frame Index and not the actual frame number. 
			}

			//Check if the page table can be evicted.
			int pageTBLStatus = isPageTableFree(pageDirectoryEntry->pd_base - FRAME0);			//The function is in the pagingfncs.c
			if(pageTBLStatus == FRM_FREE)
			{
				int frameID = pageDirectoryEntry->pd_base - FRAME0;
				nsrPrintf("\nEvicting Page Table %d", frameID + FRAME0);
				frameTab[frameID].pid[currpid] = 0;
				frameTab[frameID].vpno[currpid] = 0;
				frameTab[frameID].status = FRM_FREE;
				frameTab[frameID].bs = -1;
				frameTab[frameID].bs_page = -1;
				frameTab[frameID].refcnt = 0;
				frameTab[frameID].age = 0;
				long *addr;
			}
				//set_frm(FRM_FREE, 0, 0, 0, 0, 0, 0, pageDirectoryEntry->pd_base - FRAME0);		//Releasing the Page Table frame of the process.
			write_cr3((procPDBR) * NBPG);		//Invalidate all TLB Content as this will contain the page table values which are old. Refer Intel Volume III for invlpg. 
		}
	}

	//Updating the Backing Store Map in the Proctab and the backing store itself. 
	bsTab[bsID].procList[currpid] = 0;
	pptr->bsList[bsID].bs_status = 0;
	pptr->bsList[bsID].bs_vpno = 0;
	pptr->bsList[bsID].bs_npages = 0;
	pptr->vmemlist->mnext = 0;

	restore(ps);
	return OK;
}