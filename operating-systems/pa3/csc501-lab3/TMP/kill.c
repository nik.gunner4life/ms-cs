/* kill.c - kill */

#include <conf.h>
#include <kernel.h>
#include <proc.h>
#include <paging.h>
#include <sem.h>
#include <mem.h>
#include <io.h>
#include <q.h>
#include <stdio.h>

/*------------------------------------------------------------------------
 * kill  --  kill a process and remove it from the system
 *------------------------------------------------------------------------
 */
SYSCALL kill(int pid)
{
	STATWORD ps;    
	struct	pentry	*pptr = &proctab[pid];		/* points to proc. table for pid*/
	int	dev;
	nsrPrintf("\nKilling the process with PID %d\n", pid);
	if(pptr->xmMapStatusForVheap == 1)
	{
		int k = 0;
		for(k = 0; k <= MAX_ID; k++)
		{
			if(pptr->bsList[k].bs_status == 1)
			{
				xmhunmap(pptr->bsList[k].bs_vpno);
			}
		}
	}
	else
	{
		int k = 0;
		for(k = 0; k <= MAX_ID; k++)
		{
			if(pptr->bsList[k].bs_status == 1)
			{
				xmunmap(pptr->bsList[k].bs_vpno);
			}
		}
	}
	nsrPrintf("\nAll frames and page tables have been released\n");
	/*kprintf("\nEvicting the Process Page Directory");
	int frameID = pptr->pdbr - FRAME0;
	frameTab[frameID].status = FRM_FREE;
	frameTab[frameID].bs = -1;
	frameTab[frameID].bs_page = -1;
	frameTab[frameID].pid[pid] = 0;
	frameTab[frameID].vpno[pid] = 0;
	frameTab[frameID].refcnt = 0;*/

	disable(ps);
	if (isbadpid(pid) || (pptr= &proctab[pid])->pstate==PRFREE) {
		restore(ps);
		return(SYSERR);
	}
	if (--numproc == 0)
		xdone();

	dev = pptr->pdevs[0];
	if (! isbaddev(dev) )
		close(dev);
	dev = pptr->pdevs[1];
	if (! isbaddev(dev) )
		close(dev);
	dev = pptr->ppagedev;
	if (! isbaddev(dev) )
		close(dev);
	
	send(pptr->pnxtkin, pid);
	
	freestk(pptr->pbase, pptr->pstklen);
	switch (pptr->pstate) {

	case PRCURR:
		
		pptr->pstate = PRFREE;	/* suicide */
				//Releasing the PDBR frame of the process
		resched();

	case PRWAIT:	
		semaph[pptr->psem].semcnt++;

	case PRREADY:	
		dequeue(pid);
		pptr->pstate = PRFREE;
				//Releasing the PDBR frame of the process
		break;

	case PRSLEEP:
	case PRTRECV:	unsleep(pid);
						/* fall through	*/
	default:	pptr->pstate = PRFREE;
	}
	
	restore(ps);
	return(OK);
}
