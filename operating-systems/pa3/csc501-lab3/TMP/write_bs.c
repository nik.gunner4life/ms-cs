#include <conf.h>
#include <kernel.h>
#include <proc.h>
#include <mark.h>
#include <bufpool.h>
#include <paging.h>

SYSCALL write_bs(int frameNo) 
{
	/* In write_bs.c, we get the destination Frame and the source frame from the fifoPagingDeQ() function.
	   The necessary Calculations are done here. 
	*/
	STATWORD ps;
	disable(ps);
	int bsID = frameTab[frameNo-FRAME0].bs;
	int bsPage = frameTab[frameNo-FRAME0].bs_page;
	nsrPrintf("\nPut Frame %d to Page: %d of BS: %d", frameNo, bsPage, bsID);
	long *destinationAddress = (long *)((bsTab[bsID].baseAddr + bsPage) * NBPG);
	long *srcAddress = (long *)(frameNo * NBPG);
	blkcopy(destinationAddress, srcAddress, NBPG);
	restore(ps);
}

