/* vgetmem.c - vgetmem */

#include <conf.h>
#include <kernel.h>
#include <mem.h>
#include <proc.h>
#include <paging.h>
#include <stdio.h>

/*------------------------------------------------------------------------
 * vgetmem  --  allocate private heap storage, returning lowest WORD address
 *------------------------------------------------------------------------
 */
WORD *vgetmem(unsigned nbytes)
{
	STATWORD ps;    
	struct	mblock	*p, *q, *leftover;

	disable(ps);
	struct pentry *pptr = &proctab[currpid];
	if(pptr->xmMapStatusForVheap == 0)
	{
		xmhmap(((LASTPAGE + 1) *NBPG)>>12, pptr->store, pptr->vhpnpages);
		pptr->xmMapStatusForVheap = 1;
	}
	if (nbytes==0 || pptr->vmemlist->mnext == (struct mblock *) NULL) 
	{
		//kprintf("\n1. Getting SYSERR\n");
		restore(ps);
		return( (WORD *)SYSERR);
	}
	nbytes = (unsigned int) roundmb(nbytes);
	for (q= pptr->vmemlist,p=pptr->vmemlist->mnext; p != (struct mblock *) NULL; q=p,p=p->mnext)
	{
		//nsrPrintf("\nGetting executed\n");
		if ( p->mlen == nbytes) 
		{
			q->mnext = p->mnext;
			restore(ps);
			return( (WORD *)p );
		} 
		else if ( p->mlen > nbytes )
		{
			leftover = (struct mblock *)( (unsigned)p + nbytes );
			q->mnext = leftover;
			leftover->mnext = p->mnext;
			leftover->mlen = p->mlen - nbytes;
			restore(ps);
			return( (WORD *)p );
		}
	}
	restore(ps);
	return( (WORD *)SYSERR );
}
