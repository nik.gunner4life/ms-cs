#include <conf.h>
#include <kernel.h>
#include <mark.h>
#include <bufpool.h>
#include <proc.h>
#include <paging.h>

SYSCALL read_bs(int destinationFrame, int bsID, int page) 
{

	/* In read_bs.c, we get the required page from the backing store and move it to the destination using blkcopy.
	Finding the RIGHT BS_ID and so on is handled in pfint.c.
	*/
	STATWORD ps;
	disable(ps);
	long *destinationAddress = (long *)(destinationFrame * NBPG);
	long *sourceAddress = (long *)((bsTab[bsID].baseAddr + page) * NBPG);
	nsrPrintf("\nMap BS %d, Page %d to Frame %d\n", bsID, page, destinationFrame);
	blkcopy(destinationAddress, sourceAddress, NBPG);
	restore(ps);
}


