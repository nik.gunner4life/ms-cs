/* paging.h */

typedef unsigned int	 bsd_t;

/* Structure for a page directory entry */

typedef struct {

  unsigned int pd_pres	: 1;		/* page table present?		*/
  unsigned int pd_write : 1;		/* page is writable?		*/
  unsigned int pd_user	: 1;		/* is use level protection?	*/
  unsigned int pd_pwt	: 1;		/* write through cachine for pt?*/
  unsigned int pd_pcd	: 1;		/* cache disable for this pt?	*/
  unsigned int pd_acc	: 1;		/* page table was accessed?	*/
  unsigned int pd_mbz	: 1;		/* must be zero			*/
  unsigned int pd_fmb	: 1;		/* four MB pages?		*/
  unsigned int pd_global: 1;		/* global (ignored)		*/
  unsigned int pd_avail : 3;		/* for programmer's use		*/
  unsigned int pd_base	: 20;		/* location of page table?	*/
} pd_t;

/* Structure for a page table entry */

typedef struct {

  unsigned int pt_pres	: 1;		/* page is present?		*/
  unsigned int pt_write : 1;		/* page is writable?		*/
  unsigned int pt_user	: 1;		/* is use level protection?	*/
  unsigned int pt_pwt	: 1;		/* write through for this page? */
  unsigned int pt_pcd	: 1;		/* cache disable for this page? */
  unsigned int pt_acc	: 1;		/* page was accessed?		*/
  unsigned int pt_dirty : 1;		/* page was written?		*/
  unsigned int pt_mbz	: 1;		/* must be zero			*/
  unsigned int pt_global: 1;		/* should be zero in 586	*/
  unsigned int pt_avail : 3;		/* for programmer's use		*/
  unsigned int pt_base	: 20;		/* location of page?		*/
} pt_t;

typedef struct{
  unsigned int pg_offset : 12;		/* page offset			*/
  unsigned int pt_offset : 10;		/* page table offset		*/
  unsigned int pd_offset : 10;		/* page directory offset	*/
} virt_addr_t;

typedef struct{
  int bs_status;			/* MAPPED or UNMAPPED		*/
  int bs_pid;				/* process id using this slot   */
  int bs_vpno;				/* starting virtual page number */
  int bs_npages;			/* number of pages in the store */
  int bs_sem;				/* semaphore mechanism ?	*/
  int pid;
} bs_map_t;

typedef struct{
	int fr_status;			/* MAPPED or UNMAPPED		*/
	int fr_pid;				/* process id using this frame  */
	int fr_vpno;				/* corresponding virtual page no*/
	int fr_refcnt;			/* reference count		*/
	int fr_type;				/* FR_DIR, FR_TBL, FR_PAGE	*/
	int fr_dirty;
	void *cookie;				/* private data structure	*/
	unsigned long int fr_loadtime;	/* when the page is loaded 	*/
	struct _frame_t *fifo; /* when the page is loaded, in ticks*/
	int age; /* Used for page replacement policy AGING */
}fr_map_t;

//New Structure for Frames (From the message board).
struct _frame_t {
	int status; /* FRM_FREE, FRM_PGD, FRM_PGT, FRM_BS*/
	int vpno[50];			//For the time being
	/*If the frame is a FRM_PGT, refcnt is the number of mappings install 
	 in this PGT. release it when refcnt is zero. When the frame is
	 a FRM_BS, how many times this frame is mapped by processes. If refcnt
	 is zero, time to release the page*/
	int refcnt;
	/*Data used only if FRM_BS. The backstore pages this frame is mapping
	 to and the list of all the frames for this backstore*/
	int bs;
	int bs_page;
	struct _frame_t *bs_next;
	int pid[50];				//For the time being. 
	struct _frame_t *fifo; /* when the page is loaded, in ticks*/
	int age; /* Used for page replacement policy AGING */
};

//New Structure for BS (From the message board).
typedef struct {
	int status;
	int as_heap; /* is this bs used by heap?*/
	int npages; /* number of pages in the store */
	bs_map_t bsMapTab[NPROC]; /* where it is mapped*/
	int procList[NPROC];
	long baseAddr;
} bs_t;

extern bs_map_t bsm_tab[];
extern fr_map_t frm_tab[];
extern struct _frame_t frameTab[];
extern pt_t page_tab[];
extern bs_t bsTab[];
extern int page_replace_policy;
/* Prototypes for required API calls */
SYSCALL xmmap(int, bsd_t, int);
SYSCALL xunmap(int);

/* given calls for dealing with backing store */
extern int fifoForPaging[];
int get_bs(bsd_t, unsigned int);
SYSCALL release_bs(bsd_t);
SYSCALL read_bs(int, int, int);
SYSCALL write_bs(int);

#define NBPG		4096	/* number of bytes per page	*/
#define LASTPAGE	4095	//Defines the last frame in the RAM

#define FRAME0		1024	/* zero-th frame		*/

#define NFRAMES 	20	/* number of frames. Be careful while using this. Ensure you are not getting confused between this and PAGETABLE_ENTRIES*/

#define BSM_UNMAPPED	0
#define BSM_MAPPED	1

#define FRM_UNMAPPED	0
#define FRM_MAPPED	1

#define FR_PAGE		3
#define FR_TBL		1
#define FR_DIR		2
#define FRM_FREE	0
#define FRM_USED	99
#define	FR_IGNORE_ENTRY		-10		//This value is used when we don't want to change the a value in the frameTab. Check set_frm() in frame.c for the application.

#define FIFO		3
#define AGING		4

#define MAX_ID          7              /* You get 8 mappings, 0 - 8 */
#define	GLOBAL_PID		100

#define BACKING_STORE_BASE	2048
#define BACKING_STORE_UNIT_SIZE 256

#define PAGETABLE_ENTRIES	1024			//Be careful while using this. Ensure you are not getting confused between this and NFRAMES


