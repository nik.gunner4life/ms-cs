/* user.c - main */

#include <conf.h>
#include <kernel.h>
#include <proc.h>
#include <sem.h>
#include <lock.h>
#include <stdio.h>


void halt();
/*------------------------------------------------------------------------
 *  main  --  user main program
 *------------------------------------------------------------------------
 */
int ldes1, ldes2, ldes3;
int sem1;
#define DEFAULT_LOCK_PRIO 20

/*---------------------------------Semaphore Test --------------------------------------*/
void semWriter(char *msg)
{
	int ret;
	struct pentry *pptr = &proctab[currpid];
	kprintf ("  %s: to acquire Semaphore. Priority is %d\n", msg, pptr->pprio);
    wait(sem1);
    kprintf ("  %s: has acquired Semaphore. Priority is %d\n", msg, pptr->pprio);
    sleep (5);
    kprintf ("  %s: to release Semaphore. Priority is %d\n", msg, pptr->pprio);
	signal(sem1);
	kprintf ("  %s: has released Semaphore. Priority is %d\n", msg, pptr->pprio);
}

void lockWriter (char *msg)
{
	struct pentry *pptr = &proctab[currpid];
	kprintf ("  %s: to acquire lock. Priority is %d\n", msg, pptr->pprio);
    lock (ldes1, WRITE, 25);
    kprintf ("  %s: has acquired lock. Priority is %d\n", msg, pptr->pprio);
    sleep (5);
    kprintf ("  %s: to release lock. Priority is %d\n", msg, pptr->pprio);
    releaseall (1, ldes1);
	kprintf ("  %s: has released lock. Priority is %d\n", msg, pptr->pprio);
}

void test9()
{
	kprintf("\n\n");
	int i, s;
	int count = 0;
	char buf[8];

	kprintf("\nPlease Input 1 for Semaphore and 2 for Lock:");
	while ((i = read(CONSOLE, buf, sizeof(buf))) <1);
	buf[i] = 0;
	s = atoi(buf);
	kprintf("\n");
	switch(s)
	{
		case 1:
			{
				sem1 = screate(1);
				int wr3, wr2, wr1; 
				wr3 = create(semWriter, 2000, 25, "writer3", 1, "writer3");
				wr2 = create(semWriter, 2000, 30, "writer2", 1, "writer2");
				wr1 = create(semWriter, 2000, 20, "writer1", 1, "writer1");
				resume(wr1);
				sleep10(1);
				resume(wr2);
				sleep10(1);
				resume(wr3);
				break;
			}
		case 2:
			{
				ldes1 = lcreate();
				int wr3, wr2, wr1;
				wr3 = create(lockWriter, 2000, 30, "writer3", 1, "writer3");
				wr2 = create(lockWriter, 2000, 25, "writer2", 1, "writer2");
				wr1 = create(lockWriter, 2000, 20, "writer1", 1, "writer1");
				resume(wr1);
				sleep10(1);
				resume(wr2);
				sleep10(1);
				resume(wr3);

			}
	}
	

	
}

/*--------------------------------------------------------------------------------------*/

int main()
{
	kprintf("\n\nHello World, Xinu lives\n\n");
	test9();
	return 0;
}
