#include <conf.h>
#include <kernel.h>
#include <proc.h>
#include <lock.h>
#include <q.h>
#include <stdio.h>

int laArray[100];
int	lock(int ldes, int procType, int waitPrio)
{
	STATWORD ps;    
	disable(ps);
	struct lockentry *lptr = &locks[ldes];
	if(lptr->lstate == LFREE || lptr->lfArray[currpid] != 0)
	{
		restore(ps);
		return SYSERR;
	}
	register struct pentry *pptr, *tempPtr;
	pptr = &proctab[currpid];
	int toResched = 0;
	initializaAndEmptyArray();
	//Locking Starts Here
	int isBlockQEmpty = 0;
	int isHoldQEmpty = 0;
	int minPrio = 0;
	//Check if BlockQ is empty
	if(q[lptr->lbtail].qprev != lptr->lbhead)
	{
		isBlockQEmpty = 1;
	}
	int i = 0;
	int count = 0;

	//Check if HoldQ is empty
	for(i = 0; i<NPROC; i++)
	{
		if(lptr->lhArray[i] != 0)
		{
			count++;
		}
	}
	if(count != 0)
	{
		isHoldQEmpty = 1;
	}

	//Major Functionality Here
	if(procType == WRITE)
	{
		if(isBlockQEmpty == 0 && isHoldQEmpty == 0)
		{
			//Insert in Holding Q
			lptr->lhArray[currpid] = waitPrio;
			//Update minHoldPrio
			if(lptr->minHoldPriority > waitPrio)
				lptr->minHoldPriority = waitPrio;
			lptr->lockCnt++;
			lptr->writerCount++;
			//Update Locks Held
			pptr = &proctab[currpid]; 
			pptr->locksHeld[ldes] = waitPrio;
			//Schedule Current Process i.e. DONT insert in BlockQ
			toResched = 1;
		}
		else if(isBlockQEmpty == 0 && isHoldQEmpty == 1)
		{
			//Update MaxWaitPriority
			if(waitPrio >= lptr->maxWaitPriority)
				lptr->maxWaitPriority = waitPrio;
			//Insert Block Lock ID in Proc Table
			pptr = &proctab[currpid]; 
			pptr->blockLock = ldes;
			//kprintf("\n%s blocked on %d\n", pptr->pname, ldes);
			//Block Current Proc
			lockInsert(currpid, lptr->lbhead, lptr->lbtail, waitPrio, procType);
			//Priority Inheritance
			minPrio = findMinPrio(ldes);
			int tqpid = 0;
			tqpid = q[currpid].qnext;
			tempPtr = &proctab[tqpid];
			while(tqpid < NPROC)
			{
				tempPtr = &proctab[tqpid];
				if(tempPtr->pOldPrio == 0)
					tempPtr->pOldPrio = tempPtr->pprio;
				if(tempPtr->pprio < pptr->pprio)
					chprio(tqpid, pptr->pprio);
				//kprintf("\n1. %s Priority has been changed to %d\n", tempPtr->pname, tempPtr->pprio);
				tqpid = q[tqpid].qnext;
			}
			if(pptr->pprio > minPrio)
			{
				initializaAndEmptyArray();
				doPriorityInversion(ldes, pptr->pprio);
				initializaAndEmptyArray();
			}

			toResched = 1;
		}
		else if(isBlockQEmpty == 1 && isHoldQEmpty == 1)
		{
			//Update MaxWaitPriority
			if(waitPrio >= lptr->maxWaitPriority)
				lptr->maxWaitPriority = waitPrio;
			//Insert Block Lock ID in Proc Table
			pptr = &proctab[currpid]; 
			pptr->blockLock = ldes;
			//Block Current Proc
			lockInsert(currpid, lptr->lbhead, lptr->lbtail, waitPrio, procType);
			//Priority Inheritance
			minPrio = findMinPrio(ldes);
			int tqpid = 0;
			tqpid = q[currpid].qnext;
			tempPtr = &proctab[tqpid];
			while(tqpid < NPROC)
			{
				tempPtr = &proctab[tqpid];
				if(tempPtr->pOldPrio == 0)
					tempPtr->pOldPrio = tempPtr->pprio;
				if(tempPtr->pprio < pptr->pprio)
					chprio(tqpid, pptr->pprio);
				//kprintf("\n1. %s Priority has been changed to %d\n", tempPtr->pname, tempPtr->pprio);
				tqpid = q[tqpid].qnext;
			}
			if(pptr->pprio > minPrio)
			{
				initializaAndEmptyArray();
				doPriorityInversion(ldes, pptr->pprio);
				initializaAndEmptyArray();
			}
			toResched = 1;
		}

	}

	else if(procType == READ)
	{
		if(isBlockQEmpty == 0 && isHoldQEmpty == 0)
		{
			//kprintf("\nCase 1:");
			//Insert in Holding Q
			lptr->lhArray[currpid] = waitPrio;
			//Update Reader and Lock Count
			lptr->readerCount++;
			lptr->lockCnt++;
			//Update minHoldPrio
			if(lptr->minHoldPriority > waitPrio)
				lptr->minHoldPriority = waitPrio;
			//Update Locks Held
			pptr = &proctab[currpid]; 
			pptr->locksHeld[ldes] = waitPrio;
			//Schedule Current Process i.e. DONT insert in BlockQ
			toResched = 1;
		}

		else if(isBlockQEmpty == 0 && isHoldQEmpty == 1)
		{
			if(lptr->readerCount > 0)
			{
				//kprintf("\nCase 2:");
				lptr->lhArray[currpid] = waitPrio;
				//Update Reader and Lock Count
				lptr->readerCount++;
				lptr->lockCnt++;
				//Update minHoldPrio
				if(lptr->minHoldPriority > waitPrio)
					lptr->minHoldPriority = waitPrio;
				//Update Locks Held
				pptr = &proctab[currpid]; 
				pptr->locksHeld[ldes] = waitPrio;
				//Schedule Current Process i.e. DONT insert in BlockQ
				toResched = 1;
			}
			else
			{
				if(waitPrio >= lptr->maxWaitPriority)
					lptr->maxWaitPriority = waitPrio;
				//Insert Block Lock ID in Proc Table
				pptr = &proctab[currpid]; 
				pptr->blockLock = ldes;
				//Block Current Proc
				lockInsert(currpid, lptr->lbhead, lptr->lbtail, waitPrio, procType);
				//Priority Inheritance
				minPrio = findMinPrio(ldes);
				int tqpid = 0;
				tqpid = q[currpid].qnext;
				tempPtr = &proctab[tqpid];
				while(tqpid < NPROC)
				{
					tempPtr = &proctab[tqpid];
					if(tempPtr->pOldPrio == 0)
						tempPtr->pOldPrio = tempPtr->pprio;
					if(tempPtr->pprio < pptr->pprio)
						chprio(tqpid, pptr->pprio);
					//kprintf("\n1. %s Priority has been changed to %d\n", tempPtr->pname, tempPtr->pprio);
					tqpid = q[tqpid].qnext;
				}
				if(pptr->pprio > minPrio)
				{
					initializaAndEmptyArray();
					doPriorityInversion(ldes, pptr->pprio);
					initializaAndEmptyArray();
				}
				toResched = 1;
			}
		}

		else if(isBlockQEmpty == 1 && isHoldQEmpty == 1)
		{
			if(lptr->readerCount > 0 && waitPrio > lptr->maxWaitPriority)
			{
				lptr->lhArray[currpid] = waitPrio;
				//Update Reader and Lock Count
				lptr->readerCount++;
				lptr->lockCnt++;
				//Update minHoldPrio
				if(lptr->minHoldPriority > waitPrio)
					lptr->minHoldPriority = waitPrio;
				//Schedule Current Process i.e. DONT insert in BlockQ
				//Update Locks Held
				pptr = &proctab[currpid]; 
				pptr->locksHeld[ldes] = waitPrio;
				toResched = 1;
			}
			else if(lptr->readerCount > 0 && waitPrio == lptr->maxWaitPriority)
			{
				int turnPID = 0;
				turnPID = q[lptr->lbtail].qprev;
				struct pentry *turnPtr = &proctab[turnPID];
				if(turnPtr->counter >= 3)
				{
					pptr = &proctab[currpid]; 
					pptr->blockLock = ldes;
					//Block Current Proc
					lockInsert(currpid, lptr->lbhead, lptr->lbtail, waitPrio, procType);
					//Priority Inheritance
					minPrio = findMinPrio(ldes);
					int tqpid = 0;
					tqpid = q[currpid].qnext;
					tempPtr = &proctab[tqpid];
					while(tqpid < NPROC)
					{
						tempPtr = &proctab[tqpid];
						if(tempPtr->pOldPrio == 0)
							tempPtr->pOldPrio = tempPtr->pprio;
						if(tempPtr->pprio < pptr->pprio)
							chprio(tqpid, pptr->pprio);
						//kprintf("\n1. %s Priority has been changed to %d\n", tempPtr->pname, tempPtr->pprio);
						tqpid = q[tqpid].qnext;
					}
					if(pptr->pprio > minPrio)
					{
						initializaAndEmptyArray();
						doPriorityInversion(ldes, pptr->pprio);
						initializaAndEmptyArray();
					}
					toResched = 1;
				}
				else
				{
					turnPtr->counter++;
					lptr->lhArray[currpid] = waitPrio;
					//Update Reader and Lock Count
					lptr->readerCount++;
					lptr->lockCnt++;
					//Update minHoldPrio
					if(lptr->minHoldPriority > waitPrio)
						lptr->minHoldPriority = waitPrio;
					//Schedule Current Process i.e. DONT insert in BlockQ
					//Update Locks Held
					pptr = &proctab[currpid]; 
					pptr->locksHeld[ldes] = waitPrio;
					toResched = 1;
				}

				
			}
			else if(lptr->readerCount > 0 && waitPrio < lptr->maxWaitPriority)
			{
				if(waitPrio >= lptr->maxWaitPriority)
					lptr->maxWaitPriority = waitPrio;
				//Insert Block Lock ID in Proc Table
				pptr = &proctab[currpid]; 
				pptr->blockLock = ldes;
				//Block Current Proc
				lockInsert(currpid, lptr->lbhead, lptr->lbtail, waitPrio, procType);
				//Priority Inheritance
				minPrio = findMinPrio(ldes);
				int tqpid = 0;
				tqpid = q[currpid].qnext;
				tempPtr = &proctab[tqpid];
				while(tqpid < NPROC)
				{
					tempPtr = &proctab[tqpid];
					if(tempPtr->pOldPrio == 0)
						tempPtr->pOldPrio = tempPtr->pprio;
					if(tempPtr->pprio < pptr->pprio)
						chprio(tqpid, pptr->pprio);
					//kprintf("\n1. %s Priority has been changed to %d\n", tempPtr->pname, tempPtr->pprio);
					tqpid = q[tqpid].qnext;
				}
				if(pptr->pprio > minPrio)
				{
					initializaAndEmptyArray();
					doPriorityInversion(ldes, pptr->pprio);
					initializaAndEmptyArray();
				}
				toResched = 1;
			}
			else if(lptr->writerCount > 0)
			{
				if(waitPrio >= lptr->maxWaitPriority)
					lptr->maxWaitPriority = waitPrio;
				//Insert Block Lock ID in Proc Table
				pptr = &proctab[currpid]; 
				pptr->blockLock = ldes;
				//Block Current Proc
				lockInsert(currpid, lptr->lbhead, lptr->lbtail, waitPrio, procType);
				//Priority Inheritance
				minPrio = findMinPrio(ldes);
				int tqpid = 0;
				tqpid = q[currpid].qnext;
				while(tqpid < NPROC)
				{
					tempPtr = &proctab[tqpid];
					//kprintf("\nID after me is %d\n", tqpid);
					if(tempPtr->pOldPrio == 0)
						tempPtr->pOldPrio = tempPtr->pprio;
					if(tempPtr->pprio < pptr->pprio)
						chprio(tqpid, pptr->pprio);
					//kprintf("\n1. %s Priority has been changed to %d\n", tempPtr->pname, tempPtr->pprio);
					tqpid = q[tqpid].qnext;
				}
				if(pptr->pprio > minPrio)
				{
					initializaAndEmptyArray();
					doPriorityInversion(ldes, pptr->pprio);
					initializaAndEmptyArray();
				}
				toResched = 1;
			}
		}
	}
	restore(ps);
	resched();
	pptr = &proctab[currpid];
	return pptr->plockret;
}

int doPriorityInversion(int ldes, int prio)
{
	struct lockentry *lptr = &locks[ldes];
	struct pentry *pptr, *tptr;
	//Check if HoldQ is empty
	int i = 0;
	int count = 0;
	int isHoldQEmpty = 0;
	for(i = 0; i<NPROC; i++)
	{
		if(lptr->lhArray[i] != 0)
		{
			count++;
		}
	}
	if(count != 0)
	{
		isHoldQEmpty = 1;
	}
	if(lptr->lstate != LFREE && isHoldQEmpty == 1)
	{
		laArray[ldes] = 1;
		for(i = 0; i<NPROC; i++)
		{
			if(lptr->lhArray[i] != 0)
			{
				pptr = &proctab[i];
				if(pptr->pOldPrio == 0)
					pptr->pOldPrio = pptr->pprio;
				if(pptr->pprio < prio)
					chprio(i, prio);
				//kprintf("\n%s Inherited Priority has changed to %d\n", pptr->pname, pptr->pprio);
				int qpid = q[lptr->lbtail].qprev;
				while(qpid < NPROC)
				{
					tptr = &proctab[qpid];
					if(tptr->pOldPrio == 0)
						tptr->pOldPrio = tptr->pprio;
					if(tptr->pprio < prio)
						chprio(qpid, prio);
					else if(tptr->pprio >= prio)
						break;
					//kprintf("\n%s Inherited Priority has changed to %d\n", tptr->pname, tptr->pprio);
					qpid = q[qpid].qprev;
				}
				if(pptr->blockLock != -1)
				{
					if(laArray[pptr->blockLock] == -1)
					{
						doPriorityInversion(pptr->blockLock, prio);
					}
				}
			}
		}
	}
	return OK;
}

int initializaAndEmptyArray()
{
	int i = 0;
	for(i = 0; i<100; i++)
		laArray[i] = -1;
}
int findMinPrio(int ldes)
{
	struct lockentry *lptr = &locks[ldes];
	struct pentry *pptr;
	int i = 0;
	int minPrio = 0;
	for(i = 0; i<NPROC; i++)
	{
		if(lptr->lhArray[i] != 0)
		{
			pptr = &proctab[i];
			if(minPrio == 0)
			{
				minPrio = pptr->pprio;
			}
			else if(minPrio != 0 && minPrio > pptr->pprio)
				minPrio = pptr->pprio;
		}
	}
	//kprintf("\nMin Priority is %d\n", minPrio);
	return minPrio;
}

