/* screate.c - screate, newsem */

#include <conf.h>
#include <kernel.h>
#include <proc.h>
#include <q.h>
#include <lock.h>
#include <stdio.h>

LOCAL int newLock();

/*------------------------------------------------------------------------
 * screate  --  create and initialize a semaphore, returning its id
 *------------------------------------------------------------------------
 */
int lcreate()
{
	int	lockID;
	lockID = newLock();
	locks[lockID].lockCnt = 0;
	return(lockID);
}

/*------------------------------------------------------------------------
 * newLock  --  allocate an unused lock and return its index
 *------------------------------------------------------------------------
 */
LOCAL int newLock()
{
	int	lockID;
	int	i;

	for (i=0 ; i<NLOCKS ; i++) {
		lockID=nextLock--;
		if (nextLock < 0)
			nextLock = NLOCKS-1;
		if (locks[lockID].lstate==LFREE) {
			locks[lockID].lstate = LUSED;
			return(lockID);
		}
	}
	return(SYSERR);
}
