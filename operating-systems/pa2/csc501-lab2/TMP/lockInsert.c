/* LockInsert.c  -  insert */

#include <conf.h>
#include <kernel.h>
#include <q.h>
#include <proc.h>
#include <lock.h>
#include <stdio.h>

/*------------------------------------------------------------------------
 * lockInsert.c  --  Insert a Process based on priority
 *------------------------------------------------------------------------
 */
int lockInsert(int proc, int head, int tail, int wprio, int procType)
{
	struct pentry *pptr;
	int qpid = q[tail].qprev;
	int tempPid = 0;
	int isWriterOfSamePrioPresent = 0;
	if(procType == WRITE)
	{
		if(q[tail].qprev == head)
		{
			//kprintf("\nFirst Writer\n");
			pptr = &proctab[proc];
			pptr->pstate = PRLOCKWAIT;
			insertInBlockQ(proc, head, tail, wprio, procType, head);
			return OK;
		}
		while(qpid < NPROC)
		{
			if(q[qpid].qwaitPrio >= wprio)
			{
				qpid = q[qpid].qprev;
				if(qpid == head)
				{
					pptr = &proctab[proc];
					pptr->pstate = PRLOCKWAIT;
					tempPid = q[qpid].qnext; 
					insertInBlockQ(proc, qpid, tempPid, wprio, procType, head);
					return OK;
				}
			}
			else
			{
				//kprintf("\Future Writers\n");
				pptr = &proctab[proc];
				pptr->pstate = PRLOCKWAIT;
				tempPid = q[qpid].qnext; 
				insertInBlockQ(proc, qpid, tempPid, wprio, procType, head);
				return OK;
			}
		}
	}

	else if(procType == READ)
	{
		//kprintf("\nEntered Read");
		while(qpid < NPROC)
		{
			if(q[qpid].qType == WRITE && q[qpid].qwaitPrio == wprio)
			{
				isWriterOfSamePrioPresent = 1;
				break;
			}
			qpid = q[qpid].qprev;
		}

		if(isWriterOfSamePrioPresent == 0)
		{
			if(q[tail].qprev == head)
			{
				//kprintf("\nFirst Reader\n");
				pptr = &proctab[proc];
				pptr->pstate = PRLOCKWAIT;
				insertInBlockQ(proc, head, tail, wprio, procType, head);
				return OK;
			}
			qpid = q[tail].qprev;
			//kprintf("\nCurrent Process in Block Q: %d", qpid);
			while(qpid < NPROC)
			{
				if(q[qpid].qwaitPrio >= wprio)
				{
					qpid = q[qpid].qprev;
					if(qpid == head)
					{
						pptr = &proctab[proc];
						pptr->pstate = PRLOCKWAIT;
						tempPid = q[qpid].qnext; 
						insertInBlockQ(proc, qpid, tempPid, wprio, procType, head);
						return OK;
					}
				}
				else
				{
					//kprintf("\Future Readers\n");
					tempPid = q[qpid].qnext; 
					pptr = &proctab[proc];
					pptr->pstate = PRLOCKWAIT;
					insertInBlockQ(proc, qpid, tempPid, wprio, procType, head);
					return OK;
				}
			}
		}

		else if(isWriterOfSamePrioPresent == 1)
		{
			qpid = q[tail].qprev;
			while(qpid < NPROC)
			{
				if(q[qpid].qType == WRITE && q[qpid].qwaitPrio == wprio)
				{
					break;
				}
				qpid = q[qpid].qprev;
			}
			pptr = &proctab[proc];
			pptr->pstate = PRLOCKWAIT;
			tempPid = q[qpid].qnext; 
			insertInBlockQ(proc, qpid, tempPid, wprio, procType, head);
			return OK;
		}
	}
	//kprintf("\nI am here");
	return(OK);
}

int printBlockQ(int head)
{
	int qqpid = q[head].qnext; 
	//kprintf("\n");
	while(qqpid < NPROC)
	{
		//kprintf("%d: %d -> ", q[qqpid].qType, q[qqpid].qwaitPrio);
		qqpid = q[qqpid].qnext;
	}
}


int insertInBlockQ(int proc, int prev, int next, int wprio, int procType, int head)
{
	q[proc].qnext = next;
	q[proc].qprev = prev;
	q[proc].qwaitPrio = wprio;
	q[proc].qType = procType;
	q[prev].qnext = proc;
	q[next].qprev = proc;
	//printBlockQ(head);
}

