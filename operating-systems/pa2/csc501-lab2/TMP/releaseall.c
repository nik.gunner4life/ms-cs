#include <conf.h>
#include <kernel.h>
#include <proc.h>
#include <q.h>
#include <lock.h>
#include <stdio.h>

int releaseall(nargs, args)
long nargs;
long args;
{
	unsigned long	*allLockPtrs;		/* points to list of args	*/
	long ldes;
	STATWORD 	ps;   
	disable(ps);
	int toResched = 0;
	allLockPtrs = (unsigned long *)(&args) + (nargs-1); /* last argument	*/
	int numLocksInNargs = nargs;
	for ( ; nargs > 0 ; nargs--)	
	{
		ldes =*allLockPtrs--;
		struct lockentry *lptr = &locks[ldes];
		//kprintf("\nCurrent PID is %d. Forbidden Array is %d\n", currpid, lptr->lfArray[currpid]);
		if(lptr->lstate == LFREE || lptr->lfArray[currpid] != 0)
		{
			//kprintf("\nCurrent PID is %d. Forbidden Array is %d\n", currpid, lptr->lfArray[currpid]);
			restore(ps);
			return SYSERR;
		}
		register struct pentry *pptr;
		pptr = &proctab[currpid];
		//Change Block Lock to -1. This is not needed but it is a safety condition.
		pptr->blockLock = -1;
		
		int setPrio = 0;
		//Remove Inherited Priority
		int prioToSend = 0;
		if(pptr->pOldPrio != 0)
		{
			setPrio = setPriority(ldes, pptr->pOldPrio);
			//kprintf("\nSending Old Prio\n");
		}
		else
		{
			setPrio = setPriority(ldes, pptr->pprio);
			//kprintf("\nSending Inherited/PPrio\n");
		}
		chprio(currpid, setPrio);
		//kprintf("\n%s Priority has been changed back to %d\n", pptr->pname, pptr->pprio);
		pptr->locksHeld[ldes] = -1;
		int isBlockQEmpty = 0;
		int isHoldQEmpty = 0;
		int qpid;
		//Check if BlockQ is empty
		if(q[lptr->lbtail].qprev != lptr->lbhead)
		{
			isBlockQEmpty = 1;
		}
	
		//------------------------------------------------------------
		//Check if current process is a reader process
		//------------------------------------------------------------
	
		if(lptr->readerCount > 0)
		{
			//Decrement the count
			lptr->lockCnt--;
			lptr->readerCount--;
			//Remove from the HoldQ
			lptr->lhArray[currpid] = 0;
			if(lptr->readerCount == 0)
			{
				if(isBlockQEmpty == 1 && q[q[lptr->lbtail].qprev].qType == WRITE)
				{
					int proc = q[lptr->lbtail].qprev;
					//Increment Count
					lptr->lockCnt++;
					lptr->writerCount++;
					//Update MinHoldPriority
					lptr->minHoldPriority = q[proc].qwaitPrio;
					//Update MaxWaitPriority
					if(q[proc].qprev != lptr->lbhead)
						lptr->maxWaitPriority = q[q[proc].qprev].qwaitPrio;
					else
						lptr->maxWaitPriority = 0;
					//Remove the element from BlockQ
					removeFromBlockQ(lptr->lbhead, lptr->lbtail, proc);
					//Put Process in HoldQ
					lptr->lhArray[proc] = q[proc].qwaitPrio;
					//Change the process state to PRREADY and Insert in RdyQ
					pptr = &proctab[proc];
					pptr->pstate = PRREADY;
					//Update Locks Held
					pptr->locksHeld[ldes] = q[proc].qwaitPrio;
					pptr->blockLock = -1;
					insert(proc, rdyhead, pptr->pprio);
					toResched = 1;
				}
				else if(isBlockQEmpty == 1 && q[q[lptr->lbtail].qprev].qType == READ)
				{
					int qtraversalPID = q[lptr->lbtail].qprev;
					while(q[qtraversalPID].qType != WRITE)
					{
						qtraversalPID = q[qtraversalPID].qprev;
					}
					if(qtraversalPID != lptr->lbhead)
					{
						//Increment Count
						lptr->lockCnt++;
						lptr->writerCount++;
						//Update MinHoldPriority
						lptr->minHoldPriority = q[qtraversalPID].qwaitPrio;
						//Update MaxWaitPriority
						lptr->maxWaitPriority = q[qtraversalPID].qwaitPrio;
						//Remove the element from BlockQ
						removeFromBlockQ(lptr->lbhead, lptr->lbtail, qtraversalPID);
						//Put the Process in the HoldQ
						lptr->lhArray[qtraversalPID] = q[qtraversalPID].qwaitPrio;
						//Change the process state to PRREADY and Insert in RdyQ
						pptr = &proctab[qtraversalPID];
						pptr->pstate = PRREADY;
						pptr->blockLock = -1;
						//Update Locks Held
						pptr->locksHeld[ldes] = q[qtraversalPID].qwaitPrio;
						insert(qtraversalPID, rdyhead, pptr->pprio);
						toResched = 1;
					}
					else
					{
					}
				}
			}
	
		}

		//------------------------------------------------------------
		//Check if current process is a writer process
		//------------------------------------------------------------

		else if(lptr->writerCount > 0)
		{
			//Decrement the Count
			lptr->writerCount--;
			lptr->lockCnt--;
			//Remove from the HoldQ
			lptr->lhArray[currpid] = 0;
			int proc = q[lptr->lbtail].qprev;
			if(isBlockQEmpty == 0)
			{
				//Do Nothing. All Processes have completed as of now. 
				toResched = 1;
			}
			else if(q[proc].qType == WRITE)
			{
				//Increment Count
				lptr->lockCnt++;
				lptr->writerCount++;
				//Update MinHoldPriority
				lptr->minHoldPriority = q[proc].qwaitPrio;
				//Update MaxWaitPriority
				if(q[proc].qprev != lptr->lbhead)
					lptr->maxWaitPriority = q[q[proc].qprev].qwaitPrio;
				else
					lptr->maxWaitPriority = 0;
				//Remove the element from BlockQ
				removeFromBlockQ(lptr->lbhead, lptr->lbtail, proc);
				//Put the Process in the HoldQ
				lptr->lhArray[proc] = q[proc].qwaitPrio;
				//Change the process state to PRREADY and Insert in RdyQ
				pptr = &proctab[proc];
				pptr->pstate = PRREADY;
				pptr->blockLock = -1;
				//Update Locks Held
				pptr->locksHeld[ldes] = q[proc].qwaitPrio;
				insert(proc, rdyhead, pptr->pprio);
				toResched = 1;
			}	
			else if(q[proc].qType == READ)
			{
				//Loop through the block Q and increment priorityCounter once for every
				//group of read priorities till we reach a Write Proc and store writerProc ID. 
				qpid = lptr->lbtail;
				qpid = q[qpid].qprev;
				while(q[qpid].qType != WRITE)
				{
					qpid = q[qpid].qprev;
					//kprintf("%d", qpid);
					if(qpid == lptr->lbhead)
					{
						break;
					}
				}
	
				if(qpid == lptr->lbhead)
				{
					qpid = lptr->lbtail;
					qpid = q[qpid].qprev;
					//No Writer Process Found and hence schedule all reader processes
					//kprintf("\nI am here becoz no writer Processes found.\n");
					while(qpid<NPROC)
					{
						lptr->lockCnt++;
						lptr->readerCount++;
						//Update MinHoldPriority
						lptr->minHoldPriority = q[qpid].qwaitPrio;
						//Update MaxWaitPriority
						if(q[qpid].qprev != lptr->lbhead)
							lptr->maxWaitPriority = q[q[qpid].qprev].qwaitPrio;
						else
							lptr->maxWaitPriority = 0;
						//Remove the element from BlockQ
						removeFromBlockQ(lptr->lbhead, lptr->lbtail, qpid);
						//Put the process in HoldQ
						lptr->lhArray[qpid] = q[qpid].qwaitPrio;
						//Change the process state to PRREADY and Insert in RdyQ
						pptr = &proctab[qpid];
						pptr->pstate = PRREADY;
						pptr->blockLock = -1;
						//Update Locks Held
						pptr->locksHeld[ldes] = q[qpid].qwaitPrio;
						insert(qpid, rdyhead, pptr->pprio);
						qpid = q[lptr->lbtail].qprev;
					}	
					toResched = 1;
				}
				else if(q[qpid].qType == WRITE)
				{
					//kprintf("\nI am here becoz writer Processes WERE found.\n");
					//Update Priority Counter
					int tempPrio = q[qpid].qwaitPrio;
					int tempPID = qpid;
					struct pentry *tempPtr = &proctab[tempPID];
					if(q[q[qpid].qnext].qType == READ && q[q[qpid].qnext].qwaitPrio == tempPrio)
					{
						tempPtr->counter++;
					}
					qpid = q[lptr->lbtail].qprev;
	
					if(q[qpid].qwaitPrio == tempPrio)
					{
						if(tempPtr->counter > TURNS)
						{
							//kprintf("\nCount is GREATER than three and hence schedule all readers upto readers of that Prio\n");
							//Increment Count
							lptr->lockCnt++;
							lptr->writerCount++;
							//Update MinHoldPriority
							lptr->minHoldPriority = q[tempPID].qwaitPrio;
							//Update MaxWaitPriority
							lptr->maxWaitPriority = q[tempPID].qwaitPrio;
							//Remove the element from BlockQ
							removeFromBlockQ(lptr->lbhead, lptr->lbtail, tempPID);
							//Put the Process in the HoldQ
							lptr->lhArray[tempPID] = q[tempPID].qwaitPrio;
							//Change the process state to PRREADY and Insert in RdyQ
							pptr = &proctab[tempPID];
							pptr->pstate = PRREADY;
							pptr->blockLock = -1;
							//Update Locks Held
							pptr->locksHeld[ldes] = q[tempPID].qwaitPrio;
							insert(tempPID, rdyhead, pptr->pprio);
							toResched = 1;
						}
						else
						{
							qpid = q[lptr->lbtail].qprev;
							while(qpid != tempPID)
							{
								//kprintf("\nCount is lesser than three and hence schedule all readers\n");
								lptr->lockCnt++;
								lptr->readerCount++;
								//Update MinHoldPriority
								lptr->minHoldPriority = q[qpid].qwaitPrio;
								//Update MaxWaitPriority
								if(q[qpid].qprev != lptr->lbhead)
									lptr->maxWaitPriority = q[q[qpid].qprev].qwaitPrio;
								else
									lptr->maxWaitPriority = 0;
								//Remove the element from BlockQ
								removeFromBlockQ(lptr->lbhead, lptr->lbtail, qpid);
								//Put the process in HoldQ
								lptr->lhArray[qpid] = q[qpid].qwaitPrio;
								//Change the process state to PRREADY and Insert in RdyQ
								pptr = &proctab[qpid];
								pptr->pstate = PRREADY;
								pptr->blockLock = -1;
								//Update Locks Held
								pptr->locksHeld[ldes] = q[qpid].qwaitPrio;
								insert(qpid, rdyhead, pptr->pprio);
								qpid = q[lptr->lbtail].qprev;
							}
							toResched = 1;
						}	
					}	
					else if(q[qpid].qwaitPrio != tempPrio)
					{
						if(tempPtr->counter > TURNS)
						{
							while(q[qpid].qwaitPrio != tempPrio)
							{
								lptr->lockCnt++;
								lptr->readerCount++;
								//Update MinHoldPriority
								lptr->minHoldPriority = q[qpid].qwaitPrio;
								//Update MaxWaitPriority
								if(q[qpid].qprev != lptr->lbhead)
									lptr->maxWaitPriority = q[q[qpid].qprev].qwaitPrio;
								else
									lptr->maxWaitPriority = 0;
								//Remove the element from BlockQ
								removeFromBlockQ(lptr->lbhead, lptr->lbtail, qpid);
								//Put the process in HoldQ
								lptr->lhArray[qpid] = q[qpid].qwaitPrio;
								//Change the process state to PRREADY and Insert in RdyQ
								pptr = &proctab[qpid];
								pptr->pstate = PRREADY;
								pptr->blockLock = -1;
								//Update Locks Held
								pptr->locksHeld[ldes] = q[qpid].qwaitPrio;
								insert(qpid, rdyhead, pptr->pprio);
								qpid = q[lptr->lbtail].qprev;
							}
						}
						else
						{
							while(qpid != tempPID)
							{
								lptr->lockCnt++;
								lptr->readerCount++;
								//Update MinHoldPriority
								lptr->minHoldPriority = q[qpid].qwaitPrio;
								//Update MaxWaitPriority
								if(q[qpid].qprev != lptr->lbhead)
									lptr->maxWaitPriority = q[q[qpid].qprev].qwaitPrio;
								else
									lptr->maxWaitPriority = 0;
								//Remove the element from BlockQ
								removeFromBlockQ(lptr->lbhead, lptr->lbtail, qpid);
								//Put the process in HoldQ
								lptr->lhArray[qpid] = q[qpid].qwaitPrio;
								//Change the process state to PRREADY and Insert in RdyQ
								pptr = &proctab[qpid];
								pptr->pstate = PRREADY;
								pptr->blockLock = -1;
								//Update Locks Held
								pptr->locksHeld[ldes] = q[qpid].qwaitPrio;
								insert(qpid, rdyhead, pptr->pprio);
								qpid = q[lptr->lbtail].qprev;
							}
						}
						
					}
				
				}
			}
		}
	}
	if(toResched == 1)
	{
		restore(ps);
		resched();
		struct pentry *finalPtr;
		finalPtr = &proctab[currpid];
		return finalPtr->plockret;
	}
}

int removeFromBlockQ(int head, int tail, int proc)
{
	int qpid = q[tail].qprev;
	while(qpid != proc)
	{
		qpid = q[qpid].qprev;
		if(qpid == head)
		{
			//kprintf("\nProcess not in BlockedQ");
			return OK;
		}
	}
	int prev = q[qpid].qprev;
	int next = q[qpid].qnext; 
	q[prev].qnext = next;
	q[next].qprev = prev;
}

int setPriority(int ldes, int testPrio)
{
	struct lockentry *lptr;
	struct pentry *pptr = &proctab[currpid];
	struct pentry *traversalpptr;
	int maxPrio = 0;
	int i = 0;
	for(i = 0; i<NLOCKS; i++)
	{
		if(pptr->locksHeld[i] != -1 && i != ldes)
		{
			lptr = &locks[i];
			int qpid = q[lptr->lbtail].qprev;
			while(qpid<NPROC)
			{
				traversalpptr = &proctab[qpid];
				if(traversalpptr->pOldPrio != 0 && traversalpptr->pOldPrio > maxPrio)
				{
					maxPrio = traversalpptr->pOldPrio;
					//kprintf("\n1. Priority Changing here\n");
				}
				else if(traversalpptr->pOldPrio == 0 && traversalpptr->pprio > maxPrio)
				{
					maxPrio = traversalpptr->pprio;
					//kprintf("\n2. Priority Changing here\n");
				}
				qpid = q[qpid].qprev;
			}
		}
	}
	if(maxPrio != 0)
	{
		maxPrio = pptr->pOldPrio;
		if(maxPrio > testPrio)
		{
		//Do Nothing
		}
		else if(maxPrio <= testPrio)
		{
			maxPrio == testPrio;
		}
	}
	else if(maxPrio == 0)
	{
		maxPrio = pptr->pOldPrio;
	}
	return maxPrio;
}