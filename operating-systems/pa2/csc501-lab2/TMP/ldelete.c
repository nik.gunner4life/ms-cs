#include <conf.h>
#include <kernel.h>
#include <proc.h>
#include <q.h>
#include <lock.h>
#include <stdio.h>

/************************************************************** */
/* Lock Delete												    */
/************************************************************** */
int ldelete(int ldes)
{
	STATWORD ps; 
	disable(ps);
	struct	lockentry *lptr;
	struct pentry *pptr;
	if(ldes >= NLOCKS || (lptr = &locks[ldes])->lstate == LFREE)
	{
		restore(ps);
		return SYSERR;
	}
	//kprintf("\nLdelete has been called\n");
	lptr = &locks[ldes];
	int i = 0;
	//Insert Processes from the HoldQ into the Forbidden Kingdom
	for(i = 0; i<NPROC; i++)
	{
		if(lptr->lhArray[i] != 0)
		{
			lptr->lfArray[i] = i;
			pptr = &proctab[i];
			pptr->plockret = DELETED;
			//kprintf("\nThe Lock has been deleted. %s PLOCKRET has been set to %d\n", pptr->pname, pptr->plockret);
			//pptr->pstate = PRREADY;
		}
	}

	//Insert All Processes from the Blocked Q into the Forbidden Kingdom
	int qpid = q[lptr->lbtail].qprev;
	while(qpid < NPROC)
	{
		lptr->lfArray[qpid] = qpid;
		pptr = &proctab[qpid];
		pptr->plockret = DELETED;
		//kprintf("\nThe Lock has been deleted. %s PLOCKRET has been set to %d\n", pptr->pname, pptr->plockret);
		removeProcessFromBlockQOfTheLock(ldes, qpid);
		pptr->pstate = PRREADY;
		insert(qpid, rdyhead, pptr->pprio);
		qpid = q[lptr->lbtail].qprev;
	}
	//Free the lock for future Use
	lptr->lstate = LFREE;
	restore(ps);
	resched();
	return(OK);
}

removeProcessFromBlockQOfTheLock(int ldes, int procPID)
{
	struct lockentry *lptr = &locks[ldes];
	struct pentry *pptr, *tptr;
	pptr = &proctab[procPID];
	int next = q[procPID].qnext;
	int prev = q[procPID].qprev;
	q[next].qprev = prev;
	q[prev].qnext = next;
}
