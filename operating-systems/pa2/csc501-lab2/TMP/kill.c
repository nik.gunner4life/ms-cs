/* kill.c - kill */

#include <conf.h>
#include <kernel.h>
#include <proc.h>
#include <sem.h>
#include <lock.h>
#include <mem.h>
#include <io.h>
#include <q.h>
#include <stdio.h>

/*------------------------------------------------------------------------
 * kill  --  kill a process and remove it from the system
 *------------------------------------------------------------------------
 */
SYSCALL kill(int pid)
{
	STATWORD ps;    
	struct	pentry	*pptr;		/* points to proc. table for pid*/
	int	dev;

	disable(ps);
	if (isbadpid(pid) || (pptr= &proctab[pid])->pstate==PRFREE) {
		restore(ps);
		return(SYSERR);
	}
	if (--numproc == 0)
		xdone();

	dev = pptr->pdevs[0];
	if (! isbaddev(dev) )
		close(dev);
	dev = pptr->pdevs[1];
	if (! isbaddev(dev) )
		close(dev);
	dev = pptr->ppagedev;
	if (! isbaddev(dev) )
		close(dev);
	//kprintf("\nDo I come here? PID is %d and state is %c\n", pid, proctab[pid].pstate);
	if(pptr->pstate != PRLOCKWAIT)
	{
		int count = 0;
		int i = 0;
		for(i = 0; i<NLOCKS; i++)
		{
			if(pptr->locksHeld[i] != -1)
				count ++;
		}
		if(count == 0)
		{
			//This means the process Has reached here after completion.
		}
		else
		{
			//kprintf("\nI enter this stupid condition\n");
			releaseAllTheLocks(pid);
		}
	}
	else
	{
		killABlockedProcess(pid);
	}
	send(pptr->pnxtkin, pid);

	freestk(pptr->pbase, pptr->pstklen);
	switch (pptr->pstate) {

	case PRCURR:
		{
			pptr->pstate = PRFREE;	/* suicide */
			resched();
			break;
		}

	case PRWAIT:	semaph[pptr->psem].semcnt++;

	case PRREADY:	
		{
			dequeue(pid);
			pptr->pstate = PRFREE;
			break;
		}

	case PRLOCKWAIT:
		break;
	case PRSLEEP:
	case PRTRECV:	unsleep(pid);
						/* fall through	*/
	default:	pptr->pstate = PRFREE;
	}
	restore(ps);
	return(OK);
}

int killABlockedProcess(int pid)
{
	struct lockentry *lptr;
	struct pentry *pptr, *tptr;
	pptr = &proctab[pid];
	int i = 0;
	lptr = &locks[pptr->blockLock];
	int isBlockQEmpty = 0;
	if(q[pid].qnext != lptr->lbtail && q[pid].qprev == lptr->lbhead)
	{
		//This means there are processes ahead of me and none behind me. Either with the same priority or a higher Priority. We cannot definitely say who gave them that priority.
		//Hence do the following:
		int qpid = q[pid].qnext; 
		//With This every1 in the BlockQ has been updated.
		int newPrio = 0;
		while(qpid < NPROC)
		{
			tptr = &proctab[qpid];
			if(tptr->pprio > pptr->pprio)
			{
				//kprintf("\nHere the value is greater and hence break.\n");
				break;
			}
			else if(tptr->pprio == pptr->pprio)
			{
				//THis basically means that its priority is the same and has not been inherited and hence we can break. 
				if(tptr->pOldPrio == 0 || tptr->pOldPrio == tptr->pprio)
				{
					//kprintf("\nHere the value is greater but old value is lesser and hence break.\n");
					break;
				}
				//If its old Priority is smaller than my current running priority that would mean he has inherited that from me of from someone behind me. Eitherways do the same.
				else if(tptr->pOldPrio != 0 && tptr->pOldPrio < pptr->pprio)
				{
					if(newPrio < tptr->pOldPrio)
						newPrio = tptr->pOldPrio;
					chprio(qpid, newPrio); // There is no 1 behind me. Hence I gave this priority. And hence I am changing it back
					
					//kprintf("\nHere the value is greater but old value is lesser and hence change priority. %s prio has been changed to %d\n", tptr->pname, tptr->pprio);
				}
			}
			else
			{
				//By my calculations this case should not occur
				//kprintf("\n1. This case should not occur\n");
			}
			qpid = q[qpid].qnext;
		}
		redoPriorityInversion(pptr->blockLock, newPrio, pptr->pprio);
	}
	else if(q[pid].qnext != lptr->lbtail && q[pid].qprev != lptr->lbhead)
	{
		//There are processes behind me and there are processes ahead of me. The processes ahead of me will have the same or higher priority. Again we cannot say who gave them that 
		//priority. Do the following: 
		int qpid = q[pid].qnext; 
		tptr = &proctab[q[pid].qprev];
		int newPrio = tptr->pprio;
		//With This every1 in the BlockQ has been updated.
		while(qpid < NPROC)
		{
			tptr = &proctab[qpid];
			if(tptr->pprio > pptr->pprio)
			{
				//kprintf("\nHere the value is greater and hence break.\n");
				break;
			}
			else if(tptr->pprio == pptr->pprio)
			{
				//THis basically means that its priority is the same and has not been inherited and hence we can break. 
				if(tptr->pOldPrio == 0 || tptr->pOldPrio == tptr->pprio)
				{
					//kprintf("\nHere the value is equal but the old value is also the same and hence break.\n");
					break;
				}
				//If its old Priority is smaller than my current running priority that would mean he has inherited that from me of from someone behind me. Eitherways do the same.
				else if(tptr->pOldPrio < pptr->pprio)
				{
					if(newPrio <= tptr->pOldPrio)
					{
						chprio(qpid, tptr->pOldPrio);
						newPrio = tptr->pOldPrio;
					}
					else
					{
						chprio(qpid, newPrio);
					}
					//kprintf("\n1. Here the value is greater but old value is lesser and hence change priority. %s prio has been changed to %d\n", tptr->pname, tptr->pprio);
				}
			}
			else
			{
				//By my calculations this case should not occur
				//kprintf("\n2. This case should not occur\n");
			}
			qpid = q[qpid].qnext;
		}
		redoPriorityInversion(pptr->blockLock, newPrio, pptr->pprio);

	}
	else if(q[pid].qnext == lptr->lbtail && q[pid].qprev != lptr->lbhead)
	{
		tptr = &proctab[q[pid].qprev];
		//Processes behind me and none ahead of me. Do the following.
		redoPriorityInversion(pptr->blockLock, tptr->pprio, pptr->pprio);
	}
	else
	{
		//I am the only process in the Q.
		redoPriorityInversion(pptr->blockLock, 0, pptr->pprio);
	}
	removeProcessFromBlockQAndClearProcess(pptr->blockLock, pid);
}

removeProcessFromBlockQAndClearProcess(int ldes, int procPID)
{
	struct lockentry *lptr = &locks[ldes];
	struct pentry *pptr, *tptr;
	pptr = &proctab[procPID];
	int next = q[procPID].qnext;
	int prev = q[procPID].qprev;
	q[next].qprev = prev;
	q[prev].qnext = next;
	int i = 0;
	for(i = 0; i < NLOCKS; i++)
	{
		pptr->locksHeld[i] = -1;
	}
	pptr->counter = 0;
	pptr->blockLock = -1;
}

int redoPriorityInversion(int ldes, int newPrio, int killedProcessPrio)
{
	struct lockentry *lptr = &locks[ldes];
	struct pentry *pptr, *tptr;
	int i = 0;
	int qpid = 0;
	int count = 0;
	int isHoldQEmpty = 0;
	for(i = 0; i<NPROC; i++)
	{
		if(lptr->lhArray[i] != 0)
		{
			count++;
		}
	}
	if(count != 0)
	{
		isHoldQEmpty = 1;
	}
	if(lptr->lstate != LFREE && isHoldQEmpty == 1)
	{
		for(i = 0; i<NLOCKS; i++)
		{
			if(lptr->lhArray[i] != 0)
			{
				tptr = &proctab[i];
				if(tptr->pprio > killedProcessPrio);
					//Dont Do Anything
				else if(tptr->pprio == killedProcessPrio)
				{
					if(tptr->pOldPrio == 0 || tptr->pOldPrio == tptr->pprio)
					{
						//Dont Do anything
					}
					//If its old Priority is smaller than my current running priority that would mean he has inherited that from me of from someone behind me. Eitherways do the same.
					else if(tptr->pOldPrio < killedProcessPrio)
					{
						if(newPrio == 0)
							chprio(i, tptr->pOldPrio);
						else
							chprio(i, newPrio);
						//kprintf("\nIn REDO: %s Priority has been changed to %d\n", tptr->pname, tptr->pprio);
					}
				}
				else
				{
					//This case should not happen
					//kprintf("\nTHis case should not happen\n");
				}
				if(tptr->blockLock != -1)
				{
					redoPriorityInversion(tptr->blockLock, newPrio, killedProcessPrio);
				}

			}
		}
	}
}

int releaseAllTheLocks(int pid)
{
	struct pentry *pptr = &proctab[pid];
	int i = 0;
	if(pid != 49 && pid != 0)
	{
		for(i = 0; i<NLOCKS; i++)
		{
			if(pptr->locksHeld[i] != -1)
			{
				//kprintf("\NLOCKS Number: %d\n", i);
				releaseall(1, i);
			}
		}
	}
	return OK;
}

int printingBlockQ(int head)
{
	int qqpid = q[head].qnext; 
	//kprintf("\n");
	while(qqpid < NPROC)
	{
		//kprintf("%d: %d -> ", q[qqpid].qType, q[qqpid].qwaitPrio);
		qqpid = q[qqpid].qnext;
	}
}