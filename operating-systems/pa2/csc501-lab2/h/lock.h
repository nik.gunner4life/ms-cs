#ifndef _LOCK_H_
#define _LOCK_H_make

#ifndef	NLOCKS
#define	NLOCKS		50	/* number of locks, if not defined	*/
#endif

#define	LFREE	1		/* this lock is free		*/
#define	LUSED	2		/* this lock is used		*/
#define TURNS	3

struct	lockentry	{		/* lock table entry		*/
	int	lstate;		/* the state LFREE or LUSED		*/
	int	lockCnt;		/* count for this Lock		*/
	int	lbhead;		/* q index of head of list for processes currently holding the lock */
	int	lbtail;		/* q index of tail of list for processes currently holding the lock	*/
	int lhArray[50];
	int lfArray[50];
	int prioCounter[100];
	int maxWaitPriority;
	int minHoldPriority;
	int readerCount;
	int writerCount;
};
extern	struct	lockentry	locks[];
extern	int	nextLock;

#define	isbadlock(s)	(s<0 || s>=NLOCK)

#endif
