/* user.c - main */

#include <conf.h>
#include <kernel.h>
#include <proc.h>
#include <stdio.h>

void halt();

/*------------------------------------------------------------------------
 *  main  --  user main program
 *------------------------------------------------------------------------
 */
int main()
{
	kprintf("\n\nHello World, Xinu lives\n\n");
	kprintf("\nInput: 0xAABBCCDD");
	kprintf("\nOutput: %08X\n", zfunction(0xAABBCCDD));
	printsegaddress();
	printprocstks(50);
	printtos();
	return 0;
}
