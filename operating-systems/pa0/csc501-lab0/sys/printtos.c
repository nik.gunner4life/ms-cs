#include <conf.h>
#include <kernel.h>
#include <proc.h>
#include <stdio.h>

static unsigned long *ebp;

void printtos()
{
	asm("movl %ebp,ebp");
	int a = 4;
	int b = 2;
	int c = 474;
	int d = 56;	
	kprintf("\nBefore[0x%08X]: 0x%08X\n", ebp+2, *(ebp+2));
	kprintf("After[0x%08X]: 0x%08X\n", ebp, *ebp);
	kprintf("\telement[0x%08X]: 0x%08X\n", ebp-1, *(ebp-1));
	kprintf("\telement[0x%08X]: 0x%08X\n", ebp-2, *(ebp-2));
	kprintf("\telement[0x%08X]: 0x%08X\n", ebp-3, *(ebp-3));
	kprintf("\telement[0x%08X]: 0x%08X\n", ebp-4, *(ebp-4));
}