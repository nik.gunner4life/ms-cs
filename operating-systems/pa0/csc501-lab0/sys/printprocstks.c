#include <conf.h>
#include <kernel.h>
#include <proc.h>
#include <stdio.h>

static unsigned long *esp;

void printprocstks(int priority)
{		
		int pid = 0;
		unsigned long *sp;
		kprintf("# Filter: %d", priority);
		for(pid = 0; pid<NPROC; pid++)
		{
			struct pentry *proc = &proctab[pid];
			if(proc->pprio<priority)
			{
				if(proc->pstate != PRFREE && proc->pstate != PRCURR)
				{
					kprintf("\nProcess [%s]\n", proc->pname);
					kprintf("\tPID: %d\n", pid);
					kprintf("\tPriority: %d\n", proc->pprio);
					kprintf("\tLength: %d\n", proc->pstklen);
					kprintf("\tBase: 0x%08X\n", proc->pbase);
					kprintf("\tLimit: 0x%08X\n", proc->plimit);
					kprintf("\tPointer: 0x%08X\n", proc->pesp);
				}	
				else if(proc->pstate == PRCURR)
				{
					asm("movl %esp,esp");
					sp = esp;
					kprintf("\nProcess [%s]\n", proc->pname);
					kprintf("\tPID: %d\n", pid);
					kprintf("\tPriority: %d\n", proc->pprio);
					kprintf("\tLength: %d\n", proc->pstklen);
					kprintf("\tBase: 0x%08X\n", proc->pbase);
					kprintf("\tLimit: 0x%08X\n", proc->plimit);
					kprintf("\tPointer: 0x%08X\n", sp);
				
				}
				else
				{
				}
			}
		}
		
}