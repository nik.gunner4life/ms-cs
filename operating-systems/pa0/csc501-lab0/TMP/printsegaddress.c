#include <conf.h>
#include <kernel.h>
#include <proc.h>
#include <stdio.h>

extern long etext, edata;

void printsegaddress()
{
	
    kprintf("Current: etext[0x%08X] = 0x%08X, edata[0x%08X] = 0x%08X\n", &etext, *(&etext), &edata, *(&edata));
	kprintf("Preceding: etext[0x%08X] = 0x%08X, edata[0x%08X] = 0x%08X\n", &etext-1, *(&etext-1), &edata-1, *(&edata-1));
	kprintf("Following: etext[0x%08X] = 0x%08X, edata[0x%08X] = 0x%08X\n", &etext+1, *(&etext+1), &edata+1, *(&edata+1));
}