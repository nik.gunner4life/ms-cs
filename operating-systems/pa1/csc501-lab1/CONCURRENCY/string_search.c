#include<stdio.h>
#include<stdlib.h>
#include<pthread.h>
#include<string.h>
#include<time.h>
#include<sys/time.h>

char* stringPattern;
char* stringToSearch;

void* findPattern(void* charPtr)
{
	char* pattern;
	pattern = stringPattern;
	int length = strlen(pattern);
    int count = 0;
    for (charPtr = strstr(charPtr, pattern); charPtr; charPtr = strstr(charPtr + length, pattern))
        ++count;
    return (void*)count;
}


int main(int argc, char* argv[])
{
	//Get the pattern string
	stringPattern = (char*)malloc(sizeof(char*)*strlen(argv[1]));
	stringPattern = argv[1];
	int patternLength = strlen(stringPattern);

	//Read File into a string. stdin is a file pointer. 
	int stringToSearchLength = 0;

	//Find Size of the file
	fseek(stdin, 0, SEEK_END);
    stringToSearchLength = ftell(stdin);
	rewind(stdin);

	//Read the file to a string
	stringToSearch = (char*)malloc(sizeof(char) * stringToSearchLength);
	fread(stringToSearch,1,stringToSearchLength,stdin);

	//Split the String into blocks and give them to a thread
	int numThreads = 200;
	char* tempPointer = stringToSearch;
	char** stringArray = (char**)malloc(sizeof(char**)*numThreads);
	int threadStringLength = stringToSearchLength/numThreads;
	int finalStringLength = stringToSearchLength - ((numThreads - 1) * threadStringLength);
	int i = 0;
	pthread_t * threadArray = (pthread_t *)malloc(sizeof(pthread_t) * numThreads);
	for(i = 0; i<numThreads; i++)
	{
		//For the final thread assign whatever is remaining. 
		if(i == numThreads -1)
		{
			//Splitting the string based on index
			stringArray[numThreads - 1] = (char*)malloc(sizeof(char*)*(threadStringLength + patternLength + 1));
			strncpy(stringArray[numThreads - 1], tempPointer, finalStringLength);
			//Creating the thread and passing the appropriate substring to the thread. 
			pthread_create(&(threadArray[i]), NULL, findPattern, (void*) stringArray[i]);
		}
		else
		{
			//Splitting the string based on index
			stringArray[i] = (char*)malloc(sizeof(char*)*(threadStringLength + patternLength + 1));
			strncpy(stringArray[i], tempPointer, threadStringLength + patternLength);
			tempPointer = tempPointer + threadStringLength + 1;
			//Creating the thread and passing the appropriate substring to the thread. 
			pthread_create(&(threadArray[i]), NULL, findPattern, (void*) stringArray[i]);
		}
	}

	//The string has been split into various blocks and the main string is not needed. Hence the memory is freed. 
	free(stringToSearch);
	//Join all the threads and get the return value so as to find final count. 
	int* countInBlockArray = (int*)malloc(sizeof(int) * numThreads);
	int sum = 0;
	for(i = 0; i<numThreads; i++)
	{
		pthread_join(threadArray[i], (void**)&(countInBlockArray[i]));
		sum = sum + countInBlockArray[i];
	}
	//The string array is not needed and hence the memory is freed. 
	free(stringArray);
	//The count from every thread is not needed anymore and hence the memory is freed. 
	free(countInBlockArray);
	printf("\n%d", sum);
}