#include <conf.h>
#include <kernel.h>
#include <proc.h>
#include <q.h>
#include <stdio.h>

double power(double x, int y)
{
	int i = 1;
	int exp = 0;
	double prod = 1.0;
	if(y<0)
	{
		exp = y * (-1);
	}
	else
		exp = y;
	for(i = 1; i<=exp; i++)
	{
		prod = prod*x;
	}
	if(y<0)
		return 1/prod;
	else
		return prod;
}

double log(double x)
{
	int i = 1;
	double sumEven = 0.0;
	double sumOdd = 0.0;
	for(i = 1; i<=21; i+=2)
	{
		sumOdd = sumOdd + power(((x-1)/(x+1)),i)/i;
	}
	return sumOdd *2 ;
}

int expdev()
{
	double lambda = 0.1;
	double dummy;
	double returnValue = 0.00;
	int i = 0;
	do
	{
		dummy= (double) rand() / 32767.00; 
		returnValue = -log(dummy) / lambda;
	}while(dummy == 0.0);
    return (int)returnValue;
}
