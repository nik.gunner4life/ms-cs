/* resched.c  -  resched */

#include <conf.h>
#include <kernel.h>
#include <proc.h>
#include <q.h>
#include <stdio.h>

unsigned long currSP;	/* REAL sp of current process */
extern int ctxsw(int, int, int, int);
/*-----------------------------------------------------------------------
 * resched  --  reschedule processor to highest priority ready process
 *
 * Notes:	Upon entry, currpid gives current process id.
 *		Proctab[currpid].pstate gives correct NEXT state for
 *			current process if other than PRREADY.
 *------------------------------------------------------------------------
 */
int resched()
{
	int class_sched = getschedclass();

	//Exponential Distribution Scheduler
	if(class_sched == EXPDISTSCHED)
	{
		register struct	pentry	*optr;	/* pointer to old process entry */
		register struct	pentry	*nptr;	/* pointer to new process entry */

		struct	pentry	*proc;	/* pointer to old process entry */
		optr = &proctab[currpid];
		if (optr->pstate == PRCURR) 
		{
			optr->pstate = PRREADY;
			insert(currpid,rdyhead,optr->pprio);
		}
		
		int qpid = q[rdytail].qprev;
		int expPrio = expdev();
		/*if(expPrio == 0)
		{
			expPrio = expdev();
		}*/
		int minPrio = 0;
		int minPid = 0;
		if(expPrio>q[q[rdytail].qprev].qkey)
		{
			minPrio = q[q[rdytail].qprev].qkey;
			minPid = q[rdytail].qprev;
		}
		else
		{
			while(qpid < NPROC)
			{
			
				if(q[qpid].qkey>expPrio && q[qpid].qkey != minPrio)
				{
					minPrio = q[qpid].qkey;
					minPid = qpid;
				}
				qpid = q[qpid].qprev;
			}
		}
	
		/* force context switch */
		if(q[q[rdytail].qprev].qkey != NULLPROC)
		{
			currpid = minPid;
			nptr = &proctab[currpid];
			int dequeueID = dequeue(minPid);
			nptr->pstate = PRCURR;		/* mark it currently running	*/
		
			#ifdef	RTCLOCK
				preempt = QUANTUM;		/* reset preemption counter	*/
			#endif
			ctxsw((int)&optr->pesp, (int)optr->pirmask, (int)&nptr->pesp, (int)nptr->pirmask);
			/* The OLD process returns here when resumed. */
		}
		else
		{
			currpid = NULLPROC;
			nptr = &proctab[currpid];
			int dequeueID = dequeue(NULLPROC);
			nptr->pstate = PRCURR;		/* mark it currently running	*/
		
			#ifdef	RTCLOCK
				preempt = QUANTUM;		/* reset preemption counter	*/
			#endif
			ctxsw((int)&optr->pesp, (int)optr->pirmask, (int)&nptr->pesp, (int)nptr->pirmask);
			/* The OLD process returns here when resumed. */
		}
		return OK;
	}
	
	//Linux Based Scheduler
	else if(class_sched == LINUXSCHED)
	{
		register struct	pentry	*optr;	/* pointer to old process entry */
		register struct	pentry	*nptr;	/* pointer to new process entry */
		struct	pentry	*proc;	/* pointer to old process entry */
		int maxGoodness = 0; //Highest Goodness Value
		int maxGoodnessPID = NPROC;  //PID of the process with the highest Goodness value
		int goodness = 0;
		int qpid = q[rdytail].qprev;	//QPID is the traversal ID

		optr = &proctab[currpid];
		optr->pcounter = preempt;		//Store the current preempt value in the pcounter. 
		

		/*Add the current process to the ready Q*/
		if (optr->pstate == PRCURR) 
		{
			optr->pstate = PRREADY;
			insert(currpid,rdyhead,optr->pprio);
		}

		/*Find process with the highest goodness value*/
		while(qpid < NPROC)
		{
			proc = &proctab[qpid];
			//kprintf("%s", proc->pname);
			if(proc->pcounter!=0)
			{
				goodness = proc->pcounter + proc->pprio;
				if(goodness>maxGoodness)
				{
					maxGoodness = goodness;
					maxGoodnessPID = qpid;
				}
			}
			qpid = q[qpid].qprev;
		}
		
		//Current Epoch Still Running
		if(maxGoodness!=0)
		{
			currpid = maxGoodnessPID;
			nptr = &proctab[currpid];
			int dequeueID = dequeue(maxGoodnessPID);
			nptr->pstate = PRCURR;		/* mark it currently running	*/
			preempt = nptr->pcounter;		/* reset preemption counter	*/
			ctxsw((int)&optr->pesp, (int)optr->pirmask, (int)&nptr->pesp, (int)nptr->pirmask);
			return OK;
		}

		//All Processes are completed and Only NULLPROC Left. 
		else if(maxGoodness == 0 && q[rdytail].qprev == NULLPROC)
		{
			currpid = NULLPROC;
			nptr = &proctab[currpid];
			int dequeueID = dequeue(NULLPROC);
			nptr->pstate = PRCURR;		/* mark it currently running	*/
		
			#ifdef	RTCLOCK
				preempt = QUANTUM;		/* reset preemption counter	*/
			#endif
			ctxsw((int)&optr->pesp, (int)optr->pirmask, (int)&nptr->pesp, (int)nptr->pirmask);
			return OK;
		}

		//maxGoodness is 0 and there are processes in the Q other than the NULLPROC. Start a new epoch
		else
		{
			goodness = 0;
			maxGoodnessPID = 0;
			int qpid = q[rdytail].qprev;
			int counter = 0;
			while(qpid < NPROC)
			{
				proc = &proctab[qpid];
				counter = (int)((proc->pcounter)/2);
				proc->pcounter = counter + proc->pprio;
				if(proc->pcounter!=0)
				{
					goodness = proc->pcounter + proc->pprio;
					if(goodness>maxGoodness)
					{
						maxGoodness = goodness;
						maxGoodnessPID = qpid;
					}
				}	
				qpid = q[qpid].qprev;
			}
			currpid = maxGoodnessPID;
			nptr = &proctab[currpid];
			int dequeueID = dequeue(maxGoodnessPID);
			nptr->pstate = PRCURR;		/* mark it currently running	*/
			preempt = nptr->pcounter;		/* reset preemption counter	*/
			ctxsw((int)&optr->pesp, (int)optr->pirmask, (int)&nptr->pesp, (int)nptr->pirmask);
			return OK;
		}
		return OK;
	}

	//FIFO Scheduler
	else
	{
		return OK;
	}
	return OK;
}
