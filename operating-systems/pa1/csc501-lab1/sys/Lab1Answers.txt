Nikhil Srivatsan Srinivasan
nsrivat
PA1
02/04/2013

Additional Questions
--------------------
1) Analyze and explain the results of Test 1.

Solution:
---------
Process A: 1130980
Process B: 431611
Process C: 260076

Overall: 1822667

Process A Ratio: 0.621
Process B Ratio: 0.236
Process C Ratio: 0.143

Now above are the results of Test1. We know according to the exponential distribution function that has been created
we have lambda of 0.1 which yeilds us an average of close to 10. And hence Process A is executed more number of times
when compared to Process B and Process C. As the we along the curve we find that for values 20 which is the next priority
higher than the average is executed most number of times after process A. And similarly also for process C which is furthest
from the average. The ratio that we observe reflects the exponential nature of the scheduler. We can see that Process A which
is closest to the average is executed more number of times when compared to Process B which in turn is executed more number of
times when compared to C. 
------------------------------------------------------------------------------------------------------------------------------

2) What are the advantages and disadvantages of each of the two scheduling policies? 
Also, give the advantages and disadvantages of the round robin scheduling policy originally implemented in Xinu.

Solution:
---------
Exponential Distribution Scheduling Policy. 

Advantages:
1) One major advantage of this distribution policy is that it ensures we do not have too much starvation. All processes are
executed based on their priority and we get a good distribution of the process execution.

Disadvantages:
1) One major disadvantage of this is however that as we move along the curve processes which are very far away from the mean
(based on priority) will hardly get executed till the higher priority processes have been completed. Further this policy, even
though is proportional to the priority, does not give enough attention to processes much further away from the mean.This case
is handled well in the Linux Scheduler

Linux Scheduling Policy

Advantages:
1) One major advantage of this scheduler is that it get rids of starvation allowing processes with lesser priority to execute
atleast once during every epoch. Hence we know that every process will get its turn and the amount of time given to every
process proportional to the priority
2) Another advantage is that processes that have been suspended do not lose the time that was alloted for them in the previous
epoch. Rather this is carried over the successive epochs and this is a great feature. 

Disadvantages:
1) one major disadvantage is however that during the start of a new epoch, every process in the Ready Q and the suspended Q has
to be checked. If the number of processes becomes very large, then this results in a long time before the correct process
is chosen and scheduled. 

The advantage both the process have over the FIFO scheduler implemented in XINU is that both these policies address starvation.
However the XINU scheduler does not do that. THis is because it always scheduled the process which had the highest priority
and hence the processes with smaller priorities were starved. 

