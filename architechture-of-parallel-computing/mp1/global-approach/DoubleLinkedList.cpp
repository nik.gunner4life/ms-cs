// Double Linked List.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
/***************************************************************
 *  a simple doubly linked list implementation
 *     
 ****************************************************************/
#include<stdio.h>
#include<stdlib.h>
#include<assert.h>
#include<omp.h>

#include "dlist.h"

// #include<omp.h>



pIntListNode IntListNode_Create(int x)
{
	pIntListNode newNode = (pIntListNode) malloc(sizeof(IntListNode));
	newNode->data = x;
	newNode->next = NULL;
	newNode->prev = NULL;
	return newNode;
}



void IntList_Init(pIntList list) 
{
	list->head = NULL;
}


//
// IntList_Insert inserts a new node into the list. The new node is taken
// from an array of pre-allocated nodes, with data assigned as x. The
// reason for pre-allocating nodes is because the memory allocation function
// (malloc) is not thread safe, so it can't be called by parallel threads. 
//
void IntList_InsertP(pIntList pList, int x, pArrNode an) 
{
	pIntListNode prev, p , prev1, next1, 
	newNode = ArrNode_getNode(an, x);
	assert(newNode!=NULL);
	p = pList->head;
	prev = NULL;
	prev1 = NULL;
	while (p != NULL && p->data < newNode->data) 
	{
		if(pList->head == NULL)
		{
			break;
		}
		prev = p;
		prev1=p->prev;
		p = p->next;
	}
#pragma omp critical 
	{
		//Ensuring the perspective is still the same
		if(pList->head != NULL)
		{
			if(prev->prev==prev1 && prev->next==p && prev->prev->deleted!=1 && prev->next->deleted != 1)
			{
			}
		}
		else if (pList->head == NULL)
		{
		}
		else
		{
			p = pList->head;
			prev = NULL;
			while (p != NULL && p->data < newNode->data) 
			{
				if(pList->head == NULL)
				{
					break;
				}
				prev = p;
				p = p->next;
			}
		}
		if (pList->head == NULL) 
			{ /* list is empty, insert the first element */
				pList->head = newNode;
			}
		else 
		{ /* list is not empty, find the right place to insert element */
			if (p == NULL)
			{ /* insert as the last element */
				prev->next = newNode;
				newNode->prev = prev;
			}
			else if (prev == NULL)
			{ /* insert as the first element */
				pList->head = newNode;
				newNode->next = p;
				p->prev = newNode;
			}
			else 
			{ /* insert right between prev and p */
				prev->next = newNode;
				newNode->prev = prev;
				newNode->next = p;
				p->prev = newNode;
			}
		}
	} // end of pragma omp critical
}
void IntList_Insert(pIntList pList, int x, pArrNode an) 
{
	pIntListNode prev, p , 
	newNode = ArrNode_getNode(an, x);
	// assert(newNode!=NULL);
	
	// this #pragma omp critical sequentializes all Inserts and Deletes. It 
	// should be removed and replaced by your own solution. 
#pragma omp critical 
	{
		if (pList->head == NULL) { /* list is empty, insert the first element */
			pList->head = newNode;
		}
		else { /* list is not empty, find the right place to insert element */
			p = pList->head;
			prev = NULL;
			while (p != NULL && p->data < newNode->data) {
				prev = p;
				p = p->next;
			}
			
			if (p == NULL) { /* insert as the last element */
				prev->next = newNode;
				newNode->prev = prev;
			}
			else if (prev == NULL) { /* insert as the first element */
				pList->head = newNode;
				newNode->next = p;
				p->prev = newNode;
			}
			else { /* insert right between prev and p */
				prev->next = newNode;
				newNode->prev = prev;
				newNode->next = p;
				p->prev = newNode;
			}
		}
		
	} // end of pragma omp critical
}

//Deletion Parrallelized
void IntList_DeleteP(pIntList pList, int x) 
{
	pIntListNode prev, p, prev1, next1;
	p=pList->head;
	prev1=NULL;
	next1=pList->head;
	while(p!=NULL && p->data!=x)
	{
		if(pList->head == NULL)
		{
			break;
		}
		p=p->next;
		/*prev1 = p;
		p=p->next;
		next1 = p->next; */
	}
#pragma omp critical 
	{
		//Ensuring Perspective is still the same
		pArrNode an1 = (pArrNode) malloc(sizeof(ArrNode));
		while(p!=NULL && p->data!=x)
		{
		if(pList->head == NULL)
		{
			break;
		}
		prev1 = p;
		p=p->next;
		next1 = p->next; 
		}
		if (pList->head == NULL) { /* list is empty, do nothing */
		}
		else { /* list is not empty, find the desired element */
			p = pList->head;
			/*while (p != NULL && p->data != x) 
				p = p->next;
		*/	
			if (p == NULL) { /* element not found, do nothing */
			}
			else {
				if (p->prev == NULL) { /* delete the head element */
					pList->head = p->next;
					if (p->next != NULL)
						p->next->prev = NULL;
					p->deleted = 1;
				}
				else { /* delete non-head element */
					p->prev->next = p->next;
					if (p->next != NULL) {
						p->next->prev = p->prev;
					}
					p->deleted=1;
				}
			}
		}
	} // end of pragma omp critical
}
/* delete the first element that has a data value equal to x */
void IntList_Delete(pIntList pList, int x) 
{
	pIntListNode prev, p;
	
	// this #pragma omp critical sequentializes all Inserts and Deletes. It 
	// should be removed and replaced by your own solution. 
#pragma omp critical 
	{
		if (pList->head == NULL) { /* list is empty, do nothing */
		}
		else { /* list is not empty, find the desired element */
			p = pList->head;
			while (p != NULL && p->data != x) 
				p = p->next;
			
			if (p == NULL) { /* element not found, do nothing */
			}
			else {
				if (p->prev == NULL) { /* delete the head element */
					pList->head = p->next;
					if (p->next != NULL)
						p->next->prev = NULL;
					free(p);
				}
				else { /* delete non-head element */
					p->prev->next = p->next;
					if (p->next != NULL) {
						p->next->prev = p->prev;
					}
					free(p);
				}
			}
		}
	} // end of pragma omp critical
}


void IntList_Print(pIntList list) 
{
	pIntListNode p = list->head;
	int i=0;
	
	while (p != NULL) {
		printf("list element %d: %d\n", i, p->data);
		p = p->next;
		i++;
	}
}

void ArrNode_Init(pArrNode an) 
{
	int i;
	
	for (i=0; i<NUM_ELEMENT; i++) {
		int x = (int) 10000 * ((double)rand() / ((double)(RAND_MAX)+(double)(1)) ) ;    
		an->nodes[i] = (pIntListNode) IntListNode_Create(x);
	}
	an->curPtr = 0;
}

void ArrNode_Print(pArrNode an)
{
	int i;
	
	for (i=0; i<NUM_ELEMENT; i++) 
		printf("ArrNode_Print: element %d points to %d\n", i, an->nodes[i]);
}

pIntListNode ArrNode_getNode(pArrNode an, int x)
{
	int oldIdx = an->curPtr;
	assert(an->curPtr < NUM_ELEMENT);
	an->nodes[oldIdx]->data = x;
	an->nodes[oldIdx]->deleted = 0;
	an->curPtr++;
	return an->nodes[oldIdx];
}


int main() 
{
	int i=0;
	pIntList pList = (pIntList) malloc(sizeof(IntList));
	pArrNode an1 = (pArrNode) malloc(sizeof(ArrNode));
	pArrNode an2 = (pArrNode) malloc(sizeof(ArrNode));
	IntList_Init(pList);
	srand(SEED);
	int size=NUM_ELEMENT;
	ArrNode_Init(an1);
	ArrNode_Init(an2);
double time1 = omp_get_wtime();
#pragma omp parallel sections default(shared) private(i) num_threads(2)
	{
#pragma omp section
		{
			for (i=0; i<size; i++){
				IntList_InsertP(pList, i, an1);
			}
		}
#pragma omp section
		{
			for (i=0; i<size; i++)
			{
				IntList_InsertP(pList, size-i, an2);
			}
		}
#pragma omp section
		{
			for (i=0; i<size; i++)
			{
				IntList_DeleteP(pList, 9*i % size);
			}
		}
#pragma omp section
		{
			for (i=0; i<size; i++)
			{
				IntList_DeleteP(pList, 7*i % size);
			}
		}

//#pragma omp section
//		{
//			for (i=0; i<NUM_ELEMENT; i++)
//				IntList_Delete(pList, 7919 * i % NUM_ELEMENT);
//		}
}
	double time2 = omp_get_wtime();
	IntList_Print(pList);
	printf("Time is %f\n", time2-time1);
	
}


