// FineGrainDLL.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"


/***************************************************************
 *  a simple doubly linked list implementation
 *     
 ****************************************************************/
#include "stdafx.h"
#include<stdio.h>
#include<stdlib.h>
#include<assert.h>
#include<omp.h>

#include "dlist.h"

// #include<omp.h>



pIntListNode IntListNode_Create(int x)
{
	pIntListNode newNode = (pIntListNode) malloc(sizeof(IntListNode));
	newNode->data = x;
	newNode->deleted = 0;
	newNode->lock = 0;
	newNode->next = NULL;
	newNode->prev = NULL;
	return newNode;
}



void IntList_Init(pIntList list) 
{
	list->head = NULL;
}


//
// IntList_Insert inserts a new node into the list. The new node is taken
// from an array of pre-allocated nodes, with data assigned as x. The
// reason for pre-allocating nodes is because the memory allocation function
// (malloc) is not thread safe, so it can't be called by parallel threads. 
//

//Insert in between parallelized
void IntList_InsertMiddle(pIntList pList, int x, pArrNode an) 
{
	//First Traverse

	pIntListNode prev, p ,prev1, 
	newNode = ArrNode_getNode(an, x);
	prev1 = NULL;
	p=pList->head;
	prev=NULL;

	while(p!=NULL && p->data<newNode->data)
	{
		while(p!= NULL && p->lock == 1)
		{
			if(p->lock == 0)
			{
				break;
			}
		}
		prev=p;
		while(p->next!=NULL && p->next->lock == 1)
		{
			if(p->next->lock == 0)
			{
				break;
			}
		}
		p=p->next;
	}
	//Traversal Completed
	
	//For Beginning
	//Two cases: 1) when list is empty .... 2) when the element getting inserted is the smallest
	//Empty List
	if(pList->head == NULL)
	{
		pList->head = newNode;
		p=newNode;
	}
	else if(prev==NULL)
	{
		p=pList->head;
		while(p->lock == 1)
		{
			if(p->lock == 0)
			{
				break;
			}
		}
		#pragma omp critical 
		{
			p->lock = 1; //acquire lock on first element
		}

		int succeed = 0;
		do
		{
			if(p->deleted != 1)
			{
				newNode->prev = p->prev;
				newNode->next = p;
				p->prev=newNode;
				pList->head=newNode;
				p->lock = 0;
				succeed = 1;
			}
			else
			{
				p->lock = 0;

				//Retraverse the list
				p=pList->head;
				prev=NULL;

				while(p!=NULL && p->data<newNode->data)
				{
					while(p!= NULL && p->lock == 1)
					{
						if(p->lock == 0)
						{
							break;
						}
					}
					prev=p;
					while(p->next!=NULL && p->next->lock == 1)
					{
						if(p->next->lock == 0)
						{
							break;
						}
					}
					p=p->next;
				}
				succeed = 0;
				}

		}while(succeed == 0);
	}




	//Acquire Locks And Insert
	//For Middle
	if(prev!=NULL && p!=NULL)
	{
		while(prev!=NULL && prev->lock == 1)
		{
			if(prev->lock == 0)
			{
				break;
			}
		}
		#pragma omp critical
		{
			prev->lock = 1;
			//printf("....Locking %d\n", prev->data);
		}
		while(p!=NULL && p->lock == 1)
		{
			if(p->lock == 0)
			{
				break;
			}
		}
		#pragma omp critical
		{
			p->lock = 1;
			//printf("..Locking %d\n", p->data);
		}
		int succeed = 0;
	do
	{
		if(prev->next == p && prev->deleted != 1 && p->deleted != 1)
		{
			prev->next = newNode;
			newNode->prev = prev;
			newNode->next = p;
			p->prev = newNode;
			//printf("%d Inserted\n", newNode->data);
			//printf("..Unlocking %d\n", prev->data);
			prev->lock = 0;
			//printf("....Unlocking %d\n", p->data);
			p->lock = 0;
			succeed = 1;
		}
		else
		{
			//printf("UNLOCKING. Failed");
			prev->lock = 0;
			//printf("UNLOCKING. FAILED");
			p->lock = 0;
			p=pList->head;
			prev=NULL;
			while(p!=NULL && p->data<newNode->data)
			{
				while(p!= NULL && p->lock == 1)
				{
					if(p->lock == 0)
					{
						break;
					}
				}
				prev=p;
				while(p->next!=NULL && p->next->lock == 1)
				{
					if(p->next->lock == 0)
					{
						break;
					}
				}
				p=p->next;
			}
			if(prev!=NULL && p!=NULL)
			{
				while(prev!=NULL && prev->lock == 1)
				{
					if(prev->lock == 0)
					{
						break;
					}
				}
				#pragma omp critical
				{
					prev->lock = 1;
					//printf("....Locking %d\n", prev->data);
				}
				while(p!=NULL && p->lock == 1)
				{
					if(p->lock == 0)
					{
						break;
					}
				}
				#pragma omp critical
				{
					p->lock = 1;
					//printf("..Locking %d\n", p->data);
				}
			}
			succeed = 0;
		}
	}while(succeed == 0);
	}

	//Insertion and Checking Perspective
	

	//For End
	if(p==NULL && prev->next == NULL)
	{
		while(prev!=NULL && prev->lock == 1)
		{
			if(prev->lock == 0)
			{
				break;
			}
		}
		#pragma omp critical
		{
			prev->lock = 1;
			//printf("Locking %d\n", prev->data);
		}
		int succeed = 0;
		do
		{
			if(prev!=NULL && p == NULL && prev->deleted != 1)
			{
				prev->next = newNode;
				newNode->prev = prev;
				//printf("Inserted %d\n", newNode->data);
				//printf("Unlocking %d\n", prev->data);
				prev->lock = 0;
				succeed=1;
			}
			else
			{
				//printf("Failed, hence unlocking\n");
				prev->lock = 0;
				p=pList->head;
				prev=NULL;
				while(p!=NULL && p->data<newNode->data)
				{
					while(p!= NULL && p->lock == 1)
					{
						if(p->lock == 0)
						{
							break;
						}
					}
					prev=p;
					while(p->next!=NULL && p->next->lock == 1)
					{
						if(p->next->lock == 0)
						{
							break;
						}
					}
					p=p->next;
				}


				if(p==NULL && prev->next == NULL)
				{
					while(prev!=NULL && prev->lock == 1)
					{
						if(prev->lock == 0)
						{
							break;
						}
					}
					#pragma omp critical
					{
						prev->lock = 1;
						//printf("Locking %d\n", prev->data);
					}
				}
				succeed = 0;
			}
		}while(succeed == 0);
	}
	//End of For End
}



//Insertion Parallelized

void IntList_InsertPFG(pIntList pList, int x, pArrNode an) 
{
	pIntListNode prev, p ,prev1, 
	newNode = ArrNode_getNode(an, x);
	assert(newNode!=NULL);
	p = pList->head;
	prev = NULL;
	prev1 = NULL;

	//Traversing by ensuring that we do not violate any write locks
	while(p!=NULL && p->data<newNode->data)
	{
		if(pList->head == NULL)
		{
			break;
		}
		else
		{
			while(p!=NULL && p->lock == 1)
			{
				if(p->lock == 0)
				{
					break;
				}
			}
			prev=p;
			while(p->next!=NULL && p->next->lock == 1)
			{
				if(p->next->lock==0)
				{
					break;
				}
			}
			p=p->next;
		}
	}
	//Traversal Completed

	//Acquiring Locks for prev and p
	//if(pList->head == NULL)
	//{
	//	//Do Nothing
	//}
	//else if(prev!= NULL && prev->prev==NULL && prev->next!=NULL)
	//{
	//	/*while(prev->lock == 1)
	//	{
	//		if(prev->lock == 0)
	//		{
	//			break;
	//		}
	//	}
	//	#pragma omp critical
	//	{
	//		prev->lock = 1;
	//	}*/
	//}
	if(prev!=NULL && p!=NULL)
	{
		while(prev!=NULL && prev->lock == 1)
		{
			if(prev->lock == 0)
			{
				break;
			}
		}
		#pragma omp critical
		{
			printf("Locking %d\n", prev->data);
			prev->lock = 1;
		}
		while(p!=NULL && p->lock == 1)
		{
			if(p->lock == 0)
			{
				break;
			}
		}
		#pragma omp critical
		{
			printf("Locking %d\n", p->data);
			p->lock = 1;
		}
	}
	/*else if(prev != NULL && p == NULL)
	{
		while(prev->lock == 1)
		{
			if(prev->lock == 0)
			{
				break;
			}
		}
		#pragma omp critical
		{
			printf("Locking %d\n", prev->data);
			prev->lock = 1;
		}
	}
*/
	//To ensure perspective is still correct

	int succeed = 0;
	do
	{
		if(prev == NULL || pList->head == NULL) // Insertion at the beginning 
		{
			succeed = 1;
		}
		else if(prev->next == p && p!=NULL && p->deleted!=1 && prev->deleted != 1) //Insertion in the Middle
		{
			succeed = 1;
		}
		else if(prev->next == NULL && prev!=NULL) //Insertion At the end
		{
			succeed = 1;
		}
		else
		{
			succeed = 0;
			p = pList->head;
			prev=NULL;
			while(p!=NULL && p->data<newNode->data)
			{
			if(pList->head == NULL)
			{
				break;
			}
			else
			{
				while(p!=NULL && p->lock == 1)
				{
					if(p->lock == 0)
					{
						break;
					}
				}
				prev=p;
				while(p->next!=NULL && p->next->lock == 1)
				{
					if(p->next->lock==0)
					{
						break;
					}
				}
				if(p->next !=NULL)
				{
					p=p->next;
				}
			}
		}
	}
	}while(succeed == 0);

	//Insertion
	//if (pList->head == NULL) { /* list is empty, insert the first element */
	//	pList->head = newNode;
	//	printf("%d Inserted", newNode->data);
	//}
	/*else
	{*/
		//if (p == NULL) { /* insert as the last element */
		//	prev->next = newNode;
		//	newNode->prev = prev;
		//	prev->lock = 0;
		//	printf("%d Inserted", newNode->data);
		//	printf("Unlocking %d\n", prev->data);
		//}
		//else if (prev == NULL) { /* insert as the first element */
		//	pList->head = newNode;
		//	newNode->next = p;
		//	p->prev = newNode;
		//	printf("%d Inserted", newNode->data);
		//}
		{ /* insert right between prev and p */
			prev->next = newNode;
			newNode->prev = prev;
			newNode->next = p;
			p->prev = newNode;
			printf("%d Inserted", newNode->data);
			printf("Unlocking %d\n", prev->data);
			prev->lock = 0;
			printf("Unlocking %d\n", p->data);
			p->lock =0;
		}
	}


//End of Insertion Parallelized
void IntList_Insert(pIntList pList, int x, pArrNode an) 
{
	pIntListNode prev, p , 
	newNode = ArrNode_getNode(an, x);
	// assert(newNode!=NULL);
	
	// this #pragma omp critical sequentializes all Inserts and Deletes. It 
	// should be removed and replaced by your own solution. 
#pragma omp critical 
	{
		if (pList->head == NULL) { /* list is empty, insert the first element */
			pList->head = newNode;
		}
		else { /* list is not empty, find the right place to insert element */
			p = pList->head;
			prev = NULL;
			while (p != NULL && p->data < newNode->data) {
				prev = p;
				p = p->next;
			}
			
			if (p == NULL) { /* insert as the last element */
				prev->next = newNode;
				newNode->prev = prev;
			}
			else if (prev == NULL) { /* insert as the first element */
				pList->head = newNode;
				newNode->next = p;
				p->prev = newNode;
			}
			else { /* insert right between prev and p */
				prev->next = newNode;
				newNode->prev = prev;
				newNode->next = p;
				p->prev = newNode;
			}
		}
		
	} // end of pragma omp critical
}



//Deletion Parallelized
void IntList_DeleteP(pIntList pList, int x) 
{
	pIntListNode prev, p, next;
	next = NULL;
	p=pList->head;
	prev=NULL;
	//Traversal
	while(p!=NULL && p->data != x)
	{
		while(p->lock == 1)
		{
			if(p->lock == 0)
			{
				break;
			}
		}
		prev=p;
		while(p->next!=NULL && p->next->lock == 1)
		{
			if(p->next->lock == 0)
			{
				break;
			}
		}
		p=p->next;
	}
	//End of Traversal

	//Delete Head Element
	if(pList->head == NULL)
	{
		//Do Nothing. List is empty.
	}

	else if(p!= NULL && p == pList->head)
	{
		while(p->lock == 1)
		{
			if(p->lock == 0)
			{
				break;
			}
		}
		#pragma omp critical
		{
			p->lock = 1;
			//printf("Locking %d\n", p->data);
		}
		while(p->next != NULL && p->next->lock == 1)
		{
			if(p->next->lock == 0)
			{
				break;
			}
		}
		#pragma omp critical
		{
			if(p->next != NULL)
			{
				p->next->lock = 1;
				//printf("Locking %d\n", p->next->data);
			}
			
		}	

		//Deletion and Verification
		int success = 0;
		do
		{
			if(p->prev == NULL)
			{
				pList->head = p->next;
				if (p->next != NULL)
					p->next->prev = NULL;
				//printf("Deleting %d\n", p->data);
				p->deleted = 1; 
				//printf("Unlocking %d\n", p->data);
				p->lock = 0;
				if(pList->head != NULL)
				{
					//printf("Unlocking %d\n", pList->head->data);
					pList->head->lock = 0;
				}
				success = 1;
			}
			else
			{
				//printf("UNLOCKING BOTH\n");
				p->lock = 0;
				if(p->next != NULL)
					p->next->lock = 0;

				//Retraverse the List
				p=pList->head;
				prev=NULL;
				//Traversal
				while(p!=NULL && p->data != x)
				{
					while(p->lock == 1)
					{
						if(p->lock == 0)
						{	
							break;
						}
					}
					prev=p;
					while(p->next!=NULL && p->next->lock == 1)
					{
						if(p->next->lock == 0)
						{
							break;
						}
					}
					p=p->next;
				}
				if(p == pList->head)
				{
					while(p->lock == 1)
					{
						if(p->lock == 0)
						{
							break;
						}
					}
					#pragma omp critical
					{
						p->lock = 1;
						//printf("Locking %d\n", p->data);
					}
					while(p->next->lock == 1)
					{
						if(p->next->lock == 0)
						{
							break;
						}
					}
					#pragma omp critical
					{
						if(p->next != NULL)
						{
							p->next->lock = 1;
							//printf("Locking %d\n", p->next->data);
						}
					}
					success = 0;
				}
			}
		}while(success == 0);
	}

	//Delete Non Head

	if(pList->head != NULL && p!= NULL && p->prev!=NULL)
	{
		while(p->prev!=NULL && p->prev->lock == 1)
		{
			if(p->prev->lock == 0)
			{
				break;
			}
		}
		#pragma omp critical
		{
			if(p->prev!=NULL)
			{
				p->prev->lock = 1;
				//printf("Locking %d\n", p->prev->data);
			}
		}
		while(p!=NULL && p->lock == 1)
		{
			if(p->lock == 0)
			{
				break;
			}
		}
		#pragma omp critical
		{
			if(p!=NULL)
			{
				p->lock = 1;
				//printf("Locking %d\n", p->data);
			}
		}
		while(p->next!=NULL && p->next->lock == 1)
		{
			if(p->next->lock == 0)
			{
				break;
			}
		}
		#pragma omp critical
		{
			if(p->next != NULL)
			{
				p->next->lock = 1;
				//printf("Locking %d\n", p->next->data);
			}
			else
			{
				//Do Nothing
			}
			
		}
		int success = 0;
		do
		{
			prev = p->prev;
			next = p->next;
			if(p!=NULL && p->prev!=NULL)
			{
				prev->next = next;
				if (next != NULL) {
					next->prev = prev;
				}
				p->deleted=1;
				//printf("Deleting %d\n", p->data);
				if(prev!=NULL)
				{
					prev->lock = 0;
					//printf("Unlocking %d\n", prev->data);
				}
				p->lock = 0;
				//printf("Unlocking %d\n", p->data);
				if(next!=NULL)
				{
					next->lock = 0;
					//printf("Unlocking %d\n", next->data);
				}
				success = 1;
			}
			else
			{
				if(prev!=NULL)
				{
					prev->lock = 0;
				}
				p->lock = 0;
				if(next!=NULL)
				{
					next->lock = 0;
				}
				//printf("UNLOCKING EVERYTHING ...\n");

				//Retraverse
				p=pList->head;
				prev=NULL;
				//Traversal
				while(p!=NULL && p->data != x)
				{
					while(p->lock == 1)
					{
						if(p->lock == 0)
						{
							break;
						}
					}
					prev=p;
					while(p->next!=NULL && p->next->lock == 1)
					{
						if(p->next->lock == 0)
						{
							break;
						}
					}
					p=p->next;
				}
				//Re acquiring the locks
				while(p->prev!=NULL && p->prev->lock == 1)
				{
					if(p->prev->lock == 0)
					{
						break;
					}
				}
				#pragma omp critical
				{
					if(p->prev!=NULL)
					{
						p->prev->lock = 1;
						//printf("Locking %d\n", p->prev->data);
					}
				}
				while(p!=NULL && p->lock == 1)
				{
					if(p->lock == 0)
					{
						break;
					}
				}
				#pragma omp critical
				{
					if(p!=NULL)
					{
						p->lock = 1;
						//printf("Locking %d\n", p->data);
					}
				}
				while(p->next!=NULL && p->next->lock == 1)
				{	
					if(p->next->lock == 0)
					{
						break;
					}
				}
				#pragma omp critical
				{
					if(p->next!=NULL)
					{
						p->next->lock = 1;
						//printf("Locking %d\n", p->next->data);
					}
					else
					{
						//Do Nothing
					}

				}
				success = 0;
			}
		}while(success == 0);

	}



//#pragma omp critical 
//	{
//		if (pList->head == NULL) { /* list is empty, do nothing */
//		}
//		else { /* list is not empty, find the desired element */
//			p = pList->head;
//			while (p != NULL && p->data != x) 
//				p = p->next;
//			
//			if (p == NULL) { /* element not found, do nothing */
//			}
//			else {
//				if (p->prev == NULL) { /* delete the head element */
//					pList->head = p->next;
//					if (p->next != NULL)
//						p->next->prev = NULL;
//					free(p);
//				}
//				else { /* delete non-head element */
//					p->prev->next = p->next;
//					if (p->next != NULL) {
//						p->next->prev = p->prev;
//					}
//					free(p);
//				}
//			}
//		}
//	} // end of pragma omp critical
}


/* delete the first element that has a data value equal to x */
void IntList_Delete(pIntList pList, int x) 
{
	pIntListNode prev, p;
	
	// this #pragma omp critical sequentializes all Inserts and Deletes. It 
	// should be removed and replaced by your own solution. 
#pragma omp critical 
	{
		if (pList->head == NULL) { /* list is empty, do nothing */
		}
		else { /* list is not empty, find the desired element */
			p = pList->head;
			while (p != NULL && p->data != x) 
				p = p->next;
			
			if (p == NULL) { /* element not found, do nothing */
			}
			else {
				if (p->prev == NULL) { /* delete the head element */
					pList->head = p->next;
					if (p->next != NULL)
						p->next->prev = NULL;
					free(p);
				}
				else { /* delete non-head element */
					p->prev->next = p->next;
					if (p->next != NULL) {
						p->next->prev = p->prev;
					}
					free(p);
				}
			}
		}
	} // end of pragma omp critical
}


void IntList_Print(pIntList list) 
{
	pIntListNode p = list->head;
	int i=0;
	
	while (p != NULL) {
		printf("list element %d: %d\n", i, p->data);
		p = p->next;
		i++;
	}
}




void ArrNode_Init(pArrNode an) 
{
	int i;
	
	for (i=0; i<NUM_ELEMENT; i++) {
		int x = (int) 10000 * ((double)rand() / ((double)(RAND_MAX)+(double)(1)) ) ;    
		an->nodes[i] = (pIntListNode) IntListNode_Create(x);
	}
	an->curPtr = 0;
}

void ArrNode_Print(pArrNode an)
{
	int i;
	
	for (i=0; i<NUM_ELEMENT; i++) 
		printf("ArrNode_Print: element %d points to %d\n", i, an->nodes[i]);
}

pIntListNode ArrNode_getNode(pArrNode an, int x)
{
	int oldIdx = an->curPtr;
	assert(an->curPtr < NUM_ELEMENT+4);
	an->nodes[oldIdx]->data = x;
	an->nodes[oldIdx]->deleted = 0;
	an->nodes[oldIdx]->lock = 0;
	an->curPtr++;
	return an->nodes[oldIdx];
}


int main() 
{
	int i;
	pIntList pList = (pIntList) malloc(sizeof(IntList));
	pArrNode an1 = (pArrNode) malloc(sizeof(ArrNode));
	pArrNode an2 = (pArrNode) malloc(sizeof(ArrNode));
	
	IntList_Init(pList);
	
	srand(SEED);
	ArrNode_Init(an1);
	ArrNode_Init(an2);

	int size=100;
	// ArrNode_Print(an);
	/*IntList_Insert(pList, 10, an1);*/
	double time_init = omp_get_wtime();
	#pragma omp parallel sections default(shared) private(i) num_threads(1)
	{
#pragma omp section
		{
			for (i=0; i<size; i++){
				IntList_InsertMiddle(pList, i, an1);
				//printf("Section 1111111111111111111111111111");
			}
		}
#pragma omp section
		{
			for (i=0; i<size; i++)
			{
				IntList_InsertMiddle(pList, size-i, an2);
				//printf("Section 22222222222222222222222222");
			}
		}
#pragma omp section
		{
			for (i=0; i<size; i++)
			{
				IntList_DeleteP(pList, 79*i % size);
			}
		}
#pragma omp section
		{
			for (i=0; i<size; i++)
			{
				IntList_DeleteP(pList,  7919* i % size);
			}
		}
}
	double time_final = omp_get_wtime();
	IntList_Print(pList);
	printf("\n%f",time_final - time_init);
	
}


