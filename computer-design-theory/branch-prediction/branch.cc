/***********************************************
Author:   Nikhil Srivatsan Srinivasan
Unity ID: nsrivat
Project:  Branch Prediction
Date:     11/18/2013
***********************************************/

#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <string>
#include <string.h>
#include <iostream>
#include <fstream>
#include <stdint.h>
#define nsrPrintf(...) if(nsrDebugPrintf) { fflush(stdout); printf(__VA_ARGS__); fflush(stdout);}
int nsrDebugPrintf;
using namespace std;

#define BIMODAL 1
#define GSHARE 2
#define HYBRID 3
#define INCREMENT 1
#define DECREMENT -1
#define GSHARE_SHIFT 1
#define ADDRESS_LENGTH 32
#define PC_SHIFT 2
#define OK 100
#define SYSERR -100
#define READSIZE 30

class PredictionBlock;
class PredictionTable;
char *fileName;
char *predictor;
long lineCount;

PredictionTable *bimodalTable, *gshareTable, *hybridTable;

class PredictionBlock
{
public:
	uint32_t  pcAddress;
	int predictionState;
	uint32_t  index;
public:
	PredictionBlock()
	{
		//Set Prediction State to Weakly Taken
		predictionState = 2;
		//Set PC Address to 0
		pcAddress = 0;
	}
	uint32_t  getAddress()
	{
		return pcAddress;
	}

	int setAddress(uint32_t  address)
	{
		pcAddress = address;
		return OK;
	}

	int getPredictionState()
	{
		return predictionState;
	}

	int setPredictionState(char operation)
	{
		if(operation == 't')
			predictionState++;
		else
			predictionState--;
		if(predictionState > 3)
			predictionState = 3;
		else if(predictionState < 0)
			predictionState = 0;
		else
		{
			//Do Nothing. 
		}
		return OK;
	}

	uint32_t  getIndex()
	{
		return index;
	}

	int setIndex(uint32_t  indexValue)
	{
		index = indexValue;
		return OK;
	}
};

class PredictionTable
{
public:
	int m;
	int n;
	int k;
	int setCount;
	long misPredictionCount;
	long correctPredictionCount;
	long interferenceCount;
	int predictionType;
	int pcShiftBits;
	int gshareShiftBits;
	uint32_t  gshareHistoryRegister;
	PredictionBlock *predictionTable;
public:
	PredictionTable(int m1, int n1, int k1, int type)
	{
		m = m1;
		n = n1;
		k = k1;
		predictionType = type;
		//Initialize the predictionTableArray
		if(predictionType != HYBRID)
		{
			setCount = (int)(pow(2.0, m));
			predictionTable = new PredictionBlock[setCount];
			int i = 0;
			for(i = 0; i < setCount; i++)
				predictionTable[i].setIndex(i);
		}
		else
		{
			setCount = (int)(pow(2.0, k));
			predictionTable = new PredictionBlock[setCount];
			int i = 0;
			for(i = 0; i < setCount; i++)
				predictionTable[i].setIndex(i);
		}
		misPredictionCount = 0;
		correctPredictionCount = 0;
		interferenceCount = 0;
		gshareHistoryRegister = 0;
	}

	uint32_t  getShiftedAddress(uint32_t  address)
	{
		address = address >> PC_SHIFT;
		return address;
	}

	uint32_t  getBimodalIndex(uint32_t  address)
	{
		uint32_t  index;
		index = address << (ADDRESS_LENGTH - m);
		index = index >> (ADDRESS_LENGTH - m);
		nsrPrintf("\nBimodal Index: %ld", index);
		return index;
	}

	uint32_t  getGshareIndex(uint32_t  address)
	{
		if(n < m)
		{
			uint32_t  index;
			index = address << (ADDRESS_LENGTH - m);
			index = index >> (ADDRESS_LENGTH - m);
			uint32_t  mostSignificantBits = index >> (m - n);
			mostSignificantBits = mostSignificantBits ^ gshareHistoryRegister;
			mostSignificantBits = mostSignificantBits << (m - n);
			nsrPrintf("\nGshare MSB is: %d", mostSignificantBits);
			index = index << (ADDRESS_LENGTH + n - m);
			index = index >> (ADDRESS_LENGTH + n - m);
			index = mostSignificantBits + index;
			return index;
		}
		else if(n == m)
		{
			uint32_t  index;
			index = address << (ADDRESS_LENGTH - m);
			index = index >> (ADDRESS_LENGTH - m);
			index = index ^ gshareHistoryRegister;
			nsrPrintf("\nN is equal to M. Gshare Index is: %d", index);
			return index;
		}
		else
			nsrPrintf("\nHoly Cow Batman!! How can this happen?");
	}

	int updateGShareRegister(char operation)
	{
		// The branch was taken. Therefore right shift it by 1 bit
		// and and the most significant bit 1.
		if(operation == 't')
		{
			gshareHistoryRegister = gshareHistoryRegister >> GSHARE_SHIFT;
			int valueToAdd = (int)(pow(2.0, n-1));
			gshareHistoryRegister = gshareHistoryRegister + valueToAdd;
		}
		// The branch was not taken. Right shift it by 1 bit and make
		// the MSB 0. The right shift takes care of this. No need to
		// make the MSB 0 explicitly. 
		else
			gshareHistoryRegister = gshareHistoryRegister >> GSHARE_SHIFT;
		return OK;
	}

	uint32_t  getHybridIndex(uint32_t  address)
	{
		uint32_t  index;
		index = address << (ADDRESS_LENGTH - k);
		index = index >> (ADDRESS_LENGTH - k);
		return index;
	}

	// BIMODAL PREDICTION
	int bimodalPrediction(uint32_t  address, char operation)
	{
		uint32_t  index = getBimodalIndex(getShiftedAddress(address));
		nsrPrintf("\nBimodal Index: %ld", index);
		//The branch was actually taken. Find inline comments. 
		if(operation == 't')
		{
			// I predicted the branch was not taken. Hence I need to increment
			// prediction state by 1. Increment the mis-prediction counter and
			// update the prediction state. 
			if(predictionTable[index].getPredictionState() < 2)
			{
				misPredictionCount++;
				predictionTable[index].setPredictionState(operation);
			}
			// My prediction and the actual path match. So just increment the
			// correct prediction Counter and set the prediction state. 
			else
			{
				correctPredictionCount++;
				predictionTable[index].setPredictionState(operation);
			}
		}
		else
		{
			// I predicted that the branch would not be taken and the actual
			// path matches that. So just increment the correct prediction
			// counter and set the predictions state.
			if(predictionTable[index].getPredictionState() < 2)
			{
				correctPredictionCount++;
				predictionTable[index].setPredictionState(operation);
			}
			// I predicted the branch will be taken but it actually was not.
			// Increment the mis-prediction counter and set the prediction state. 
			else
			{
				misPredictionCount++;
				predictionTable[index].setPredictionState(operation);
			}
		}
		return OK;
	}

	// GSHARE PREDICTION
	int gsharePrediction(uint32_t  address, char operation)
	{
		uint32_t  index = getGshareIndex(getShiftedAddress(address));
		nsrPrintf("\nGshare Index: %ld", index);
		//The branch was actually taken. Find inline comments. 
		if(operation == 't')
		{
			// I predicted the branch was not taken. Hence I need to increment
			// prediction state by 1. Increment the mis-prediction counter and
			// update the prediction state. 
			if(predictionTable[index].getPredictionState() < 2)
			{
				misPredictionCount++;
				predictionTable[index].setPredictionState(operation);
				updateGShareRegister(operation);
			}
			// My prediction and the actual path match. So just increment the
			// correct prediction Counter and set the prediction state. 
			else
			{
				correctPredictionCount++;
				predictionTable[index].setPredictionState(operation);
				updateGShareRegister(operation);
			}
		}
		else
		{
			// I predicted that the branch would not be taken and the actual
			// path matches that. So just increment the correct prediction
			// counter and set the predictions state.
			if(predictionTable[index].getPredictionState() < 2)
			{
				correctPredictionCount++;
				predictionTable[index].setPredictionState(operation);
				updateGShareRegister(operation);
			}
			// I predicted the branch will be taken but it actually was not.
			// Increment the mis-prediction counter and set the prediction state. 
			else
			{
				misPredictionCount++;
				predictionTable[index].setPredictionState(operation);
				updateGShareRegister(operation);
			}
		}
		return OK;
	}

	int hybridPrediction(uint32_t  address, char operation)
	{
		uint32_t  shiftedAddress = getShiftedAddress(address);
		uint32_t  bimodalIndex = bimodalTable->getBimodalIndex(shiftedAddress);
		uint32_t  gshareIndex = gshareTable->getGshareIndex(shiftedAddress);
		nsrPrintf("\nBimodal Index: %ld, Gshare Index: %ld, Shifted Address: %ld", bimodalIndex, gshareIndex, shiftedAddress);
		int bimodalPrediction = bimodalTable->predictionTable[bimodalIndex].getPredictionState();
		int gsharePrediction = gshareTable->predictionTable[gshareIndex].getPredictionState();
		uint32_t  index = getHybridIndex(shiftedAddress);
		if(predictionTable[index].getPredictionState() < 2)
		{
			bimodalTable->bimodalPrediction(address, operation);
			gshareTable->updateGShareRegister(operation);
		}
		else
			gshareTable->gsharePrediction(address, operation);

		if(operation == 't')
		{
			if(bimodalPrediction >= 2 && gsharePrediction >= 2)
			{
				// Both are correct. Do not do anything.
			}
			else if(bimodalPrediction < 2 && gsharePrediction < 2)
			{
				// Both are incorrect. Don't do anything.
			}
			else if(bimodalPrediction >= 2 && gsharePrediction < 2)
			{
				// Bimodal is correct and Gshare is incorrect. Decrement the state. 
				predictionTable[index].predictionState--;
				if(predictionTable[index].predictionState < 0)
					predictionTable[index].predictionState = 0;
			}
			else if(bimodalPrediction < 2 && gsharePrediction >= 2)
			{
				//Gshare is correct and Bimodal is incorrect. Increment the counter. 
				predictionTable[index].predictionState++;
				if(predictionTable[index].predictionState > 3)
					predictionTable[index].predictionState = 3;
			}
			else
				nsrPrintf("/nSomething is wrong. This case should not be happening. Check code again.");
		}
		else
		{
			if(bimodalPrediction >= 2 && gsharePrediction >= 2)
			{
				// Both are incorrect. Do not do anything.
			}
			else if(bimodalPrediction < 2 && gsharePrediction < 2)
			{
				// Both are correct. Don't do anything.
			}
			else if(bimodalPrediction < 2 && gsharePrediction >= 2)
			{
				// Bimodal is correct and Gshare is incorrect. Decrement the state. 
				predictionTable[index].predictionState--;
				if(predictionTable[index].predictionState < 0)
					predictionTable[index].predictionState = 0;
			}
			else if(bimodalPrediction >= 2 && gsharePrediction < 2)
			{
				//Gshare is correct and Bimodal is incorrect. Increment the counter. 
				predictionTable[index].predictionState++;
				if(predictionTable[index].predictionState > 3)
					predictionTable[index].predictionState = 3;
			}
			else
				nsrPrintf("/nSomething is wrong. This case should not be happening. Check code again.");
		}
		return OK;
	}


};

int triggerBranchPrediction(uint32_t  address, char operation, int predictionType)
{
	if(predictionType == BIMODAL)
		bimodalTable->bimodalPrediction(address, operation);
	else if(predictionType == GSHARE)
		gshareTable->gsharePrediction(address, operation);
	else
		hybridTable->hybridPrediction(address, operation);
	return OK;
}

int printOutput(int predictionType)
{
	printf("COMMAND");
	if(predictionType == BIMODAL)
	{
		printf("\n./sim %s %d %s", predictor, bimodalTable->m, fileName);
		printf("\nOUTPUT");
		printf("\nnumber of predictions: %d", lineCount);
		printf("\nnumber of mispredictions: %d", bimodalTable->misPredictionCount);
		printf("\nmisprediction rate: %.2f%%", (bimodalTable->misPredictionCount * 1.0/lineCount) * 100);
		printf("\nFINAL BIMODAL CONTENTS");
		int i = 0;
		for(i = 0; i < bimodalTable->setCount; i++)
			printf("\n%d %d", i, bimodalTable->predictionTable[i].getPredictionState());
	}
	else if(predictionType == GSHARE)
	{
		printf("\n./sim %s %d %d %s", predictor, gshareTable->m, gshareTable->n, fileName);
		printf("\nOUTPUT");
		printf("\nnumber of predictions: %d", lineCount);
		printf("\nnumber of mispredictions: %d", gshareTable->misPredictionCount);
		printf("\nmisprediction rate: %.2f%%", (gshareTable->misPredictionCount * 1.0/lineCount) * 100);
		printf("\nFINAL GSHARE CONTENTS");
		int i = 0;
		for(i = 0; i < gshareTable->setCount; i++)
			printf("\n%d %d", i, gshareTable->predictionTable[i].getPredictionState());
	}
	else
	{
		printf("\n./sim %s %d %d %d %d %s", predictor, hybridTable->k, gshareTable->m, gshareTable->n, bimodalTable->m, fileName);
		printf("\nOUTPUT");
		printf("\nnumber of predictions: %d", lineCount);
		printf("\nnumber of mispredictions: %d", bimodalTable->misPredictionCount + gshareTable->misPredictionCount);
		printf("\nmisprediction rate: %.2f%%", ((bimodalTable->misPredictionCount + gshareTable->misPredictionCount) * 1.0/lineCount) * 100);
		int i = 0;
		printf("\nFINAL CHOOSER CONTENTS");
		for(i = 0; i < hybridTable->setCount; i++)
			printf("\n%d %d", i, hybridTable->predictionTable[i].getPredictionState());
		printf("\nFINAL GSHARE CONTENTS");
		for(i = 0; i < gshareTable->setCount; i++)
			printf("\n%d %d", i, gshareTable->predictionTable[i].getPredictionState());
		printf("\nFINAL BIMODAL CONTENTS");
		for(i = 0; i < bimodalTable->setCount; i++)
			printf("\n%d %d", i, bimodalTable->predictionTable[i].getPredictionState());
	}
	return OK;
}

int setAllPredictionStates()
{
	int i = 0;
	for(i = 0; i < hybridTable->setCount; i++)
		hybridTable->predictionTable[i].predictionState = 1;
	return OK;
}

int main(int argc, char **argv)
{
	predictor = new char[READSIZE];
	fileName = new char[READSIZE];
	int k, m1, m2, n;
	if(argc == 4)
	{
		predictor = argv[1];
		m2 = atoi(argv[2]);
		fileName = argv[3];
	}
	else if(argc == 5)
	{
		predictor = argv[1];
		m1 = atoi(argv[2]);
		n = atoi(argv[3]);
		fileName = argv[4];
	}
	else if(argc == 7)
	{
		predictor = argv[1];
		k = atoi(argv[2]);
		m1 = atoi(argv[3]);
		n = atoi(argv[4]);
		m2 = atoi(argv[5]);
		fileName = argv[6];
	}
	else
	{
		nsrPrintf("\nThe number of arguments is wrong. Check arguments again.");
		return SYSERR;
	}
	
	uint32_t  address;
	char operation;
	int predictorType; 
	if(strcmp(predictor, "bimodal") == 0)
	{
		nsrPrintf("\nI have entered this.");
		predictorType = BIMODAL;
		bimodalTable = new PredictionTable(m2, 0, 0, BIMODAL);
	}
	else if(strcmp(predictor, "gshare") == 0)
	{
		predictorType = GSHARE;
		gshareTable = new PredictionTable(m1, n, 0, GSHARE);
	}
	else if(strcmp(predictor, "hybrid") == 0)
	{
		predictorType = HYBRID;
		bimodalTable = new PredictionTable(m2, 0, 0, BIMODAL);
		gshareTable = new PredictionTable(m1, n, 0, GSHARE);
		hybridTable = new PredictionTable(0, 0, k, HYBRID);
		// YUCK!! Ugly Hack! 
		setAllPredictionStates();
	}

	FILE *inputFile;
	inputFile = fopen(fileName, "r");
	if(inputFile != NULL)
	{
		while(feof(inputFile) == 0)
		{
			fscanf(inputFile, "%x %c\n", &address, &operation);
			// FIRE!!!
			nsrPrintf("\nAddress is: %x", address);
			nsrPrintf("\nLine Count is: %d", lineCount + 1);
			triggerBranchPrediction(address, operation, predictorType);
			nsrPrintf("\n------------------------------", lineCount);
			lineCount++;
		}
		fclose(inputFile);
	}
	else
		nsrPrintf("\nThere was a mistake while opening the file. Please check the file again.");

	printOutput(predictorType);
	return OK;
}

