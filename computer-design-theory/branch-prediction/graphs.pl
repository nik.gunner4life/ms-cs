use strict;
use warnings;
use Time::HiRes qw( time );
use IO::Handle;

sub cleanUp
{
	print("\n=============== Clean Up ===============");
	# Clean up all Validation Runs. 
	my $command = "rm -rf VALIDATION_RUNS";
	system($command);
	# Clean up all Runs for GRAPHS_1. 
	$command = "rm -rf GRAPHS_1";
	system($command);
	# Clean up all Runs for GRAPHS_2. 
	$command = "rm -rf GRAPHS_2";
	system($command);
	# Clean up all PLOTTED_GRAPHS. 
	$command = "rm -rf PLOTTED_GRAPHS";
	system($command);
	print "\nRemoved all files and directories.\n\n";
}

sub validationRuns
{
	print("\n=============== Validation Runs ===============");
	my $command = "mkdir VALIDATION_RUNS";
	system($command);
	my $outputFile;
	my $output;
	my @commands;
	$command = "./sim bimodal 6 gcc_trace.txt > VALIDATION_RUNS/validationRun1.txt";
	push(@commands, $command);
	$command = "./sim bimodal 12 gcc_trace.txt > VALIDATION_RUNS/validationRun2.txt";
	push(@commands, $command);
	$command = "./sim bimodal 4 jpeg_trace.txt > VALIDATION_RUNS/validationRun3.txt";
	push(@commands, $command);
	$command = "./sim bimodal 5 perl_trace.txt > VALIDATION_RUNS/validationRun4.txt";
	push(@commands, $command);
	$command = "./sim gshare 9 3 gcc_trace.txt > VALIDATION_RUNS/validationRun5.txt";
	push(@commands, $command);
	$command = "./sim gshare 14 8 gcc_trace.txt > VALIDATION_RUNS/validationRun6.txt";
	push(@commands, $command);
	$command = "./sim gshare 11 5 jpeg_trace.txt > VALIDATION_RUNS/validationRun7.txt";
	push(@commands, $command);
	$command = "./sim gshare 10 6 perl_trace.txt > VALIDATION_RUNS/validationRun8.txt";
	push(@commands, $command);
	$command = "./sim hybrid 8 14 10 5 gcc_trace.txt > VALIDATION_RUNS/validationRun9.txt";
	push(@commands, $command);
	$command = "./sim hybrid 5 10 7 5 jpeg_trace.txt > VALIDATION_RUNS/validationRun10.txt";
	push(@commands, $command);
	my $i = 1;
	my $allRunsPassed = 0;
	foreach my $comm (@commands)
	{
		system($comm);
		$command = "diff -iw  VALIDATION_RUNS/validationRun$i.txt val$i.txt";
		$output = qx($command);
		if($output eq "")
		{
			print "\nValidation Run $i passed.";
			$allRunsPassed++;
		}
		else
		{
			print "\nValidation Run $i failed.";
			print "\n---> Check $comm ";
		}
		$i++;		
	}
	if($allRunsPassed eq 10)
	{
		print "\nALL VALIDATION RUNS HAVE PASSED\n\n";
	}	
}

sub createGraphDirectory
{
	my $command = "mkdir PLOTTED_GRAPHS";
	system($command);
}

sub graph1
{
	print("\n=============== Graph 1 ===============");
	my $command = "mkdir GRAPHS_1";
	system($command);
	my ($outputFile, $inputFile);
	my $output;
	my $m = 7;
	my @traceFiles;
	push(@traceFiles, "gcc_trace.txt");
	push(@traceFiles, "jpeg_trace.txt");
	push(@traceFiles, "perl_trace.txt");
	foreach my $traceFile (@traceFiles)
	{
		my @arrayTraces = split("_", $traceFile);
		my $traceName = $arrayTraces[0];
		$outputFile = "PLOTTED_GRAPHS/bimodal_" . $traceName . ".txt";
		open my $outputFileHandle, ">>$outputFile" or die "Could not open $outputFile $!";
		for($m = 7; $m < 13; $m++)
		{
			
			$command = "./sim bimodal $m $traceFile > GRAPHS_1/bimodal_" . $traceName . "_" . $m . ".txt";
			system($command);
			$inputFile = "GRAPHS_1/bimodal_" . $traceName . "_" . $m . ".txt";
			open my $inputFileHandle, $inputFile or die "Could not open $inputFile $!";
			while( my $line = <$inputFileHandle>) 
			{   
				if(index($line, "misprediction rate") >= 0)
				{
					my @arrayMisPredictionRate = split(":", $line);
					$arrayMisPredictionRate[1] =~ s/^\s+//;
					$arrayMisPredictionRate[1] =~ s/\s+$//;
					my @getValue = split("%", $arrayMisPredictionRate[1]);
					my $totalSize = (2 ** ($m- 2)); 
					print { $outputFileHandle } "\n$m,$getValue[0],$totalSize";
				}
			}
			close($inputFileHandle);
			print "\n./sim bimodal $m $traceFile completed.";
		}
		close($outputFileHandle);	
	}
	print "\n\n";	
}

sub graph2
{
	print("\n=============== Graph 2 ===============");
	my $command = "mkdir GRAPHS_2";
	system($command);
	my ($outputFile, $inputFile);
	my $output;
	my $m = 7;
	my $n = 2;
	my @traceFiles;
	push(@traceFiles, "gcc_trace.txt");
	push(@traceFiles, "jpeg_trace.txt");
	push(@traceFiles, "perl_trace.txt");
	foreach my $traceFile (@traceFiles)
	{
		my @arrayTraces = split("_", $traceFile);
		my $traceName = $arrayTraces[0];
		$outputFile = "PLOTTED_GRAPHS/gshare_" . $traceName . ".txt";
		open my $outputFileHandle, ">>$outputFile" or die "Could not open $outputFile $!";
		for($m = 7; $m < 13; $m++)
		{
			for($n = 2; $n <= $m; $n+=2)
			{
				$command = "./sim gshare $m $n $traceFile > GRAPHS_2/gshare_" . $traceName . "_" . $m . "_" . $n . ".txt";
				system($command);
				$inputFile = "GRAPHS_2/gshare_" . $traceName . "_" . $m . "_" . $n . ".txt";
				open my $inputFileHandle, $inputFile or die "Could not open $inputFile $!";
				while( my $line = <$inputFileHandle>) 
				{   
					if(index($line, "misprediction rate") >= 0)
					{
						my @arrayMisPredictionRate = split(":", $line);
						$arrayMisPredictionRate[1] =~ s/^\s+//;
						$arrayMisPredictionRate[1] =~ s/\s+$//;
						my @getValue = split("%", $arrayMisPredictionRate[1]);
						my $totalSize = (2 ** ($m- 2)); 
						print { $outputFileHandle } "\n$m,$n,$getValue[0],$totalSize";
					}
				}
				close($inputFileHandle);
				print "\n./sim gshare $m $n $traceFile completed.";
			}			
		}
		close($outputFileHandle);	
	}
	print "\n\n";
}

sub plotGraphs
{
	print("\n=============== PLOT GRAPHS ===============");
	my ($inputFile, $outputFile);
	my @traceNames;
	push(@traceNames, "gcc");
	push(@traceNames, "jpeg");
	push(@traceNames, "perl");
	my $n = 2;
	foreach my $traceName (@traceNames)
	{
		$inputFile = "PLOTTED_GRAPHS/gshare_" . $traceName . ".txt";
		open my $inputFileHandle, $inputFile or die "Could not open $inputFile $!";
		for($n = 2; $n <= 12; $n+=2)
		{
			seek($inputFileHandle,0,0);
			$outputFile = "PLOTTED_GRAPHS/gshare_" . $traceName . "_" . $n . ".txt";
			open my $outputFileHandle, ">$outputFile" or die "Could not open $outputFile $!";
			while( my $line = <$inputFileHandle>) 
			{
				if(defined($line))
				{
					my @array = split(",", $line);				
					if(defined($array[1]) && $array[1] eq "$n")
					{
						print {$outputFileHandle} "\n$array[0],$array[2]";
					}
				}
			}
			close($outputFileHandle);
		}
		close($inputFileHandle);		
	}
	
	my $command = "./plotGraphs";
	system($command);
	my @graphs;
	push(@graphs, "graph1.png");
	push(@graphs, "graph2.png");
	push(@graphs, "graph3.png");
	push(@graphs, "graph4.png");
	push(@graphs, "graph5.png");
	push(@graphs, "graph6.png");
	my $count = 1;
	foreach my $graph (@graphs)
	{
		$command = "mv $graph PLOTTED_GRAPHS/$graph";
		system($command);
		print "\nGraph $count plotted";
		$count++;
	}
	print "\n\n"; 	
}

sub main
{
	my $start = time(); 
	cleanUp();
	validationRuns();
	createGraphDirectory();
	graph1();
	graph2();
	plotGraphs();
	my $end = time();
	print "\nElapsed Time is " . ($end - $start) . "\n"; 	
}

# Main method. Just execute this shite!!
main();