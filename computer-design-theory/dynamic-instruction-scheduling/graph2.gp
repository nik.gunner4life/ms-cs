set terminal png
set output "./graph2.png"
set datafile separator ","
set title "PERL: Scheduling Queue Size vs IPC"
set xlabel "Scheduling Queue Size (S)"
set ylabel "IPC"
plot "./PLOTTED_GRAPHS/perl_1.txt" using 1:2 with linespoints  title "N: 1", "./PLOTTED_GRAPHS/perl_2.txt" using 1:2 with linespoints  title "N: 2", "./PLOTTED_GRAPHS/perl_4.txt" using 1:2 with linespoints  title "N: 4", "./PLOTTED_GRAPHS/perl_8.txt" using 1:2 with linespoints  title "N: 8"