set terminal png
set output "./graph1.png"
set datafile separator ","
set title "GCC: Scheduling Queue Size vs IPC"
set xlabel "Scheduling Queue Size (S)"
set ylabel "IPC"
plot "./PLOTTED_GRAPHS/gcc_1.txt" using 1:2 with linespoints  title "N: 1", "./PLOTTED_GRAPHS/gcc_2.txt" using 1:2 with linespoints  title "N: 2", "./PLOTTED_GRAPHS/gcc_4.txt" using 1:2 with linespoints  title "N: 4", "./PLOTTED_GRAPHS/gcc_8.txt" using 1:2 with linespoints  title "N: 8"