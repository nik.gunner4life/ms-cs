use strict;
use warnings;
use Time::HiRes qw( time );
use IO::Handle;

sub cleanUp
{
	print("\n=============== Clean Up ===============");
	# Clean up all Validation Runs. 
	my $command = "rm -rf VALIDATION_RUNS";
	system($command);
	# Clean up all Runs for GRAPHS_1. 
	$command = "rm -rf GRAPHS_1";
	system($command);
	# Clean up all PLOTTED_GRAPHS. 
	$command = "rm -rf PLOTTED_GRAPHS";
	system($command);
	print "\nRemoved all files and directories.\n\n";
}

sub validationRuns
{
	print("\n=============== Validation Runs ===============");
	my $command = "mkdir VALIDATION_RUNS";
	system($command);
	my $outputFile;
	my $output;
	my @commands;
	$command = "./sim 64 8 val_trace_gcc1 > VALIDATION_RUNS/validationRun1.txt";
	push(@commands, $command);
	$command = "./sim 16 3 val_trace_perl1 > VALIDATION_RUNS/validationRun2.txt";
	push(@commands, $command);
	my $i = 1;
	my $allRunsPassed = 0;
	foreach my $comm (@commands)
	{
		system($comm);
		$command = "diff -iw  VALIDATION_RUNS/validationRun$i.txt val$i.txt";
		$output = qx($command);
		if($output eq "")
		{
			$allRunsPassed++;
		}
		else
		{
			print "\nValidation Run $i failed.";
			print "\n---> Check $comm ";
		}
	}
	if($allRunsPassed eq 2)
	{
		print "\nALL VALIDATION RUNS HAVE PASSED\n\n";
	}
}

sub graph1
{
	print("\n=============== Graph 1 ===============");
	my $command;
	my @S;
	my @N;
	my @traceFiles;
	my @traces;
	my ($outputFile, $inputFile);
	push(@S, 8);
	push(@S, 16);
	push(@S, 32);
	push(@S, 64);
	push(@S, 128);
	push(@S, 256);
	
	push(@N, 1);
	push(@N, 2);
	push(@N, 4);
	push(@N, 8);
	
	push(@traceFiles, "val_trace_gcc1");
	push(@traceFiles, "val_trace_perl1");
	
	push(@traces, "gcc");
	push(@traces, "perl");
	my $looper = 0;
	
	$command = "mkdir PLOTTED_GRAPHS";
	system($command);
	$command = "mkdir GRAPHS_1";
	system($command);
	foreach my $traceFile (@traceFiles)
	{		
		foreach my $n (@N)
		{
			my $traceName = $traces[$looper];
			$outputFile = "PLOTTED_GRAPHS/" . $traceName . "_" . $n . ".txt";
			open my $outputFileHandle, ">>$outputFile" or die "Could not open $outputFile $!";
			foreach my $s (@S)
			{
				$inputFile =  "GRAPHS_1/" . $traceName . "_" . $s . "_" . $n . ".txt";
				$command = "./sim $s $n $traceFile > $inputFile";
				system($command);
				open my $inputFileHandle, $inputFile or die "Could not open $inputFile $!";
				while( my $line = <$inputFileHandle>) 
				{   
					if(index($line, "IPC =") >= 0)
					{
						my @arrayIPC = split("=", $line);
						$arrayIPC[1] =~ s/^\s+//;
						$arrayIPC[1] =~ s/\s+$//;						
						print { $outputFileHandle } "\n$s, $arrayIPC[1]";
					}
				}
				close($inputFileHandle);
				print "\n Completed $command";
			}
			close($outputFileHandle);
		}
		$looper++;
	}
	print "\n\n";	
}

sub plotGraphs
{
	print("\n=============== PLOT GRAPHS ===============");
	my $command;
	my @graphFiles;
	push(@graphFiles, "graph1.gp");
	push(@graphFiles, "graph2.gp");
	
	my @traces;
	push(@traces, "GCC");
	push(@traces, "PERL");
	
	my $count = 1;
	foreach my $gp (@graphFiles)
	{
		$command = "gnuplot< $gp";
		system($command);
		print "\n$count graph has been plotted.";
		$count++;		
	}
	print "\n\n";	
}

sub main
{
	cleanUp();
	validationRuns();
	graph1();
	plotGraphs();
	print "\nGoodbye!!";
}

main();