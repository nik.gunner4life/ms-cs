
#pragma region AUTHOR INFORMATION
/***********************************************************************
Author:   Nikhil Srivatsan Srinivasan
Unity ID: nsrivat
Project:  Dynamic Instruction Scheduling
Date:     12/09/2013
***********************************************************************/
#pragma endregion

#pragma region HEADERS
#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <string>
#include <string.h>
#include <iostream>
#include <fstream>
#include <stdint.h>
#include <vector>
#pragma endregion

#pragma region PREPROCESSORS
#define nsrPrintf(...) if(nsrDebugPrintf) { fflush(stdout); printf(__VA_ARGS__); fflush(stdout);}
int nsrDebugPrintf;
using namespace std;

// System Level Pass or Fail values. 
#define OK 100
#define SYSERR -100
#define VALID 1
#define INVALID -1
#define READY 2
#define NOT_READY -2
#define NO_REGISTER -1
#define PIPELINE_SIZE 1024
#define REGISTER_COUNT 128
#define MY_REGISTERS 3
#define READSIZE 50

// Pipeline stages that have been modeled.
#define NO_STATE 1000
#define FETCH_INSTRUCTION 1001
#define DECODE_INSTRUCTION 1002
#define ISSUE_INSTRUCTION 1003
#define EXECUTE_INSTRUCTION 1004
#define WRITE_BACK 1005
#define NUMBER_OF_STATES 5

// Used for state cycles. Have them here
// so that I don't end up updating the wrong
// state. 
#define SC_FETCH 0
#define SC_DECODE 1
#define SC_ISSUE 2
#define SC_EXECUTE 3
#define SC_WRITEBACK 4
#pragma endregion

#pragma region DECLARATIONS
class InstructionObject;
class PipelineObject;
class RegisterObject;
class SourceRegisters;
class Pipeline;

RegisterObject *registers;
Pipeline *pipeline;
InstructionObject *instructionQ;
char *fileName;
long lineCount;

#pragma endregion

#pragma region DEFINITIONS

/*This class will hold all the registers for the instruction. 
Along with this, we will also have the ready state of the
registers and which instruction (if any) they are dependent
on. Fun, fun, fun!! :( */
class SourceRegisters
{
public:
	long sourceTag;
	int sourceReady;
	int registerID;

	SourceRegisters()
	{
		sourceTag = -1;
		sourceReady = NOT_READY;
		registerID = NO_REGISTER;
	}
};

/* This class is going to hold the details of the instruction.
Details include address, registers, tag, type and so on. 
Additionally it will also be holding the cycle number in which
it entered a particular state. This is important for two reasons.
Primarily, I need this for the output and secondly for debugging. */
class InstructionObject
{
public:
	uint32_t pcAddress;
	long tag;
	SourceRegisters *myRegisters;
	int type, executionCycles;
	int stateCycles[NUMBER_OF_STATES];

	InstructionObject()
	{
		pcAddress = type = executionCycles = 0;
		tag = type = -1;
		myRegisters = new SourceRegisters[MY_REGISTERS];
		int i = 0;
		for(i = 0; i < NUMBER_OF_STATES; i++)
			stateCycles[i] = -1;
	}
};

/* This is the pipeline object. It is going to contain the state and 
so on. These are going to be the entries in the ROB queue.        */
class PipelineObject
{
public:
	int state, executionCycles, ready;
	int stateCycles[NUMBER_OF_STATES];
	SourceRegisters *myRegisters;
	long tag;
	int validBit;

	uint32_t pcAddress;

	PipelineObject()
	{
		pcAddress = 0;
		executionCycles = 0;
		tag = -1;
		myRegisters = new SourceRegisters[MY_REGISTERS];
		int i = 0;
		for(i = 0; i < NUMBER_OF_STATES; i++)
			stateCycles[i] = -1;
		validBit = INVALID;
		ready = NOT_READY;
		state = NO_STATE;
	}

	// Clear that object and bring it back to default state i.e the state
	// in which it was created. 
	int clearPipelineObjects()
	{
		pcAddress = 0;
		executionCycles = 0;
		tag = -1;
		int i = 0;
		for(i = 0; i < MY_REGISTERS; i++)
		{
			myRegisters[i].sourceTag = -1;
			myRegisters[i].sourceReady = NOT_READY;
			myRegisters[i].registerID = NO_REGISTER;
		}
		for(i = 0; i < NUMBER_OF_STATES; i++)
			stateCycles[i] = -1;
		validBit = INVALID;
		ready = NOT_READY;
		state = NO_STATE;
		return OK;
	}
};

class RegisterObject
{
public:
	int registerID;
	int validBit;
	long tag;

	RegisterObject()
	{
		registerID = NO_REGISTER;
		// Initially make all registers valid. This means that
		// any instruction can use prehistoric values in the 
		// registers. It is made invalid only when it is used
		// as a destination register. It can be made invalid
		// only in dispatch instructions phase and not anytime
		// before. Similarly it can once again be valid, only
		// if the tags match after execution. Otherwise it means
		// that a future instruction has already taken it up. 
		// Instructions that were depending on this instruction's
		// value will pick it up from the bus i.e in the execution
		// phase. This will be explained in detail in the class
		// methods. 
		validBit = VALID;
		tag = -1;
	}

	int mapRegisters(long instructionTag, int regID)
	{
		// Sanity check. 
		if(regID == registerID)
		{
			tag = instructionTag;
			validBit = INVALID;
			return OK;
		}
		else
			nsrPrintf("Bad scene man!! Really bad scene.");
	}
};

class Pipeline
{
public:
	int totalCycles;
	PipelineObject *pipelineQ;
	PipelineObject *tempPipelineQ;
	int S, N;
	int exCount, fetchCount, disCount, issCount, wbCount, pipelineCount;
	int currentInstruction;
	int alreadyLookedAt[PIPELINE_SIZE];
	
	Pipeline(int s, int n)
	{
		exCount = fetchCount = disCount = issCount = wbCount = pipelineCount = 0;
		totalCycles = currentInstruction = 0;
		S = s;
		N = n;
		pipelineQ = new PipelineObject[PIPELINE_SIZE];
		tempPipelineQ = new PipelineObject[PIPELINE_SIZE];
		int i = 0;
		for(i = 0; i < PIPELINE_SIZE; i++)
		{
			alreadyLookedAt[i] = -1;
		}
	}

	int movePipelineObjectsToTemp(int index1, int index2)
	{
		// Sanity Check!
		if(index1 >= 0 && index2 >= 0)
		{
			tempPipelineQ[index1].executionCycles = pipelineQ[index2].executionCycles;
			tempPipelineQ[index1].pcAddress = pipelineQ[index2].pcAddress;
			tempPipelineQ[index1].ready = pipelineQ[index2].ready;
			tempPipelineQ[index1].state = pipelineQ[index2].state;
			tempPipelineQ[index1].tag = pipelineQ[index2].tag;
			tempPipelineQ[index1].validBit = pipelineQ[index2].validBit;
			int i = 0;
			for(i = 0; i < MY_REGISTERS; i++)
			{
				tempPipelineQ[index1].myRegisters[i].registerID = pipelineQ[index2].myRegisters[i].registerID;
				tempPipelineQ[index1].myRegisters[i].sourceReady = pipelineQ[index2].myRegisters[i].sourceReady;
				tempPipelineQ[index1].myRegisters[i].sourceTag = pipelineQ[index2].myRegisters[i].sourceTag;
			}
			for(i = 0; i < NUMBER_OF_STATES; i++)
				tempPipelineQ[index1].stateCycles[i] = pipelineQ[index2].stateCycles[i];
		}
		else
			nsrPrintf("\nPass proper indexes you fool.");
		return OK;
	}

	int movePipelineObjectsBackToMain(int index1, int index2)
	{
		// Sanity Check!
		if(index1 >= 0 && index2 >= 0)
		{
			pipelineQ[index1].executionCycles = tempPipelineQ[index2].executionCycles;
			pipelineQ[index1].pcAddress = tempPipelineQ[index2].pcAddress;
			pipelineQ[index1].ready = tempPipelineQ[index2].ready;
			pipelineQ[index1].state = tempPipelineQ[index2].state;
			pipelineQ[index1].tag = tempPipelineQ[index2].tag;
			pipelineQ[index1].validBit = tempPipelineQ[index2].validBit;
			int i = 0;
			for(i = 0; i < MY_REGISTERS; i++)
			{
				pipelineQ[index1].myRegisters[i].registerID = tempPipelineQ[index2].myRegisters[i].registerID;
				pipelineQ[index1].myRegisters[i].sourceReady = tempPipelineQ[index2].myRegisters[i].sourceReady;
				pipelineQ[index1].myRegisters[i].sourceTag = tempPipelineQ[index2].myRegisters[i].sourceTag;
			}
			for(i = 0; i < NUMBER_OF_STATES; i++)
				pipelineQ[index1].stateCycles[i] = tempPipelineQ[index2].stateCycles[i];
		}
		else
			nsrPrintf("\nPass proper indexes you fool.");
		return OK;
	}

	bool nextCycle()
	{
		if(currentInstruction == lineCount && pipelineCount == 0)
			return true;
		else
		{
			totalCycles++;
			return false;
		}
	}

	// This method will fetch instruction from the instruction queue. 
	// Be careful to fetch only up to a max of N instructions
	int fetchInstructions()
	{
		int i = 0;
		int maxDispatchCount = 2 * N;
		fetchCount = 0;
		while(currentInstruction < lineCount)
		{
			if(fetchCount == N || disCount == maxDispatchCount)
				break;
			pipelineQ[pipelineCount].validBit = VALID;
			pipelineQ[pipelineCount].pcAddress = instructionQ[currentInstruction].pcAddress;
			pipelineQ[pipelineCount].executionCycles = instructionQ[currentInstruction].executionCycles;
			pipelineQ[pipelineCount].tag = instructionQ[currentInstruction].tag;
			for(i = 0; i < MY_REGISTERS; i++)
				pipelineQ[pipelineCount].myRegisters[i].registerID = instructionQ[currentInstruction].myRegisters[i].registerID;
			pipelineQ[pipelineCount].state = FETCH_INSTRUCTION;
			//Update the cycle when it entered the fetch phase. 
			pipelineQ[pipelineCount].stateCycles[SC_FETCH] = totalCycles;
			fetchCount++;
			disCount++;
			pipelineCount++;
			currentInstruction++;
		}
		return OK;
	}

	// This method will dispatch instructions from the dispatch queue.
	// The dispatch queue is the same pipeline queue but the states will
	// change and register tags will be mapped. 
	int dispatchInstructions()
	{
		int i = 0;
		for(i = 0; i < PIPELINE_SIZE; i++)
		{
			// Sanity Check. Also reduces the number of times we loop through
			// the pipeline queue. Idea being if it is invalid, we can break.
			// This is because as instructions leave the pipeline, we move the
			// other instructions ahead in the queue and make all the vacated 
			// spaces INVALID. 
			if(pipelineQ[i].validBit == VALID)
			{
				// Move objects that are in the fetch state to the decode phase.
				// There is no need to decrement the dispatch count, because they
				// will continue residing in the dispatch queue. Update the cycle
				// in which it entered the decode phase. 
				if(pipelineQ[i].state == FETCH_INSTRUCTION)
				{
					pipelineQ[i].state = DECODE_INSTRUCTION;
					pipelineQ[i].stateCycles[SC_DECODE] = totalCycles;
				}
				// It is in the decode phase. Check if the scheduling queue is full
				// If not, remove it from the dispatch queue and add it to the issue
				// queue. This simply means reducing the disCount and incrementing the
				// issCount. Change state to ISSUE and rename the registers. For source
				// operands, check the tag on the register and set the tag. If register
				// is valid then set them as ready as they don't depend on previous
				// instructions. If not, set them as NOT_READY. Further if it has no
				// source operands at all i.e. it is NO_REGISTER, then just set them as
				// READY. Destination operands are different. Update the tag in the 
				// register and set it to INVALID. If scheduling queue is full, do
				// nothing. Update the cycle when it enters the issue phase. 
				else if(pipelineQ[i].state == DECODE_INSTRUCTION)
				{
					if(issCount < S)
					{
						pipelineQ[i].state = ISSUE_INSTRUCTION;
						pipelineQ[i].stateCycles[SC_ISSUE] = totalCycles;
						issCount++;
						disCount--;
						int j = 1;
						for(j = 1; j < MY_REGISTERS; j++)
						{
							int srcReg = pipelineQ[i].myRegisters[j].registerID;
							if(srcReg != NO_REGISTER)
							{
								pipelineQ[i].myRegisters[j].sourceTag = registers[srcReg].tag;
								if(registers[srcReg].validBit == VALID)
									pipelineQ[i].myRegisters[j].sourceReady = READY;
								else
									pipelineQ[i].myRegisters[j].sourceReady = NOT_READY;
							}
							else
								pipelineQ[i].myRegisters[j].sourceReady = READY;
						}
						int destReg = pipelineQ[i].myRegisters[0].registerID;
						if(destReg != NO_REGISTER)
							registers[destReg].mapRegisters(pipelineQ[i].tag, destReg);
					}
					else
					{
						// Do nothing. We cannot break from here since instruction down the
						// pipeline might be in the fetch phase. Breaking will cause all those
						// instructions to not move ahead, thereby resulting in a deadlock. 
					}
				}
			}
		}
		return OK;
	}

	// This method will issue instructions i.e. move them to the execute
	// phase provided they are ready to be executed. Look at inline comments
	// for these. 
	int issueInstructions()
	{
		int i = 0;
		int cdbCounter = 0;
		for(i = 0; i < PIPELINE_SIZE; i++)
		{
			// Sanity Check. Also reduces the number of times we loop through
			// the pipeline queue. Idea being if it is invalid, we can break.
			// This is because as instructions leave the pipeline, we move the
			// other instructions ahead in the queue and make all the vacated 
			// spaces INVALID. 
			if(pipelineQ[i].validBit == VALID)
			{
				// Check to see if it is in the issue phase. If so, check the source
				// operands. If all the source operands are ready, mark the object as 
				// ready. If the execute list has less than N elements, then go ahead
				// and set its state to EXECUTE. Remove it from the issue list. The
				// execution cycles has already been set so we are good. If the execute 
				// queue is not empty, don't do anything. Similarly, if the source
				// operands are not ready, don't do anything. If it does move to the
				// execute phase, update the cycle when it enters the execute phase. 
				if(pipelineQ[i].state == ISSUE_INSTRUCTION)
				{
					if((pipelineQ[i].myRegisters[1].sourceReady == READY) && (pipelineQ[i].myRegisters[2].sourceReady == READY))
						pipelineQ[i].ready = READY;
					if((cdbCounter < N) && (pipelineQ[i].ready == READY))
					{
						pipelineQ[i].state = EXECUTE_INSTRUCTION;
						pipelineQ[i].stateCycles[SC_EXECUTE] = totalCycles;
						issCount--;
						exCount++;
						cdbCounter++;
					}
				}
			}
		}
		return OK;
	}

	// This method will execute instructions. It will decrement exec cycles
	// till it reaches 0. Once it reaches 0, it will move it the write back
	// state and wake up dependent instructions. Look at inline comments for
	// further details. Since we have the number of cycles it should be running
	// for in the instruction queue, we can change this value. If needed, I'll
	// move the type here also, just to make sure no information is lost. 
	int executeInstructions()
	{
		int i = 0;
		int j = 0;
		for(i = 0; i < PIPELINE_SIZE; i++)
		{
			// Sanity Check. Also reduces the number of times we loop through
			// the pipeline queue. Idea being if it is invalid, we can break.
			// This is because as instructions leave the pipeline, we move the
			// other instructions ahead in the queue and make all the vacated 
			// spaces INVALID. 
			if(pipelineQ[i].validBit == VALID)
			{
				// If it is in the execute phase, check if it has completed. This can
				// be seen from the execution cycles. If exec cycles > 0, decrement it. 
				// If it is 0, move it to the WRITE BACK phase. Remove it from the exec
				// queue (exCount--) and add it to the write back queue (wbCount++).
				// Now comes the critical part. Look at all instructions in the pipeline
				// and wake up those instructions whose source operand tags match the
				// current instructions tag. If they match, make those source operands
				// ready. If object is moving to the write back state, update the cycle
				// in which it enters the WRITE BACK phase. Also, make the destination
				// register valid, if the tags match. 
				if(pipelineQ[i].state == EXECUTE_INSTRUCTION)
				{
					if((totalCycles - pipelineQ[i].stateCycles[SC_EXECUTE]) < pipelineQ[i].executionCycles)
					{
						// Do Nothing
					}
					else if((totalCycles - pipelineQ[i].stateCycles[SC_EXECUTE]) == pipelineQ[i].executionCycles)
					{
						pipelineQ[i].state = WRITE_BACK;
						pipelineQ[i].stateCycles[SC_WRITEBACK] = totalCycles;
						exCount--;
						wbCount++;
						int destReg = pipelineQ[i].myRegisters[0].registerID;
						if(destReg != NO_REGISTER)
						{
							if(registers[destReg].tag == pipelineQ[i].tag)
								registers[destReg].validBit = VALID;
						}
						for(j = 0; j < PIPELINE_SIZE; j++)
						{
							// Potentially I could start the j loop from i + 1, as only future
							// instructions after me would be dependent on me. I will change it
							// once the code runs or doesn't. Whichever happens first. 
							if(j != i)
							{
								if(pipelineQ[j].myRegisters[1].sourceTag == pipelineQ[i].tag)
									pipelineQ[j].myRegisters[1].sourceReady = READY;
								if(pipelineQ[j].myRegisters[2].sourceTag == pipelineQ[i].tag)
									pipelineQ[j].myRegisters[2].sourceReady = READY;
							}
						}

					}
					else
					{
						nsrPrintf("Shut Up!! JUST. SHUT. UP.");
					}
				}
			}
			
		}
		return OK;
	}

	// Remove all instructions from the pipelineQ which are in Write Back
	// Phase. Here I have differed from the instructions in the specs.
	// In the specs, we remove instructions that are in WB phase till we
	// reach an instruction that is not in the WB phase. This is beneficial
	// when we want to print in the retire function itself. However this is
	// not elegant programming. I have created objects (instructionQ) which
	// will hold this information for me, and I will finally print everything
	// from there. Memory wise, I do use more. But in the world of GB of memory
	// why are we worrying about MB of data?
	int retireInstructions()
	{
		// This is the ugliest piece of hack that I will ever write. But it is
		// getting late and I am really tired. So, HERE. WE. GO.
		int i = 0;
		int tempCount = 0;
		for(i = 0; i < PIPELINE_SIZE; i++)
		{
			if(pipelineQ[i].validBit == VALID && pipelineQ[i].state != WRITE_BACK)
			{
				movePipelineObjectsToTemp(tempCount, i);
				tempCount++;
			}
			else if(pipelineQ[i].validBit == VALID && pipelineQ[i].state == WRITE_BACK)
			{
				int k = 0;
				int my_tag = pipelineQ[i].tag;
				instructionQ[my_tag].stateCycles[SC_FETCH] = pipelineQ[i].stateCycles[SC_FETCH];
				instructionQ[my_tag].stateCycles[SC_DECODE] = pipelineQ[i].stateCycles[SC_DECODE];
				instructionQ[my_tag].stateCycles[SC_ISSUE] = pipelineQ[i].stateCycles[SC_ISSUE];
				instructionQ[my_tag].stateCycles[SC_EXECUTE] = pipelineQ[i].stateCycles[SC_EXECUTE];
				instructionQ[my_tag].stateCycles[SC_WRITEBACK] = pipelineQ[i].stateCycles[SC_WRITEBACK];
			}
		}
		// tempCount should now hold the number of objects in the pipeline that
		// are not WB. This, I think, should be equal to pipelineCount - wbCount
		// Lets move it back to pipelineQ. God I hope this works. 
		for(i = 0; i < tempCount; i++)
			movePipelineObjectsBackToMain(i, i);
		pipelineCount = pipelineCount - wbCount;
		wbCount = 0;
		for(i = pipelineCount; i < PIPELINE_SIZE; i++)
			pipelineQ[i].clearPipelineObjects();
		for(i = 0; i < PIPELINE_SIZE; i++)
			tempPipelineQ[i].clearPipelineObjects();
		return OK;
	}


	// TODO Add comments. 
	int triggerPipelining()
	{
		do 
		{
			retireInstructions();
			executeInstructions();
			issueInstructions();
			dispatchInstructions();
			fetchInstructions();
		} while (!nextCycle());
		nsrPrintf("\nTotal Cycles: %d", totalCycles);
		return OK;
	}
};

#pragma endregion

#pragma region HELPER FUNCTIONS

/* Not required because I will be using the index to get the register
that I require. However, I am just doing this.	*/
int initializeRegisters()
{
	int i = 0;
	registers = new RegisterObject[REGISTER_COUNT];
	for(i = 0; i < REGISTER_COUNT; i++)
		registers[i].registerID = i;
	return OK;
}

/* Populate the instruction Queue. This will simply gobble up the entire
trace file and store it. This way I don't have to keep reading the file
over and over again. */
int populateInstructionQ()
{
	
	lineCount = 0;
	uint32_t address;
	int operation;
	int destinationRegister, sourceRegister1, sourceRegister2;
	FILE *inputFile;
	inputFile = fopen(fileName, "r");
	if(inputFile != NULL)
	{
		while(feof(inputFile) == 0)
		{
			fscanf(inputFile, "%x %d %d %d %d\n", &address, &operation, &destinationRegister, &sourceRegister1, &sourceRegister1);
			lineCount++;
		}
	}
	fclose(inputFile);
	long count = 0;
	instructionQ = new InstructionObject[lineCount];
	inputFile = fopen(fileName, "r");
	if(inputFile != NULL)
	{
		while(feof(inputFile) == 0)
		{
			fscanf(inputFile, "%x %d %d %d %d\n", &address, &operation, &destinationRegister, &sourceRegister1, &sourceRegister2);
			instructionQ[count].pcAddress = address;
			instructionQ[count].tag = count;
			instructionQ[count].type = operation;
			instructionQ[count].myRegisters[0].registerID = destinationRegister;
			instructionQ[count].myRegisters[1].registerID = sourceRegister1;
			instructionQ[count].myRegisters[2].registerID = sourceRegister2;
			if(operation == 0)
				instructionQ[count].executionCycles = 1;
			else if(operation == 1)
				instructionQ[count].executionCycles = 2;
			else if(operation == 2)
				instructionQ[count].executionCycles = 5;
			else
				nsrPrintf("\nGet it together trace file. Come on!!");
			count++;
		}
	}
	fclose(inputFile);
	return OK;
}

int printOutput(int S, int N)
{
	int i = 0;
	for(i = 0; i < lineCount; i++)
	{
		int startFetch, startDecode, startIssue, startExecute, startWB, fetchCycles, decodeCycles, issueCycles, executeCycles, wbCycles;
		startFetch = instructionQ[i].stateCycles[SC_FETCH];
		startDecode = instructionQ[i].stateCycles[SC_DECODE];
		startIssue = instructionQ[i].stateCycles[SC_ISSUE];
		startExecute = instructionQ[i].stateCycles[SC_EXECUTE];
		startWB = instructionQ[i].stateCycles[SC_WRITEBACK];
		fetchCycles = startDecode - startFetch;
		decodeCycles = startIssue - startDecode;
		issueCycles = startExecute - startIssue;
		executeCycles = startWB - startExecute;
		wbCycles = 1;
		printf("%d fu{%d} src{%d,%d} dst{%d} IF{%d,%d} ID{%d,%d} IS{%d,%d} EX{%d,%d} WB{%d,%d}\n", instructionQ[i].tag, instructionQ[i].type, instructionQ[i].myRegisters[1].registerID, instructionQ[i].myRegisters[2].registerID, instructionQ[i].myRegisters[0].registerID, 
			startFetch, fetchCycles, startDecode, decodeCycles, startIssue, issueCycles, startExecute, executeCycles, startWB, wbCycles);
	}
	printf("CONFIGURATION\n");
	printf("superscalar bandwidth (N) = %d\n", N);
	printf("dispatch queue size (2*N) = %d\n", 2*N);
	printf("schedule queue size (S)   =  %d\n", S);
	printf("RESULTS\n");
	printf("number of instructions = %d\n", lineCount);
	printf("number of cycles = %d\n", pipeline->totalCycles);
	printf("IPC = %.2f", ((lineCount * 1.0)/pipeline->totalCycles));
	return OK;
}
#pragma endregion

#pragma region MAIN
int main(int argc, char **argv)
{
	fileName = new char[READSIZE];
	int s, n;
	s = atoi(argv[1]);
	n = atoi(argv[2]);
	fileName = argv[3];
	if(argc == 5)
	{
		if(strcmp(argv[4], "-debug") == 0)
			nsrDebugPrintf = 1;
	}
	
	initializeRegisters();
	populateInstructionQ();
	pipeline = new Pipeline(s, n);
	pipeline->triggerPipelining();
	printOutput(s, n);
	return OK;
}

#pragma endregion