set terminal png
set output "./graph5.png"
set datafile separator ","
set title "Cache Size(L1) vs AAT"
set xlabel "Cache Size L1 (Log2 Scale)"
set ylabel "AAT (ns)"
plot "./CS32_5.txt" using 1:2 with linespoints  title "Cache Size L2: 32KB", "./CS64_5.txt" using 1:2 with linespoints  title "Cache Size L2: 64KB", "./CS128_5.txt" using 1:2 with linespoints  title "Cache Size L2: 128KB", "./CS256_5.txt" using 1:2 with linespoints  title "Cache Size L2: 256KB", "./CS512_5.txt" using 1:2 with linespoints  title "Cache Size L2: 512KB", "./CS1024_5.txt" using 1:2 with linespoints  title "Cache Size L2: 1MB"

