set terminal png
set output "./graph4.png"
set datafile separator ","
set title "Block Size vs Miss Rate"
set xlabel "Block Size (Log2 Scale)"
set ylabel "Miss Rate"
plot "./CS1_4.txt" using 1:2 with linespoints  title "Cache Size: 1KB", "./CS2_4.txt" using 1:2 with linespoints  title "Cache Size: 2KB", "./CS4_4.txt" using 1:2 with linespoints  title "Cache Size: 4KB", "./CS8_4.txt" using 1:2 with linespoints  title "Cache Size: 8KB", "./CS16_4.txt" using 1:2 with linespoints  title "Cache Size: 16KB", "./CS32_4.txt" using 1:2 with linespoints  title "Cache Size: 32KB"


