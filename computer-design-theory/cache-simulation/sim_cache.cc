// CacheSimulation_2.cpp : Defines the entry point for the console application.
//

#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include<string>
#include<string.h>
#include<iostream>
#include<fstream>
#define nsrPrintf(...) if(nsrDebugPrintf) { printf(__VA_ARGS__);}
int nsrDebugPrintf;
using namespace std;

#define DIRTY 1
#define CLEAN 0
#define VALID 1
#define INVALID 0
#define RANK_UNDEFINED -1
#define READSIZE 30
#define OK 100
#define SYSERR -100
#define ACTIVE 1
#define INACTIVE -1
#define CL1 1
#define CL2 2
#define SB1 11
#define SB2 12
#define MEM 20

//Some cache variables which will be used strictly for diferentiating. 
#define CACHE_OK 1000
#define CACHE_RETURN_BLOCK_DIRTY -1

#define STREAMBUFFER_HIT 50
#define STREAMBUFFER_MISS 51

#define READ_ORIGINATING_FROM_PREFETCH 1
#define READ_NOT_ORIGINATING_FROM_PREFETCH 0

//These variables will be used to indicate whether the respective caches
//and stream buffers are valid or not. 
int sb1, sb2, cl1, cl2, blockSize, cacheSizeL1, cacheSizeL2, assocL1, assocL2, prefNL1, prefML1, prefNL2, prefML2;
string traceFile;
char *fileName;
class cacheBlock;
class Cache;
Cache *cacheL1, *cacheL2;
//Helper functions
int log2(int value)
{
	double valueDouble = (double)value;
	return ((int)((log10(valueDouble))/log10(2.0)));
}

class cacheBlock
{
public:
	unsigned long tagBits;
	int indexBits;
	unsigned long offsetBits;
	unsigned long address;
	int validBit;
	int dirtyBit;
	int lruRank;
public:
	cacheBlock()
	{
		//INVALID means that the block is not being used.
		validBit = INVALID;
		dirtyBit = CLEAN;
		lruRank = RANK_UNDEFINED; 
	}

	bool isDirty()
	{
		if(dirtyBit == DIRTY)
			return true;
		else
			return false;
	}

	bool isValid()
	{
		if(validBit == VALID)
			return true;
		else
			return false;
	}

	int getRank()
	{
		return lruRank;
	}

	bool setCacheBlock(unsigned long tag, int index, char operation, unsigned long addr)
	{
		validBit = VALID;
		tagBits = tag; 
		indexBits = index;
		address = addr;
		if(operation == 'w')
			dirtyBit = DIRTY;
		else
			dirtyBit = CLEAN;
		return true;
	}

	bool setStreamBuffer(unsigned long addr)
	{
		validBit = VALID;
		address = addr;
		return true;
	}

	bool invalidate()
	{
		//lruRank = RANK_UNDEFINED;
		dirtyBit = CLEAN;
		validBit = INVALID;
		return true;
	}

	bool setDirty()
	{
		dirtyBit = DIRTY;
		return true;
	}

	unsigned long getIndex()
	{
		return indexBits;
	}

	unsigned long getTag()
	{
		return tagBits;
	}

	unsigned long getOffset()
	{
		return offsetBits;
	}

	unsigned long getAddress()
	{
		return address;
	}
};

class Cache
{
public:
	int cacheID;
	int readCount;
	int readMisses;
	double missRate;
	int writeCount;
	int writeMisses;
	int writeBackCount;
	int prefetchCount;
	int memoryTraffic;
	cacheBlock **cache; 
	cacheBlock **streamBuffer;
	int readStatus;
	int associativity;
	int setCount;
	int blockSize;
	int cacheSize;
	int nextMemoryLevel;
	int myStreamBuffer;
	int prefetchFromL1;
	int readFromPrefetch;
	int readNotFromPrefetch;
	int readMissFromPrefetch;
	int readMissNotFromPrefetch;
	//These two variables will be used to find the Tag address. Not needed for anything else. 
	int offsetBitCount;
	int setsBitCount; 
	int prefN, prefM;
public:
	Cache(int assoc, int blkSize, int totalSize, int ID, int streamBufferCount, int streamBufferLength)
	{
		nextMemoryLevel = 0; 
		cacheID = ID;
		//These parameters tell us what the next level of memory is for each cache. 
		if(cacheID == CL1)
		{
			if(cl2 == ACTIVE)
				nextMemoryLevel = CL2;
			else
				nextMemoryLevel = MEM;
		}
		if(cacheID == CL2)
		{
			nextMemoryLevel = MEM;
		}
		readCount = readMisses = writeCount = writeMisses = writeBackCount = writeMisses = prefetchCount = memoryTraffic = prefetchFromL1 = 0;
		readStatus = -1;
		readFromPrefetch = readNotFromPrefetch = readMissFromPrefetch = readMissNotFromPrefetch = 0;
		associativity = assoc;
		blockSize = blkSize;
		cacheSize = totalSize;
		setCount = cacheSize/(blockSize * associativity);
		cache = new cacheBlock*[setCount];
		int i, j;
		i = j = 0;
		for(i = 0; i < setCount; i++)
		{
			cache[i] = new cacheBlock[associativity];
			//This can be removed as the constructor will be called. Keeping it for the time being. 
			for(j = 0; j < associativity; j++)
			{
				cache[i][j].invalidate();
				cache[i][j].lruRank = j;
			}
		}
		//Initialize Stream Buffers
		if(streamBufferLength > 0)
		{
			initializeStramBuffers(streamBufferCount, streamBufferLength);
			myStreamBuffer = ACTIVE;
		}
		else
			myStreamBuffer = INACTIVE;
		offsetBitCount = log2(blockSize);
		setsBitCount = log2(setCount);
	}

	//This method initializes the stream buffers and sets the prefetch details. 
	int initializeStramBuffers(int streamBufferCount, int streamBufferLength)
	{
		prefN = streamBufferCount;
		prefM = streamBufferLength;
		int i, j;
		i = j = 0;
		streamBuffer = new cacheBlock*[prefN];
		for(i = 0; i < prefN; i++)
		{
			streamBuffer[i] = new cacheBlock[prefM];
			//Invalidate can be removed as the constructor for CacheBlock will be called. 
			//Keeping it for the time being. 
			for(j = 0; j < prefM; j++)
			{
				streamBuffer[i][j].invalidate();
				streamBuffer[i][j].lruRank = i;
			}
		}
		return OK;
	}

	//This method will calculate the tag address based on the given address. 
	unsigned long calculateTag(unsigned long address)
	{
		unsigned long tag; 
		tag = address >> (offsetBitCount + setsBitCount);
		return tag;
	}

	//This method will calculate the index/set based on the address given. 
	int calculateIndex(unsigned long address)
	{
		unsigned long n = (unsigned long)(pow(2.0, setsBitCount));
		n--;
		unsigned long index = (address >> offsetBitCount) & n;
		return (int)index; 
	}
	
	//This method calculates the block address for a given address i.e.
	//It calculates the address without the offset bits. 
	unsigned long calculateAddressForStreamBuffer(unsigned long address)
	{
		unsigned long returnAddress = address >> offsetBitCount;
		returnAddress = returnAddress << offsetBitCount;
		return returnAddress;
	}

	//This will calculate the next address that has to be inserted into 
	//the stream. 
	unsigned long calculateNextAddressForStreamBuffer(unsigned long address)
	{
		unsigned long returnAddress = address >> offsetBitCount;
		returnAddress++;
		returnAddress = returnAddress << offsetBitCount;
		return returnAddress;
	}

	//This method is used to check of the block is present in the cache. 
	bool findBlock(unsigned long address)
	{
		unsigned long tagID = calculateTag(address);
		unsigned long setID = calculateIndex(address);
		int j = 0;
		for(j = 0; j < associativity; j++)
		{
			if(cache[setID][j].tagBits == tagID && cache[setID][j].validBit == VALID)
			{
				return true;
			}
		}
		return false;
	}

	//Find the block in the stream buffer. This method is called only when
	//we know for sure it is in the stream buffer. The reason for duplication
	//is because I don't know what the prefM will be for the cache and I don't
	//want the assigned preprocessor value to be equal to the streamID. I 
	//could always assing a negative value, but for the sake easier execution
	//I have chosen this method. 
	int findInStreamBuffer(unsigned long address)
	{
		int i = 0;	
		int k = 0;
		for(i = 0; i < prefN; i++)
		{
			if((streamBuffer[i][0].address == calculateAddressForStreamBuffer(address)) && (streamBuffer[i][0].isValid()))
			{
				return i;
			}
		}
		nsrPrintf("\nThis case should not be happening. Check trigger cache and lookInCacheAndStream for errors.");
		return SYSERR;
	}

	//Checks if the block is present. If the block is present it will
	//call the update LRU method and update the rank of all the blocks 
	//in that set. If the block is not present it will simply return false. 
	bool isBlockPresent(unsigned long address, char operation)
	{
		unsigned long tagID = calculateTag(address);
		unsigned long setID = calculateIndex(address);
		int j = 0;
		for(j = 0; j < associativity; j++)
		{
			if(cache[setID][j].tagBits == tagID && cache[setID][j].validBit == VALID)
			{
				updateRankByLRU(setID, j);
				if(operation == 'w')
					cache[setID][j].setCacheBlock(tagID, setID, operation, address);
				return true;
			}
		}
		return false; 
	}

	//This method is used to look in the Cache and Stream buffers. If it
	//was a cache hit, the LRU is updated and OK is returned. If it misses
	//the stream buffers are checked. If it hits in the first block of the
	//the stream buffers, the LRU rank of that stream is updated and stream
	//buffer hit is returned. Else stream buffer miss is returned. 
	int lookInCacheAndStream(unsigned long address, char operation)
	{
		if(isBlockPresent(address, operation))
			return OK;
		//This means that it was absent in the Cache. Look in the stream Buffers.
		//For the sake of consistency, I am using i for iterating over rows and j
		//for iterating over columns. We use address here instead of tags because
		//tags could be the same. The address that is stored in the stream buffer
		//is the block address and not the actual address i.e. the starting address
		//of the block. If the block exists in the stream buffer, the block is moved
		//to the cache, and the next block is brought in. 
		int i = 0;	
		int k = 0;
		for(i = 0; i < prefN; i++)
		{
			if((streamBuffer[i][0].address == calculateAddressForStreamBuffer(address)) && (streamBuffer[i][0].isValid()))
			{
				updateLRUForStreamBuffer(i);
				return STREAMBUFFER_HIT;
			}
		}
		//Missed in both Cache and Stream Buffer. 
		return STREAMBUFFER_MISS;
	}

	//This method will look in the stream buffers and invalidate all blocks
	//based on the given address. 
	int lookInStreamBufferAndInvalidate(unsigned long address)
	{
		unsigned long tagID = calculateTag(address);
		int i, j;
		i = j = 0;
		for(i = 0; i < prefN; i++)
		{
			for(j = 0; j < prefM; j++)
			{
				if((streamBuffer[i][j].address == calculateAddressForStreamBuffer(address)))
				{
					streamBuffer[i][j].validBit = INVALID;
				}
			}
		}
		return OK;
	}

	//This method is used to allocate a block in the cache. If the block being
	//evicted is invalid, the assocID is returned. If it is valid but not dirty
	//once again the blockID/assocID is returned. If valid and dirty, then the 
	//CACHE_RETURN_BLOCK_DIRTY is returned. This will be used in order to issue
	//write requests to lower levels and for incrementing the write back counts. 
	unsigned long allocateCacheBlock(unsigned long address)
	{
		unsigned long tagID = calculateTag(address);
		int setID = calculateIndex(address);
		int assocID = getLRUBlock(setID);
		if(!cache[setID][assocID].isValid())
			return assocID;
		else if(cache[setID][assocID].isValid() && !cache[setID][assocID].isDirty())
			return assocID;
		else if(cache[setID][assocID].isValid() && cache[setID][assocID].isDirty())
			return CACHE_RETURN_BLOCK_DIRTY;
		nsrPrintf("\nSomething is wrong with your Allocate Cache Block implementation. Check code again.");
		return SYSERR;
	}

	//This method simply inserts the block into the cache based on the address. 
	int insertBlockInCache(unsigned long address, char operation)
	{
		unsigned long tagID = calculateTag(address);
		int setID = calculateIndex(address);
		int assocID = getLRUBlock(setID);
		updateRankByLRU(setID, assocID);
		cache[setID][assocID].setCacheBlock(tagID, setID, operation, address);
		return OK;
	}

	//This method will be used to insert a block in the Stream. By default
	//It will insert it at the end. It is like a queue implementation. It
	//will then push every other block ahead of it. Once this is done,
	//update the LRU Rank for the stream buffer. 
	int insertInStreamBuffer(int streamID)
	{
		if(nextMemoryLevel == MEM)
		{
			int i, j;
			i = j = 0;
			unsigned long addressToInsert = streamBuffer[streamID][0].address;
			if(!streamBuffer[streamID][0].isValid())
				nsrPrintf("\nThe hell");
			for(j = 0; j < (prefM - 1); j++)
			{
				streamBuffer[streamID][j].setStreamBuffer(streamBuffer[streamID][j + 1].address);
				if(!streamBuffer[streamID][j + 1].isValid())
					streamBuffer[streamID][j].invalidate();
				addressToInsert = streamBuffer[streamID][j].address;
			}
			addressToInsert = calculateNextAddressForStreamBuffer(addressToInsert);
			streamBuffer[streamID][prefM - 1].setStreamBuffer(addressToInsert);
			return OK;
		}
		else if(nextMemoryLevel == CL2)
		{
			int i, j;
			i = j = 0;
			unsigned long addressToInsert = streamBuffer[streamID][0].address;
			if(!streamBuffer[streamID][0].isValid())
				nsrPrintf("\nThe hell");
			for(j = 0; j < (prefM - 1); j++)
			{
				streamBuffer[streamID][j].setStreamBuffer(streamBuffer[streamID][j + 1].address);
				if(!streamBuffer[streamID][j + 1].isValid())
					streamBuffer[streamID][j].invalidate();
				addressToInsert = streamBuffer[streamID][j].address;
			}
			addressToInsert = calculateNextAddressForStreamBuffer(addressToInsert);
			cacheL2->readStatus = READ_ORIGINATING_FROM_PREFETCH;
			cacheL2->triggerCache(addressToInsert, 'r');
			streamBuffer[streamID][prefM - 1].setStreamBuffer(addressToInsert);
			return OK;
		}
	}

	int emptyAndRefillStream(unsigned long address)
	{
		int j = 0;
		int streamID = getLRUStream();
		unsigned long addressToInsert = calculateNextAddressForStreamBuffer(address);
		if(nextMemoryLevel == MEM)
		{
			for(j = 0; j < prefM; j++)
			{
				streamBuffer[streamID][j].setStreamBuffer(addressToInsert);
				streamBuffer[streamID][j].validBit = VALID;
				addressToInsert = calculateNextAddressForStreamBuffer(addressToInsert);
			}
			updateLRUForStreamBuffer(streamID);
		}
		else if(nextMemoryLevel == CL2)
		{
			for(j = 0; j < prefM; j++)
			{
				//Issue a read to L2. Once that is done, the block will be present in L2.
				//It can now be brought into the Stream buffer. 
				cacheL2->readStatus = READ_ORIGINATING_FROM_PREFETCH;
				cacheL2->triggerCache(addressToInsert, 'r');
				streamBuffer[streamID][j].setStreamBuffer(addressToInsert);
				streamBuffer[streamID][j].validBit = VALID;
				addressToInsert = calculateNextAddressForStreamBuffer(addressToInsert);
			}
			cacheL2->prefetchFromL1+=prefM;
			updateLRUForStreamBuffer(streamID);
		}
		return OK;
	}

	//Iterate over the set and update the rank of all the blocks. By my
	//algorithm the LRU block is ranked associativity - 1(the last rank).
	//Therefore the MRU block would have a rank of 0. Hence for this, all
	//blocks ranks are increased and the block which was accessed, it's rank
	//is set to 0. 
	int updateRankByLRU(int setID, int assocID)
	{
		int j = 0;
		int rankToModify = cache[setID][assocID].lruRank;
		for(j = 0; j < associativity; j++)
		{
			if(cache[setID][j].lruRank < rankToModify)
				cache[setID][j].lruRank++;
		}
		cache[setID][assocID].lruRank = 0;
		return OK;
	}
	
	//This method looks at the first block of every stream buffer to updates
	//the rank based on LRU. It updates it for every block in that stream.
	//This ensures that as we move blocks up the stream, we dont need to set
	//rank again and again. All that logic is taken care of here. 
	int updateLRUForStreamBuffer(int streamBufferID)
	{
		int i, j;
		i = j = 0;
		int rankToModify = streamBuffer[streamBufferID][0].lruRank;
		for(i = 0; i < prefN; i++)
		{
			if(streamBuffer[i][0].lruRank < rankToModify)
			{
				for(j = 0; j < prefM; j++)
				{
					streamBuffer[i][j].lruRank++;
				}
			}
		}
		for(j = 0; j < prefM; j++)
		{
			streamBuffer[streamBufferID][j].lruRank = 0;
		}
		return OK;
	}

	//Returns the block ID of the LRU block. 
	int getLRUBlock(int setID)
	{
		int j = 0;
		int a = 0;
		int set = 0;
		for(j = 0; j < associativity; j++)
		{
			if(cache[setID][j].lruRank == associativity - 1)
			{
				a = j;
				set++;
				if(set > 1)
					nsrPrintf("\nThis should not be happening. ");
			}
		}
		return a;
	}

	//Returns the stream ID for the LRU stream. 
	int getLRUStream()
	{
		int i = 0;
		for(i = 0; i < prefN; i++)
		{
			if(streamBuffer[i][0].lruRank == prefN - 1)
			{
				return i;
			}
		}
		nsrPrintf("\nSomething is wrong with your LRU implementation for Stream Buffers. Check code again.");
		return SYSERR;
	}

	//This method will be called for when the stream buffers are inactive. 
	int streamBuffersInactive(unsigned long address, char operation)
	{
		int status;
		if(nextMemoryLevel == MEM)
		{
			if(isBlockPresent(address, operation))
			{
				if(operation == 'r')
				{
					readCount++;
					if(readStatus == READ_ORIGINATING_FROM_PREFETCH)
						readFromPrefetch++;
					else if(readStatus == READ_NOT_ORIGINATING_FROM_PREFETCH)
						readNotFromPrefetch++;
				}
				else
					writeCount++;
			}
			else
			{
				status = allocateCacheBlock(address);
				if(operation == 'r')
				{
					if(readStatus == READ_ORIGINATING_FROM_PREFETCH)
					{
						readMissFromPrefetch++;
						readFromPrefetch++;
					}
					else if(readStatus == READ_NOT_ORIGINATING_FROM_PREFETCH)
					{
						readNotFromPrefetch++;
						readMissNotFromPrefetch++;
					}
					readMisses++;
					readCount++;
					memoryTraffic++;
				}
				else
				{
					writeMisses++;
					writeCount++;
					memoryTraffic++;
				}
				if(status != CACHE_RETURN_BLOCK_DIRTY)
					insertBlockInCache(address, operation);
				else
				{
					insertBlockInCache(address, operation);
					writeBackCount++;
					memoryTraffic++;
				}
			}
		}
		else if(nextMemoryLevel == CL2)	
		{	
			if(isBlockPresent(address, operation))
			{
				if(operation == 'r')
					readCount++;
				else
					writeCount++;
			}
			//The block is not present in L1. Move to L2. 
			else
			{
				//Find the block to evict in L1 and if dirty, it should
				//issue a write request to L2. 
				status = allocateCacheBlock(address);
				if(operation == 'r')
				{
					readMisses++;
					readCount++;
				}
				else
				{
					writeMisses++;
					writeCount++;
				}
				//If the block returned is not dirty, the go ahead and assign that
				//insert that block in L1 and bring the same block into L2. 
				if(status != CACHE_RETURN_BLOCK_DIRTY)
				{
					//This will take care of everything. If the cache block is present
					//in L2, then the readCount will be incremented. If it is not present
					//then it will goto the next level i.e. memory and do we should be fine. 
					cacheL2->readStatus = READ_NOT_ORIGINATING_FROM_PREFETCH;
					cacheL2->triggerCache(address, 'r');
					//The block is now in L2 and hence can be inserted into L1 and the block
					//being evicted can be removed without any hesitations as there are no
					//dirty blocks being evicted. 
					insertBlockInCache(address, operation);
				}
				else
				{
					unsigned long tagID = calculateTag(address);
					int setID = calculateIndex(address);
					int assocID = getLRUBlock(setID);
					//Now we know which block is going to be evicted. We can now issue a 
					//write to L2 with that address and make sure L2, handles it properly.
					writeBackCount++;
					cacheL2->triggerCache(cache[setID][assocID].address, 'w');
					//Once the block has been sent to L2 and L2 has updated that block
					//status, we can now proceed with issuing a read to L2 and then
					//inserting the block into L1. 
					cacheL2->readStatus = READ_NOT_ORIGINATING_FROM_PREFETCH;
					cacheL2->triggerCache(address, 'r');
					//The block is now in L2 and hence can be inserted into L1 and the block
					//being evicted can be removed without any hesitations as it has already
					//been written back to L2.  
					insertBlockInCache(address, operation);
				}
			}
		}
		return OK;
	}


	int streamBuffersActive(unsigned long address, char operation)
	{
		int streamStatus;
		int cacheStatus;
		if(nextMemoryLevel == MEM)
		{
			streamStatus = lookInCacheAndStream(address, operation);
			//This means there was a cache hit. So just update the read/write
			//counter. 
			if(streamStatus == OK)
			{
				if(operation == 'r')
				{
					readCount++;
					if(readStatus == READ_ORIGINATING_FROM_PREFETCH)
						readFromPrefetch++;
					else if(readStatus == READ_NOT_ORIGINATING_FROM_PREFETCH)
						readNotFromPrefetch++;
				}
				else
					writeCount++;
			}
			else if(streamStatus == STREAMBUFFER_HIT)
			{
				int streamID = findInStreamBuffer(address);
				//I now have the stream where the block is present. 
				cacheStatus = allocateCacheBlock(address);
				if(operation == 'r')
				{
					readCount++;
					if(readStatus == READ_ORIGINATING_FROM_PREFETCH)
					{
						readFromPrefetch++;
					}
					else if(readStatus == READ_NOT_ORIGINATING_FROM_PREFETCH)
					{
						readNotFromPrefetch++;
					}
				}
				else
				{
					writeCount++;
				}
				if(cacheStatus != CACHE_RETURN_BLOCK_DIRTY)
					insertBlockInCache(address, operation);
				else
				{
					//A dirty block has been evicted. Check your Stream and invalidate
					//all the necessary blocks. 
					unsigned long tagID = calculateTag(address);
					int setID = calculateIndex(address);
					int assocID = getLRUBlock(setID);
					lookInStreamBufferAndInvalidate(cache[setID][assocID].address);
					insertBlockInCache(address, operation);
					writeBackCount++;
					memoryTraffic++;
				}
				//A block has been allocated and the address has been inserted
				//in the cache. Now remove that block from the Stream buffer
				//insert the next address block. 
				insertInStreamBuffer(streamID);
				//The block is now in the stream buffer. Update the LRU of that
				//stream.
				updateLRUForStreamBuffer(streamID);
				//Prefetching would involve bringing a block from the memory so
				//update the memory traffic. 
				memoryTraffic++;
				prefetchCount++;
			}
			//It has missed everywhere. 
			else if(streamStatus == STREAMBUFFER_MISS)
			{
				cacheStatus = allocateCacheBlock(address);
				if(operation == 'r')
				{
					readMisses++;
					readCount++;
					if(readStatus == READ_ORIGINATING_FROM_PREFETCH)
					{
						readMissFromPrefetch++;
						readFromPrefetch++;
					}
					else if(readStatus == READ_NOT_ORIGINATING_FROM_PREFETCH)
					{
						readNotFromPrefetch++;
						readMissNotFromPrefetch++;
					}
				}
				else
				{
					writeMisses++;
					writeCount++;
				}
				if(cacheStatus != CACHE_RETURN_BLOCK_DIRTY)
					insertBlockInCache(address, operation);
				else
				{
					//A dirty block has been evicted. Check your Stream and invalidate
					//all the necessary blocks. 
					unsigned long tagID = calculateTag(address);
					int setID = calculateIndex(address);
					int assocID = getLRUBlock(setID);
					lookInStreamBufferAndInvalidate(cache[setID][assocID].address);

					insertBlockInCache(address, operation);
					writeBackCount++;
					memoryTraffic++;
				}
				//Once the block is in the cache, just go ahead and bring all the 
				//new blocks(next addresses) into the stream buffer. 
				memoryTraffic++;
				memoryTraffic = memoryTraffic + prefM;
				prefetchCount = prefetchCount + prefM;
				int streamID = getLRUStream();
				emptyAndRefillStream(address);
			}
		}

		//The next level in the memory hierarchy is the Cache Level 2. 
		else if(nextMemoryLevel == CL2)
		{
			streamStatus = lookInCacheAndStream(address, operation);
			//This means there was a cache hit. So just update the read/write
			//counter. 
			if(streamStatus == OK)
			{
				if(operation == 'r')
					readCount++;
				else
					writeCount++;
			}
			else if(streamStatus == STREAMBUFFER_HIT)
			{
				int streamID = findInStreamBuffer(address);
				//I now have the stream where the block is present. 
				cacheStatus = allocateCacheBlock(address);
				if(operation == 'r')
					readCount++;
				else
					writeCount++;
				//If the block being evicted is clean, we don't need to worry.
				//Just go ahead and remove it. 
				if(cacheStatus != CACHE_RETURN_BLOCK_DIRTY)
					insertBlockInCache(address, operation);
				else
				{
					//The block being evicted is dirty. Look in the stream buffers
					//and invalidate all copies. 
					unsigned long tagID = calculateTag(address);
					int setID = calculateIndex(address);
					int assocID = getLRUBlock(setID);
					lookInStreamBufferAndInvalidate(cache[setID][assocID].address);
					//Once all copies have been invalidated, issue a write request to
					//L2 and send the dirty block there. 
					writeBackCount++;
					cacheL2->triggerCache(cache[setID][assocID].address, 'w');
					//The dirty block has been moved to L2. Now we can go ahead and
					//insert the new block in L1. 
					insertBlockInCache(address, operation);
				}
				//A block has been allocated and the address has been inserted
				//in the cache. Now remove that block from the Stream buffer
				//insert the next address block. 
				insertInStreamBuffer(streamID);
				//The block is now in the stream buffer. Update the LRU of that
				//stream.
				updateLRUForStreamBuffer(streamID);
				//Increment the prefetch count.
				prefetchCount++;
				cacheL2->prefetchFromL1++;
			}
			//There was a stream buffer miss. So now we have to do the following.
			//Firstly allocate a new block in L1. If the block is clean, then good
			//else issue a write request to lower level cache. Once the block has
			//been moved to L2, bring the new block into the cache L2, and then L1.
			//Once this has been done, find the LRU stream and empty and refill it.
			//Emptying and refilling in this case would involve issuing read requests
			//to the lower level cache. This functionality is already present in the
			//emptyAndRefill... method. Just use that. 
			else if(streamStatus == STREAMBUFFER_MISS)
			{
				cacheStatus = allocateCacheBlock(address);
				if(operation == 'r')
				{
					readCount++;
					readMisses++;
				}
				else
				{
					writeCount++;
					writeMisses++;
				}
				if(cacheStatus != CACHE_RETURN_BLOCK_DIRTY)
				{
					//This will take care of everything. If the cache block is present
					//in L2, then the readCount will be incremented. If it is not present
					//then it will goto the next level i.e. memory and do we should be fine. 
					cacheL2->readStatus = READ_NOT_ORIGINATING_FROM_PREFETCH;
					cacheL2->triggerCache(address, 'r');
					//The block is now in L2. So just go ahead and inser that block in L1. 
					insertBlockInCache(address, operation);
				}
				else if(cacheStatus == CACHE_RETURN_BLOCK_DIRTY)
				{
					//A dirty block is being evicted. 
					unsigned long tagID = calculateTag(address);
					int setID = calculateIndex(address);
					int assocID = getLRUBlock(setID);
					//Now we know which block is going to be evicted. We can now issue a 
					//write to L2 with that address and make sure L2, handles it properly.
					writeBackCount++;
					cacheL2->triggerCache(cache[setID][assocID].address, 'w');
					lookInStreamBufferAndInvalidate(cache[setID][assocID].address);
					//Once the block has been sent to L2 and L2 has updated that block
					//status, we can now proceed with issuing a read to L2 and then
					//inserting the block into L1. 
					cacheL2->readStatus = READ_NOT_ORIGINATING_FROM_PREFETCH;
					cacheL2->triggerCache(address, 'r');
					//The block is now in L2 and hence can be inserted into L1 and the block
					//being evicted can be removed without any hesitations as it has already
					//been written back to L2.  
					insertBlockInCache(address, operation);
				}
				//Now we empty and refill the LRU stream. 
				int streamID = getLRUStream();
				emptyAndRefillStream(address);
				prefetchCount+=prefM;
			}

		}
		return OK;
	}
	
	//The logic is implemented here. Find inline comments. 
	int triggerCache(unsigned long address, char operation)
	{
		//The reason I have split this up is because it helps while debugging.
		//Further this does not require regression testing as none of the existing
		//functionality will be affected in any way. In order to ensure that, new
		//methods are created while keeping the existing methods just the way they
		//were. 
		if(myStreamBuffer == INACTIVE)
			streamBuffersInactive(address, operation);
		else if(myStreamBuffer == ACTIVE)
			streamBuffersActive(address, operation);
		return OK;
	}

};

//This method is used to print the output in the specified format. 
bool printOutput()
{
	printf("===== Simulator configuration =====");
	printf("\n BLOCKSIZE:\t%d", blockSize);
	printf("\n L1_SIZE:\t%d", cacheSizeL1);
	printf("\n L1_ASSOC:\t%d", assocL1);
	printf("\n L1_PREF_N:\t%d", prefNL1);
	printf("\n L1_PREF_M:\t%d", prefML1);
	printf("\n L2_SIZE:\t%d", cacheSizeL2);
	printf("\n L2_ASSOC:\t%d", assocL2);
	printf("\n L2_PREF_N:\t%d", prefNL2);
	printf("\n L2_PREF_M:\t%d", prefML2);
	printf("\n trace_file:\t %s", fileName);
	if(cl1 == ACTIVE)
	{
		printf("\n===== L1 contents =====");
		int i, j, k;
		i = j = 0;
		//Selection Sort is implemented as the blocks need to be printed in
		//MRU to LRU format. 
		int currentRank = 0;
		for(i = 0; i < cacheL1->setCount; i++)
		{
			printf("\nSet\t%d:", i);
			currentRank = 0;
			for(j = 0; j < cacheL1->associativity; j++)
			{
				for(k = 0; k < cacheL1->associativity; k++)
				{
					if(cacheL1->cache[i][k].lruRank == currentRank)
					{
						currentRank++;
						printf("\t%x ", cacheL1->cache[i][k].tagBits);
						if(cacheL1->cache[i][k].isDirty())
							printf("D\t");
						else
							printf(" \t");
						break;
					}
					
				}
			}
		}
	}
	if(sb1 == ACTIVE)
	{
		printf("\n===== L1-SB contents =====");
		int i, j, k;
		int currentRank = 0;
		for(i = 0; i < cacheL1->prefN; i++)
		{
			printf("\n");
			for(j = 0; j < cacheL1->prefN; j++)
			{
				if(cacheL1->streamBuffer[j][0].getRank() == currentRank)
				{
					for(k = 0; k < cacheL1->prefM; k++)
					{
						printf("\t%x", cacheL1->streamBuffer[j][k].address);
					}
					currentRank++;
					break;
				}
			}
		}
	}
	if(cl2 == ACTIVE)
	{
		printf("\n===== L2 contents =====");
		int i, j, k;
		i = j = 0;
		//Selection Sort is implemented as the blocks need to be printed in
		//MRU to LRU format. 
		int currentRank = 0;
		for(i = 0; i < cacheL2->setCount; i++)
		{
			printf("\nSet\t%d:", i);
			currentRank = 0;
			for(j = 0; j < cacheL2->associativity; j++)
			{
				for(k = 0; k < cacheL2->associativity; k++)
				{
					if(cacheL2->cache[i][k].lruRank == currentRank)
					{
						currentRank++;
						printf("\t%x ", cacheL2->cache[i][k].tagBits);
						if(cacheL2->cache[i][k].isDirty())
							printf("D\t");
						else
							printf(" \t");
						break;
					}
					
				}
			}
		}
	}
	if(sb2 == ACTIVE)
	{
		printf("\n===== L2-SB contents =====");
		int i, j, k;
		int currentRank = 0;
		for(i = 0; i < cacheL2->prefN; i++)
		{
			printf("\n");
			for(j = 0; j < cacheL2->prefN; j++)
			{
				if(cacheL2->streamBuffer[j][0].getRank() == currentRank)
				{
					for(k = 0; k < cacheL2->prefM; k++)
					{
						printf("\t%x", cacheL2->streamBuffer[j][k].address);
					}
					currentRank++;
					break;
				}
			}
		}
	}
	if(cl2 == ACTIVE)
	{
		printf("\n===== Simulation results (raw) =====");
		printf("\na. number of L1 reads:\t\t%d", cacheL1->readCount); 
		printf("\nb. number of L1 read misses:\t\t%d", cacheL1->readMisses); 
		printf("\nc. number of L1 writes:\t\t%d", cacheL1->writeCount); 
		printf("\nd. number of L1 write misses:\t\t%d", cacheL1->writeMisses); 
		printf("\ne. L1 miss rate:\t\t%f", (double)(cacheL1->readMisses + cacheL1->writeMisses)/(cacheL1->readCount + cacheL1->writeCount)); 
		printf("\nf. number of L1 writebacks:\t\t%d", cacheL1->writeBackCount); 
		printf("\ng. number of L1 prefetches:\t\t%d", cacheL1->prefetchCount); 
		printf("\nh. number of L2 reads that did not originate from L1 prefetches:\t\t%d", cacheL2->readNotFromPrefetch); 
		printf("\ni. number of L2 read misses that did not originate from L1 prefetches:\t\t%d", cacheL2->readMissNotFromPrefetch); 
		printf("\nj. number of L2 reads that originated from L1 prefetches:\t\t%d", cacheL2->readFromPrefetch); 
		printf("\nk. number of L2 read misses that originated from L1 prefetches:\t\t%d", cacheL2->readMissFromPrefetch); 
		printf("\nl. number of L2 writes:\t\t%d", cacheL2->writeCount); 
		printf("\nm. number of L2 write misses:\t\t%d", cacheL2->writeMisses); 
		printf("\nn. L2 miss rate:\t\t%f", (double)(cacheL2->readMissNotFromPrefetch)/(cacheL2->readNotFromPrefetch)); 
		printf("\no. number of L2 writebacks:\t\t%d", cacheL2->writeBackCount); 
		printf("\np. number of L2 prefetches:\t\t%d", cacheL2->prefetchCount); 
		printf("\nq. total memory traffic:\t\t%d", cacheL2->memoryTraffic); 
	}
	else
	{
		printf("\n===== Simulation results (raw) =====");
		printf("\na. number of L1 reads:\t\t%d", cacheL1->readCount); 
		printf("\nb. number of L1 read misses:\t\t%d", cacheL1->readMisses); 
		printf("\nc. number of L1 writes:\t\t%d", cacheL1->writeCount); 
		printf("\nd. number of L1 write misses:\t\t%d", cacheL1->writeMisses); 
		printf("\ne. L1 miss rate:\t\t%f", (double)(cacheL1->readMisses + cacheL1->writeMisses)/(cacheL1->readCount + cacheL1->writeCount)); 
		printf("\nf. number of L1 writebacks:\t\t%d", cacheL1->writeBackCount); 
		printf("\ng. number of L1 prefetches:\t\t%d", cacheL1->prefetchCount); 
		printf("\nh. number of L2 reads that did not originate from L1 prefetches:\t\t%d", 0); 
		printf("\ni. number of L2 read misses that did not originate from L1 prefetches:\t\t%d", 0); 
		printf("\nj. number of L2 reads that originated from L1 prefetches:\t\t%d", 0); 
		printf("\nk. number of L2 read misses that originated from L1 prefetches:\t\t%d", 0); 
		printf("\nl. number of L2 writes:\t\t%d", 0); 
		printf("\nm. number of L2 write misses:\t\t%d", 0); 
		printf("\nn. L2 miss rate:\t\t%d", 0); 
		printf("\no. number of L2 writebacks:\t\t%d", 0); 
		printf("\np. number of L2 prefetches:\t\t%d", 0); 
		printf("\nq. total memory traffic:\t\t%d", cacheL1->memoryTraffic); 
	}
	return true;
}

int main(int argc, char ** argv)
{
	if (argc != 11)
	{
		printf("\nPlease use the format below to run the program\n");
		printf("sim_cache <block_size> <L1 size> <L1 associativity> <L1 pref_N> <L1 pref_M> <L2 size> <L2 associativity> <L2 pref_N> <L2 pref_M> <file name>\n");
		return 1;
	}
	char fileRow[READSIZE];
	fileName = new char[READSIZE];
	int lineCount = 0;
	blockSize = atoi(argv[1]);
	cacheSizeL1 = atoi(argv[2]);
	assocL1 = atoi(argv[3]);
	prefNL1 = atoi(argv[4]);
	prefML1 = atoi(argv[5]);
	cacheSizeL2 = atoi(argv[6]);
	assocL2 = atoi(argv[7]);
	prefNL2 = atoi(argv[8]);
	prefML2 = atoi(argv[9]);
	fileName = argv[10];

	sb1 = INACTIVE;
	sb2 = INACTIVE;
	cl1 = ACTIVE;
	if(cacheSizeL2 > 0)
	{
		cl2 = ACTIVE;
		cacheL2 = new Cache(assocL2, blockSize, cacheSizeL2, CL2, prefNL2, prefML2);
	}
	if(prefML1 > 0)
		sb1 = ACTIVE;
	if(prefML2 > 0)
		sb2 = ACTIVE;
	
	cacheL1 = new Cache(assocL1, blockSize, cacheSizeL1, CL1, prefNL1, prefML1);
	char operation;
	unsigned long address;
	
	FILE *inputFile;
	inputFile = fopen(fileName, "r");
	if(inputFile!=NULL)
	{
		while(feof(inputFile) == 0)
		{
			fscanf(inputFile, "%c %x\n", &operation, &address);
			// FIRE!!!
			cacheL1->triggerCache(address, operation);
			lineCount++;
		}
		fclose(inputFile);
	}
	else
		printf("\nThere was a problem with the input file. Please check to see if input file is present.");
	printOutput();
	return 1;
}
