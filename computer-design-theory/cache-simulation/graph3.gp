set terminal png
set output "./graph3.png"
set datafile separator ","
set title "Cache Size vs AAT"
set xlabel "Cache Size (Log2 Scale)"
set ylabel "AAT (ns)"
plot "./Assoc_3_1.txt" using 1:2 with linespoints  title "Associativity: 1", "./Assoc_3_2.txt" using 1:2 with linespoints  title "Associativity: 2", "./Assoc_3_4.txt" using 1:2 with linespoints  title "Associativity: 4", "./Assoc_3_8.txt" using 1:2 with linespoints  title "Associativity: 8", "./Assoc_3_FA.txt" using 1:2 with linespoints  title "Associativity: FA"


