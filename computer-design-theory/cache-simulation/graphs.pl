use strict;
use warnings;
use Time::HiRes qw( time );
use IO::Handle;

sub getHitTime 
{
	my $index = shift;
	my @hitTimes = (0.114797, 0.140329, 0.14682, 0, 0.155484, 0.12909, 0.161691, 0.154496, 0.180686, 0.176515, 0.147005, 0.181131, 0.185685, 0.189065, 0.182948, 0.16383, 0.194195, 0.211173, 0.212911, 0.198581, 0.198417, 0.223917, 0.233936, 0.254354, 0.205608, 0.233353, 0.262446, 0.27125, 0.288511, 0.22474, 0.294627, 0.300727, 0.319481, 0.341213, 0.276281, 0.3668, 0.374603, 0.38028, 0.401236, 0.322486, 0.443812, 0.445929, 0.457685, 0.458925, 0.396009, 0.563451, 0.567744, 0.564418, 0.578177, 0.475728, 0.69938, 0.706046, 0.699607, 0.705819, 0.588474);
	return $hitTimes[$index];	
}

sub getAccessTime
{
	my $cacheSize = shift;
	my $blockSize = shift;
	my $associativity = shift;
	my $cactiFile = "cacti.csv";
	open my $cactiFileHandle, $cactiFile or die "Could not open $cactiFile $!";
	while(my $line = <$cactiFileHandle>)
	{
		my @array = split(",", $line);
		$array[0] =~ s/^\s+//;
		$array[0] =~ s/\s+$//;
		$array[1] =~ s/^\s+//;
		$array[1] =~ s/\s+$//;
		$array[2] =~ s/^\s+//;
		$array[2] =~ s/\s+$//;
		$array[3] =~ s/^\s+//;
		$array[3] =~ s/\s+$//;
		if((index($array[0], $cacheSize) >= 0) && (index($array[1], $blockSize) >= 0) && (index($array[2], $associativity) >= 0))
		{
			close($cactiFileHandle);		
			return $array[3];
		}
	}
	close($cactiFileHandle);
	return -1;
}

sub cleanUp
{
	my $command = "rm -rf GRAPHS_1";
	system($command);
	$command = "rm -rf VALIDATION_RUNS";
	system($command);
	$command = "rm -f graphs1AndGraphs2.txt";
	system($command);
	$command = "rm -rf GRAPHS_3";
	system($command);
	$command = "rm -f graphs3.txt";
	system($command);
	$command = "rm -rf GRAPHS_4";
	system($command);
	$command = "rm -f graphs4.txt";
	system($command);
	$command = "rm -rf GRAPHS_5";
	system($command);
	$command = "rm -f graphs5.txt";
	system($command);
	$command = "rm -rf GRAPHS_6";
	system($command);
	$command = "rm -f graphs6.txt";
	system($command);	
	$command = "rm -rf PLOTTED_GRAPHS";
	system($command);
}
sub validationRuns
{
	print("\n=============== Validation Runs ===============");
	my $command = "mkdir VALIDATION_RUNS";
	system($command);
	my $outputFile;
	my $output;
	my $blockSize = 16;
	my @cacheSizeL1 = (1024, 1024, 1024, 1024, 1024, 1024, 1024, 1024, 1024);
	my @assocL1 = (2, 1, 2, 1, 1, 1, 1, 1, 64);
	my @prefNL1 = (0, 0, 0, 0, 1, 3, 2, 0, 0);
	my @prefML1 = (0, 0, 0, 0, 4, 4, 4, 0, 0);
	my @cacheSizeL2 = (0, 0, 8192, 8192, 8192, 8192, 8192, 8192, 4096);
	my @assocL2 = (0, 0, 4, 4, 4, 4, 4, 4, 256);
	my @prefNL2 = (0, 0, 0, 0, 0, 0, 4, 3, 0);
	my @prefML2 = (0, 0, 0, 0, 0, 0, 4, 8, 0);
	my $traceFile = "gcc_trace.txt";
	my $i = 0;
	for($i = 0; $i < 9; $i++)
	{
		$command = "./sim_cache $blockSize $cacheSizeL1[$i] $assocL1[$i] $prefNL1[$i] $prefML1[$i] $cacheSizeL2[$i] $assocL2[$i] $prefNL2[$i] $prefML2[$i] gcc_trace.txt > VALIDATION_RUNS/cache_output".$i.".txt";
		system($command);
		$command = "diff -iw gcc_output" . $i . ".txt VALIDATION_RUNS/cache_output" . $i . ".txt";
		$output = qx($command);
		if($output eq "")
		{
			print "\nValidation Run $i passed.";
		}
		else
		{
			print "\nValidation Run $i FAILED. Check Cache Code.";
		}
	}
	
}

sub graphs1And2
{
	print("\n=============== GRAPHS 1 and 2 ===============");
	my $command = "mkdir GRAPHS_1";
	system($command);
	my ($inputFile, $outputFile);
	$outputFile = "graphs1AndGraphs2.txt";
	open my $outputFileHandle, ">>$outputFile" or die "Could not open $outputFile $!";
	my ($blockSize, $cacheSizeL1, $cacheSizeL2, $assocL1, $assocL2, $prefNL1, $prefNL2, $prefML1, $prefML2, $traceFile);
	$blockSize = 32;
	my ($i, $j);
	my $count = 1; 
	my ($hitTime, $missPenalty);
	$missPenalty = 20 + (0.1 * ($blockSize/16));
	my ($globalMinAAT, $globalMinCacheSize, $globalMinBlockSize, $globalMinAssoc);
	print { $outputFileHandle } "Graph 1 and 2\n";
	print { $outputFileHandle } "<cacheSizeL1>, <assocL1>, <L1 Miss Rate>, <AAT>\n";
	$globalMinAAT = 100;
	for($i = 10; $i <= 20; $i++)
	{
		$cacheSizeL1 = 2 ** $i;	
		for($j = 0; $j < 5; $j++)
		{
			if($j == 0)
			{
				$assocL1 = 1;	
			}
			elsif($j == 1)
			{
				$assocL1 = 2;					
			}
			elsif($j == 2)
			{
				$assocL1 = 4;				
			}
			elsif($j == 3)
			{
				$assocL1 = 8;				
			}
			elsif($j == 4)
			{
				$assocL1 = ($cacheSizeL1/$blockSize);				
			}
			$command = "./sim_cache ".$blockSize." ".$cacheSizeL1." ".$assocL1." "."0 0 0 0 0 0 gcc_trace.txt > GRAPHS_1/cache_output".$count.".txt";
			system($command);
			$inputFile = "GRAPHS_1/cache_output".$count.".txt";
			open my $inputFileHandle, $inputFile or die "Could not open $inputFile $!";
			
			while( my $line = <$inputFileHandle>) 
			{   
				if(index($line, "L1 miss rate") >= 0)
				{
					my @array = split(":", $line);
					my $hitTime;
					if($j ne 4)
					{
						$hitTime= getAccessTime($cacheSizeL1, $blockSize, $assocL1);
					}
					if($j eq 4)
					{
						$hitTime= getAccessTime($cacheSizeL1, $blockSize, "FA");
					}
					
					if($hitTime == -1)
					{
						# print { $outputFileHandle } $cacheSizeL1 . ", " . $assocL1 . ": Access Time is not available in the CACTI File. Hence skipping this. \n";
						next;
					}
					$array[1] =~ s/^\s+//;
					$array[1] =~ s/\s+$//;
					my $AAT =  $hitTime + ($array[1] * $missPenalty);
					if($AAT < $globalMinAAT)
					{
						$globalMinAAT = $AAT;
						$globalMinCacheSize = $cacheSizeL1;
						$globalMinBlockSize = $blockSize;
						$globalMinAssoc = $assocL1;
					}
					print { $outputFileHandle } $cacheSizeL1 . ", " . $assocL1 . ", " . $array[1] . ", " . $AAT . "\n";
				}   				
			}
			close ($inputFileHandle);
			print "\ncache_output".$count." is available";
			$count++;
		}
	}
	print { $outputFileHandle } "======= Minimum Times =======";
	print { $outputFileHandle } "\nMinimum Time: $globalMinAAT\nCache Size: $globalMinCacheSize\nBlock Size: $globalMinBlockSize\nAssociativity: $globalMinAssoc";
	close($outputFileHandle);
	print "\nGraphs 1 is completed.";
	return 	$globalMinAAT;
}

sub graphs3
{
	my $globalMinAAT = shift;
	print("\n=============== GRAPHS 3 ===============");
	my $command = "mkdir GRAPHS_3";
	system($command);
	my ($inputFile, $outputFile);
	$outputFile = "graphs3.txt";
	open my $outputFileHandle, ">>$outputFile" or die "Could not open $outputFile $!";
	my ($blockSize, $cacheSizeL1, $cacheSizeL2, $assocL1, $assocL2, $prefNL1, $prefNL2, $prefML1, $prefML2, $traceFile);
	$blockSize = 32;
	$cacheSizeL2 = 2 ** 19;
	$assocL2 = 8; 
	my ($i, $j);
	my $count = 1; 
	my ($hitTime, $missPenalty);
	$missPenalty = 20 + (0.1 * ($blockSize/16));
	my $hitTimeL2 = getAccessTime($cacheSizeL2, $blockSize, $assocL2);
	my ($minAAT, $minCacheSize, $minBlockSize, $minAssocL1);
	$minAAT = 100;
	print { $outputFileHandle } "Graph 3\n";
	print { $outputFileHandle } "L2 Hit time is: $hitTimeL2\n";
	print { $outputFileHandle } "<cacheSizeL1>, <assocL1>, <AAT>\n";
	for($i = 10; $i <= 18; $i++)
	{
		$cacheSizeL1 = 2 ** $i;	
		for($j = 0; $j < 5; $j++)
		{
			if($j == 0)
			{
				$assocL1 = 1;	
			}
			elsif($j == 1)
			{
				$assocL1 = 2;					
			}
			elsif($j == 2)
			{
				$assocL1 = 4;				
			}
			elsif($j == 3)
			{
				$assocL1 = 8;				
			}
			elsif($j == 4)
			{
				$assocL1 = ($cacheSizeL1/$blockSize);				
			}
			$command = "./sim_cache $blockSize $cacheSizeL1 $assocL1 0 0 $cacheSizeL2 $assocL2 0 0 gcc_trace.txt > GRAPHS_3/cache_output" . $count . ".txt";
			system($command);
			$inputFile = "GRAPHS_3/cache_output".$count.".txt";
			open my $inputFileHandle, $inputFile or die "Could not open $inputFile $!";
			my (@arrayL1, @arrayL2);
			while( my $line = <$inputFileHandle>) 
			{   
				if(index($line, "L1 miss rate") >= 0)
				{
					@arrayL1 = split(":", $line);
					$arrayL1[1] =~ s/^\s+//;
					$arrayL1[1] =~ s/\s+$//;
				}
				if(index($line, "L2 miss rate") >= 0)
				{
					@arrayL2 = split(":", $line);
					$arrayL2[1] =~ s/^\s+//;
					$arrayL2[1] =~ s/\s+$//;
				}      				
			}
			my $hitTimeL1;
			if($j ne 4)
			{
				$hitTimeL1= getAccessTime($cacheSizeL1, $blockSize, $assocL1);
			}
			if($j eq 4)
			{
				$hitTimeL1= getAccessTime($cacheSizeL1, $blockSize, "FA");
			}
			if($hitTimeL1 == -1)
			{
				# print { $outputFileHandle } $cacheSizeL1 . ", " . $blockSize . ", " . $assocL1 . ": Access Time is not available in the CACTI File. Hence skipping this. \n";
				next;
			}
			my $AAT =  $hitTimeL1 + ($arrayL1[1] * ($hitTimeL2 + ($arrayL2[1] * $missPenalty)));
			if($AAT < $minAAT)
			{
				$minAAT = $AAT;
				$minCacheSize = $cacheSizeL1;
				$minBlockSize = $blockSize;
				$minAssocL1 = $assocL1;
			}
			print { $outputFileHandle } $cacheSizeL1 . ", " . $assocL1 . ", " . $AAT . "\n";
			close ($inputFileHandle);
			print "\ncache_output".$count." is available";
			$count++;
		}
	}
	print { $outputFileHandle } "======= Minimum Times =======";
	print { $outputFileHandle } "\nMinimum Time: $minAAT\nCache Size: $minCacheSize\nBlock Size: $minBlockSize\nAssociativity: $minAssocL1";
	
	$inputFile = "graphs3.txt";
	open my $inputFileHandle, $inputFile or die "Could not open $inputFile $!";
	my $offByFivePercentAATNegative = $globalMinAAT - (0.05 * $globalMinAAT);
	my $offByFivePercentAATPositive = $globalMinAAT + (0.05 * $globalMinAAT);
	print { $outputFileHandle } "\n===== Off by 5% Results ======\n";
	$count = 1;
	while(my $line = <$inputFileHandle>)
	{
		if($count < 4) 
		{ 
			$count++;
			next; 			
		}
		if(index($line, "===") >= 0) { last; }
		my @array = split(", ", $line);
		if($array[2] <= $offByFivePercentAATPositive && $array[2] >= $offByFivePercentAATNegative)
		{
			print { $outputFileHandle } $line;
		}
		$count++;
		
	}
	close($outputFileHandle);
	print "\nGraphs 3 is completed.\n";	
}

sub graphs4
{
	print("\n=============== GRAPHS 4 ===============");
	my $command = "mkdir GRAPHS_4";
	system($command);
	my ($inputFile, $outputFile);
	$outputFile = "graphs4.txt";
	open my $outputFileHandle, ">>$outputFile" or die "Could not open $outputFile $!";
	my ($blockSize, $cacheSizeL1, $cacheSizeL2, $assocL1, $assocL2, $prefNL1, $prefNL2, $prefML1, $prefML2, $traceFile);
	$assocL1 = 4;
	my ($i, $j);
	my $count = 1; 
	my ($hitTime, $missPenalty);
	# $missPenalty = 20 + (0.1 * ($blockSize/16));
	print { $outputFileHandle } "Graph 4\n";
	print { $outputFileHandle } "<CacheSizeL1>, <BlockSize>, <L1 Miss Rate>\n";
	for($i = 10; $i <= 15; $i++)
	{
		$cacheSizeL1 = 2 ** $i;
		for($j = 4; $j <= 7; $j++)
		{
			$blockSize = 2 ** $j;	
			$command = "./sim_cache $blockSize $cacheSizeL1 $assocL1 0 0 0 0 0 0 gcc_trace.txt > GRAPHS_4/cache_output" . $count . ".txt";
			system($command);
			$inputFile = "GRAPHS_4/cache_output".$count.".txt";
			open my $inputFileHandle, $inputFile or die "Could not open $inputFile $!";
			my (@arrayL1, @arrayL2);
			while( my $line = <$inputFileHandle>) 
			{   
				if(index($line, "L1 miss rate") >= 0)
				{
					@arrayL1 = split(":", $line);
					$arrayL1[1] =~ s/^\s+//;
					$arrayL1[1] =~ s/\s+$//;
				}								
			}
			print { $outputFileHandle } $cacheSizeL1 . ", " . $blockSize . ", " . $arrayL1[1] . "\n";
			close ($inputFileHandle);
			print "\ncache_output".$count." is available";
			$count++;
		}
	}
	close($outputFileHandle);
	print "\nGraph 4 is completed.\n\n";	
}

sub graphs5
{
	print("\n=============== GRAPHS 5 ===============");
	my $command = "mkdir GRAPHS_5";
	system($command);
	my ($inputFile, $outputFile);
	$outputFile = "graphs5.txt";
	open my $outputFileHandle, ">>$outputFile" or die "Could not open $outputFile $!";
	my ($blockSize, $cacheSizeL1, $cacheSizeL2, $assocL1, $assocL2, $prefNL1, $prefNL2, $prefML1, $prefML2, $traceFile);
	$assocL1 = 4;
	$assocL2 = 8;
	$blockSize = 32;
	my ($i, $j);
	my $count = 1; 
	my ($hitTimeL2, $missPenalty);
	$missPenalty = 20 + (0.1 * ($blockSize/16));
	print { $outputFileHandle } "Graph 5\n";
	print { $outputFileHandle } "<CacheSizeL2>, <CacheSizeL1>, <AAT>\n";
	for($i = 15; $i <= 20; $i++)
	{
		$cacheSizeL2 = 2 ** $i;
		$hitTimeL2 = getAccessTime($cacheSizeL2, $blockSize, $assocL2);
		if($hitTimeL2 == -1) 
		{ 
			print "Something wrong!"; 
			$hitTimeL2 = 0; 
		}
		
		for($j = 10; $j < $i; $j++)
		{
			$cacheSizeL1 = 2 ** $j;	
			$command = "./sim_cache $blockSize $cacheSizeL1 $assocL1 0 0 $cacheSizeL2 $assocL2 0 0 gcc_trace.txt > GRAPHS_5/cache_output" . $count . ".txt";
			system($command);
			$inputFile = "GRAPHS_5/cache_output".$count.".txt";
			open my $inputFileHandle, $inputFile or die "Could not open $inputFile $!";
			my (@arrayL1, @arrayL2);
			while( my $line = <$inputFileHandle>) 
			{   
				if(index($line, "L1 miss rate") >= 0)
				{
					@arrayL1 = split(":", $line);
					$arrayL1[1] =~ s/^\s+//;
					$arrayL1[1] =~ s/\s+$//;
				}
				if(index($line, "L2 miss rate") >= 0)
				{
					@arrayL2 = split(":", $line);
					$arrayL2[1] =~ s/^\s+//;
					$arrayL2[1] =~ s/\s+$//;
				} 												
			}
			my $hitTimeL1 = getAccessTime($cacheSizeL1, $blockSize, $assocL1);
			if($hitTimeL1 == -1) 
			{ 
				print "Something wrong!"; 
				$hitTimeL1 = 0; 
			}
			my $AAT =  $hitTimeL1 + ($arrayL1[1] * ($hitTimeL2 + ($arrayL2[1] * $missPenalty)));
			print { $outputFileHandle } $cacheSizeL2 . ", " . $cacheSizeL1 . ", " . $AAT . "\n";
			close ($inputFileHandle);
			print "\ncache_output".$count." is available";
			$count++;
		}
	}
	close($outputFileHandle);
	print "\nGraph 5 is completed.\n\n";	
}

sub findBestFit
{
	## Upto 240 simulations. 
	print("\n=============== DESIGN-SPACE EXPLORATION ===============");
	my $command = "mkdir GRAPHS_6";
	system($command);
	my ($inputFile, $outputFile);
	$outputFile = "graphs6.txt";
	open my $outputFileHandle, ">>$outputFile" or die "Could not open $outputFile $!";
	my ($blockSize, $cacheSizeL1, $cacheSizeL2, $assocL1, $assocL2, $prefNL1, $prefNL2, $prefML1, $prefML2, $traceFile);
	$assocL1 = 4;
	$assocL2 = 8;
	my $minCount = 0;
	my $minAAT = 100.0;
	my ($minBlockSize, $minCacheSizeL1, $minPrefNL1, $minPrefML1);
	# $blockSize = 32;
	$cacheSizeL2 = 2 ** 16; # 64KB
	my ($i, $j, $n, $m);
	my $count = 1; 
	my ($hitTimeL2, $missPenalty);
	print { $outputFileHandle } "Design Space Exploration\n";
	print { $outputFileHandle } "<CacheSizeL1>, <BlockSize>, <L1_PREF_N>, <L1_PREF_M>, <AAT>\n";
	for($i = 10; $i <= 14; $i++)
	{
		$cacheSizeL1 = 2 ** $i;
		for($j = 4; $j <= 6; $j++)
		{
			$blockSize = 2 ** $j;
			$hitTimeL2 = getAccessTime($cacheSizeL2, $blockSize, $assocL2);
			if($hitTimeL2 == -1) 
			{ 
				# The value for this does not exist. Skip this simulation.
				print "\nSkipping this $count";
				last; 
			}
			$missPenalty = 20 + (0.1 * ($blockSize/16));
			for($n = 1; $n <= 4; $n++)
			{
				$prefNL1 = $n;
				for($m = 1; $m <= 4; $m++)
				{
					$prefML1 = $m;
					$command = "./sim_cache $blockSize $cacheSizeL1 $assocL1 $prefNL1 $prefML1 $cacheSizeL2 $assocL2 0 0 gcc_trace.txt > GRAPHS_6/cache_output" . $count . ".txt";
					system($command);
					$inputFile = "GRAPHS_6/cache_output".$count.".txt";
					open my $inputFileHandle, $inputFile or die "Could not open $inputFile $!";
					my (@arrayL1, @arrayL2);
					while( my $line = <$inputFileHandle>) 
					{   
						if(index($line, "L1 miss rate") >= 0)
						{
							@arrayL1 = split(":", $line);
							$arrayL1[1] =~ s/^\s+//;
							$arrayL1[1] =~ s/\s+$//;
						}
						if(index($line, "L2 miss rate") >= 0)
						{
							@arrayL2 = split(":", $line);
							$arrayL2[1] =~ s/^\s+//;
							$arrayL2[1] =~ s/\s+$//;
						} 												
					}
					my $hitTimeL1 = getAccessTime($cacheSizeL1, $blockSize, $assocL1);
					if($hitTimeL1 == -1) 
					{ 
						# The value for this does not exist. Skip this simulation.
						print "\nSkipping this $count";
						last;
					}
					my $AAT =  $hitTimeL1 + ($arrayL1[1] * ($hitTimeL2 + ($arrayL2[1] * $missPenalty)));
					if($AAT < $minAAT)
					{
						# This will hold the line number and the min AAT
						$minAAT = $AAT;
						$minBlockSize = $blockSize;
						$minCacheSizeL1 = $cacheSizeL1;
						$minPrefNL1 = $prefNL1;
						$minPrefML1 = $prefML1;
						$minCount = $count;
					}
					print { $outputFileHandle } $cacheSizeL1 . ", " . $blockSize . ", " . $prefNL1 . ", " . $prefML1 . ", " . $AAT . "\n";
					close ($inputFileHandle);
					print "\n" . $cacheSizeL1 . ", " . $blockSize . ", " . $prefNL1 . ", " . $prefML1 . ", " . $AAT;
					print "\ncache_output".$count." is available";
					$count++;					
				}
			}
		}
	}
	print { $outputFileHandle } "\n===== Design Space Exploration Results ======";
	print { $outputFileHandle } "\nMinimum Time: $minAAT\nCache Size: $minCacheSizeL1\nBlock Size: $minBlockSize\nPrefetch N: $minPrefNL1\nPrefetch M: $minPrefML1";
	
	$inputFile = "graphs6.txt";
	open my $inputFileHandle, $inputFile or die "Could not open $inputFile $!";
	my $offByFivePercentAAT = $minAAT + (0.05 * $minAAT);
	print { $outputFileHandle } "\n===== Off by 5% Results ======\n";
	$count = 1;
	while(my $line = <$inputFileHandle>)
	{
		if($count < 3) 
		{ 
			$count++;
			next; 			
		}
		if(index($line, "===") >= 0) { last; }
		my @array = split(", ", $line);
		if($array[4] <= $offByFivePercentAAT)
		{
			print { $outputFileHandle } $line;
		}
		$count++;
		
	}
	close($outputFileHandle);
	print "\nGraph 6 is completed.\n\n";	
}

sub log2
{
	my $number = shift;
	return log($number)/log(2);
}
sub plotGraphsUsingGnuplot
{
	# This is for Graph 1 and 2. 
	my $command = "mkdir PLOTTED_GRAPHS";
	system($command);
	$command = "cp graph1.gp PLOTTED_GRAPHS/";
	system($command);
	$command = "cp graph2.gp PLOTTED_GRAPHS/";
	system($command);
	$command = "cp graph3.gp PLOTTED_GRAPHS/";
	system($command);
	$command = "cp graph4.gp PLOTTED_GRAPHS/";
	system($command);
	$command = "cp graph5.gp PLOTTED_GRAPHS/";
	system($command);
	my ($inputFile, $outputFile, $inputFileHandle, $outputFileHandle);
	$inputFile = "graphs1AndGraphs2.txt";
	open $inputFileHandle, $inputFile or die "Could not open $inputFile $!";
	$outputFile = "PLOTTED_GRAPHS/Assoc_1.txt";
	open $outputFileHandle, ">>$outputFile" or die "Could not open $outputFile $!";
	my $number;
	while(my $line = <$inputFileHandle>)
	{
		if(index($line, "===") >= 0) { last; }
		my @array = split(", ", $line);
		if($array[1] eq 1)
		{
			$number =  log2($array[0]);
			print { $outputFileHandle } "$number,$array[2],$array[3]";
		}		
	}
	close($outputFileHandle);
	close($inputFileHandle);
	
	open $inputFileHandle, $inputFile or die "Could not open $inputFile $!";
	$outputFile = "PLOTTED_GRAPHS/Assoc_2.txt";
	open $outputFileHandle, ">>$outputFile" or die "Could not open $outputFile $!";
	while(my $line = <$inputFileHandle>)
	{
		if(index($line, "===") >= 0) { last; }
		my @array = split(", ", $line);
		if($array[1] eq 2)
		{
			$number =  log2($array[0]);
			print { $outputFileHandle } "$number,$array[2],$array[3]";
		}		
	}
	close($outputFileHandle);
	close($inputFileHandle);
	
	open $inputFileHandle, $inputFile or die "Could not open $inputFile $!";
	$outputFile = "PLOTTED_GRAPHS/Assoc_4.txt";
	open $outputFileHandle, ">>$outputFile" or die "Could not open $outputFile $!";
	while(my $line = <$inputFileHandle>)
	{
		if(index($line, "===") >= 0) { last; }
		my @array = split(", ", $line);
		if($array[1] eq 4)
		{
			$number =  log2($array[0]);
			print { $outputFileHandle } "$number,$array[2],$array[3]";
		}		
	}
	close($outputFileHandle);
	close($inputFileHandle);
	
	open $inputFileHandle, $inputFile or die "Could not open $inputFile $!";
	$outputFile = "PLOTTED_GRAPHS/Assoc_8.txt";
	open $outputFileHandle, ">>$outputFile" or die "Could not open $outputFile $!";
	while(my $line = <$inputFileHandle>)
	{
		if(index($line, "===") >= 0) { last; }
		my @array = split(", ", $line);
		if($array[1] eq 8)
		{
			$number =  log2($array[0]);
			print { $outputFileHandle } "$number,$array[2],$array[3]";
		}		
	}
	close($outputFileHandle);
	close($inputFileHandle);
	
	open $inputFileHandle, $inputFile or die "Could not open $inputFile $!";
	$outputFile = "PLOTTED_GRAPHS/Assoc_FA.txt";
	open $outputFileHandle, ">>$outputFile" or die "Could not open $outputFile $!";
	while(my $line = <$inputFileHandle>)
	{
		if(index($line, "===") >= 0) { last; }
		my @array = split(", ", $line);
		if($array[1] > 8)
		{
			$number =  log2($array[0]);
			print { $outputFileHandle } "$number,$array[2],$array[3]";
		}		
	}
	close($outputFileHandle);
	close($inputFileHandle);
	
	# This is for Graph 3. 
	$inputFile = "graphs3.txt";
	open $inputFileHandle, $inputFile or die "Could not open $inputFile $!";
	$outputFile = "PLOTTED_GRAPHS/Assoc_3_1.txt";
	open $outputFileHandle, ">>$outputFile" or die "Could not open $outputFile $!";
	my $number;
	while(my $line = <$inputFileHandle>)
	{
		if(index($line, "===") >= 0) { last; }
		my @array = split(", ", $line);
		if($array[1] eq 1)
		{
			$number =  log2($array[0]);
			print { $outputFileHandle } "$number,$array[2]";
		}		
	}
	close($outputFileHandle);
	close($inputFileHandle);
	
	open $inputFileHandle, $inputFile or die "Could not open $inputFile $!";
	$outputFile = "PLOTTED_GRAPHS/Assoc_3_2.txt";
	open $outputFileHandle, ">>$outputFile" or die "Could not open $outputFile $!";
	while(my $line = <$inputFileHandle>)
	{
		if(index($line, "===") >= 0) { last; }
		my @array = split(", ", $line);
		if($array[1] eq 2)
		{
			$number =  log2($array[0]);
			print { $outputFileHandle } "$number,$array[2]";
		}		
	}
	close($outputFileHandle);
	close($inputFileHandle);
	
	open $inputFileHandle, $inputFile or die "Could not open $inputFile $!";
	$outputFile = "PLOTTED_GRAPHS/Assoc_3_4.txt";
	open $outputFileHandle, ">>$outputFile" or die "Could not open $outputFile $!";
	while(my $line = <$inputFileHandle>)
	{
		if(index($line, "===") >= 0) { last; }
		my @array = split(", ", $line);
		if($array[1] eq 4)
		{
			$number =  log2($array[0]);
			print { $outputFileHandle } "$number,$array[2]";
		}		
	}
	close($outputFileHandle);
	close($inputFileHandle);
	
	open $inputFileHandle, $inputFile or die "Could not open $inputFile $!";
	$outputFile = "PLOTTED_GRAPHS/Assoc_3_8.txt";
	open $outputFileHandle, ">>$outputFile" or die "Could not open $outputFile $!";
	while(my $line = <$inputFileHandle>)
	{
		if(index($line, "===") >= 0) { last; }
		my @array = split(", ", $line);
		if($array[1] eq 8)
		{
			$number =  log2($array[0]);
			print { $outputFileHandle } "$number,$array[2]";
		}		
	}
	close($outputFileHandle);
	close($inputFileHandle);
	
	open $inputFileHandle, $inputFile or die "Could not open $inputFile $!";
	$outputFile = "PLOTTED_GRAPHS/Assoc_3_FA.txt";
	open $outputFileHandle, ">>$outputFile" or die "Could not open $outputFile $!";
	while(my $line = <$inputFileHandle>)
	{
		if(index($line, "===") >= 0) { last; }
		my @array = split(", ", $line);
		if($array[1] > 8)
		{
			$number =  log2($array[0]);
			print { $outputFileHandle } "$number,$array[2]";
		}		
	}
	close($outputFileHandle);
	close($inputFileHandle);
	
	# This is for Graph 4
	$inputFile = "graphs4.txt";
	open $inputFileHandle, $inputFile or die "Could not open $inputFile $!";
	$outputFile = "PLOTTED_GRAPHS/CS1_4.txt";
	open $outputFileHandle, ">>$outputFile" or die "Could not open $outputFile $!";
	my $number;
	while(my $line = <$inputFileHandle>)
	{
		if(index($line, "===") >= 0) { last; }
		my @array = split(", ", $line);
		if($array[0] eq 2 ** 10)
		{
			$number =  log2($array[1]);
			print { $outputFileHandle } "$number,$array[2]";
		}		
	}
	close($outputFileHandle);
	close($inputFileHandle);
	
	open $inputFileHandle, $inputFile or die "Could not open $inputFile $!";
	$outputFile = "PLOTTED_GRAPHS/CS2_4.txt";
	open $outputFileHandle, ">>$outputFile" or die "Could not open $outputFile $!";
	while(my $line = <$inputFileHandle>)
	{
		if(index($line, "===") >= 0) { last; }
		my @array = split(", ", $line);
		if($array[0] eq 2 ** 11)
		{
			$number =  log2($array[1]);
			print { $outputFileHandle } "$number,$array[2]";
		}		
	}
	close($outputFileHandle);
	close($inputFileHandle);
	
	open $inputFileHandle, $inputFile or die "Could not open $inputFile $!";
	$outputFile = "PLOTTED_GRAPHS/CS4_4.txt";
	open $outputFileHandle, ">>$outputFile" or die "Could not open $outputFile $!";
	while(my $line = <$inputFileHandle>)
	{
		if(index($line, "===") >= 0) { last; }
		my @array = split(", ", $line);
		if($array[0] eq 2 ** 12)
		{
			$number =  log2($array[1]);
			print { $outputFileHandle } "$number,$array[2]";
		}		
	}
	close($outputFileHandle);
	close($inputFileHandle);
	
	open $inputFileHandle, $inputFile or die "Could not open $inputFile $!";
	$outputFile = "PLOTTED_GRAPHS/CS8_4.txt";
	open $outputFileHandle, ">>$outputFile" or die "Could not open $outputFile $!";
	while(my $line = <$inputFileHandle>)
	{
		if(index($line, "===") >= 0) { last; }
		my @array = split(", ", $line);
		if($array[0] eq 2 ** 13)
		{
			$number =  log2($array[1]);
			print { $outputFileHandle } "$number,$array[2]";
		}		
	}
	close($outputFileHandle);
	close($inputFileHandle);
	
	open $inputFileHandle, $inputFile or die "Could not open $inputFile $!";
	$outputFile = "PLOTTED_GRAPHS/CS16_4.txt";
	open $outputFileHandle, ">>$outputFile" or die "Could not open $outputFile $!";
	while(my $line = <$inputFileHandle>)
	{
		if(index($line, "===") >= 0) { last; }
		my @array = split(", ", $line);
		if($array[0] eq 2 ** 14)
		{
			$number =  log2($array[1]);
			print { $outputFileHandle } "$number,$array[2]";
		}		
	}
	close($outputFileHandle);
	close($inputFileHandle);
	
	open $inputFileHandle, $inputFile or die "Could not open $inputFile $!";
	$outputFile = "PLOTTED_GRAPHS/CS32_4.txt";
	open $outputFileHandle, ">>$outputFile" or die "Could not open $outputFile $!";
	while(my $line = <$inputFileHandle>)
	{
		if(index($line, "===") >= 0) { last; }
		my @array = split(", ", $line);
		if($array[0] eq 2 ** 15)
		{
			$number =  log2($array[1]);
			print { $outputFileHandle } "$number,$array[2]";
		}		
	}
	close($outputFileHandle);
	close($inputFileHandle);
	
	# This is for graph 5. This is the final graph. 
	$inputFile = "graphs5.txt";
	open $inputFileHandle, $inputFile or die "Could not open $inputFile $!";
	$outputFile = "PLOTTED_GRAPHS/CS32_5.txt";
	open $outputFileHandle, ">>$outputFile" or die "Could not open $outputFile $!";
	my $number;
	while(my $line = <$inputFileHandle>)
	{
		if(index($line, "===") >= 0) { last; }
		my @array = split(", ", $line);
		if($array[0] eq 2 ** 15)
		{
			$number =  log2($array[1]);
			print { $outputFileHandle } "$number,$array[2]";
		}		
	}
	close($outputFileHandle);
	close($inputFileHandle);
	
	open $inputFileHandle, $inputFile or die "Could not open $inputFile $!";
	$outputFile = "PLOTTED_GRAPHS/CS64_5.txt";
	open $outputFileHandle, ">>$outputFile" or die "Could not open $outputFile $!";
	while(my $line = <$inputFileHandle>)
	{
		if(index($line, "===") >= 0) { last; }
		my @array = split(", ", $line);
		if($array[0] eq 2 ** 16)
		{
			$number =  log2($array[1]);
			print { $outputFileHandle } "$number,$array[2]";
		}		
	}
	close($outputFileHandle);
	close($inputFileHandle);
	
	open $inputFileHandle, $inputFile or die "Could not open $inputFile $!";
	$outputFile = "PLOTTED_GRAPHS/CS128_5.txt";
	open $outputFileHandle, ">>$outputFile" or die "Could not open $outputFile $!";
	while(my $line = <$inputFileHandle>)
	{
		if(index($line, "===") >= 0) { last; }
		my @array = split(", ", $line);
		if($array[0] eq 2 ** 17)
		{
			$number =  log2($array[1]);
			print { $outputFileHandle } "$number,$array[2]";
		}		
	}
	close($outputFileHandle);
	close($inputFileHandle);
	
	open $inputFileHandle, $inputFile or die "Could not open $inputFile $!";
	$outputFile = "PLOTTED_GRAPHS/CS256_5.txt";
	open $outputFileHandle, ">>$outputFile" or die "Could not open $outputFile $!";
	while(my $line = <$inputFileHandle>)
	{
		if(index($line, "===") >= 0) { last; }
		my @array = split(", ", $line);
		if($array[0] eq 2 ** 18)
		{
			$number =  log2($array[1]);
			print { $outputFileHandle } "$number,$array[2]";
		}		
	}
	close($outputFileHandle);
	close($inputFileHandle);
	
	open $inputFileHandle, $inputFile or die "Could not open $inputFile $!";
	$outputFile = "PLOTTED_GRAPHS/CS512_5.txt";
	open $outputFileHandle, ">>$outputFile" or die "Could not open $outputFile $!";
	while(my $line = <$inputFileHandle>)
	{
		if(index($line, "===") >= 0) { last; }
		my @array = split(", ", $line);
		if($array[0] eq 2 ** 19)
		{
			$number =  log2($array[1]);
			print { $outputFileHandle } "$number,$array[2]";
		}		
	}
	close($outputFileHandle);
	close($inputFileHandle);
	
	open $inputFileHandle, $inputFile or die "Could not open $inputFile $!";
	$outputFile = "PLOTTED_GRAPHS/CS1024_5.txt";
	open $outputFileHandle, ">>$outputFile" or die "Could not open $outputFile $!";
	while(my $line = <$inputFileHandle>)
	{
		if(index($line, "===") >= 0) { last; }
		my @array = split(", ", $line);
		if($array[0] eq 2 ** 20)
		{
			$number =  log2($array[1]);
			print { $outputFileHandle } "$number,$array[2]";
		}		
	}
	close($outputFileHandle);
	close($inputFileHandle);
		
	# Fire!!!
	$command = "./plotGraphs";
	system($command);
	
	
			
}

# Call all the graph functions
print "\n\nWelcome to Cache Simulation. This script will run through all the simulations that need to be performed and save all the outputs that need to be plotted on the graph in a specific files.
	\nFor graphs 1 and 2, check graphs1AndGraphs2.txt. For 3, 4 and 5 check graphs3.txt, graphs4.txt and graphs5.txt. For the best fit, check graphs6.txt. All the outputs are also available in
	\nthe respective directries.";
print "\n1. Create a directory and copy your sim_cache executable over there.";
print "\n2. All outputs for the respective graphs will be placed in the respective graph directory.";
print "\n3. The files that will be needed for plotting will be available in the direcotry that you had previously created.";

my $start = time(); 
cleanUp();
validationRuns();
my $value = graphs1And2();
graphs3($value);
graphs4();
graphs5();
findBestFit();
plotGraphsUsingGnuplot();
my $end = time();
print "\nElapsed Time is " . ($end - $start) . "\n"; 
