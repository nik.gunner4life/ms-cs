set terminal png
set output "./graph2.png"
set datafile separator ","
set title "Cache Size vs AAT"
set xlabel "Cache Size (Log2 Scale)"
set ylabel "AAT (ns)"
plot "./Assoc_1.txt" using 1:3 with linespoints  title "Associativity: 1", "./Assoc_2.txt" using 1:3 with linespoints  title "Associativity: 2", "./Assoc_4.txt" using 1:3 with linespoints  title "Associativity: 4", "./Assoc_8.txt" using 1:3 with linespoints  title "Associativity: 8", "./Assoc_FA.txt" using 1:3 with linespoints  title "Associativity: FA"
