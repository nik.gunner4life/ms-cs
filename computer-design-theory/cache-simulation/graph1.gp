set terminal png
set output "./graph1.png"
set datafile separator ","
set title "Cache Size vs Miss Rate"
set xlabel "Cache Size (Log2 Scale)"
set ylabel "Miss Rate"
plot "./Assoc_1.txt" using 1:2 with linespoints  title "Associativity: 1", "./Assoc_2.txt" using 1:2 with linespoints  title "Associativity: 2", "./Assoc_4.txt" using 1:2 with linespoints  title "Associativity: 4", "./Assoc_8.txt" using 1:2 with linespoints  title "Associativity: 8", "./Assoc_FA.txt" using 1:2 with linespoints  title "Associativity: FA"

