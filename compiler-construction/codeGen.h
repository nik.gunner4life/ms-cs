
#pragma region AUTHOR INFORMATION
/***********************************************************************
Author:   Nikhil Srivatsan Srinivasan
Unity ID: nsrivat
Project:  Code Generation
Date:     04/13/2014
***********************************************************************/
#pragma endregion

#pragma region HEADERS
#ifndef CODEGEN_H
#define CODEGEN_H
#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <string>
#include <string.h>
#include <stdint.h>
#include <iostream>
#include <fstream>
#include <ostream>
#include <sstream>
#include <vector>
#include <stack>
#include "debug.h"
using namespace std;
#pragma endregion

#pragma region PREPROCESSORS
// The names of the different registers that we will be using.
// Do not change the number of PC. PC has to be register 7. 
// This is what TM will be using as the program counter and hence
// this cannot be changed(TM will only use register 7 as the PC).
#define ZERO 0
#define AC1 1
#define AC2 2
#define PC 7
#define SP 6
#define FP 5
#define BP 4
#define REG1 3
#pragma endregion

class BreakStuff
{
public:
	vector<int> innerBreakStack;
};

class faStuff
{
	public:
		vector<int> faStack;
};

class CodeGen 
{
	// Class Members.
public:
	// The line number that will be used in the assembly. 
	int assemblyLineNumber;
	vector<long> falseLineNumber;
	vector<long> trueLineNumber;
	vector<int> doTrueStack;
	vector<int> doFalseStack;
	vector<BreakStuff*> breakStack;
	vector<int> ifVector;
	vector<int> framePointerBackPatching;
	int unconditionalJumpToAvoidAProc;
	int addressOfVariable;

	string faConst;
	vector<int> faFalseStack;
	vector<int> faTrueStack;
	vector<string> faLoopVariables;
	// The name of the file where the output will be available. 
	// This will be the name of the ice9 file with .tm extension.
	ofstream outputFile;
	// Class Methods. More to come.
	int BPAndFP[2];
public:
	CodeGen();
	// Basic instructions.
	int genLD(int, int, int);
	int genLDC(int, int, int);
	// Used for backpatching FP amongst other things.
	int genLDC(int, int, int, int);
	int genLDA(int, int, int);
	// Used for backpatching FP amongst other things.
	int genLDA(int, int, int, int);
	int genOut(int);
	int genStore(int, int, int);
	int genStore(int, int, int, int);
	int genOutNL();
	int genHalt();
	int genJGE(int, int);
	int genJLE(int, int);
	int genJGT(int, int);
	int genJLT(int, int);
	int genJEQ(int, int); 
	int genJEQToRegister(int, int);
	int genJNE(int, int);
	int genJLE(int, int, int);
	int genJEQ(int, int, int);
	int genJGT(int, int, int);
	int genComment(string);

	// Combination of Instructions. 
	int genOperation(string, int, int, int);
	int genFlip(int);
	int genPush();
	// Overloaded for BP.
	int genPush(int);
	int genPop();
	int genIfFalse();
	int genIfTrue();
	int genIfElse();
	int genIfElsifElse();
	int genDoFalse();
	int genDoTrue();
	int genLE();
	int genLT();
	int genGT();
	int genGE();
	int genNE();
	int genEQ();
	int genAssign(int);
	int genLocalAssign(int);
	int genLoadValue(int);
	int genLoadLocalValue(int);
	int genCompare(expressionType);
	int genBreak(int);
	int genBackPatchBPAndFP(int);

	// Helper Methods
	int getAssemblyLineNumber();
	int closeStream();
	int pushFalseLineNumber();
	int returnTopAndPopFalseLineNumber();
	int pushTrueLineNumber();
	int returnTopAndPopTrueLineNumber();
	int increaseIfDepth();
	int decreaseIfDepth();
	int incrementIfCountAtTop();
	int returnTopIfCount();
	int printIfVector();
	int pushToBreakStack();
	BreakStuff* popFromBreakStack();
	int updateBreakStack(int);
	
	// THings I need for Do.
	int popFromDoFalseStack();
	int pushInDoTrueStack();
	int popFromDoTrueStack();
	int pushInDoFalseStack();
	int pushInFaTrueStack();
	int pushInFaFalseStack();
	int popFromFaTrueStack();
	int popFromFaFalseStack();

	int pushToFaVarStack(string);
	int popFromFaVarStack();
	string getTopFaVarStack();

	// This will be used along with semantics. 
	// No idea why I am placing it here.
	int getNextAddress();
	// Because of my idiotic design choice in semantics, I am now
	// stuck doing this. 
	int decrementAddress();
	int getCurrentMemoryAddress();
	int saveLinesForBPAndFP();

	// For Debug purposes.
	int outRegister(int);
};

#endif