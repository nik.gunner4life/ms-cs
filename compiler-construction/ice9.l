%{
#include <stdio.h>
#include "ice9.tab.h"
#include <string>

extern void yyerror(char *);
int yynewlines=1;

%}

%%


if	return  TK_IF;
fi  return  TK_FI;
else return  TK_ELSE;
do return  TK_DO;
od  return  TK_OD;
fa  return  TK_FA;
af  return  TK_AF;
to return  TK_TO;
proc return  TK_PROC;
end return  TK_END;
return return TK_RETURN;
forward return  TK_FORWARD;
var  return  TK_VAR;
type return  TK_TYPE;
break return  TK_BREAK;
exit return  TK_EXIT;
true return  TK_TRUE;
false return  TK_FALSE;
write return  TK_WRITE;
writes return  TK_WRITES;
read return  TK_READ;
"[]" return  TK_BOX;
"->" return  TK_ARROW;
"{" return  TK_LPAREN;
"}" return  TK_RPAREN;
"(" return  TK_LBRACK;
")" return  TK_RBRACK;
"[" return  TK_LSBRACK;
"]" return  TK_RSBRACK;
":" return  TK_COLON;
";" return  TK_SEMI;
":=" return  TK_ASSIGN;
"?" return  TK_QUEST;
"," return  TK_COMMA;
"+" return  TK_PLUS;
"-" return  TK_MINUS;
"*" return  TK_STAR;
"/" return  TK_SLASH;
"%" return  TK_MOD;
"=" return  TK_EQ;
"!=" return  TK_NEQ;
">" return  TK_GT;
"<" return  TK_LT;
">=" return  TK_GE;
"<=" return  TK_LE;
\"[^"\n]*\"  return TK_SLIT;
'[^'\n]*' return TK_SLIT;
[0-9]+  yylval.longg = atol(yytext); return TK_INT;
[A-Za-z][A-Za-z0-9_]*  yylval.str = strdup(yytext); return TK_ID;
#.*\n{0,1}  yynewlines++;
[ ]{1,} ;
[\t ]{1,} ;

\n			{ yynewlines++; }

.		{yyerror("Illegal character"); }
%%


