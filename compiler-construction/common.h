#pragma region AUTHOR INFORMATION
/***********************************************************************
Author:   Nikhil Srivatsan Srinivasan
Unity ID: nsrivat
Project:  Code Generation
Date:     04/18/2014
***********************************************************************/
#pragma endregion

#pragma region HEADERS
#ifndef COMMON_H
#define COMMON_H
#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <string>
#include <string.h>
#include <stdint.h>
#include <iostream>
#include <fstream>
#include <ostream>
#include <sstream>
#include <vector>
#include <stack>
#include "debug.h"
using namespace std;
#pragma endregion

// Helper Methods that are common to all of them.
int createPST(mvrDeets*, string, string);
int loadValueForID(string);
int loadGlobalID(string);
int loadLocalID(string);
int storeGlobalID(string);
int storeLocalID(string);
int storeValueInID(string);
int backPatchBPAndFP();
int pushReturnAddress();
int saveProcDeclarationLineNumber(string);
int createActivationRecord_1(string);
int createActivationRecord_2();
int saveUnconditionalJumpToAvoidAProc();
int backPatchUnconditionalJumpToAvoidAProc();
int popARAndReturnFromProc();
int functionCall(string);
int handleReturn();
int implicitlyLoadReturnValueInAC1WhenQutting();
int handleFa1(string);
int handleFa2();
int handleFa3();
int loadIDIncludingFa(string);
string appendToVariable(string, string, int);
int insertInPST(string, string, vector<int>);
#endif

