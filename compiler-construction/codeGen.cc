
#pragma region AUTHOR INFORMATION
/***********************************************************************
Author:   Nikhil Srivatsan Srinivasan
Unity ID: nsrivat
Project:  Code Generation
Date:     04/13/2014
***********************************************************************/
#pragma endregion

#pragma region HEADERS
#include "codeGen.h"
#include "debug.h"
using namespace std;
#pragma endregion
// Might not be using this at all. 
extern int nsrDebugPrintf;
extern int codeDebug;
extern int procFlag;
extern int writeDebug;
extern int faFlag;


CodeGen::CodeGen()
{
	outputFile.open("test.tm");
	faConst = "-fa";
	assemblyLineNumber = 0;
	// Get the overall memory size and push the value onto the 
	// SP register. 
	genComment("Initializing Zero and SP registers.");
	genLD(SP, 0, 0);
	// Load the ZERO register with the value 0, Captain Obvious.
	genLDC(ZERO, 0, 0);
	string comment = "Saving this line number for the Base Pointer(BP)";
	genComment(comment);
	comment = "Saving this line number for the Frame Pointer(FP)";
	genComment(comment);
	genComment("End of initialization");
	genComment("");	
	saveLinesForBPAndFP();
	addressOfVariable = 0;
}

int CodeGen::outRegister(int registerVal)
{
	if(writeDebug)
	{
		genOut(registerVal);
		genOutNL();
	}
}

int CodeGen::saveLinesForBPAndFP()
{
	BPAndFP[0] = assemblyLineNumber;
	assemblyLineNumber++;
	BPAndFP[1] = assemblyLineNumber;
	assemblyLineNumber++;
}

int CodeGen::genBackPatchBPAndFP(int displacement)
{
	// Load the value of displacement i.e. the value after the number of symbols
	// to this.
	genComment("Backpatching BP");
	outputFile << BPAndFP[0] << ": " << "LDC " << BP << ", " << displacement << "(" << ZERO << ")\n";
	genComment("Backpatching FP");
	outputFile << BPAndFP[1] << ": " << "LDC " << FP << ", " << displacement << "(" << ZERO << ")\n";
}

int CodeGen::getAssemblyLineNumber()
{
	return assemblyLineNumber;
}

int CodeGen::genLD(int dest, int displacement, int src)
{
	outputFile << assemblyLineNumber << ": " << "LD " << dest << ", " << displacement << "(" << src << ")\n";
	assemblyLineNumber++;
}

int CodeGen::genLDC(int dest, int displacement, int src)
{
	outputFile << assemblyLineNumber << ": " << "LDC " << dest << ", " << displacement << "(" << src << ")\n";  
	assemblyLineNumber++;
}

int CodeGen::genLDC(int currentNumber, int dest, int displacement, int src)
{
	outputFile << currentNumber << ": " << "LDC " << dest << ", " << displacement << "(" << src << ")\n"; 
}

int CodeGen::genLDA(int dest, int displacement, int src)
{
	outputFile << assemblyLineNumber << ": " << "LDA " << dest << ", " << displacement << "(" << src << ")\n";  
	assemblyLineNumber++;
}
// Overloaded method.
int CodeGen::genLDA(int currentLineNumber, int dest, int displacement, int src)
{
	outputFile << currentLineNumber << ": " << "LDA " << dest << ", " << displacement << "(" << src << ")\n";  
}

int CodeGen::genOut(int src)
{
	outputFile << assemblyLineNumber << ": " << "OUT " << src << ", " << ZERO << ", " << ZERO << "\n";
	assemblyLineNumber++;
}

int CodeGen::genOutNL()
{
	outputFile << assemblyLineNumber << ": " << "OUTNL " << ZERO << ", " << ZERO << ", " << ZERO << "\n";
	assemblyLineNumber++;
}

int CodeGen::genHalt()
{
	outputFile << assemblyLineNumber << ": " << "HALT " << 0 << ", " << 0 << ", " << 0 << "\n";
	assemblyLineNumber++;
}

int CodeGen::genStore(int src, int displacement, int dest)
{
	outputFile << assemblyLineNumber << ": " << "ST " << src << ", " << displacement << "(" << dest << ")\n";
	assemblyLineNumber++;
}

int CodeGen::genStore(int currentLineNumber, int src, int displacement, int dest)
{
	outputFile << currentLineNumber << ": " << "ST " << src << ", " << displacement << "(" << dest << ")\n";
}

int CodeGen::genJLE(int src, int lineNumber)
{
	outputFile << assemblyLineNumber << ": " << "JLE " << src << ", " << lineNumber <<  "(" << ZERO << ")\n";
	assemblyLineNumber++;
}
int CodeGen::genJLT(int src, int lineNumber)
{
	outputFile << assemblyLineNumber << ": " << "JLT " << src << ", " << lineNumber <<  "(" << ZERO << ")\n";
	assemblyLineNumber++;
}
int CodeGen::genJGE(int src, int lineNumber)
{
	outputFile << assemblyLineNumber << ": " << "JGE " << src << ", " << lineNumber <<  "(" << ZERO << ")\n";
	assemblyLineNumber++;
}
int CodeGen::genJGT(int src, int lineNumber)
{
	outputFile << assemblyLineNumber << ": " << "JGT " << src << ", " << lineNumber <<  "(" << ZERO << ")\n";
	assemblyLineNumber++;
}
int CodeGen::genJEQ(int src, int lineNumber)
{
	outputFile << assemblyLineNumber << ": " << "JEQ " << src << ", " << lineNumber <<  "(" << ZERO << ")\n";
	assemblyLineNumber++;
}
int CodeGen::genJNE(int src, int lineNumber)
{
	outputFile << assemblyLineNumber << ": " << "JNE " << src << ", " << lineNumber <<  "(" << ZERO << ")\n";
	assemblyLineNumber++;
}



// Overloaded. To be used strictly for branch statements. 
int CodeGen::genJLE(int currentLineNumber, int src, int jumpToLineNumber)
{
	outputFile << currentLineNumber << ": " << "JLE " << src << ", " << jumpToLineNumber <<  "(" << ZERO << ")\n";
}
// Overloaded. To be used strictly for branch statements. 
int CodeGen::genJEQ(int currentLineNumber, int src, int jumpToLineNumber)
{
	outputFile << currentLineNumber << ": " << "JEQ " << src << ", " << jumpToLineNumber <<  "(" << ZERO << ")\n";
}

int CodeGen::genJGT(int currentLineNumber, int src, int jumpToLineNumber)
{
	outputFile << currentLineNumber << ": " << "JGT " << src << ", " << jumpToLineNumber <<  "(" << ZERO << ")\n";
}

int CodeGen::genJEQToRegister(int src, int dest)
{
	outputFile << assemblyLineNumber << ": " << "JEQ " << src << ", " << 0 <<  "(" << dest << ")\n";
	assemblyLineNumber++;
}

int CodeGen::genOperation(string operation, int destination, int source1, int source2)
{
	if(operation == "+")
	{
		outputFile << assemblyLineNumber << ": " << "ADD " << destination << ", " << source1 << ", " << source2 << "\n";
		assemblyLineNumber++;
	}
	else if(operation == "*")
	{
		outputFile << assemblyLineNumber << ": " << "MUL " << destination << ", " << source1 << ", " << source2 << "\n";
		assemblyLineNumber++;
	}
	else if(operation == "/")
	{
		outputFile << assemblyLineNumber << ": " << "DIV " << destination << ", " << source1 << ", " << source2 << "\n";
		assemblyLineNumber++;
	}
	else if(operation == "-")
	{
		outputFile << assemblyLineNumber << ": " << "SUB " << destination << ", " << source1 << ", " << source2 << "\n";
		assemblyLineNumber++;
	}
}

int CodeGen::genAssign(int address)
{
	// Load the address in AC2
	// Immediate load since I know the exact address.
	genLDC(AC2, address, ZERO);
	// The address is now in AC2. Store the value in
	// AC1 to dMem[0 + reg[AC2]] = reg[AC1];
	genStore(AC1, 0, AC2);
}

int CodeGen::genLocalAssign(int offset)
{
	// Load the address in AC2
	// AC1 to dMem[0 + reg[AC2]] = reg[AC1];
	genStore(AC1, offset, BP);
}

// This method will be modified when I deal with arrays. For now
// This is simply going to be this.
int CodeGen::genLoadValue(int address)
{
	// The value is now stored in AC1
	genLD(AC1, address, ZERO);
}

int CodeGen::genLoadLocalValue(int offset)
{
	// Load the value onto AC1.
	genLD(AC1, offset, BP);
}

int CodeGen::genCompare(expressionType expType)
{
	genComment("Evaluating boolean expression");
	genPop();
	genOperation("-", AC1, AC2, AC1);
	int tempAssemblyLineNumber = assemblyLineNumber;
	switch (expType)
	{
	case LT:
		{
			genJLT(AC1, tempAssemblyLineNumber + 3);
			break;
		}
	case GT:
		{
			genJGT(AC1, tempAssemblyLineNumber + 3);
			break;
		}
	case LE:
		{
			genJLE(AC1, tempAssemblyLineNumber + 3);
			break;
		}
	case GE:
		{
			genJGE(AC1, tempAssemblyLineNumber + 3);
			break;
		}
	case EQ:
		{
			genJEQ(AC1, tempAssemblyLineNumber + 3);
			break;
		}
	case NE:
		{
			genJNE(AC1, tempAssemblyLineNumber + 3);
			break;
		}
	default:
		{
			nsrPrintf("Holy Cow Batman!! I should not have come here.");
		}
	}
	genLDA(AC1, 0, ZERO);
	genJEQ(ZERO, tempAssemblyLineNumber + 4);
	genLDA(AC1, 1, ZERO);
	genComment("Finished evaluating Boolean expression"); 
}

// Push the value in AC1 to Stack.
// The stack grows downwards. Hence decrement the stack
// pointer and store the value of AC1 to top of stack.
// SP points to top of stack. 
int CodeGen::genPush()
{
	// Decrement SP
	genLDA(SP, -1, SP);
	// Load the value of AC1 into dMem[SP]
	genStore(AC1, 0, SP);
}

int CodeGen::genPush(int registerName)
{
	// Decrement SP
	genLDA(SP, -1, SP);
	// Load the value of AC1 into dMem[SP]
	genStore(registerName, 0, SP);
}

int CodeGen::genFlip(int reg)
{
	int tempAssemblyNumber = assemblyLineNumber;
	genComment("Flipping the bit");
	genJLE(reg, tempAssemblyNumber + 3);
	genLDA(reg, 0, ZERO);
	genJEQ(ZERO, tempAssemblyNumber + 4);
	genLDA(reg, 1, ZERO);
	genComment("Finished flipping");
}

// Pop the stack and load the value in top of stack
// into AC2. 
int CodeGen::genPop()
{
	// Load the value in the top of stack into AC2.
	genLD(AC2, 0, SP);
	// Increment SP.
	genLDA(SP, 1, SP);
}

int CodeGen::genIfFalse()
{
	int currentLineNumber = returnTopAndPopFalseLineNumber();
	genJLE(currentLineNumber, AC1, assemblyLineNumber);
}

int CodeGen::genIfTrue()
{
	int numberOfInstructionsToEmit = returnTopIfCount();
	for(int i = 0; i < numberOfInstructionsToEmit; i++)
	{
		int currentLineNumber = returnTopAndPopTrueLineNumber();
		genJEQ(currentLineNumber, ZERO, assemblyLineNumber);
	}
}

int CodeGen::genDoTrue()
{
	int top = popFromDoTrueStack();
	// Using the first method.
	codeGenPrintf("\nThe value that has been popped (genDoTrue) is %d", top);
	genJEQ(ZERO, top);
}
int CodeGen::genDoFalse()
{
	int top = popFromDoFalseStack();
	codeGenPrintf("\nThe value that has been popped (genDoFalse) is %d", top);
	genJLE(top, AC1, assemblyLineNumber);
}

int CodeGen::pushInFaTrueStack()
{
	codeGenPrintf("\nPushing into the FA TRUE STACK");
	codeGenPrintf("\n\t The number that has been pushed in TRUE STACK is %d", assemblyLineNumber);
	faTrueStack.push_back(assemblyLineNumber);
}

int CodeGen::pushInFaFalseStack()
{
	codeGenPrintf("\nPushing into the FA FALSE STACK");
	codeGenPrintf("\n\t The number that has been pushed into FALSE STACK is %d", assemblyLineNumber);
	faFalseStack.push_back(assemblyLineNumber);
	assemblyLineNumber++;
}

int CodeGen::popFromFaTrueStack()
{
	codeGenPrintf("\nPopping from the FA TRUE STACK");
	int top;
	if(faTrueStack.size() > 0)
	{
		top = faTrueStack[faTrueStack.size() - 1];
		faTrueStack.pop_back();
		return top;
	}
	codeGenPrintf("\nSomething is woefully wrong in pop from FA TRUE stack.");
	return -1;
}

int CodeGen::popFromFaFalseStack()
{
	codeGenPrintf("\nPopping from the FA FALSE Stack.");
	int top;
	if(faFalseStack.size() > 0)
	{
		top = faFalseStack[faFalseStack.size() - 1];
		codeGenPrintf("\n\tThe number that has been POPPED from the FA FALSE STACK is %d", top);
		faFalseStack.pop_back();
		return top;
	}
	codeGenPrintf("\nSomething is woefully wrong in pop from FA FALSE stack.");
	return -1;
	
}

int CodeGen::pushToFaVarStack(string nameOfVariable)
{
	faLoopVariables.push_back(nameOfVariable);
	return faLoopVariables.size();
}

int CodeGen::popFromFaVarStack()
{
	faLoopVariables.pop_back();
}

string CodeGen::getTopFaVarStack()
{
	string top;
	int height = faLoopVariables.size() - 1;
	if(height < 0)
	{
		codeGenPrintf("\n\nTROUBLE, TROUBLE! There is nothing in the FA VAR stack. Check getTopFaVarStack().");
		return "";
	}
	return faLoopVariables[height];
}


int CodeGen::pushInDoFalseStack()
{
	codeGenPrintf("\nPushing into the DO FALSE STACK");
	codeGenPrintf("\n\t The number that has been pushed into FALSE STACK is %d", assemblyLineNumber);
	doFalseStack.push_back(assemblyLineNumber);
	assemblyLineNumber++;
}

int CodeGen::popFromDoFalseStack()
{
	codeGenPrintf("\nPopping from the DO False STACK");
	int top;
	if(doFalseStack.size() > 0)
	{
		top = doFalseStack[doFalseStack.size() - 1];
		codeGenPrintf("\n\tThe number that has been POPPED from the DO FALSE STACK is %d", top);
		doFalseStack.pop_back();
		return top;
	}
	codeGenPrintf("\nSomething is woefully wrong in pop from DO FALSE stack.");
	return -1;
}
int CodeGen::pushInDoTrueStack()
{
	// Here we simply need to save the line number. We do not 
	// need to increment the assembly because it will just go back
	// to the beginning. Something like creating a label.
	codeGenPrintf("\nPushing into the DO TRUE STACK");
	codeGenPrintf("\n\t The number that has been pushed in TRUE STACK is %d", assemblyLineNumber);
	doTrueStack.push_back(assemblyLineNumber);
}

int CodeGen::popFromDoTrueStack()
{
	codeGenPrintf("\nPopping from the DO TRUE STACK");
	int top;
	if(doTrueStack.size() > 0)
	{
		top = doTrueStack[doTrueStack.size() - 1];
		doTrueStack.pop_back();
		return top;
	}
	codeGenPrintf("\nSomething is woefully wrong in pop from DO TRUE stack.");
	return -1;
}

int CodeGen::genBreak(int loopDepth)
{
	BreakStuff* topOfBreakStack = popFromBreakStack();
	if(topOfBreakStack == NULL)
	{
		codeGenPrintf("\nSomething is wrong in GEN BREAK. The top of break stack was null");
	}
	else
	{
		int numberOfBreaks = topOfBreakStack->innerBreakStack.size();
		if(numberOfBreaks == 0)
		{
			codeGenPrintf("\nNo Breaks were present in %d loop depth", loopDepth);
		}
		else
		{
			int i = 0;
			for(i = 0; i < numberOfBreaks; i++)
			{
				int top = topOfBreakStack->innerBreakStack[i];
				codeGenPrintf("\nUnconditionally jump to end of loop body");
				genComment("Unconditionally jump to end of loop body");
				genJEQ(top, ZERO, assemblyLineNumber);
			}
		}
	}
}

// Call this method when I start a do/fa.
int CodeGen::pushToBreakStack()
{
	codeGenPrintf("\nPushing a new object into the break stack.");
	BreakStuff* newBreakObject = new BreakStuff();
	breakStack.push_back(newBreakObject);
}

// Call this method when I am finished with it.
BreakStuff* CodeGen::popFromBreakStack()
{
	if(breakStack.size() > 0)
	{
		BreakStuff* topOfBreakStack = breakStack[breakStack.size() - 1];
		breakStack.pop_back();
		return topOfBreakStack;
	}
	else
	{
		codeGenPrintf("\nSomething is woefully wrong in POP FROM BREAK STACK.");
		return NULL;
	}
	
}

int CodeGen::updateBreakStack(int loopDepth)
{
	if(loopDepth <= 0)
	{
		codeGenPrintf("\nThere is a mistake in UPDATE BREAK STACK.");
		codeGenPrintf("\n\tThe loop depth is %d", loopDepth);
		return -1;
	}
	if(breakStack[loopDepth - 1] == NULL)
	{
		codeGenPrintf("\nThere is a mistake in UPDATE BREAK STACK.");
		codeGenPrintf("\n\tThe object in breakStack[loopDepth - 1] is NULL. Loop Depth is %d", loopDepth);
		return -1;
	}
	codeGenPrintf("\nUPDATE BREAK STACK");
	codeGenPrintf("\n\tPushing into breakStack[%d] the value %d", loopDepth - 1, assemblyLineNumber);
	breakStack[loopDepth - 1]->innerBreakStack.push_back(assemblyLineNumber);
	assemblyLineNumber++;
}

int CodeGen::closeStream()
{
	outputFile.close();
}

int CodeGen::pushTrueLineNumber()
{
	codeGenPrintf("\nPushing true line number %d", assemblyLineNumber);
	trueLineNumber.push_back(assemblyLineNumber);
	assemblyLineNumber++;
}

int CodeGen::returnTopAndPopTrueLineNumber()
{
	int top;
	int size = trueLineNumber.size();
	if(trueLineNumber.size() > 0)
	{
		top = trueLineNumber.at(size - 1);
		trueLineNumber.pop_back();
		return top;
	}
	codeGenPrintf("\nSomething is woefully wrong at top and pop true line number!!");
	return -1;
}

int CodeGen::pushFalseLineNumber()
{
	// Save the line number and move to the next line.
	falseLineNumber.push_back(assemblyLineNumber);
	assemblyLineNumber++;
}

int CodeGen::returnTopAndPopFalseLineNumber()
{
	int top;
	int size = falseLineNumber.size();
	if(falseLineNumber.size() > 0)
	{
		top = falseLineNumber.at(size - 1);
		falseLineNumber.pop_back();
		return top;
	}
	codeGenPrintf("\nSomething is woefully wrong at top and pop false line number!!");
	return -1;
}

int CodeGen::increaseIfDepth()
{
	ifVector.push_back(0);
	codeGenPrintf("\nIncreasing Depth. Current Depth is %d", ifVector.size());
}

int CodeGen::decreaseIfDepth()
{
	ifVector.pop_back();
	codeGenPrintf("\nDecreasing Depth. Current Depth is %d", ifVector.size());
	printIfVector();
}

int CodeGen::incrementIfCountAtTop()
{
	codeGenPrintf("\n\tIncreasing IF COUNT");
	int height = ifVector.size() - 1;
	codeGenPrintf("\n\tCurrent Depth (from increment if count) is %d", ifVector.size());
	if(height >= 0)
	{
		ifVector[height]++;
		codeGenPrintf("\n\tCurrent Depth's if count is %d", ifVector[height]);
		return 0;
	}
	codeGenPrintf("\n\n**Something is woefully wrong at increment if count!!**\n\n");
	return -1;
}
int CodeGen::returnTopIfCount()
{
	int top;
	int size = ifVector.size();
	if(size > 0)
	{
		top = ifVector.at(size - 1);
		return top;
	}
	codeGenPrintf("\nSomething is woefully wrong and top and pop if!!");
	return -1;
}

int CodeGen::genComment(string comment)
{
	outputFile << "* " << comment << "\n";
}

int CodeGen::printIfVector()
{
	int i = 0;
	int height = ifVector.size() - 1;
	codeGenPrintf("\n\tCurrent Depth of If Vector (from print Vector) %d", ifVector.size());
	codeGenPrintf("\n\tPrinting IFVECTOR\n\t\t");
	for(i = 0; i <= height; i++)
	{
		codeGenPrintf("%d, ", ifVector[i]);
	}
}

int CodeGen::getNextAddress()
{
	addressOfVariable++;
	return addressOfVariable;
}

int CodeGen::decrementAddress()
{
	addressOfVariable--;
	return addressOfVariable;
}

int CodeGen::getCurrentMemoryAddress()
{
	return addressOfVariable;
}

// Helper Methods

// New comment.
