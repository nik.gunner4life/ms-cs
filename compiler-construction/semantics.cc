
#pragma region AUTHOR INFORMATION
/***********************************************************************
Author:   Nikhil Srivatsan Srinivasan
Unity ID: nsrivat
Project:  Semantic Analysis
Date:     02/20/2014
***********************************************************************/
#pragma endregion

#pragma region HEADERS
#include "semantics.h"
#include "debug.h"
#pragma endregion
extern int nsrDebugPrintf;
extern int codeDebug;
extern int procFlag;

// TODO: Set semanticErrors to 1 in the code before submitting.
extern int semanticErrors;
extern ScopeStack* scopeStackObj;
char *fileName;
extern int yynewlines;
extern char* yytext;

// TODO: Change the way I iterate over the vectors. VERY VERY IMPORTANT.
// This is the constructor of the class. Declare the three base types here
// and add them to the vector. 
int TypeObject::printTypeObject()
{
	nsrPrintf("\n\tAlias Name: %s, Base Type: %s, ", aliasName.c_str(), baseType.c_str());
	nsrPrintf("Dimensions: ");
	vector<int>::iterator fit;
	int i = 0;
	for(fit = dimensions.begin(); fit != dimensions.end(); fit++, i++)
	{
		nsrPrintf("%d, ", dimensions[i]);
	}
}

ProcObject::ProcObject()
{
	lineWhereIWasCalled = 0;
	PST = new SymbolTable();
}
int ProcObject::printProcObject()
{
	nsrPrintf("\n\tProc Name: %s, Proc Type: %s, ", procName.c_str(), procType.c_str());
	nsrPrintf("\n\tArguments Types: ");
	vector<TypeObject*>::iterator fit;
	int i = 0;
	for(fit = argumentList.begin(); fit != argumentList.end(); fit++, i++)
	{
		argumentList[i]->printTypeObject();
	}
}

TypeTable::TypeTable()
{
}

// This will simply insert type object in the insert table. There is some logic
// here that is needed. We first need to calculate the base type before we insert
// it into the type table. This will be present in the baseObject. Just get the
// values from there and push it into the type table. 
int TypeTable::insertInTypeTable(string alias, string type, vector<int> currentDimensions, TypeObject* baseObject)
{
	nsrPrintf("\nInserting in type table");
	TypeObject *newObjToInsert = new TypeObject();
	vector<int>::iterator fit;
	int i = 0;
	for(fit = currentDimensions.begin(); fit != currentDimensions.end(); fit++, i++)
		newObjToInsert->dimensions.push_back(currentDimensions[i]);
	i = 0;
	for(fit = baseObject->dimensions.begin(); fit != baseObject->dimensions.end(); fit++, i++)
		newObjToInsert->dimensions.push_back(baseObject->dimensions[i]);
	newObjToInsert->aliasName = alias;
	newObjToInsert->baseType = baseObject->baseType;
	typeTable.push_back(newObjToInsert);
	return OK;
}

int TypeTable::lookUpAlias(string alias)
{
	nsrPrintf("\n\tLooking Up alias in type Table.");
	vector<TypeObject*>::iterator fit;
	int i = 0;
	for(fit = typeTable.begin(); fit != typeTable.end(); fit++, i++)
	{
		if(typeTable[i]->aliasName == alias)
			return i;
	}
	return NOT_PRESENT;
}

int TypeTable::printTypeTable()
{
	nsrPrintf("\nType Table:\n");
	int length = typeTable.size();
	int i = 0;
	vector<TypeObject*>::iterator fit;
	for(fit = typeTable.begin(); fit != typeTable.end(); fit++, i++)
	{
		string alias = typeTable[i]->aliasName;
		string baseType = typeTable[i]->baseType;
		nsrPrintf("\n\tAlias:%s, Type:%s, Dimensions:", alias.c_str(), baseType.c_str());
		int dimensionLength = typeTable[i]->dimensions.size();
		vector<int>::iterator fitDim;
		int j = 0;
		for(fitDim = typeTable[i]->dimensions.begin(); fitDim != typeTable[i]->dimensions.end(); fitDim++, j++)
			nsrPrintf("%d, ", typeTable[i]->dimensions[j]);
	}
}

// Pass the name of the variable, alias and dimensions (if any).
// Further also get the baseObject this refers to. 
SymbolObject::SymbolObject(string name, string alias, vector<int> currentDimensions, TypeObject* baseObj)
{
	nameOfVariable = name;
	typeObj = new TypeObject();
	typeObj->aliasName = alias;
	typeObj->baseType = baseObj->baseType;
	int i = 0;
	vector<int>::iterator fit;
	for(fit = currentDimensions.begin(); fit != currentDimensions.end(); fit++, i++)
		typeObj->dimensions.push_back(currentDimensions[i]);
	i = 0;
	for(fit = baseObj->dimensions.begin(); fit != baseObj->dimensions.end(); fit++, i++)
		typeObj->dimensions.push_back(baseObj->dimensions[i]);
}

// This method will look iterate over the symbol table
// and check if the symbol exists. If it does, it will
// return the index. Otherwise, it will return NOT_PRESENT.
int SymbolTable::lookUpSymbol(string name)
{
	vector<SymbolObject*>::iterator fit;
	int i = 0;
	for(fit = symbolTable.begin(); fit != symbolTable.end(); fit++, i++)
	{
		if(symbolTable[i]->nameOfVariable == name)
			return i;
	}
	return NOT_PRESENT;
}

SymbolObject* SymbolTable::lookUpSymbolInProc(string name)
{
	vector<SymbolObject*>::iterator fit;
	int i = 0;
	for(fit = symbolTable.begin(); fit != symbolTable.end(); fit++, i++)
	{
		if(symbolTable[i]->nameOfVariable == name)
			return symbolTable[i];
	}
	return NULL;
}

void SymbolTable::incrementSymbolCount()
{
	numberOfSymbols++;
}

int SymbolTable::getNumberOfSymbols()
{
	codeGenPrintf("\nReturning the number of symbols");
	if(symbolTable.size() == numberOfSymbols)
	{
		codeGenPrintf("\n\tThe number of symbols is the same as vector size. CONSISTENT. The number of symbols is %d", numberOfSymbols);
		return numberOfSymbols;
	}
	else
	{
		codeGenPrintf("\n\tThe number of symbols is not the same as the vector size: INCOSISTENT");
		codeGenPrintf("\n\tThe symbol count is %d and vector size is %d", numberOfSymbols, symbolTable.size());
		return -1;
	}
	
}

// The constructor of the symbol object will take care of setting
// all the right values. Once the values are set, push it into the
// symbol table.
// TODO: Do the same thing for type table also. I have unnecessarily
// made this complicated. 
int SymbolTable::insertInSymbolTable(string name, string alias, vector<int> currentDimensions, TypeObject* baseObject, int address)
{
	SymbolObject *newSymbolObject = new SymbolObject(name, alias, currentDimensions, baseObject);
	newSymbolObject->addressOfVariable = address;
	codeGenPrintf("\nName of the variable: %s, Address of Variable: %d", name.c_str(), newSymbolObject->addressOfVariable);
	symbolTable.push_back(newSymbolObject);
}

void SymbolTable::printSymbolTable()
{
	codeGenPrintf("\n\nSymbol Table\n");
	vector<SymbolObject*>::iterator fit;
	int i = 0;
	for(fit = symbolTable.begin(); fit != symbolTable.end(); fit++, i++)
	{
		string alias = symbolTable[i]->typeObj->aliasName;
		string baseType = symbolTable[i]->typeObj->baseType;
		codeGenPrintf("\n\tName: %s, Alias: %s, Base Type: %s, Address: %d, Dimensions:", symbolTable[i]->nameOfVariable.c_str(), alias.c_str(), baseType.c_str(), symbolTable[i]->addressOfVariable);
		vector<int>::iterator fitDim;
		int j = 0;
		for(fitDim = symbolTable[i]->typeObj->dimensions.begin(); fitDim != symbolTable[i]->typeObj->dimensions.end(); fitDim++, j++)
			codeGenPrintf("%d, ", symbolTable[i]->typeObj->dimensions[j]);
	}
}
// Initialize the type table and symbol table.
// TODO: Initialize PROC Table also.
ScopeObject::ScopeObject()
{
	typeTableObj = new TypeTable();
	symbolTableObj = new SymbolTable();
}


int ScopeStack::initializePrimitiveDataTypes()
{
	int i = 0;
	string baseDataTypes[3] = {"int", "bool", "string"};
	for(i = 0; i < 3; i++)
	{
		TypeObject *typeObj = new TypeObject();
		vector<int> setDimensions;
		typeObj->aliasName = baseDataTypes[i];
		typeObj->baseType = baseDataTypes[i];
		typeObj->dimensions = setDimensions;
		scopeStack[0]->typeTableObj->typeTable.push_back(typeObj);
	}
}


// Look through the stack and find the index where the symbol resides.
// If the symbol is found, return index within the stack, else
// return NOT_PRESENT.
int ScopeStack::findScopeWhereSymbolResides(string name)
{
	nsrPrintf("Finding Scope where Symbol resides");
	int height = scopeStack.size();
	int i = height - 1;
	vector<ScopeObject*>::reverse_iterator rit;
	for(rit = scopeStack.rbegin(); rit != scopeStack.rend(); rit++, i--)
	{
		int j = 0;
		vector<SymbolObject*>::iterator fit;
		for(fit = scopeStack[i]->symbolTableObj->symbolTable.begin(); fit != scopeStack[i]->symbolTableObj->symbolTable.end(); fit++, j++)
		{
			if(scopeStack[i]->symbolTableObj->symbolTable[j]->nameOfVariable == name)
			{
				nsrPrintf("\n\tFound Scope where Alias resides. Scope is %d", i);
				return i;
			}
		}
	}
	nsrPrintf("\n\tCould not find a single scope where the alias resides.");
	return NOT_PRESENT;
}
// Look through the stack and find the index where the alias resides.
// If the alias is found, return index within the stack, else
// return NOT_PRESENT.
int ScopeStack::findScopeWhereAliasResides(string alias)
{
	nsrPrintf("\nFinding Scope where Alias resides.");
	int height = scopeStack.size();
	int i = height - 1;
	vector<ScopeObject*>::reverse_iterator rit;
	for(rit = scopeStack.rbegin(); rit != scopeStack.rend(); rit++, i--)
	{
		int j = 0;
		vector<TypeObject*>::iterator fit;
		for(fit = scopeStack[i]->typeTableObj->typeTable.begin(); fit != scopeStack[i]->typeTableObj->typeTable.end(); fit++, j++)
		{
			if(scopeStack[i]->typeTableObj->typeTable[j]->aliasName == alias)
			{
				nsrPrintf("\n\tFound Scope where Alias resides. Scope is %d", i);
				return i;
			}
		}
	}
	nsrPrintf("\n\tCould not find a single scope where the alias resides.");
	return NOT_PRESENT;
}


// Once the alias has been found, return the type Object which matches that.
// This will be useful when we need to calculate base types. This method will
// return an object that will contain the alias and it's base type along with
// the dimensions (if any). In the type table, we store all the aliases along
// with their primitive types and dimensions. 
TypeObject* ScopeStack::getTypeObjectWhichMatchedOurType(int indexWithinStack, string alias)
{
	nsrPrintf("\nFinding Type Object.");
	vector<TypeObject*>::iterator fit;
	int j = 0;
	for(fit = scopeStack[indexWithinStack]->typeTableObj->typeTable.begin(); fit != scopeStack[indexWithinStack]->typeTableObj->typeTable.end(); fit++, j++)
	{
		if(scopeStack[indexWithinStack]->typeTableObj->typeTable[j]->aliasName == alias)
		{
			nsrPrintf("\n\tFound type object. Returning this.");
			return scopeStack[indexWithinStack]->typeTableObj->typeTable[j];
		}
	}
}

// Simple create a new scope.
int ScopeStack::createNewScope()
{
	ScopeObject* newScopeObj = new ScopeObject();
	scopeStack.push_back(newScopeObj);
	nsrPrintf("\nCreated new scope");
}

int ScopeStack::popScope()
{
	scopeStack.pop_back();
	nsrPrintf("\nPopping Scope.");
}

// This method will check if a given symbol already exists in the current scope.
// If it does, then it means that it is being redefined and that is an error.
// We further ensure that there is a scope to check in. If not, throw an error.
int ScopeStack::checkIfSymbolExistsInCurrentScope(string name)
{
	nsrPrintf("\nChecking if symbol exists in the current scope.");
	int lastIndex = scopeStack.size() - 1;
	if(lastIndex < 0)
	{
		nsrPrintf("\nThere is no scope. Ensure you create a scope first before checking SYMBOLS.");
		return SYSERR;
	}
	int found = scopeStack[lastIndex]->symbolTableObj->lookUpSymbol(name.c_str());
	if(found == NOT_PRESENT)
	{
		nsrPrintf("\n\tWe are good. This symbol is not in use.");
		return OK;
	}
	else
	{
		nsrPrintf("\n\tERROR: We are in TROUBLE. The symbol already exists in the current scope. Index in symbol table is %d", found);
		scopeStack[lastIndex]->symbolTableObj->printSymbolTable();
		return SYSERR;
	}
}
// This method will check if a given alias already exists in the current scope.
// If it does, then it means that it is being redefined and that is an error.
int ScopeStack::checkIfAliasExistsInCurrentScope(string alias)
{
	nsrPrintf("\nChecking if scope exists in the current scope.");
	int lastIndex = scopeStack.size() - 1;
	if(lastIndex < 0)
	{
		nsrPrintf("\nThere is no scope. Ensure you create a scope first before checking TYPES.");
		return SYSERR;
	}
	int found = scopeStack[lastIndex]->typeTableObj->lookUpAlias(alias.c_str());
	if(found == NOT_PRESENT)
	{
		nsrPrintf("\n\tWe are good. This alias is not being redefined.");
		return OK;
	}
	else
	{
		nsrPrintf("\n\tERROR: We are in TROUBLE. The alias already exists in the current scope. Index in type table is %d", found);
		scopeStack[lastIndex]->typeTableObj->printTypeTable();
		return SYSERR;
	}
}

// First check if there is no symbol with the same name in the current
// scope. If there is, throw an error. If not, check if the alias
// exists in the type table. If not, throw an error. Otherwise, get
// type object that matches our symbol alias and insert it into the 
// symbol table. 
int ScopeStack::createSymbolIfAllChecksAreFine(string name, string alias, vector<int> currentDimensions, int address)
{
	nsrPrintf("\nDoing all the checks before inserting the symbol in the latest scope.");
	int checkStatus = checkIfSymbolExistsInCurrentScope(name.c_str());
	if(checkStatus == SYSERR)
	{
		nsrPrintf("\n\tERROR: Cannot insert. Symbol %s already exists. Cannot redefine in the current scope.", name.c_str());
		return SYSERR;
	}
	int index = findScopeWhereAliasResides(alias.c_str());
	if(index == NOT_PRESENT)
	{
		nsrPrintf("\n\tERROR: Base type not found.");
		return SYSERR;
	}
	else if(index != NOT_PRESENT)
	{
		TypeObject* baseObj = getTypeObjectWhichMatchedOurType(index, alias.c_str());
		int lastIndex = scopeStack.size() - 1;
		if(lastIndex < 0)
		{
			nsrPrintf("\nERROR: No scope at all. Check again. Code is effed up");
			return SYSERR;
		}
		scopeStack[lastIndex]->symbolTableObj->insertInSymbolTable(name, alias, currentDimensions, baseObj, address);
		nsrPrintf("\nInserted Symbol %s", name.c_str())
		return OK;
	}
}
// This method will check to see if the type has already been created before. 
// If so, it will throw an error. If not, then it will insert it into the 
// symbol table. 
int ScopeStack::checkIfTypeExistsAndThenInsertIntoTypeTable(string alias, string type, vector<int> currentDimensions)
{
	nsrPrintf("\nDoing all the checks before inserting the type in the type table of the latest scope.");
	int checkStatus = checkIfAliasExistsInCurrentScope(alias.c_str());
	if(checkStatus == SYSERR)
	{
		nsrPrintf("ERROR: Cannot insert. Alias %s already exists. Cannot redefine in the current scope.", alias.c_str());
		return SYSERR;
	}
	int index = findScopeWhereAliasResides(type.c_str());
	if(index != NOT_PRESENT)
	{
		// Index is present. Get the base object now. 
		TypeObject* baseObj = getTypeObjectWhichMatchedOurType(index, type.c_str());
		int lastIndex = scopeStack.size() - 1;
		scopeStack[lastIndex]->typeTableObj->insertInTypeTable(alias.c_str(), type.c_str(), currentDimensions, baseObj);
		return OK;
	}
	else if(index == NOT_PRESENT)
	{
		nsrPrintf("\n\tERROR: Base type not found.");
		return SYSERR;
	}
}

// Use this as an example of iterating over a vector. Do not use any other way.
// It will throw segmentation faults. 
int ScopeStack::printStack()
{
	int height = scopeStack.size();
	vector<ScopeObject*>::reverse_iterator rit;
	int index = height - 1;
	for(rit = scopeStack.rbegin(); rit != scopeStack.rend(); rit++, index--)
	{
		nsrPrintf("\nPrinting Tables. Scope is %d", index);
		scopeStack[index]->typeTableObj->printTypeTable();
		scopeStack[index]->symbolTableObj->printSymbolTable();
	}
	vector<ProcObject*>::iterator fit;
	int i = 0;
	for(fit = procTable.begin(); fit != procTable.end(); fit++, i++)
	{
		procTable[i]->printProcObject();
	}
}

SymbolObject* ScopeStack::getSymbolObject(string nameOfSymbol)
{
	int height = scopeStack.size();
	vector<ScopeObject*>::reverse_iterator rit;
	int i = height - 1;
	for(rit = scopeStack.rbegin(); rit != scopeStack.rend(); rit++, i--)
	{
		vector<SymbolObject*>::iterator fit;
		int j = 0;
		for(fit = scopeStack[i]->symbolTableObj->symbolTable.begin(); fit != scopeStack[i]->symbolTableObj->symbolTable.end(); fit++, j++)
		{
			if(scopeStack[i]->symbolTableObj->symbolTable[j]->nameOfVariable == nameOfSymbol)
				return scopeStack[i]->symbolTableObj->symbolTable[j];
		}
	}
	return NULL;
}

TypeObject* ScopeStack::getTypeObject(string typeToSearchFor)
{
	int height = scopeStack.size();
	vector<ScopeObject*>::reverse_iterator rit;
	int i = height - 1;
	for(rit = scopeStack.rbegin(); rit != scopeStack.rend(); rit++, i--)
	{
		vector<TypeObject*>::iterator fit;
		int j = 0;
		for(fit = scopeStack[i]->typeTableObj->typeTable.begin(); fit != scopeStack[i]->typeTableObj->typeTable.end(); fit++, j++)
		{
			if(scopeStack[i]->typeTableObj->typeTable[j]->aliasName == typeToSearchFor)
				return scopeStack[i]->typeTableObj->typeTable[j];
		}
	}
	return NULL;
}

// This method will look up the procTable and return the pointer
// to the Proc Object whose name matches the given name. If no
// such procObject exists, it will return a NULL.
ProcObject* ScopeStack::getProcObject(string name)
{
	vector<ProcObject*>::iterator fit;
	int i = 0;
	for(fit = procTable.begin(); fit != procTable.end(); fit++, i++)
	{
		if(procTable[i]->procName == name)
		{
			nsrPrintf("\nThe proc with name %s exists.", name.c_str());
			return procTable[i];
		}
	}
	nsrPrintf("\nThe proc with name %s DOES NOT exist.", name.c_str());
	return NULL;
}

int ScopeStack::insertInProcTable(vector<TypeObject*> argList, string name, string type, string rtnType)
{
	ProcObject *lookUpProcObject = getProcObject(name);
	if(lookUpProcObject == NULL)
	{
		// Proc/forward is not being redefined. We are good.
		ProcObject* newProcObject = new ProcObject();
		newProcObject->procName = name;
		newProcObject->procType = type;
		newProcObject->returnType = rtnType;
		newProcObject->argumentList = argList;
		newProcObject->lineWhereIWasCalled = 0;
		procTable.push_back(newProcObject);
		nsrPrintf("\nMade an  entry for proc %s", name.c_str());
		return OK;
	}
	else
	{
		if(type == "proc")
		{
			// If the object returned is of type forward, compare the return type 
			// along with the forward declaration. If they do no match, throw
			// and error. if they do match, then go ahead and insert it. If the object
			// returned is a proc itself, throw an error. You cannot redefine
			// a proc. Similarly, a forward declaration cannot happen after the proc
			// is defined. This is also an error. Throw that shite error. 
			if(lookUpProcObject->procType == "forward")
			{
				if(rtnType != lookUpProcObject->returnType)
				{
					nsrPrintf("\nReturn types for %s do not match", name.c_str());
					return FORWARD_AND_PROC_DONT_MATCH;
				}
				// Check to ensure that the arglist match.
				if(argList.size() == lookUpProcObject->argumentList.size())
				{
					vector<TypeObject*>::iterator fit;
					int i = 0;
					for(fit = argList.begin(); fit != argList.end(); fit++, i++)
					{
						if(compareTypes(argList[i], lookUpProcObject->argumentList[i]) != OK)
						{
							nsrPrintf("\nForward and Proc declarations do not match.");
							return FORWARD_AND_PROC_DONT_MATCH;
						}
					}
				}
				else
					return FORWARD_AND_PROC_DONT_MATCH;
				// The forward and proc are fine. So we are good to go. Now we change
				// the type to forward and move on with life. We do this because
				// we will finally make a check and ensure no forwards exist in the
				// proc table. If they do, throw an error.
				lookUpProcObject->procType = "proc";
				return OK;
			}
			else if(lookUpProcObject->procType == "proc")
			{
				nsrPrintf("\nProc %s cannot be redefined.", name.c_str());
				return PROC_REDEFINITION;
			}
			else
			{
				nsrPrintf("Holy Cow batman!! We are so effed.");
				return SYSERR;
			}
		}
		else if(type == "forward")
		{
			nsrPrintf("\nForward or proc %s is being called in an illegal manner.", name.c_str());
			return PROC_BEFORE_FORWARD;
		}
	}
}

int varDeets::printVarDeets()
{
	vector<string>::iterator fitVar;
	int i = 0;
	nsrPrintf("\n->Type Alias: %s", typeAlias.c_str());
	nsrPrintf("\n->Type Base: %s", typeBase.c_str());
	nsrPrintf("\n->Variable Names are:");
	nsrPrintf("\n->Value is %d", value);
	nsrPrintf("\n->Value Defined is %d", valueDefined);
	for(fitVar = varNames.begin(); fitVar != varNames.end(); fitVar++, i++)
		nsrPrintf("%s, ", varNames[i].c_str());
	nsrPrintf("\n->Dimensions are:");
	vector<int>::iterator fitDim;
	i = 0;
	for(fitDim = dimensions.begin(); fitDim != dimensions.end(); fitDim++, i++)
		nsrPrintf("%d, ", dimensions[i]);
	nsrPrintf("\n->Number of dimensions is %d", numberOfDimensions);
	return OK;
}

int varDeets::getTypeBaseCode()
{
	if(strcmp("int",typeBase.c_str()) == 0)
		return 1;
	else if(strcmp("string",typeBase.c_str()) == 0)
		return 2;
	else if(strcmp("bool",typeBase.c_str()) == 0)
		return 3;
	else
		return 0;
}

varDeets::varDeets()
{
	numberOfDimensions = NO_VALUE;
	valueDefined = NO_VALUE;
	iShouldBeProc = 0;
}

// Helper Methods. Should actually be in BISON. But I am keeping it here
// for ease of development (IDE Support). 
int compareTypes(varDeets* tmp1, varDeets* tmp2)
{
	TypeObject *tempTypeObj1 = scopeStackObj->getTypeObject(tmp1->typeAlias);
	TypeObject *tempTypeObj2 = scopeStackObj->getTypeObject(tmp2->typeAlias);
	// Sanity Check
	if((tempTypeObj1 == NULL) || (tempTypeObj2 == NULL))
	{
		nsrPrintf("\nSome type has not been defined.");
		return SYSERR;
	}
	if(tempTypeObj1->baseType == tempTypeObj2->baseType)
	{
		if(tempTypeObj1->dimensions.size() == tempTypeObj2->dimensions.size())
		{
			vector<int>::iterator fit;
			int i = 0;
			for(fit = tempTypeObj1->dimensions.begin(); fit != tempTypeObj1->dimensions.end(); fit++, i++)
			{
				if(tempTypeObj1->dimensions[i] != tempTypeObj2->dimensions[i])
				{
					nsrPrintf("\nThe dimensions did not match. Index is %d. Values are %d and %d", i, tempTypeObj1->dimensions[i], tempTypeObj2->dimensions[i]);
					return SYSERR;
				}
			}
		}
		else
		{
			nsrPrintf("\nThe dimension size did not match. Sizes are %d and %d", tempTypeObj1->dimensions.size(), tempTypeObj2->dimensions.size());
			return SYSERR;
		}
	}
	else
	{
		nsrPrintf("\nThe base type did not match. Types are %s and %s", tempTypeObj1->baseType.c_str() , tempTypeObj2->baseType.c_str());
		return SYSERR;
	}
	nsrPrintf("\nTypes are the same.");
	return OK;
}

 //Method overloaded to handle different types of arguments.
int compareTypes(TypeObject* tempTypeObj1, TypeObject* tempTypeObj2)
{
	if(tempTypeObj1->baseType == tempTypeObj2->baseType)
	{
		if(tempTypeObj1->dimensions.size() == tempTypeObj2->dimensions.size())
		{
			vector<int>::iterator fit;
			int i = 0;
			for(fit = tempTypeObj1->dimensions.begin(); fit != tempTypeObj1->dimensions.end(); fit++, i++)
			{
				if(tempTypeObj1->dimensions[i] != tempTypeObj2->dimensions[i])
				{
					nsrPrintf("\nThe dimensions did not match. Index is %d. Values are %d and %d", i, tempTypeObj1->dimensions[i], tempTypeObj2->dimensions[i]);
					return SYSERR;
				}
			}
		}
		else
		{
			nsrPrintf("\nThe dimension size did not match. Sizes are %d and %d", tempTypeObj1->dimensions.size(), tempTypeObj2->dimensions.size());
			return SYSERR;
		}
	}
	else
	{
		nsrPrintf("\nThe base type did not match. Types are %s and %s", tempTypeObj1->baseType.c_str() , tempTypeObj2->baseType.c_str());
		return SYSERR;
	}
	return OK;
}

int compareBaseTypes(varDeets* tmp1, varDeets* tmp2)
{
	// Sanity Check to ensure I am not working on null pointers.
	if(tmp1 != NULL && tmp2 != NULL)
	{
		if(tmp1->typeBase == tmp2->typeBase)
		{
			nsrPrintf("\nBase types match.");
			return OK;
		}
		else
		{
			nsrPrintf("\nBase Types do not match.Types are %s and %s", tmp1->typeBase.c_str(), tmp2->typeBase.c_str());
			return SYSERR;
		}
	}
	else
	{
		nsrPrintf("\nOne of these objects is NULL. Check code again.");
		return SYSERR;
	}
	nsrPrintf("\nThis should never ever print.");
	return SYSERR;
}

int compareBaseTypes(varDeets* tmp1, varDeets* tmp2, string baseToCompareWith)
{
	if(tmp1 != NULL && tmp2 != NULL)
	{
		if((tmp1->typeBase == tmp2->typeBase) && (tmp1->typeBase == baseToCompareWith))
		{
			nsrPrintf("\nBase types match.");
			return OK;
		}
		else
		{
			nsrPrintf("\nBase Types do not match.Types are %s and %s", tmp1->typeBase.c_str(), tmp2->typeBase.c_str());
			return SYSERR;
		}
	}
	else
	{
		nsrPrintf("\nOne of these objects is NULL. Check code again.");
		return SYSERR;
	}
	nsrPrintf("\nThis should never ever print.");
	return SYSERR;
}

int compareSymbols(varDeets *tmp1, varDeets *tmp2)
{
	SymbolObject *tempSymbolObject1 = scopeStackObj->getSymbolObject(tmp1->typeAlias);
	SymbolObject *tempSymbolObject2 = scopeStackObj->getSymbolObject(tmp2->typeAlias);
	// Sanity Checks
	if((tempSymbolObject1 == NULL) || (tempSymbolObject2 == NULL))
	{
		nsrPrintf("\nOne of these symbol objects does not exist i.e. it is null.");
		return SYSERR;
	}
	TypeObject *tempTypeObj1 = tempSymbolObject1->typeObj;
	TypeObject *tempTypeObj2 = tempSymbolObject2->typeObj;
	if(compareTypes(tempTypeObj1, tempTypeObj2) == OK)
		return OK;
	nsrPrintf("\nThe types of these symbols did not match.");
	return SYSERR;
}

int checkIfAnyForwardHasBeenUsed()
{
	vector<ProcObject*>::iterator fit;
	int i = 0;
	nsrPrintf("\nEnsuring that we do not use forwards that have no proc definitions.");
	for(fit = scopeStackObj->procTable.begin(); fit != scopeStackObj->procTable.end(); fit++, i++)
	{
		// A forward could have been declared but never used. If so, I believe,
		// we are fine. Similarly, if we have a forward declaration and it is
		// has been used, throw error.
		nsrPrintf("\n\t Proc Name: %s, Proc Type: %s, Line where it was used: %d", scopeStackObj->procTable[i]->procName.c_str(), scopeStackObj->procTable[i]->procType.c_str(), scopeStackObj->procTable[i]->lineWhereIWasCalled);
		if(scopeStackObj->procTable[i]->procType == "forward" && scopeStackObj->procTable[i]->lineWhereIWasCalled > 0)
		{
			semanticPrintf("line %d: a forward declaration without a proc definition cannot be used.", scopeStackObj->procTable[i]->lineWhereIWasCalled);
		}
	}
	return OK;
}

