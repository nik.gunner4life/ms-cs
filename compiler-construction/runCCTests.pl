use strict;
use warnings;
use Data::Dumper;
use File::Basename;
use IO::Handle;

my ($pathToIce9Executable, $pathToTmExecutable, $pathToTestFiles, $runTests, $pathToOutputDirectory);
my %listOfFiles;
my %skippedFiles;
my %blackListedFiles;
# For want of a better name
my %whiteListedFiles;

sub setArguments
{
	$pathToIce9Executable = $pathToTmExecutable = $pathToTestFiles = $runTests = $pathToOutputDirectory = "";
	my $argc = scalar @ARGV;
	for(my $i = 0; $i < $argc; $i++) {
		if($ARGV[$i] eq '-tm') {
			$pathToTmExecutable = $ARGV[$i + 1];
			# Remove trailing slash, if any.
			$pathToTmExecutable = $1 if($pathToTmExecutable=~/(.*)\/$/);
		}
		if($ARGV[$i] eq '-ice9') {
			$pathToIce9Executable = $ARGV[$i + 1];
			# Remove trailing slash, if any.
			$pathToIce9Executable = $1 if($pathToIce9Executable=~/(.*)\/$/);
		}
		if($ARGV[$i] eq '-testFiles') {
			$pathToTestFiles = $ARGV[$i + 1];
			# Remove trailing slash, if any.
			$pathToTestFiles = $1 if($pathToTestFiles=~/(.*)\/$/);
			print $pathToTestFiles;
		}
		if($ARGV[$i] eq '-runTests') {
			$runTests = $ARGV[$i + 1];
		}
		if($ARGV[$i] eq '-output') {
			$pathToOutputDirectory = $ARGV[$i + 1];
			# Remove trailing slash, if any.
			$pathToOutputDirectory = $1 if($pathToOutputDirectory=~/(.*)\/$/);
		}
	}	
}

sub findAndPopulateAllDirectoriesAndFiles {
	my $parent = $pathToTestFiles;
	print "\nThis should print";
	my ($parentDir, $subDir);
	opendir($parentDir, $parent);
	while (my $subFolders = readdir($parentDir)) {
		# Here put the condition to ignore the sub directories
		# that we do not want.
					
		# Skip files beginning with period.
		next if ($subFolders =~ m/^\./);  
		print "\nSub Directory is $subFolders";		
		my $path = $parent . "/" . $subFolders;
		print "\nPath is $path";
		# Skip everything that is not a directory
		next unless (-d $path);
		opendir($subDir, $path);
		my $i = 0;
		while (my $file = readdir($subDir)) {
			next if ($file =~ m/^\./);
			my $pathToFile = $path . "/" . 	$file;
			# Push the file into the hash table. 
			$listOfFiles{$pathToFile} = $file;	
		}
		closedir($subDir);				
	}
	closedir($parentDir);
	
}

sub startTesting {
	print "\n\n ----------------------------------------- Starting Tests -----------------------------------------\n\n";
	my ($command, $output, $inputFH, $outputFH);
	my ($fileName, $extension, $file);	
	foreach my $pathToFile (keys %listOfFiles) {
		$file = basename($pathToFile);
		
		# Skip files that have .expected extension.
		next if($file =~ m/\.expected$/);
		
		# Remove extension.
		$fileName = $file;
		$fileName =~ s{\.[^.]+$}{};
		
		# Path to the file without extension
		my $pathToFileWithoutExtension = $pathToFile;
		$pathToFileWithoutExtension =~ s{\.[^.]+$}{};
				
		# Check if $fileName.expected is present. If not
		# skip this file.
		my $expectedFileName = $pathToFileWithoutExtension . ".expected";
		if(!defined($listOfFiles{$expectedFileName})) {
			$skippedFiles{$pathToFile} = $file;
			next;
		}
		print "\nTesting file " . $pathToFile;
		# Compile using ice9 and assemble using tm
		my $ice9 = $pathToIce9Executable . "/ice9";
		$command = "./$ice9 test.tm < " . $pathToFile;
		system($command);
		my $tm = $pathToTmExecutable . "/tm";
		$command = "./$tm -b test.tm > test.tm.out";
		system($command);
		
		# Remove Loading ... and number of instructions executed ...
		$command = "sed -i \"/Loading/d\" test.tm.out; sed -i \"/Number of instructions/d\" test.tm.out";
		system($command);
		
		# Diff against expected output.
		print "\n\tDiffing output against $expectedFileName";
		$command = "diff -Biw test.tm.out $expectedFileName";
		my $output = qx($command);
		if($output eq "") {
			$whiteListedFiles{$pathToFile} = $file;		
		} else {			
			$blackListedFiles{$pathToFile} = $file;
		}
	}
}

sub printResults {
	print "\n\n ----------------------------------------- Test Results ----------------------------------------- \n\n";
	my @keys = keys %blackListedFiles;
	if((scalar @keys) > 0) {
		print "\nThe following files FAILED the validation run.";
		foreach my $key (@keys) {
			print "\n\t $key";
		}
	}
	print "\n";
	@keys = keys %whiteListedFiles;
	if((scalar @keys) > 0) {
		print "\nThe following files PASSED the validation run.";
		foreach my $key (@keys) {
			print "\n\t $key";
		}
	}
	print "\n";
	@keys = keys %skippedFiles;
	if((scalar @keys) > 0) {
		print "\nThe following files were skipped because no expected output was found.";
		foreach my $key (@keys) {
			print "\n\t $key";
		}
	}
	print "\n";	
}

sub sanityChecks() {
	if($pathToIce9Executable eq "") {
		die "\nThe path to the ice9 executable has not been passed. \nPlease use the parameter -ice9 to pass this. Exiting.";
	}
	if($pathToTmExecutable eq "") {
		die "\nThe path to the tm executable has not been passed. \nPlease use the parameter -tm to pass this. Exiting.";
	}
	if($pathToTestFiles eq "") {
		die "\nThe path to the ice9 executable has not been passed. \nPlease use the parameter -testFiles to pass this. Exiting.";
	}	
}

sub printListOfFiles {	
	print "\nThe below are the files that will be used for testing.";
	foreach my $key (keys %listOfFiles) {
		print "\n\t" . $key;
	}	
}



setArguments();
sanityChecks();
findAndPopulateAllDirectoriesAndFiles();
printListOfFiles();
startTesting();
printResults();
print "\n";