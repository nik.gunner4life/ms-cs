%{

#include <stdio.h>
#include "debug.h"
#include "semantics.h"
#include "codeGen.h"
#include "common.h"

extern int yynewlines;
extern char *yytext;
int nsrDebugPrintf = 0;
// TODO: Change this to 0
int codeDebug = 0;
int writeDebug = 0;

int loopCount = 0;
int procCount = 0;
int procFlag = 0;
int ifCount = 0;
int faFlag = 0;
string currentProcName;
string currentProcReturnType;

// TODO: Set this to 1 before turning in the code.
int semanticErrors = 1;
ScopeStack *scopeStackObj;
CodeGen* codeGenObj;
int yylex(void); /* function prototype */

void yyerror(char *s)
{
  if ( *yytext == '\0' )
    fprintf(stderr, "line %d: %s near end of file\n", 
	    yynewlines,s);
  else
    fprintf(stderr, "line %d: %s near %s\n",
	    yynewlines, s, yytext);
}
%}

%code requires 
{
	#include "semantics.h"
	#include "codeGen.h"
	#include "common.h"
}

%union 
{
  long longg;
  char *str;
  varDeets *varList;
  mvrDeets *mvrlist;
}

%token TK_IF
%token TK_FI
%token TK_ELSE
%token TK_DO
%token TK_OD
%token TK_FA
%token TK_AF
%token TK_TO
%token TK_PROC
%token TK_END
%token TK_RETURN
%token TK_FORWARD
%token <str> TK_VAR
%token TK_TYPE
%token TK_BREAK
%token TK_EXIT
%token TK_TRUE
%token TK_FALSE
%token TK_WRITE
%token TK_WRITES
%token TK_READ
%token TK_BOX
%token TK_ARROW
%token TK_LPAREN
%token TK_RPAREN
%token TK_LBRACK
%token TK_RBRACK
%token TK_LSBRACK
%token TK_RSBRACK
%token TK_COLON
%token TK_SEMI
%token TK_ASSIGN
%token TK_COMMA
%token TK_QUEST

%nonassoc TK_EQ TK_NEQ TK_GT TK_LT TK_GE TK_LE
%left TK_PLUS TK_MINUS
%left TK_STAR TK_SLASH TK_MOD
%right TK_QUEST

%token <str> TK_SLIT
%token <longg> TK_INT
%token <str> TK_ID

%type<str> id typeid strLit
%type<varList> pIdList varlist exp expL cgFa arrTerm zArrList zAVList avTerm lvalue decTerm zExpList semIf semDo
%type<mvrlist> mvrList declist
%type<longg> int


%start program

%%

program:   /* empty rule */
		   |
			{	
				// Create default scope that will contain the primitive datatypes.
				scopeStackObj->createNewScope();
				scopeStackObj->initializePrimitiveDataTypes();
				// Create global scope.
				scopeStackObj->createNewScope();
			} 
			zPrgList zStmList 
			{
			   scopeStackObj->printStack();
			   checkIfAnyForwardHasBeenUsed();
			   backPatchBPAndFP();
			   // Pop default and global scope.
			   scopeStackObj->popScope();
			   scopeStackObj->popScope();
			}
		   ;
zStmList: /* empty rule */ 
		  | zStmList stm
		  ;
pStmList: pStmList stm
		  | stm
		  ;
stm:	  do
		  | fa
		  | if
		  | TK_WRITE exp 
			{
				
				// Now comes the proc shite. We should ensure that dimensions
				// match for this expressions, otherwise throw a semantic 
				// error. 
				if($2->dimensionsMatch == DIMENSIONS_DO_NOT_MATCH)
					semanticPrintf("line %d: The number of dimensions do not match for %s", yynewlines, $2->varName.c_str());
				
				// Used with procs.
				if($2->typeBase == "")
				{
					semanticPrintf("line %d: write is incompatible with the expression", yynewlines);
				}
				if($2->typeBase == "bool")
				{
					// TODO: Remove comments. I have commented this because writes cannot print boolean expression. However,
					// to check, I need to see if boolean expressions are getting evaluated properly. Hence commented. 
					//free($2);
					//semanticPrintf("line %d: write is incompatible with a bool expression", yynewlines);
				}
				free($2);

				// Code Generation
				codeGenObj->genOut(AC1);
				codeGenObj->genOutNL();
			}
			TK_SEMI
		  | TK_WRITES exp
			{
				if($2->dimensionsMatch == DIMENSIONS_DO_NOT_MATCH)
					semanticPrintf("line %d: The number of dimensions do not match for %s", yynewlines, $2->varName.c_str());
				if($2->typeBase == "bool")
				{
					// TODO: Remove comments. I have commented this because writes cannot print boolean expression. However,
					// to check, I need to see if boolean expressions are getting evaluated properly. Hence commented. 
					//free($2);
					//semanticPrintf("line %d: write is incompatible with a bool expression", yynewlines);
				}
				// Used with procs.
				if($2->typeBase == "")
				{
					semanticPrintf("line %d: write is incompatible with the expression", yynewlines);
				}
				free($2);

				// Code Generation
				codeGenObj->genOut(AC1);
				// TODO: This has to be removed. 
				codeGenObj->genOutNL();
			}
			TK_SEMI
		  | exp TK_SEMI
			{
				free($1);
			}
		  | lvalue TK_ASSIGN exp TK_SEMI
			{
				// TODO: Now we can have a case where we simply assign a value
				// to an expression. That case will fail because the types
				// will not match. Need to change that now.
				// EDIT: Completed this.
				if($1->valueDefined != NO_VALUE)
				{
					semanticPrintf("line %d: a constant cannot be assigned to another value.", yynewlines);
				}
				// TODO: We can have a case, where the LHS expression can be multiple exps
				// like say a + b. Now we know this cannot hold. And hence it needs to be
				// changed.
				// EDIT: Done this.
				if($1->dimensionsMatch == DIMENSIONS_DO_NOT_MATCH)
				{
					nsrPrintf("\nThis does not match.");
					semanticPrintf("line %d: The number of dimensions do not match for %s", yynewlines, $1->varName.c_str());
				}
				if($3->dimensionsMatch == DIMENSIONS_DO_NOT_MATCH)
				{	
					nsrPrintf("\nexp does not match.");
					semanticPrintf("line %d: The number of dimensions do not match for %s", yynewlines, $3->varName.c_str());
				}

				if($3->valueDefined != NO_VALUE)
				{
					if($3->valueDefined == INTEGER && $1->typeBase == "int")
					{
						nsrPrintf("\nIt is being assigned correctly to int.");	
					}
					else if($3->valueDefined == STRING && $1->typeBase == "string")
					{
						nsrPrintf("\nIt is being assigned correctly to string.");	
					}
					else if($3->valueDefined == BOOL && $1->typeBase == "bool")
					{
						nsrPrintf("\nIt is being assigned correctly to bool.");	
					}
					else
					{
						free($1);
						free($3);
						semanticPrintf("line %d: type mismatch for :=", yynewlines);
					}
					free($1);
					free($3);
				}
				else
				{
					int status = compareBaseTypes($1, $3);
					if(status == SYSERR)
					{
						$1->printVarDeets();
						$3->printVarDeets();
						semanticPrintf("line %d: type mismatch near :=", yynewlines);						
					}
				}

				// Code Generation
				// Get the symbol's address and store the value. 
				// The value of the expression will be in AC1. Store the value of AC1
				// in the address of the variable.
				string nameOfVariable = $1->varName;
				storeValueInID(nameOfVariable);
			}
		  | TK_RETURN TK_SEMI
			{
				if(procCount > 0)
				{
					// printf("warning : line %d: return used inside a function that has no return type.", yynewlines);
				}

				// Code Generation
				// Now if the proc flag is set it means that we are inside a proc.
				// So lets take care of this. 
				handleReturn();
			}
		  | TK_BREAK TK_SEMI
			{
				// Use a global loop count variable and do this(ugly I know). 
				// Increment loop count when we see a loop. Decrement it when
				// we leave the loop. If loop count <= 0 when we see this,
				// throw error.
				if(loopCount <= 0) 
				{ 
					semanticPrintf("line %d: break can only be used inside a loop", yynewlines) 
				}

				// Code Generation
				// If I see a break statement, I must exit the current loop. This is an unconditional
				// jump to the end of the current loop body.
				// Save the line number where this has to be emitted. 
				codeGenObj->updateBreakStack(loopCount);
			}
		  | TK_EXIT TK_SEMI 
		  | TK_SEMI
		  ;
do:   
		  semDo TK_ARROW zStmList TK_OD 
			{
				// Here we emit a line that will jump
				// back to the beginning.
				codeGenObj->genDoTrue();
				// Here we will emit the line that we had
				// saved for the false condition.
				codeGenObj->genDoFalse();
				// First emit a line for any break statement
				// that may have been present in the loop body
				codeGenObj->genBreak(loopCount);
				loopCount--; 
			}
		  ;
semDo:	  
		  cgDo exp 
			{
				// Now comes the proc shite. We should ensure that dimensions
				// match for this expressions, otherwise throw a semantic 
				// error. 
				loopCount++;
				if($2->dimensionsMatch == DIMENSIONS_DO_NOT_MATCH)
					semanticPrintf("line %d: The number of dimensions do not match for %s", yynewlines, $2->varName.c_str());

				if($2->typeBase != "bool")
				{
					free($2);
					semanticPrintf("line %d: incompatible expression used in do. Expecting bool expression.", yynewlines);
				}
				// Might need this later. But for now, freeing it.
				free($2);

				// Code Generation
				// Now if the expression evaluates to true we can carry forward.
				// If not, we need to jump to the end of the loop body. For this
				// case we will need to save the false condition line number and
				// emit that later.
				// Save the line number;
				codeGenPrintf("\nCalling pushInDoFalseStack from ice9 on line %d", yynewlines);
				codeGenObj->pushInDoFalseStack();
			}
		  ;
cgDo:	
		  TK_DO
			{
				// Code Generation
				// Save the assembly line number. This will be the begin: label
				// that we will be using for later.
				codeGenPrintf("\nCalling pushInDoTrueStack from ice9 on line %d", yynewlines);
				codeGenObj->pushInDoTrueStack();

				// Also create a break stack object.
				codeGenObj->pushToBreakStack();
			}
			;
		  
fa:
		  semFa TK_ARROW zStmList TK_AF
			{
				// Code Generation
				handleFa3();
				faFlag--;
				// fa is over. Pop the scope.
				// Semantics
				loopCount--;
				scopeStackObj->popScope();
			}	
		  ;
semFa:
		  cgFa TK_TO exp
			{
				
				loopCount++;
				// Create a new scope and push id into the symbol
				// table. It has to be of type int and it should have no
				// dimensions. Then check exp TK_TO exp and ensure both
				// exp's are ints. If not, throw an error.
				// One way to check the scope is to declare a var with the
				// same name but different type(bool, string, etc) and then
				// do some operations that are valid for only bool, string
				// and so on. They should fail. If they pass, scoping has
				// failed you. Go cry in shame!!

				// Now comes the proc shite. We should ensure that dimensions
				// match for this expressions, otherwise throw a semantic 
				// error. 

				// TODO: Remove these comments.
				if($1->dimensionsMatch == DIMENSIONS_DO_NOT_MATCH)
					semanticPrintf("line %d: The number of dimensions do not match for %s", yynewlines, $1->varName.c_str());
				if($3->dimensionsMatch == DIMENSIONS_DO_NOT_MATCH)
					semanticPrintf("line %d: The number of dimensions do not match for %s", yynewlines, $3->varName.c_str());

				if(compareBaseTypes($1, $3, "int") == SYSERR)
				{
					semanticPrintf("line %d: incompatible expression types used in for.", yynewlines);
				}

				// Code Generation
				// A new scope has been created in cgFa. Also the fa flag has also been assigned.
				// Further the loop variable now also has been initialized. 
				// So this is the part of the code that we will be jumping to at every iteration.

				handleFa2();
			}
		  ;

cgFa:	  TK_FA id TK_ASSIGN exp
			{
				faFlag++;
				scopeStackObj->createNewScope();
				string loopVariableName = $2;
				string loopTypeBase = "int";
				vector<int> dimensions;
				// fa
				if(procFlag == 0)
				{
					scopeStackObj->createSymbolIfAllChecksAreFine(loopVariableName, loopTypeBase, dimensions, codeGenObj->getNextAddress());
				}
				else if(procFlag == 1)
				{
					scopeStackObj->createSymbolIfAllChecksAreFine(loopVariableName, loopTypeBase, dimensions, -1);
					insertInPST(loopVariableName, loopTypeBase, dimensions);
				}
				$$ = $4;
				handleFa1($2);
			}
if:
		  cgIf ifList TK_FI
			{
				// Code Generation
				// Now we know the expression type and we also know which line number
				// to jump to. So just emit the necessary code.
				// codeGenObj->genIfFalse();
				// codeGenObj->decreaseIfDepth();

				//// Now if there has been only one if condition
				//// this would be reflected in the if top count.
				//// So if it is one, just get that.

				int count = codeGenObj->returnTopIfCount();
				if(count == 1)
				{
					codeGenPrintf("\nOnly 1 if. Generating false conditions.");
					codeGenObj->genIfFalse();
					codeGenObj->genIfTrue();
					//// Here we need to pop the trueLineNumber stack.
					//// This is because after the if pStmList, we add a true
					//// line number. 
					//// codeGenObj->returnTopAndPopTrueLineNumber();
					//// Now decrement the if depth.
					
				}
				else if(count > 1)
				{
					codeGenPrintf("\nThere have been %d else ifs", count);
					//// Now we know that there is at least one elsif.
					//// We need to generate code for all the elsif and if
					//// conditions so that they jump out of the entire if
					//// body.
					codeGenObj->genIfFalse();
					codeGenObj->genIfTrue();
					//// Now decrement the if depth.
					codeGenObj->decreaseIfDepth();
				}
			}
		  | cgIf ifList else pStmList TK_FI
			{
				// codeGenObj->genComment("The true conditions should point here.");
				// codeGenObj->genIfTrue();
				// codeGenObj->decreaseIfDepth();
				// codeGenObj->genComment("Decreased if Depth");

				//// NEW CODE GENERATION
				//// Now we need to emit all the true conditions of all the if/elsif
				//// blocks that had come before this. So lets go ahead and do that.
				codeGenPrintf("\nEmitting all the true conditions for previous if/elsif's at %d", yynewlines);
				codeGenObj->genIfTrue();
				//// Decrease the ifDepth.
				codeGenPrintf("\nDecreased the if depth.");
				codeGenObj->decreaseIfDepth();
			}
		  ;
else:
			TK_BOX TK_ELSE TK_ARROW
			{
				// codeGenObj->genComment("The false condition should point here.");
				// codeGenObj->genIfFalse();

				//// NEW CODE GENERATION
				//// The previous elsif or if's false condition should point here.
				//// So lets do that.
				codeGenPrintf("\nHitting the else block at %d", yynewlines);
				codeGenObj->genComment("The previous if/elsif condition should point here.");
				//// Now we will generate the code for the previous if/elsif's false condition
				codeGenObj->genIfFalse();
			}
cgIf:		
			semIf TK_ARROW pStmList
			{
				// Code Generation
				// If the condition evaluates to true, we need to skip all other checks
				// and move to the end i.e. fi.
				//codeGenPrintf("\nThe line number must be pushed here.");
				// codeGenObj->pushTrueLineNumber();

				//// NEW CODE GENERATION
				//// Now let us handle the condition of if else if.
				//// if there exists an else if the false condition should
				//// jump there and we need to set the true condition to jump 
				//// out of the entire if body.
				//// So first let us save the line number here for true condition
				//// Because it is here that we will insert a jump that will jump
				//// out of the entire if body.
				codeGenPrintf("\nSaving the line number for if true condition");
				codeGenObj->genComment("Saving line number for if's true condition.");
				codeGenObj->pushTrueLineNumber();
			}
semIf:	  TK_IF exp
			{
				if($2->dimensionsMatch == DIMENSIONS_DO_NOT_MATCH)
					semanticPrintf("line %d: The number of dimensions do not match for %s", yynewlines, $2->varName.c_str());

				if($2->typeBase != "bool")
				{
					free($2);
					semanticPrintf("line %d: incompatible expression used in if.", yynewlines);
				}
				varDeets* tmp = new varDeets();
				$$ = tmp;
				$$->expType = $2->expType;
				// Might need this later. But for now, freeing it.
				free($2);

				//// NEW CODE GENERATION
				//// First thing we do is increase ifDepth
				codeGenObj->increaseIfDepth();
				codeGenObj->genComment("Increasing If depth");
				//// Next thing we do is increase the if count
				codeGenObj->incrementIfCountAtTop();
				codeGenObj->genComment("Increasing if count at top in if");
				//// Next thing we do will be to push the false line number
				//// The false line number is the line number that we should
				//// jump to incase there the exp is false.
				codeGenObj->pushFalseLineNumber();
				//// Purely for debug purposes.
				codeGenObj->printIfVector();


				// Code Generation.
				// I will now need to save the line number so that it can be emitted
				// later. No code will actually be generated here. This will be handled
				// in the if production.
				//codeGenObj->increaseIfDepth();
				// codeGenObj->genComment("Increasing if Depth");
				// codeGenObj->incrementIfCountAtTop();
				// codeGenObj->genComment("Increasing if Count");
				// codeGenObj->printIfVector();
				// codeGenObj->pushFalseLineNumber();
			}
		  ;
ifList:	  /* empty rule */
		  | ifList ifTerm		  
		  ;
ifTerm:
		  semBox TK_ARROW pStmList
			{	
				// Code Generation.
				// If the expression evaluates to true, skip all other conditions and 
				// get out the current if-fi block. 
				// codeGenObj->pushTrueLineNumber();

				//// NEW CODE GENERATION
				//// So first let us save the line number here for true condition
				//// Because it is here that we will insert a jump that will jump
				//// out of the entire if body.
				codeGenPrintf("\nSaving the line number for elsif true condition at %d", yynewlines);
				codeGenObj->genComment("Saving line number for if's true condition.");
				codeGenObj->pushTrueLineNumber();
			}
		  ;

semBox:	  	
			TK_BOX 
			{
				// Code Generation
				// If the previous one has evaluated to false, then it should jump here.
				// codeGenObj->genComment("The false condition should point here.");
				// codeGenObj->genIfFalse();

				//// NEW CODE GENERATION
				//// The previous if or else if false condition should now point here.
				//// We will be popping the latest number and adding a jump for it over
				//// here.
				codeGenObj->genIfFalse();
				codeGenPrintf("\nGenerated false code for this at %d", yynewlines);

				//// Also increment the if count at top.
				codeGenObj->incrementIfCountAtTop();
			}
			exp
			{
				
				if($3->dimensionsMatch == DIMENSIONS_DO_NOT_MATCH)
					semanticPrintf("line %d: The number of dimensions do not match for %s", yynewlines, $3->varName.c_str());

				if($3->typeBase != "bool")
				{
					free($3);
					semanticPrintf("line %d: incompatible expression used in if.", yynewlines);
				}
				// Might need this later. But for now, freeing it.
				free($3);

				// Code Generation
				// Save the line number.
				// codeGenObj->incrementIfCountAtTop();
				// codeGenObj->pushFalseLineNumber();

				//// NEW CODE GENERATION
				//// Now once again we need to check if the condition evaluates to false
				//// we need to save the number and save it here.
				codeGenPrintf("\nPushing the false line number for %d", yynewlines);
				codeGenObj->genComment("Saving the line for elsif false condition");
				codeGenObj->pushFalseLineNumber();
			}
		  ;
type:
		  TK_TYPE id TK_EQ typeid zArrList TK_SEMI
			{
				// Don't really need varDeets here. Just doing this though.
				// Might need it later.
				varDeets *tmp = new varDeets();
				tmp->typeAlias = $2;
				tmp->typeBase = $4;
				if($5 != NULL)
					tmp->dimensions = $5->dimensions;
				//tmp->printVarDeets();
				// TODO: Set the dimensions also.
				scopeStackObj->checkIfTypeExistsAndThenInsertIntoTypeTable(tmp->typeAlias, tmp->typeBase, tmp->dimensions);
				free($4);
			}
		  ;
forward:
		  funcDec TK_SEMI
		  ;
proc:	  
		  funcDef zTVList zStmList TK_END
			{
				// Code Generation
				createActivationRecord_2();
				// Write a method to get the return address
				// pop the frame pointer and get to the caller function.
				// Implicitly Load the return value in AC1.
				implicitlyLoadReturnValueInAC1WhenQutting();
				popARAndReturnFromProc();
				// Backpatch to unconditionally avoid the proc.
				backPatchUnconditionalJumpToAvoidAProc();
				
				// Semantics
				scopeStackObj->popScope();
				if(procCount > 0)
					procCount--;
				procFlag = 0;
				currentProcName = "";
				currentProcReturnType = "";
			}
		  ;
funcDec:
		  TK_FORWARD id TK_LBRACK declist TK_RBRACK
			{
				if($4 != NULL)
				{
					// Insert it all in symbol and proc table. This should never throw an 
					// when entering symbols because we have created a new scope. The only
					// error this can throw will be when it cannot find a type for the
					// symbol. 
					nsrPrintf("\nStarted bookkeeping.");
					vector<varDeets*>::iterator fit;
					int i = 0;
					vector<TypeObject*> allTypes;
					for(fit = $4->mvrVector.begin(); fit != $4->mvrVector.end(); fit++, i++)
					{
						nsrPrintf("\nStarted iteration over mvrList.");
						vector<string>::iterator fitVar;
						int j = 0;
						for(fitVar = $4->mvrVector[i]->varNames.begin(); fitVar != $4->mvrVector[i]->varNames.end(); fitVar++, j++)
						{
							TypeObject* newTypeObjToInsert = scopeStackObj->getTypeObject($4->mvrVector[i]->typeAlias);
							if(newTypeObjToInsert != NULL)
								allTypes.push_back(newTypeObjToInsert);
						}
					}
					string procName = $2;
					string procType = "forward";
					string rtnType = "";
					int insertStatus = scopeStackObj->insertInProcTable(allTypes, procName, procType, rtnType);
					if(insertStatus == OK)
					{
						// DO nothing
					}
					else if(insertStatus == PROC_BEFORE_FORWARD)
					{
						semanticPrintf("line %d:  illegal redefinition of  %s", yynewlines, $2);
					}
					else if(insertStatus == FORWARD_AND_PROC_DONT_MATCH)
					{
						semanticPrintf("line %d: forward and proc declarations don't match for %s", yynewlines, $2);
					}
					else if(insertStatus == PROC_REDEFINITION)
					{
						semanticPrintf("line %d:  illegal redefinition of  %s", yynewlines, $2);
					}
					else
					{
						semanticPrintf("line %d: forward declaration is wrong for %s.", yynewlines, $2);
					}
				}
				else
				{
					nsrPrintf("\nNull pointer. Lets see what happens.");
					string procName = $2;
					string procType = "forward";
					string rtnType = "";
					// Send an empty vector.
					vector<TypeObject*> allTypes;
					int insertStatus = scopeStackObj->insertInProcTable(allTypes, procName, procType, rtnType);
					if(insertStatus == OK)
					{
						// DO nothing
					}
					else if(insertStatus == PROC_BEFORE_FORWARD)
					{
						semanticPrintf("line %d:  illegal redefinition of  %s", yynewlines, $2);
					}
					else if(insertStatus == FORWARD_AND_PROC_DONT_MATCH)
					{
						semanticPrintf("line %d: forward and proc declarations don't match for %s", yynewlines, $2);
					}
					else if(insertStatus == PROC_REDEFINITION)
					{
						semanticPrintf("line %d:  illegal redefinition of  %s", yynewlines, $2);
					}
					else
					{
						semanticPrintf("line %d: forward declaration is wrong for %s.", yynewlines, $2);
					}
				}
			}
		  | TK_FORWARD id TK_LBRACK declist TK_RBRACK TK_COLON typeid
			{	
				TypeObject* intTypeObj = scopeStackObj->getTypeObject("int");
				TypeObject* boolTypeObj = scopeStackObj->getTypeObject("bool");
				TypeObject* stringTypeObj = scopeStackObj->getTypeObject("string");
				TypeObject* ourTypeObject = scopeStackObj->getTypeObject($7);
				string rtnType;
				if((compareTypes(intTypeObj, ourTypeObject) == OK)) 
					rtnType = "int";
				else if((compareTypes(boolTypeObj, ourTypeObject) == OK)) 
					rtnType = "bool";
				else if((compareTypes(stringTypeObj, ourTypeObject) == OK)) 
					rtnType = "string";
				if($4 != NULL)
				{
					// Insert it all in symbol and proc table. This should never throw an 
					// when entering symbols because we have created a new scope. The only
					// error this can throw will be when it cannot find a type for the
					// symbol. 
					nsrPrintf("\nStarted bookkeeping.");

					vector<varDeets*>::iterator fit;
					int i = 0;
					vector<TypeObject*> allTypes;
					for(fit = $4->mvrVector.begin(); fit != $4->mvrVector.end(); fit++, i++)
					{
						nsrPrintf("\nStarted iteration over mvrList.");
						vector<string>::iterator fitVar;
						int j = 0;
						for(fitVar = $4->mvrVector[i]->varNames.begin(); fitVar != $4->mvrVector[i]->varNames.end(); fitVar++, j++)
						{
							TypeObject* newTypeObjToInsert = scopeStackObj->getTypeObject($4->mvrVector[i]->typeAlias);
							if(newTypeObjToInsert != NULL)
								allTypes.push_back(newTypeObjToInsert);
						}
					}
					string procName = $2;
					string procType = "forward";
					int insertStatus = scopeStackObj->insertInProcTable(allTypes, procName, procType, rtnType);
					if(insertStatus == OK)
					{
						// DO nothing
					}
					else if(insertStatus == PROC_BEFORE_FORWARD)
					{
						semanticPrintf("line %d:  illegal redefinition of  %s", yynewlines, $2);
					}
					else if(insertStatus == FORWARD_AND_PROC_DONT_MATCH)
					{
						semanticPrintf("line %d: forward and proc declarations don't match for %s", yynewlines, $2);
					}
					else if(insertStatus == PROC_REDEFINITION)
					{
						semanticPrintf("line %d:  illegal redefinition of  %s", yynewlines, $2);
					}
					else
					{
						semanticPrintf("line %d: forward declaration is wrong for %s.", yynewlines, $2);
					}
				}
				else
				{
					nsrPrintf("\nNull pointer. Lets see what happens.");
					string procName = $2;
					string procType = "forward";
					// Send an empty vector.
					vector<TypeObject*> allTypes;
					int insertStatus = scopeStackObj->insertInProcTable(allTypes, procName, procType, rtnType);
					if(insertStatus == OK)
					{
						// DO nothing
					}
					else if(insertStatus == PROC_BEFORE_FORWARD)
					{
						semanticPrintf("line %d:  illegal redefinition of  %s", yynewlines, $2);
					}
					else if(insertStatus == FORWARD_AND_PROC_DONT_MATCH)
					{
						semanticPrintf("line %d: forward and proc declarations don't match for %s", yynewlines, $2);
					}
					else if(insertStatus == PROC_REDEFINITION)
					{
						semanticPrintf("line %d:  illegal redefinition of  %s", yynewlines, $2);
					}
					else
					{
						semanticPrintf("line %d: forward declaration is wrong for %s.", yynewlines, $2);
					}
				}
			}
		  ;


funcDef: 
		  procKey id TK_LBRACK declist TK_RBRACK
			{
				procCount++;
				scopeStackObj->createNewScope();
				if($4 != NULL)
				{
					// Insert it all in symbol and proc table. This should never throw an 
					// when entering symbols because we have created a new scope. The only
					// error this can throw will be when it cannot find a type for the
					// symbol. 
					nsrPrintf("\nStarted bookkeeping.");
					vector<varDeets*>::iterator fit;
					int i = 0;
					vector<TypeObject*> allTypes;
					for(fit = $4->mvrVector.begin(); fit != $4->mvrVector.end(); fit++, i++)
					{
						nsrPrintf("\nStarted iteration over mvrList.");
						vector<string>::iterator fitVar;
						int j = 0;
						for(fitVar = $4->mvrVector[i]->varNames.begin(); fitVar != $4->mvrVector[i]->varNames.end(); fitVar++, j++)
						{
							// funcDef
							if(scopeStackObj->createSymbolIfAllChecksAreFine($4->mvrVector[i]->varNames[j].c_str(), $4->mvrVector[i]->typeAlias.c_str(), $4->mvrVector[i]->dimensions, -1) == SYSERR)
							{
								codeGenObj->decrementAddress();
								semanticPrintf("line %d: The symbol %s already exists", yynewlines, $4->mvrVector[i]->varNames[j].c_str());
							}
							TypeObject* newTypeObjToInsert = scopeStackObj->getTypeObject($4->mvrVector[i]->typeAlias);
							if(newTypeObjToInsert != NULL)
							{
								allTypes.push_back(newTypeObjToInsert);
							}
						}
					}
					string procName = $2;
					string procType = "proc";
					string rtnType = "";
					int insertStatus = scopeStackObj->insertInProcTable(allTypes, procName, procType, rtnType);
					if(insertStatus == OK)
					{
						// DO nothing
					}
					else if(insertStatus == PROC_BEFORE_FORWARD)
					{
						semanticPrintf("line %d:  illegal redefinition of  %s", yynewlines, $2);
					}
					else if(insertStatus == FORWARD_AND_PROC_DONT_MATCH)
					{
						semanticPrintf("line %d: forward and proc declarations don't match for %s", yynewlines, $2);
					}
					else if(insertStatus == PROC_REDEFINITION)
					{
						semanticPrintf("line %d:  illegal redefinition of  %s", yynewlines, $2);
					}
					else
					{
						semanticPrintf("line %d: proc declaration is wrong for %s.", yynewlines, $2);
					}
				}
				else
				{
					nsrPrintf("\nNull pointer. Lets see what happens.");
					string procName = $2;
					string procType = "proc";
					string rtnType = "";
					vector<TypeObject*> allTypes;
					int insertStatus = scopeStackObj->insertInProcTable(allTypes, procName, procType, rtnType);
					if(insertStatus == OK)
					{
						// DO nothing
					}
					else if(insertStatus == PROC_BEFORE_FORWARD)
					{
						semanticPrintf("line %d:  illegal redefinition of  %s", yynewlines, $2);
					}
					else if(insertStatus == FORWARD_AND_PROC_DONT_MATCH)
					{
						semanticPrintf("line %d: forward and proc declarations don't match for %s", yynewlines, $2);
					}
					else if(insertStatus == PROC_REDEFINITION)
					{
						semanticPrintf("line %d:  illegal redefinition of  %s", yynewlines, $2);
					}
					else
					{
						semanticPrintf("line %d: proc declaration is wrong for %s.", yynewlines, $2);
					}
				}

				// Code Generation
				// What we will be doing here is that we will adding the declist to another symbol table
				// called the PST. Do it carefully. All the information is stored in the mvrDeets.
				currentProcName = $2;
				currentProcReturnType = "";
				saveProcDeclarationLineNumber(currentProcName);
				createPST($4, currentProcName, currentProcReturnType);
				// Create the activation record now.
				createActivationRecord_1(currentProcName);
			}
		  | procKey id TK_LBRACK declist TK_RBRACK TK_COLON typeid
			{
				scopeStackObj->createNewScope();
				if($4 != NULL)
				{
					// Insert it all in symbol and proc table. This should never throw an 
					// when entering symbols because we have created a new scope. The only
					// error this can throw will be when it cannot find a type for the
					// symbol. 
					nsrPrintf("\nStarted bookkeeping.");
					vector<varDeets*>::iterator fit;
					int i = 0;
					vector<TypeObject*> allTypes;

					// Create a new symbol for the return type. Same name as the proc name.
					// Ensure the return type is int/string/bool.
					TypeObject* intTypeObj = scopeStackObj->getTypeObject("int");
					TypeObject* boolTypeObj = scopeStackObj->getTypeObject("bool");
					TypeObject* stringTypeObj = scopeStackObj->getTypeObject("string");
					TypeObject* ourTypeObject = scopeStackObj->getTypeObject($7);
					string rtnType;
					if((compareTypes(intTypeObj, ourTypeObject) == OK)) 
					{
						rtnType = "int";
					}
					else if((compareTypes(boolTypeObj, ourTypeObject) == OK)) 
					{
						rtnType = "bool";
					}
					else if((compareTypes(stringTypeObj, ourTypeObject) == OK)) 
					{
						rtnType = "string";
					}
					else
					{
						semanticPrintf("line %d: the return type has to be a primitive data type.", yynewlines);
					}
					varDeets* returnSymbol = new varDeets();
					returnSymbol->varNames.push_back($2);
					returnSymbol->typeAlias = $7;
					$4->mvrVector.push_back(returnSymbol);

					for(fit = $4->mvrVector.begin(); fit != $4->mvrVector.end(); fit++, i++)
					{
						nsrPrintf("\nStarted iteration over mvrList in proc with typeid.");
						vector<string>::iterator fitVar;
						int j = 0;
						for(fitVar = $4->mvrVector[i]->varNames.begin(); fitVar != $4->mvrVector[i]->varNames.end(); fitVar++, j++)
						{
							// funcDef
							if(scopeStackObj->createSymbolIfAllChecksAreFine($4->mvrVector[i]->varNames[j].c_str(), $4->mvrVector[i]->typeAlias.c_str(), $4->mvrVector[i]->dimensions, -1) == SYSERR)
							{
								codeGenObj->decrementAddress();
								semanticPrintf("line %d: The symbol %s already exists", yynewlines, $4->mvrVector[i]->varNames[j].c_str());
							}
							TypeObject* newTypeObjToInsert = scopeStackObj->getTypeObject($4->mvrVector[i]->typeAlias);
							if(newTypeObjToInsert != NULL)
							{
								allTypes.push_back(newTypeObjToInsert);
							}
						}
					}
					// This is because I also insert the implicit return type. This is not an argument
					// though. So pop it.
					allTypes.pop_back();
					string procName = $2;
					string procType = "proc";
					
					int insertStatus = scopeStackObj->insertInProcTable(allTypes, procName, procType, rtnType);
					if(insertStatus == OK)
					{
						// DO nothing
					}
					else if(insertStatus == PROC_BEFORE_FORWARD)
					{
						semanticPrintf("line %d:  illegal redefinition of  %s", yynewlines, $2);
					}
					else if(insertStatus == FORWARD_AND_PROC_DONT_MATCH)
					{
						semanticPrintf("line %d: forward and proc declarations don't match for %s", yynewlines, $2);
					}
					else if(insertStatus == PROC_REDEFINITION)
					{
						semanticPrintf("line %d:  illegal redefinition of  %s", yynewlines, $2);
					}
					else
					{
						semanticPrintf("line %d: proc declaration is wrong for %s.", yynewlines, $2);
					}
				}
				else
				{
					nsrPrintf("\nNull pointer. Lets see what happens.");
					TypeObject* intTypeObj = scopeStackObj->getTypeObject("int");
					TypeObject* boolTypeObj = scopeStackObj->getTypeObject("bool");
					TypeObject* stringTypeObj = scopeStackObj->getTypeObject("string");
					TypeObject* ourTypeObject = scopeStackObj->getTypeObject($7);
					string procName = $2;
					string procType = "proc";
					string rtnType;
					vector<TypeObject*> allTypes;
					if((compareTypes(intTypeObj, ourTypeObject) == OK)) 
					{
						rtnType = "int";
					}
					else if((compareTypes(boolTypeObj, ourTypeObject) == OK)) 
					{
						rtnType = "bool";
					}
					else if((compareTypes(stringTypeObj, ourTypeObject) == OK)) 
					{
						rtnType = "string";
					}
					vector<int> dimensions;
					// funcDef
					scopeStackObj->createSymbolIfAllChecksAreFine($2, $7, dimensions, -1);
					int insertStatus = scopeStackObj->insertInProcTable(allTypes,procName, procType, rtnType);
					if(insertStatus == OK)
					{
						// DO nothing
					}
					else if(insertStatus == PROC_BEFORE_FORWARD)
					{
						semanticPrintf("line %d:  illegal redefinition of  %s", yynewlines, $2);
					}
					else if(insertStatus == FORWARD_AND_PROC_DONT_MATCH)
					{
						semanticPrintf("line %d: forward and proc declarations don't match for %s", yynewlines, $2);
					}
					else if(insertStatus == PROC_REDEFINITION)
					{
						semanticPrintf("line %d:  illegal redefinition of  %s", yynewlines, $2);
					}
					else
					{
						semanticPrintf("line %d: proc declaration is wrong for %s.", yynewlines, $2);
					}
				}

				// Code Generation
				// Code Generation
				// What we will be doing here is that we will adding the declist to another symbol table
				// called the PST. Do it carefully. All the information is stored in the mvrDeets.
				TypeObject* intTypeObj = scopeStackObj->getTypeObject("int");
				TypeObject* boolTypeObj = scopeStackObj->getTypeObject("bool");
				TypeObject* stringTypeObj = scopeStackObj->getTypeObject("string");
				TypeObject* ourTypeObject = scopeStackObj->getTypeObject($7);
				if((compareTypes(intTypeObj, ourTypeObject) == OK)) 
				{
					currentProcReturnType = "int";
				}
				else if((compareTypes(boolTypeObj, ourTypeObject) == OK)) 
				{
					currentProcReturnType = "bool";
				}
				else if((compareTypes(stringTypeObj, ourTypeObject) == OK)) 
				{
					currentProcReturnType = "string";
				}
				else
				{
					semanticPrintf("line %d: the return type has to be a primitive data type.", yynewlines);
				}

				// Code Generation
				currentProcName = $2;
				saveProcDeclarationLineNumber(currentProcName);
				createPST($4, currentProcName, currentProcReturnType);
				createActivationRecord_1(currentProcName);
			}
		  ;

procKey:
		  TK_PROC
			{
				// Set the procFlag to one.
				procFlag = 1;
				saveUnconditionalJumpToAvoidAProc();
			}
		  ;
var:
		  TK_VAR mvrList TK_SEMI
			{
				// Do bookkeeping. Iterate over every mvrDeet object. In every varDeet object
				// find the varNames and along with the type and dimensions send it to symbol
				// table. 
				nsrPrintf("\nStarted bookkeeping.");
				vector<varDeets*>::iterator fit;
				int i = 0;
				for(fit = $2->mvrVector.begin(); fit != $2->mvrVector.end(); fit++, i++)
				{
					nsrPrintf("\nStarted iteration over mvrList.");
					vector<string>::iterator fitVar;
					int j = 0;
					for(fitVar = $2->mvrVector[i]->varNames.begin(); fitVar != $2->mvrVector[i]->varNames.end(); fitVar++, j++)
					{	
						// var
						// If the proc flag is 0, then insert it into global memory. If not, the address will be relative to the FP. In this case
						// we insert -1 in the address space. 
						if(procFlag == 0)
						{
							if(scopeStackObj->createSymbolIfAllChecksAreFine($2->mvrVector[i]->varNames[j].c_str(), $2->mvrVector[i]->typeAlias.c_str(), $2->mvrVector[i]->dimensions, codeGenObj->getNextAddress()) == SYSERR)
							{
								codeGenObj->decrementAddress();
								semanticPrintf("line %d: The symbol %s already exists", yynewlines, $2->mvrVector[i]->varNames[j].c_str());
							}
						}
						else if(procFlag == 1)
						{
							if(scopeStackObj->createSymbolIfAllChecksAreFine($2->mvrVector[i]->varNames[j].c_str(), $2->mvrVector[i]->typeAlias.c_str(), $2->mvrVector[i]->dimensions, -1) == SYSERR)
							{
								codeGenObj->decrementAddress();
								semanticPrintf("line %d: The symbol %s already exists", yynewlines, $2->mvrVector[i]->varNames[j].c_str());
							}
						}
						
					}
				}
				if(procFlag == 1)
				{
					createPST($2, currentProcName, currentProcReturnType);
				}
				free($2);
			}		
		  ;
mvrList:  mvrList TK_COMMA varlist
			{
				mvrDeets *tmp = $1;
				tmp->mvrVector.push_back($3);
				$$ = tmp;				
			}
		  | varlist
			{
				mvrDeets *tmp = new mvrDeets();
				tmp->mvrVector.push_back($1);
				$$ = tmp;
			}
		  ;
varlist:  
		  pIdList TK_COLON typeid zArrList
			{
				varDeets *tmp = new varDeets();
				tmp->typeAlias = $3;
				tmp->varNames = $1->varNames;
				if($4 != NULL)
					tmp->dimensions = $4->dimensions;
				$$ = tmp;
				free($4);				
			} 
		  ;
declist:  /* empty rule */
			{
				mvrDeets *tmp = NULL;
				$$ = tmp;
			}
		  | declist TK_COMMA decTerm
			{
				mvrDeets *tmp = $1;
				tmp->mvrVector.push_back($3);
				$$ = tmp;	
			}
		  | decTerm
			{
				mvrDeets *tmp = new mvrDeets();
				tmp->mvrVector.push_back($1);
				$$ = tmp;
			}
		  ;
decTerm:  pIdList TK_COLON typeid
			{
				varDeets *tmp = new varDeets();
				tmp->typeAlias = $3;
				tmp->varNames = $1->varNames;
				tmp->printVarDeets();
				$$ = tmp;
				free($1);
			}
		  ;
pIdList:  
		  pIdList TK_COMMA id
			{
				varDeets *tmp = $1;
				tmp->varNames.push_back($3);
				$$ = tmp;
			}
		  | id
			{
				varDeets *tmp = new varDeets();
				tmp->varNames.push_back($1);
				$$ = tmp;
			}			
		  ;	
typeid:
		  id
			{
				$$ = $1;
			}
		  ; 
zPrgList: /* empty rule */
		  | zPrgList PrgTerm
		  ;
zTVList:  /*empty rule*/
		  | zTVList TVTerm
		  ;
PrgTerm: type
		  | var
		  | forward
		  | proc
		  ;
TVTerm:  type
		  | var
		  ;
exp:
		  int
			{
				//nsrPrintf("\nNow at int");
				varDeets* tmp = new varDeets();
				tmp->typeBase = "int";
				tmp->value = $1;
				tmp->valueDefined = INTEGER;
				tmp->iShouldBeProc = 0;
				//tmp->printVarDeets();
				$$ = tmp;

				// Code Generation
				// Load the value of the integer into AC register.
				codeGenObj->genLDC(AC1, $1, ZERO);
			}

		  | lvalue
			{
				nsrPrintf("\n\nDo dimensions match? %d\n\n", $1->dimensionsMatch);
				$$ = $1;
			}	
			
		  | TK_TRUE  
			{
				varDeets* tmp = new varDeets();
				tmp->typeBase = "bool";
				tmp->boolValue = true;
				tmp->iShouldBeProc = 0;
				tmp->valueDefined = BOOL;
				$$ = tmp;

				// Code Generation
				// Load the value of the true(1) into AC1 register.
				codeGenObj->genLDC(AC1, 1, ZERO);
			}
		  | TK_FALSE 
			{
				varDeets* tmp = new varDeets();
				tmp->typeBase = "bool";
				tmp->boolValue = false;
				tmp->iShouldBeProc = 0;
				tmp->valueDefined = BOOL;
				$$ = tmp;

				// Code Generation
				// Load the value of false(0) into AC1 register.
				codeGenObj->genLDC(AC1, 0, ZERO);
			}
		  | strLit
			{
				varDeets* tmp = new varDeets();
				tmp->typeBase = "string";
				tmp->valueDefined = STRING;
				$$ = tmp;
			}
		  | TK_READ
			{
				varDeets *tmp = new varDeets();
				tmp->typeBase = "int";
				$$ = tmp;
			}
		  | exp TK_EQ 
			{
				// Push the result onto the stack. 
				codeGenObj->genPush();
			}
			exp
			{
				// This operation can be performed only on ints or bools
				// Throw an error, otherwise. Further, if we do the following
				// bool op int or int op bool, then this fine. The type will
				// then be bool.
				// EDIT: Checked and fine.
				
				// Now comes the proc shite. We should ensure that dimensions
				// match for this expressions, otherwise throw a semantic 
				// error. 
				if($1->dimensionsMatch == DIMENSIONS_DO_NOT_MATCH)
					semanticPrintf("line %d: The number of dimensions do not match for %s", yynewlines, $1->varName.c_str());
				if($4->dimensionsMatch == DIMENSIONS_DO_NOT_MATCH)
					semanticPrintf("line %d: The number of dimensions do not match for %s", yynewlines, $4->varName.c_str());

				varDeets* tmp = new varDeets();
				if($1->typeBase == "string" || $4->typeBase == "string")
				{
					semanticPrintf("line %d: strings cannot be used here with =", yynewlines);
				}				
				if(compareBaseTypes($1, $4) == OK)
				{
					nsrPrintf("\nWe are fine. Assigning the necessary type to exp");
					tmp->typeBase = "bool";
					free($4);
					free($1);
				}
				else
				{
					semanticPrintf("line %d: incompatible types used with = operator", yynewlines);
				}
				$$ = tmp;
				$$->expType = EQ;

				// Code Generation
				codeGenObj->genCompare($$->expType);
			} 
		  | exp TK_NEQ 
			{
				// Push the result onto the stack. 
				codeGenObj->genPush();
			}
			exp
			{
				// This operation can be performed only on ints or bools
				// Throw an error, otherwise. Further, if we do the following
				// bool op int or int op bool, then this fine. The type will
				// then be bool.
				// EDIT: Checked and fine.
				
				// Now comes the proc shite. We should ensure that dimensions
				// match for this expressions, otherwise throw a semantic 
				// error. 
				if($1->dimensionsMatch == DIMENSIONS_DO_NOT_MATCH)
					semanticPrintf("line %d: The number of dimensions do not match for %s", yynewlines, $1->varName.c_str());
				if($4->dimensionsMatch == DIMENSIONS_DO_NOT_MATCH)
					semanticPrintf("line %d: The number of dimensions do not match for %s", yynewlines, $4->varName.c_str());

				varDeets* tmp = new varDeets();
				if($1->typeBase == "string" || $4->typeBase == "string")
				{
					semanticPrintf("line %d: strings cannot be used here with =", yynewlines);
				}				
				if(compareBaseTypes($1, $4) == OK)
				{
					nsrPrintf("\nWe are fine. Assigning the necessary type to exp");
					tmp->typeBase = "bool";
					free($4);
					free($1);
				}
				else
				{
					semanticPrintf("line %d: incompatible types used with != operator", yynewlines);
				}
				$$ = tmp;
				$$->expType = NE;

				// Code Generation
				codeGenObj->genCompare($$->expType);
			} 
		  | exp TK_GT 
			{
				// Push the result onto the stack. 
				codeGenObj->genPush();
			}
			exp
			{
				// This operation can be performed only on ints
				// Throw an error, otherwise. Further, if we do the following
				// bool op int or int op bool, then this fine. The type will
				// then be bool.
				// EDIT: Checked and fine.
				
				// Now comes the proc shite. We should ensure that dimensions
				// match for this expressions, otherwise throw a semantic 
				// error. 
				if($1->dimensionsMatch == DIMENSIONS_DO_NOT_MATCH)
					semanticPrintf("line %d: The number of dimensions do not match for %s", yynewlines, $1->varName.c_str());
				if($4->dimensionsMatch == DIMENSIONS_DO_NOT_MATCH)
					semanticPrintf("line %d: The number of dimensions do not match for %s", yynewlines, $4->varName.c_str());

				varDeets* tmp = new varDeets();
				if(compareBaseTypes($1, $4, "int") == OK)
				{
					nsrPrintf("\nWe are fine. Assigning the necessary type to exp");
					tmp->typeBase = "bool";
					free($4);
					free($1);
				}
				else
				{
					semanticPrintf("line %d: incompatible types used with > operator", yynewlines);
				}
				$$ = tmp;
				$$->expType = GT;

				// Code Generation
				codeGenObj->genCompare($$->expType);
			}	
		  | exp TK_LT 
			{
				// Push the result onto the stack. 
				codeGenObj->genPush();
			}
			exp
			{
				// This operation can be performed only on ints
				// Throw an error, otherwise. Further, if we do the following
				// bool op int or int op bool, then this fine. The type will
				// then be bool.
				// EDIT: Checked and fine.
				
				// Now comes the proc shite. We should ensure that dimensions
				// match for this expressions, otherwise throw a semantic 
				// error. 
				if($1->dimensionsMatch == DIMENSIONS_DO_NOT_MATCH)
					semanticPrintf("line %d: The number of dimensions do not match for %s", yynewlines, $1->varName.c_str());
				if($4->dimensionsMatch == DIMENSIONS_DO_NOT_MATCH)
					semanticPrintf("line %d: The number of dimensions do not match for %s", yynewlines, $4->varName.c_str());

				varDeets* tmp = new varDeets();
				if(compareBaseTypes($1, $4, "int") == OK)
				{
					nsrPrintf("\nWe are fine. Assigning the necessary type to exp");
					tmp->typeBase = "bool";
					free($4);
					free($1);
				}
				else
				{
					semanticPrintf("line %d: incompatible types used with < operator", yynewlines);
				}
				$$ = tmp;
				$$->expType = LT;

				// Code Generation
				codeGenObj->genCompare($$->expType);

			}	
		  | exp TK_GE 
			{
				// Push the result onto the stack. 
				codeGenObj->genPush();
			}
			exp
			{
				// This operation can be performed only on ints
				// Throw an error, otherwise. Further, if we do the following
				// bool op int or int op bool, then this fine. The type will
				// then be bool.
				// EDIT: Checked and fine.
				
				// Now comes the proc shite. We should ensure that dimensions
				// match for this expressions, otherwise throw a semantic 
				// error. 
				if($1->dimensionsMatch == DIMENSIONS_DO_NOT_MATCH)
					semanticPrintf("line %d: The number of dimensions do not match for %s", yynewlines, $1->varName.c_str());
				if($4->dimensionsMatch == DIMENSIONS_DO_NOT_MATCH)
					semanticPrintf("line %d: The number of dimensions do not match for %s", yynewlines, $4->varName.c_str());

				varDeets* tmp = new varDeets();
				if(compareBaseTypes($1, $4, "int") == OK)
				{
					nsrPrintf("\nWe are fine. Assigning the necessary type to exp");
					tmp->typeBase = "bool";
					free($4);
					free($1);
				}
				else
				{
					semanticPrintf("line %d: incompatible types used with >= operator", yynewlines);
				}
				$$ = tmp;
				$$->expType = GE;

				// Code Generation
				codeGenObj->genCompare($$->expType);
			}	
		  | exp TK_LE 
			{
				// Push the result onto the stack. 
				codeGenObj->genPush();
			}
			exp
			{
				// This operation can be performed only on ints
				// Throw an error, otherwise. Further, if we do the following
				// bool op int or int op bool, then this fine. The type will
				// then be bool.
				// EDIT: Checked and fine.
				
				// Now comes the proc shite. We should ensure that dimensions
				// match for this expressions, otherwise throw a semantic 
				// error. 
				if($1->dimensionsMatch == DIMENSIONS_DO_NOT_MATCH)
					semanticPrintf("line %d: The number of dimensions do not match for %s", yynewlines, $1->varName.c_str());
				if($4->dimensionsMatch == DIMENSIONS_DO_NOT_MATCH)
					semanticPrintf("line %d: The number of dimensions do not match for %s", yynewlines, $4->varName.c_str());

				varDeets* tmp = new varDeets();
				if(compareBaseTypes($1, $4, "int") == OK)
				{
					nsrPrintf("\nWe are fine. Assigning the necessary type to exp");
					tmp->typeBase = "bool";
					free($4);
					free($1);
				}
				else
				{
					semanticPrintf("line %d: incompatible types used with <= operator", yynewlines);
				}
				$$ = tmp;
				$$->expType = LE;

				// Code Generation
				codeGenObj->genCompare($$->expType);
			}	
		  | exp TK_PLUS 
			{
				// Push the result onto the stack. 
				codeGenObj->genPush();
			}
			exp
			{
				// This operation can be performed only on ints or bools
				// Throw an error, otherwise. Further, if we do the following
				// bool op int or int op bool, then this fine. The type will
				// then be bool.

				// Now comes the proc shite. We should ensure that dimensions
				// match for this expressions, otherwise throw a semantic 
				// error. 
				if($1->dimensionsMatch == DIMENSIONS_DO_NOT_MATCH)
					semanticPrintf("line %d: The number of dimensions do not match for %s", yynewlines, $1->varName.c_str());
				if($4->dimensionsMatch == DIMENSIONS_DO_NOT_MATCH)
					semanticPrintf("line %d: The number of dimensions do not match for %s", yynewlines, $4->varName.c_str());

				varDeets* tmp = new varDeets();
				if($1->typeBase == "string" || $4->typeBase == "string")
				{
					semanticPrintf("line %d: strings cannot be used with + operator", yynewlines);
				}				
				if(compareBaseTypes($1, $4) == OK)
				{
					nsrPrintf("\nWe are fine. Assigning the necessary type to exp");
					if($1->typeBase == "int")
						tmp->typeBase = "int";
					if($1->typeBase == "bool")
						tmp->typeBase = "bool";
					free($4);
					free($1);
				}
				else
				{
					semanticPrintf("line %d: incompatible types used with + operator", yynewlines);
				}
				$$ = tmp;

				// Code Generation
				// Pop the stack and add AC1 with AC2. 
				codeGenObj->genPop();
				codeGenObj->genOperation("+", AC1, AC2, AC1);

			} 
		  | exp TK_MINUS 
			{
				// Push the result onto the stack. 
				codeGenObj->genPush();  
			} 
			exp
			{
				// This operation can be performed only on ints
				// Throw an error, otherwise. Further, if we do the following
				// bool op int or int op bool, then this fine. The type will
				// then be bool.
				// EDIT: Checked and fine.
				
				// Now comes the proc shite. We should ensure that dimensions
				// match for this expressions, otherwise throw a semantic 
				// error. 
				if($1->dimensionsMatch == DIMENSIONS_DO_NOT_MATCH)
					semanticPrintf("line %d: The number of dimensions do not match for %s", yynewlines, $1->varName.c_str());
				if($4->dimensionsMatch == DIMENSIONS_DO_NOT_MATCH)
					semanticPrintf("line %d: The number of dimensions do not match for %s", yynewlines, $4->varName.c_str());

				varDeets* tmp = new varDeets();
				if(compareBaseTypes($1, $4, "int") == OK)
				{
					nsrPrintf("\nWe are fine. Assigning the necessary type to exp");
					tmp->typeBase = "int";
					free($4);
					free($1);
				}
				else
				{
					semanticPrintf("line %d: incompatible types used with - operator", yynewlines);
				}
				$$ = tmp;

				// Code Generation
				// Pop the stack and add AC1 with AC2. 
				codeGenObj->genPop();
				codeGenObj->genOperation("-", AC1, AC2, AC1);
			} 
		  | exp TK_STAR 
			{
				// Push the result onto the stack. 
				codeGenObj->genPush();
			}
			exp 
			{
				// This operation can be performed only on ints or bools
				// Throw an error, otherwise. Further, if we do the following
				// bool op int or int op bool, then this fine. The type will
				// then be bool.
				// EDIT: Checked and fine.
				
				// Now comes the proc shite. We should ensure that dimensions
				// match for this expressions, otherwise throw a semantic 
				// error. 
				if($1->dimensionsMatch == DIMENSIONS_DO_NOT_MATCH)
					semanticPrintf("line %d: The number of dimensions do not match for %s", yynewlines, $1->varName.c_str());
				if($4->dimensionsMatch == DIMENSIONS_DO_NOT_MATCH)
					semanticPrintf("line %d: The number of dimensions do not match for %s", yynewlines, $4->varName.c_str());

				varDeets* tmp = new varDeets();
				if($1->typeBase == "string" || $4->typeBase == "string")
				{
					semanticPrintf("line %d: strings cannot be used with * operator", yynewlines);
				}				
				if(compareBaseTypes($1, $4) == OK)
				{
					nsrPrintf("\nWe are fine. Assigning the necessary type to exp");
					if($1->typeBase == "int")
						tmp->typeBase = "int";
					if($1->typeBase == "bool")
						tmp->typeBase = "bool";
					free($4);
					free($1);
				}
				else
				{
					semanticPrintf("line %d: incompatible types used with * operator", yynewlines);
				}
				$$ = tmp;

				// Code Generation
				// Pop the stack and add AC1 with AC2. 
				codeGenObj->genPop();
				codeGenObj->genOperation("*", AC1, AC2, AC1);
			} 
		  | exp TK_SLASH 
			{
				// Push the result onto the stack. 
				codeGenObj->genPush();  
			} 
			exp
			{
				// This operation can be performed only on ints
				// Throw an error, otherwise. Further, if we do the following
				// bool op int or int op bool, then this fine. The type will
				// then be bool.
				// EDIT: Checked and fine.

				// Now comes the proc shite. We should ensure that dimensions
				// match for this expressions, otherwise throw a semantic 
				// error. 
				if($1->dimensionsMatch == DIMENSIONS_DO_NOT_MATCH)
					semanticPrintf("line %d: The number of dimensions do not match for %s", yynewlines, $1->varName.c_str());
				if($4->dimensionsMatch == DIMENSIONS_DO_NOT_MATCH)
					semanticPrintf("line %d: The number of dimensions do not match for %s", yynewlines, $4->varName.c_str());

				varDeets* tmp = new varDeets();
				if(compareBaseTypes($1, $4, "int") == OK)
				{
					nsrPrintf("\nWe are fine. Assigning the necessary type to exp");
					tmp->typeBase = "int";
					free($4);
					free($1);
				}
				else
				{
					semanticPrintf("line %d: incompatible types used with / operator", yynewlines);
				}
				$$ = tmp;

				// Code Generation
				// Pop the stack and add AC1 with AC2. 
				codeGenObj->genPop();
				codeGenObj->genOperation("/", AC1, AC2, AC1);
			} 
		  | exp TK_MOD
			{
				// Push the result of exp onto the stack. 
				codeGenObj->genPush();  
			} 
			exp
			{
				// This operation can be performed only on ints
				// Throw an error, otherwise. Further, if we do the following
				// bool op int or int op bool, then this fine. The type will
				// then be bool.
				// EDIT: Checked and fine.

				// Now comes the proc shite. We should ensure that dimensions
				// match for this expressions, otherwise throw a semantic 
				// error. 
				if($1->dimensionsMatch == DIMENSIONS_DO_NOT_MATCH)
					semanticPrintf("line %d: The number of dimensions do not match for %s", yynewlines, $1->varName.c_str());
				if($4->dimensionsMatch == DIMENSIONS_DO_NOT_MATCH)
					semanticPrintf("line %d: The number of dimensions do not match for %s", yynewlines, $4->varName.c_str());

				varDeets* tmp = new varDeets();
				if(compareBaseTypes($1, $4, "int") == OK)
				{
					nsrPrintf("\nWe are fine. Assigning the necessary type to exp");
					tmp->typeBase = "int";
					free($4);
					free($1);
				}
				else
				{
					semanticPrintf("line %d: incompatible types used with %% operator", yynewlines);
				}
				$$ = tmp;

				codeGenObj->genPop();
				// Save the quotient in REG1.
				codeGenObj->genOperation("/", REG1, AC2, AC1);
				// Multiply the quotient by the divisor.
				codeGenObj->genOperation("*", REG1, REG1, AC1);
				// Subtract the product from AC2. This will give
				// the remainder. Save the value on AC1.
				codeGenObj->genOperation("-", AC1, AC2, REG1);
			}
		  | TK_MINUS exp
			{
				// This operation can be performed only on ints or bools
				// Throw an error, otherwise. Further, if we do the following
				// bool op int or int op bool, then this fine. The type will
				// then be bool.
				// EDIT: Checked and fine.
				
				// Now comes the proc shite. We should ensure that dimensions
				// match for this expressions, otherwise throw a semantic 
				// error. 
				if($2->dimensionsMatch == DIMENSIONS_DO_NOT_MATCH)
					semanticPrintf("line %d: The number of dimensions do not match for %s", yynewlines, $2->varName.c_str());

				varDeets* tmp = new varDeets();
				if($2->typeBase == "string")
				{
					semanticPrintf("line %d: strings cannot be used with - operator", yynewlines);
				}				
				nsrPrintf("\nWe are fine. Assigning the necessary type to exp");
				if($2->typeBase == "int")
					tmp->typeBase = "int";
				if($2->typeBase == "bool")
					tmp->typeBase = "bool";
				free($2);
				$$ = tmp;

				// Code Generation.
				// Subtract the number from 0. 
				if($$->typeBase == "bool")
				{
					codeGenObj->genFlip(AC1); 
				}
				else
				{
					codeGenObj->genOperation("-", AC1, ZERO, AC1);
				}

			}	
		  | TK_QUEST exp
			{
				// This operation can be performed only on bools
				// Throw an error, otherwise. Further, if we do the following
				// bool op int or int op bool, then this fine. The type will
				// then be bool.
				// EDIT: Checked and fine.
				
				// Now comes the proc shite. We should ensure that dimensions
				// match for this expressions, otherwise throw a semantic 
				// error. 
				if($2->dimensionsMatch == DIMENSIONS_DO_NOT_MATCH)
					semanticPrintf("line %d: The number of dimensions do not match for %s", yynewlines, $2->varName.c_str());

				varDeets* tmp = new varDeets();
				if($2->typeBase == "string" || $2->typeBase == "int")
				{
					semanticPrintf("line %d: only bools can be operated upon by ? operator", yynewlines);
				}				
				nsrPrintf("\nWe are fine. Assigning the necessary type to exp");
				tmp->typeBase = "int";
				free($2);
				$$ = tmp;
			}	
			
		  | id TK_LBRACK zExpList TK_RBRACK
			{
				scopeStackObj->printStack();
				ProcObject* procObj = scopeStackObj->getProcObject($1);
				// Simply for error message ease and checking if any forward declarations alone
				// (without any proc definitions) have been used in the program.
				procObj->lineWhereIWasCalled = yynewlines;
				varDeets *tmp = new varDeets();
				if(procObj == NULL)
				{
					semanticPrintf("line %d: proc has not been previously declared.", yynewlines);
				}
				nsrPrintf("\nThis is in fact a proc.");
				tmp->typeBase = procObj->returnType;
				// Compare the base types of both objects.
				if(procObj->argumentList.size() == $3->allTypes.size())
				{
					nsrPrintf("\nThe argument sizes match.");
					vector<TypeObject*>::iterator fit;
					int i = 0;
					nsrPrintf("\n\n\n\nWho all Vector\n\n\n\n");
					i = 0;
					for(fit = $3->allTypes.begin(); fit != $3->allTypes.end(); fit++, i++)
					{
						// For each of these compare them based on if it actually is a proc
						// variable. If it is, compare the base types. If it is not, compare
						// based on types and number of dimensions of $3 and argumentList[i].
						if($3->whoAllAreProc[i] == 1)
						{
							// I must be a proc variable only. Compare types.
							if(compareTypes(procObj->argumentList[i], $3->allTypes[i]) != OK)
							{
								semanticPrintf("line %d: argument %d type mismatch", yynewlines, i);
							}
						}
						else
						{
							nsrPrintf("\n\n\n\ndeclaration type base: %s, argument type base: %s, dimensions are %d\n\n\n\n", procObj->argumentList[i]->baseType.c_str(), $3->typeBase.c_str(), procObj->argumentList[i]->dimensions.size())
							// Compare base types and number of dimensions. Because it has come as a primitive
							// type.
							if((procObj->argumentList[i]->baseType == $3->allTypes[i]->baseType) && (procObj->argumentList[i]->dimensions.size() == 0))
							{
								// We are fine
							}
							else
							{
								//nsrPrintf("\nBase Types are %s, %s.", procObj->argumentList[i]->baseType, $3->typeBase);
								semanticPrintf("line %d: argument %d type mismatch", yynewlines, i);
							}
						}
						
					}
					$$ = tmp;
				}
				else
				{
					semanticPrintf("line %d: number of arguments mismatch. Expecting %d arguments. Got %d arguments. ", yynewlines, procObj->argumentList.size(), $3->allTypes.size());
				}
				nsrPrintf("\nAll checks matched. We are good to go.");
				$$ = tmp;

				// Code Generation.
				// The zExpList values have been pushed onto the stack. Now we need to push the
				// value of the return address onto the stack. 
				nsrPrintf("We are calling the FUNCTION %s.", $1);
				functionCall($1);
			}
		  
		  | TK_LBRACK exp TK_RBRACK
			{
				nsrPrintf("\nCheck production TK_LBRACK exp TK_RBRACK");
				$$ = $2;
			}
		  ;

expL:	  exp
		  {
				$$ = $1;
				// Push the result onto the stack. 
				codeGenObj->genPush();  
		  }
		  ;
lvalue:	  
		  id zAVList
			{
				varDeets* tmp = new varDeets();
				SymbolObject* tmpSymbolObject = scopeStackObj->getSymbolObject($1);
				if(tmpSymbolObject == NULL)
				{
					semanticPrintf("line %d: The symbol near %s has not been defined", yynewlines, yytext);
				}
				tmp->symbolTypeObj = tmpSymbolObject->typeObj;
				tmp->varName = $1;
				tmp->typeAlias = tmpSymbolObject->typeObj->aliasName;
				tmp->typeBase = tmpSymbolObject->typeObj->baseType;
				tmp->dimensions = tmpSymbolObject->typeObj->dimensions;
				tmp->numberOfDimensions =  $2->numberOfDimensions;
				tmp->valueDefined = NO_VALUE;
				tmp->iShouldBeProc = 0;
				if(tmpSymbolObject->typeObj->dimensions.size() > 0 && $2->numberOfDimensions == 0)
				{
					nsrPrintf("\nI must be a proc argument. Lets see if this holds.");
					tmp->iShouldBeProc = 1;
				}
				if(tmpSymbolObject->typeObj->dimensions.size() > 0 && $2->numberOfDimensions != 0 && $2->numberOfDimensions != tmpSymbolObject->typeObj->dimensions.size())
				{
					// Can't be a proc argument or anything else. Throw error!
					semanticPrintf("line %d: number of dimensions do not match for %s", yynewlines, $1);
				}
				if(tmpSymbolObject->typeObj->dimensions.size() != $2->numberOfDimensions)
				{
					tmp->dimensionsMatch = DIMENSIONS_DO_NOT_MATCH;
					nsrPrintf("\nDimensions are %d and %d, %d", tmpSymbolObject->typeObj->dimensions.size(), $2->numberOfDimensions, tmp->dimensionsMatch);
					// This will check if the symbol being used is correct. If not, it will throw an error.
					// semanticPrintf("line %d: The number of dimensions do not match near %s", yynewlines, yytext);
				}
				else
				{
					// Since the dimensions match, it must resolve to a
					// primitive type and not anything else.
					tmp->dimensionsMatch = DIMENSIONS_MATCH;
				}
				free($2);
				$$ = tmp;

				// Code Generation.
				// Now we know the symbol ID. Get the address and load the value of the
				// symbol in AC1. 
				loadIDIncludingFa($1);
			}  
		  ;
zAVList:  /* empty rule */
			{
				varDeets *tmp = new varDeets();
				$$ = tmp;
			}
		  | zAVList avTerm
			{
				varDeets *tmp = $1;
				tmp->numberOfDimensions++;
				nsrPrintf("\nNumber of Dimensions is %d", tmp->numberOfDimensions);
				$$ = tmp;
				free($2);
			}
		  | avTerm
			{
				$$ = $1;
			}
		  ;
avTerm:   /* empty rule */
			{
				semanticPrintf("line %d: the index cannot be blank near %s", yynewlines, yytext);
			}
		  | TK_LSBRACK exp TK_RSBRACK
			{
				varDeets *tmp = $2;
				if($2->dimensionsMatch == DIMENSIONS_DO_NOT_MATCH)
				{
					nsrPrintf("/nAm I printing?");
					semanticPrintf("line %d: The number of dimensions do not match for %s", yynewlines, $2->varName.c_str());
				}
				if(tmp->typeBase != "int")
				{
					semanticPrintf("line %d: incompatible type used as array index.", yynewlines);
				}
				tmp->numberOfDimensions = 1;
				$$ = $2;
			}
		  ;	
zArrList: 
		  /* empty rule */
			{
				varDeets *tmp = new varDeets();
				$$ = tmp;	
			}
		  | zArrList arrTerm
			{
				varDeets *tmp = $1;
				tmp->dimensions.push_back($2->value);
				$$ = tmp;
				free($2);
			}
		  | arrTerm
			{
				varDeets *tmp = new varDeets();
				tmp->dimensions.push_back($1->value);
				$$ = tmp;
			}
		  ;
arrTerm:
		  TK_LSBRACK int TK_RSBRACK
			{
				varDeets *tmp = new varDeets();
				if($2 <= 0)
					semanticPrintf("line %d: array dimensions have to be positive integers.", yynewlines);
				tmp->valueDefined = INTEGER;
				tmp->typeBase = "int";
				tmp->typeAlias = "int";
				tmp->value = $2;
				$$ = tmp;
			}
		  ;
zExpList: /* empty rule */
			{
				varDeets* tmp = new varDeets();
				$$ = tmp;	
		    }
		  | zExpList TK_COMMA exp
			{
				varDeets* tmp = $1;
				TypeObject* tmpType = new TypeObject();
				tmpType->baseType = $3->typeBase; 
				tmpType->dimensions = tmp->dimensions = $3->dimensions;
				tmp->allTypes.push_back(tmpType);
				if($3->iShouldBeProc == 1)
					tmp->whoAllAreProc.push_back(1);
				else
					tmp->whoAllAreProc.push_back(0);
				$$ = tmp;

				// Code Generation.
				// Push the value of expression onto the stack.
				string comment = "Pushing the value of argument list onto the stack";
				codeGenObj->genComment(comment);
				codeGenObj->genPush();
			}
		  | exp
			{
				varDeets* tmp = new varDeets();
				TypeObject* tmpType = new TypeObject();
				tmpType->baseType = tmp->typeBase = $1->typeBase;
				tmpType->dimensions = tmp->dimensions = $1->dimensions;
				tmp->allTypes.push_back(tmpType);
				if($1->iShouldBeProc == 1)
					tmp->whoAllAreProc.push_back(1);
				else
					tmp->whoAllAreProc.push_back(0);
				$$ = tmp;

				// Code Generation.
				// Here we will push the value of the expression onto the stack
				string comment = "Pushing the value of argument list onto the stack";
				codeGenObj->genComment(comment);
				codeGenObj->genPush();

			}
		  ;
strLit:	  
		  TK_SLIT
			{
				$$ = $1;
			}
		  ;
id:
		  TK_ID
			{
				$$ = $1;
			}
		  ;
int:
		  TK_INT
			{
				if(($1 > 2147483647) || ($1 < -2147483648))
				{
					semanticPrintf("line %d: exceeded the integer bounds.", yynewlines);
				}
				
				$$ = $1;
			}
		  ;
%%

int main(int argc, char **argv) 
{
	scopeStackObj = new ScopeStack();
	codeGenObj = new CodeGen();
	int i = 0;
	for(i = 0; i < argc; i++)
	{
		if(strcmp(argv[i], "-debug") == 0)
		{
			nsrDebugPrintf = atoi(argv[i+1]);		
		}
		if(strcmp(argv[i], "-semantic") == 0)
		{
			semanticErrors = atoi(argv[i+1]);		
		}
		if(strcmp(argv[i], "-codeGen") == 0)
		{
			codeDebug = atoi(argv[i+1]);		
		}
		if(strcmp(argv[i], "-outRegisters") == 0)
		{
			writeDebug = atoi(argv[i+1]);		
		}
	}
	yyparse();
	codeGenObj->genHalt();
	codeGenObj->closeStream();
	return 0;
}