
#ifndef _DEBUG_H
#define _DEBUG_H
#include <stdio.h>
#include <stdlib.h>
#define nsrPrintf(...) if(nsrDebugPrintf) { fflush(stdout); printf(__VA_ARGS__); fflush(stdout);}
#define codeGenPrintf(...) if(codeDebug) { fflush(stdout); printf(__VA_ARGS__); fflush(stdout);}
// simply add exit(EXIT_FAILURE) here and you will be fine. All semantic errors, report them as semanticPrintf.
#define semanticPrintf(...) if(semanticErrors) { fflush(stdout); printf("\nERROR:"); printf(__VA_ARGS__); fflush(stdout); exit(EXIT_FAILURE);} 
//extern int nsrDebugPrintf;

// So ugly. But Roshan did not let me live in peace. 
enum expressionType
{
	LT,
	LE,
	GT,
	GE,
	EQ,
	NE
};

#endif
