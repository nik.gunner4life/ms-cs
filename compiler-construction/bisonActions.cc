
#pragma region AUTHOR INFORMATION
/***********************************************************************
Author:   Nikhil Srivatsan Srinivasan
Unity ID: nsrivat
Project:  Code Generation
Date:     04/18/2014
Description: Methods that will be called in Bison Actions
***********************************************************************/
#pragma endregion

#pragma region HEADERS
#include "codeGen.h"
#include "semantics.h"
#include "debug.h"
#include "common.h"
using namespace std;
#pragma endregion
// Might not be using this at all. 
extern int nsrDebugPrintf;
extern int codeDebug;
extern int procFlag;
extern ScopeStack* scopeStackObj;
extern CodeGen* codeGenObj;
extern string currentProcName;
extern string currentProcReturnType;
extern int faFlag;
extern int loopCount;

int createPST(mvrDeets* mvrStuff, string procName, string returnType)
{
	vector<varDeets*>::iterator fit;
	int i = 0;
	// Now get the proc object.
	ProcObject* procObj = scopeStackObj->getProcObject(procName);
	if(procObj == NULL)
	{
		codeGenPrintf("\nWe are in TROUBLE. Check createPST. The proc name is %s. EXITING.", procName.c_str());
		return -1;
	}
	if(mvrStuff == NULL)
	{
		// It means this is a zero argument proc.
		codeGenPrintf("\nThere are no arguments.");
		if(returnType != "")
		{
			codeGenPrintf("\nHowever, we do have a return type.");
			string variableName = procName;
			string typeAlias = returnType;
			vector<int> dimensions;
			int variableOffset = procObj->PST->getNumberOfSymbols();

			// Now get the Base type Object.
			int index = scopeStackObj->findScopeWhereAliasResides(typeAlias.c_str());
			TypeObject* baseObj = scopeStackObj->getTypeObjectWhichMatchedOurType(index, typeAlias.c_str());
			procObj->PST->insertInSymbolTable(procName, returnType, dimensions, baseObj, variableOffset);
			procObj->PST->incrementSymbolCount();
		}
	}
	else
	{
		for(fit = mvrStuff->mvrVector.begin(); fit != mvrStuff->mvrVector.end(); fit++, i++)
		{
			codeGenPrintf("\nStarted iterating over mvrDeets.");
			vector<string>::iterator fitVar;
			int j = 0;
			for(fitVar = mvrStuff->mvrVector[i]->varNames.begin(); fitVar != mvrStuff->mvrVector[i]->varNames.end(); fitVar++, j++)
			{
				string variableName = mvrStuff->mvrVector[i]->varNames[j].c_str();
				string typeAlias = mvrStuff->mvrVector[i]->typeAlias.c_str();
				vector<int> dimensions = mvrStuff->mvrVector[i]->dimensions;
				int variableOffset = procObj->PST->getNumberOfSymbols();

				// Now get the Base type Object.
				int index = scopeStackObj->findScopeWhereAliasResides(typeAlias.c_str());
				TypeObject* baseObj = scopeStackObj->getTypeObjectWhichMatchedOurType(index, typeAlias.c_str());
				procObj->PST->insertInSymbolTable(variableName, typeAlias, dimensions, baseObj, variableOffset);
				procObj->PST->incrementSymbolCount();
			}
		}
	}
	
	codeGenPrintf("\n\nPrinting the PST.");
	procObj->PST->printSymbolTable();
	codeGenPrintf("\n\tThe number of symbols is %d", procObj->PST->getNumberOfSymbols());
}

int insertInPST(string variableName, string typeAlias, vector<int> dimensions)
{
	ProcObject* procObj = scopeStackObj->getProcObject(currentProcName);
	if(procObj == NULL)
	{
		codeGenPrintf("\nWe are in TROUBLE. Check insertInPST. The proc name is %s. EXITING.", currentProcName.c_str());
		return -1;
	}
	int variableOffset = procObj->PST->getNumberOfSymbols();

	// Now get the Base type Object.
	int index = scopeStackObj->findScopeWhereAliasResides(typeAlias.c_str());
	TypeObject* baseObj = scopeStackObj->getTypeObjectWhichMatchedOurType(index, typeAlias.c_str());

	// Append -fa, faFlag number of times to the variable and insert it. 
	// Once this is done, we should be able to locate the variable. 
	string varToInsert = appendToVariable(variableName, codeGenObj->faConst, faFlag);
	codeGenPrintf("\nInseting the FA LOOP VARIABLE to the PST.");
	codeGenPrintf("\n\tInserting the variable %s to the PST. The offset is %d", varToInsert.c_str(), variableOffset);
	procObj->PST->insertInSymbolTable(varToInsert, typeAlias, dimensions, baseObj, variableOffset);
	procObj->PST->incrementSymbolCount();
}

int saveProcDeclarationLineNumber(string procName)
{
	ProcObject* procObj = scopeStackObj->getProcObject(procName);
	if(procObj == NULL)
	{
		codeGenPrintf("\nWe are in trouble. Check saveProcDeclarationLineNumber. The proc name is %s. EXITING.", procName.c_str());
		return -1;
	}
	procObj->lineWhereIWasDeclared = codeGenObj->getAssemblyLineNumber();
	codeGenPrintf("\nTHE FOLLOWING LINE NUMBER HAS BEEN SAVED AS THE BEGINNING OF PROC: %d", procObj->lineWhereIWasDeclared);
}

int loadGlobalID(string nameOfVariable)
{
	SymbolObject* tmpSymbolObject = scopeStackObj->getSymbolObject(nameOfVariable);
	int address = tmpSymbolObject->addressOfVariable;
	codeGenPrintf("\n\tLoading Gloabl ID. Address of variable: %d", address);
	string comment = "Loading value of global variable ";
	comment += nameOfVariable;
	codeGenObj->genComment(comment);
	codeGenObj->genLoadValue(address);
}

int loadLocalID(string nameOfVariable)
{
	ProcObject* procObj = scopeStackObj->getProcObject(currentProcName);
	if(procObj == NULL)
	{
		codeGenPrintf("\nWe are in trouble. Check loadLocalID. The proc name is %s. EXITING.", currentProcName.c_str());
		return -1;
	}
	SymbolObject* procTmpSymbolObject = procObj->PST->lookUpSymbolInProc(nameOfVariable);
	int offset = procTmpSymbolObject->addressOfVariable;
	int numberOfSymbols = procObj->PST->getNumberOfSymbols();
	codeGenPrintf("\n\tLoading Local ID. Offset of variable %s: %d", nameOfVariable.c_str(), offset);
	string comment = "Loading value of Local variable ";
	comment += nameOfVariable;
	codeGenObj->genComment(comment);
	codeGenObj->genLoadLocalValue(offset);
}

int loadValueForID(string nameOfVariable)
{
	// If proc flag is 0, it means we are not inside any proc.
	// Load the value from the global memory. However we can have
	// a case wherein the global ID can be accessed inside a proc.
	// Compare the symbol here to that in the PST. If the symbol 
	// does not exist in the PST use the global address. Else use
	// the relative address.
	SymbolObject* tmpSymbolObject = scopeStackObj->getSymbolObject(nameOfVariable);
	if(procFlag == 0)
	{
		loadGlobalID(nameOfVariable);
	}
	else if(procFlag == 1)
	{
		// Now we need to check if the variable exists inside the proc.
		// If it does, then we use the relative address. Else we will use
		// the global address. 
		ProcObject* procObj = scopeStackObj->getProcObject(currentProcName);
		SymbolObject* procTmpSymbolObject = procObj->PST->lookUpSymbolInProc(nameOfVariable);
		// This means that the symbol is not present in the proc table. Use the global ID.
		if(procTmpSymbolObject == NULL)
		{
			loadGlobalID(nameOfVariable);
		}
		// This means that the symbol actually exists in the proc table. Here we will need
		// to load the relative address wrt FP.
		else 
		{
			loadLocalID(nameOfVariable);
		}
	}
}

// Helper Methods to append to the string.
// Reason: Coz of my horrible design choice
// with regards to PST. Now I need to hack.
// So lets hack away. 

string appendToVariable(string name, string pattern, int count)
{
	int i = 0;
	for(i = 0; i < count; i++)
	{
		name += pattern;
	}
	codeGenPrintf("\nReturning the string %s", name.c_str());
	return name;
}

int loadIDIncludingFa(string nameOfVariable)
{
	// Base case. Just to ensure nothing before this broken.
	codeGenPrintf("\nInside Load ID Including FA");
	SymbolObject* symObj;
	// If the fa flag is zero then we do not need to look for
	// a variable with -fa appended to it. Just do the normal.
	if(faFlag == 0)
	{
		codeGenPrintf("\n\tFA Flag is 0. Defaulting. Should cause no problems.");
		loadValueForID(nameOfVariable);
	}
	else if(faFlag > 0)
	{
		// If faFlag is greater than 0, but proc flag is 0
		// then there is no need because once again, load
		// globalID will take care of everything we will
		// need. 
		codeGenPrintf("\n\tFA Flag is %d.", faFlag);
		if (procFlag == 0)
		{
			codeGenPrintf("\n\t\tFA Flag is %d. And Proc flag is %d. Defaulting.", faFlag, procFlag);
			loadValueForID(nameOfVariable);
		}
		// However if the procflag is 1. Then we need to load the
		// variable which has the -fa appended to it. Look for it
		// and load that variable's value. Now be at peace.
		else if(procFlag == 1)
		{
			codeGenPrintf("\n\t\tFA Flag is %d. And Proc flag is %d. Looking for appended string.", faFlag, procFlag);
			string tempName = nameOfVariable;
			int tempFaFlag = faFlag;
			int i = tempFaFlag;
			ProcObject* procObj = scopeStackObj->getProcObject(currentProcName);
			if(procObj == NULL)
			{
				codeGenPrintf("\n\nTROUBLE, TROUBLE! Proc Object with proc name %s does not exist.", currentProcName.c_str());
			}
			for(i = tempFaFlag; i >= 0; i--)
			{
				tempName = appendToVariable(nameOfVariable, codeGenObj->faConst, i);
				symObj = procObj->PST->lookUpSymbolInProc(tempName);
				if(symObj != NULL)
				{
					break;
				}
			}
			codeGenPrintf("\n\t\tFA Flag is %d. And Proc flag is %d. The variable that we will be loading is %s", faFlag, procFlag, tempName.c_str());
			loadValueForID(tempName);
		}
	}
}

int storeGlobalID(string nameOfVariable)
{
	SymbolObject* tempSymbolObj = scopeStackObj->getSymbolObject(nameOfVariable);
	int address = tempSymbolObj->addressOfVariable;
	codeGenPrintf("\nStoring value for variable %s in address: %d", nameOfVariable.c_str(), address);
	string comment = "Storing value of expression in Global ID ";
	comment += nameOfVariable;
	codeGenObj->genComment(comment);
	codeGenObj->genAssign(address);
}

int storeLocalID(string nameOfVariable)
{
	ProcObject* procObj = scopeStackObj->getProcObject(currentProcName);
	if(procObj == NULL)
	{
		codeGenPrintf("\nWe are in trouble. Check storeValueInID. The proc name is %s. EXITING.", currentProcName.c_str());
		return -1;
	}
	SymbolObject* procTmpSymbolObject = procObj->PST->lookUpSymbolInProc(nameOfVariable);
	int offset = procTmpSymbolObject->addressOfVariable;
	int numberOfSymbols = procObj->PST->getNumberOfSymbols();
	string comment = "Storing value of expression in Local ID ";
	comment += nameOfVariable;
	codeGenObj->genComment(comment);
	codeGenObj->genLocalAssign(offset);
}

int storeValueInID(string nameOfVariable)
{
	SymbolObject* tmpSymbolObject = scopeStackObj->getSymbolObject(nameOfVariable);
	// If proc flag is 0, it means we are not inside any proc.
	// Store the value in the global memory. However we can have
	// a case wherein the global ID can be accessed inside a proc.
	// Compare the symbol here to that in the PST. If the symbol 
	// does not exist in the PST use the global address. Else use
	// the relative address.
	if(procFlag == 0)
	{
		storeGlobalID(nameOfVariable);
	}
	else if(procFlag == 1)
	{
		// Now we need to check if the variable exists inside the proc.
		// If it does, then we use the relative address. Else we will use
		// the global address. 
		ProcObject* procObj = scopeStackObj->getProcObject(currentProcName);
		if(procObj == NULL)
		{
			codeGenPrintf("\nWe are in trouble. Check storeValueInID. The proc name is %s. EXITING.", currentProcName.c_str());
			return -1;
		}
		SymbolObject* procTmpSymbolObject = procObj->PST->lookUpSymbolInProc(nameOfVariable);
		if(procTmpSymbolObject == NULL)
		{
			storeGlobalID(nameOfVariable);
		}
		else
		{
			storeLocalID(nameOfVariable);
		}
	}
}

int backPatchBPAndFP()
{
	int size = scopeStackObj->scopeStack.size();
	codeGenPrintf("\nThe height of the scope is %d", size);
	if(size != 2)
	{
		codeGenPrintf("\nSomething is woefully wrong. Check backpatch BP and FP.");
		return -1;
	}
	// Small change here. Instead of getting number of symbols
	// we will be getting the current memory address.
	// This is because when we use FA, we will need memory for
	// that. So let us statically assign it now and use it
	// when we come to it.
	int nummberOfSymbols = codeGenObj->getCurrentMemoryAddress();
	codeGenPrintf("\nNumber Of symbols is %d", nummberOfSymbols);
	codeGenObj->genBackPatchBPAndFP(nummberOfSymbols + 1);
}

int pushReturnAddress()
{
	int returnAddress = codeGenObj->getAssemblyLineNumber();
	// We increment the returnAddress by 4 because we will need three lines to push
	// the return address onto the stack. Now after this, there will be one more line
	// which will jump to the function. Once we return from the function, we need to
	// return to the point after we jump to the function. Hence 4.
	returnAddress = returnAddress + 4;
	codeGenObj->genComment("Pushing the return address."); 
	codeGenObj->genComment("Check the displacement in the next instruction to ensure that return address is correct.");
	codeGenObj->genLDC(REG1, returnAddress, ZERO);
	codeGenObj->genPush(REG1);
}

int createActivationRecord_1(string procName)
{
	codeGenObj->genComment("");
	string comment = "Starting with proc ";
	comment += procName;
	codeGenObj->genComment(comment);
	codeGenObj->genComment("");
	ProcObject* procObj = scopeStackObj->getProcObject(procName);
	if(procObj == NULL)
	{
		codeGenPrintf("\nWe are in trouble. Check createActivationRecord_1. The proc name is %s. EXITING.", procName.c_str());
		return -1;
	}
	int decListCount = procObj->argumentList.size();
	// We may need to calculate size based on arrays later.

	// Now we will be doing a little backPatching here.
	// We know that the return address is the first value on the stack.
	// However the returnAddress is going to be saved at the top.
	// When we are handling declist, we still do not know the entire size
	// of the proc. We will know this only at the end of the proc definition.
	// So we are going to save one line(store(1)) for this. 
	// The return address population will start on the lineWhereIWasDeclared
	// which we have already stored. Let us pop the return address
	// and then we can backpatch just the store operation. Pop takes two instructions
	// so we just have to backpatch the lineWhereIWasDeclared + 2;
	
	codeGenObj->genComment("Popping the return address.");
	codeGenObj->genPop();
	codeGenObj->genComment("Saving this line to store the return address.");
	// Now increment the assembly line number.
	codeGenObj->assemblyLineNumber++;

	// Once we are done with this, we need to save the declist on the activation record.
	// The FP currently points to the bottom of the AR. The declist is on the stack.
	// Pop it and store it in the correct offset. 
	if(decListCount == 0)
	{
		// Do nothing.
	}
	else
	{
		int i = 0;
		for(i = 1; i <= decListCount; i++)
		{
			// Stores the value of the last argument in AC2.
			codeGenObj->genComment("Popping the argument onto AC2");
			codeGenObj->genPop();
			// Now we need to store this in the AR. We know the total number of arguments.
			// So we just need to keep storing from top and come down.
			// The variables will be stored in the location FP + decListCount - i.
			codeGenObj->genComment("Storing the value of AC2 on the AR.");
			codeGenObj->genLDC(AC1, decListCount - i, ZERO);
			codeGenObj->genOperation("+", AC1, FP, AC1);
			// At this point we have the address where argument must be stored. Do a store
			// operation.
			codeGenObj->genStore(AC2, 0, AC1);
			codeGenObj->genComment("Finished storing the argument on the AR.");
		}
	}
	// Push the base pointer onto the stack.
	codeGenObj->genComment("Pushing BP onto the stack.");
	codeGenObj->genPush(BP);
	// Now move the new BP to the current FP.
	codeGenObj->genComment("Setting BP to the current FP.");
	codeGenObj->genLDA(BP, 0, FP);

	// Now we need to save this line number because this is where we will be FP will be updated.
	codeGenPrintf("\nLine number %d, saved for framepointer backpatching.", codeGenObj->getAssemblyLineNumber());
	codeGenObj->genComment("Saving the line for backpatching the FP");
	codeGenObj->framePointerBackPatching.push_back(codeGenObj->getAssemblyLineNumber());
	codeGenObj->assemblyLineNumber++;

	// For DEBUG purposes.
	codeGenObj->genComment("FOR DEBUG");
	codeGenObj->outRegister(FP);
	codeGenObj->outRegister(BP);
	codeGenObj->outRegister(SP);
	codeGenObj->genComment("FOR DEBUG");
}

int createActivationRecord_2()
{
	ProcObject* procObj = scopeStackObj->getProcObject(currentProcName);
	if(procObj == NULL)
	{
		codeGenPrintf("\nWe are in trouble. Check createActivationRecord_2. The proc name is %s. EXITING.", currentProcName.c_str());
		return -1;
	}
	int numberOfSymbols = procObj->PST->getNumberOfSymbols();
	int actualDisplacement = numberOfSymbols + 1;
	int lineWhereIWasDeclared = procObj->lineWhereIWasDeclared;
	// Now we know how many symbols were present in the proc object. 
	// We simply need to push the return address to the correct location
	codeGenObj->genComment("Storing the return address");
	codeGenObj->genStore(lineWhereIWasDeclared + 2, AC2, numberOfSymbols, FP);

	// Set FP to the new value. We know the number of symbols so we simply
	// move FP to two places above that(accounting for return address also).
	if(codeGenObj->framePointerBackPatching.size() == 1)
	{
		int lineNumber = codeGenObj->framePointerBackPatching[codeGenObj->framePointerBackPatching.size() - 1];
		codeGenObj->genComment("Backpatching the frame pointer");
		codeGenObj->genLDA(lineNumber, FP, actualDisplacement, BP);
		codeGenObj->framePointerBackPatching.pop_back();
	}
	else
	{
		codeGenPrintf("\nWe are in trouble. Check createActivationRecord_2");
	}

	codeGenObj->genComment("");
	string comment = "Done with proc ";
	comment += currentProcName;
	codeGenObj->genComment(comment);
	codeGenObj->genComment("");
}

int saveUnconditionalJumpToAvoidAProc()
{
	codeGenObj->genComment("Saving this line for the unconditional jump to avoid the proc.");
	codeGenObj->unconditionalJumpToAvoidAProc = codeGenObj->getAssemblyLineNumber();
	codeGenObj->assemblyLineNumber++;
}

int backPatchUnconditionalJumpToAvoidAProc()
{
	int lineNumber = codeGenObj->unconditionalJumpToAvoidAProc;
	codeGenObj->genComment("Backpatching unconditional jump to avoid a proc.");
	codeGenObj->genJEQ(lineNumber, ZERO, codeGenObj->getAssemblyLineNumber());
}

int popARAndReturnFromProc()
{
	// Store the return address on AC2.
	// Then pop the AR and set an uncondiotional jump
	// to the value in register 3.

	// Load the return address on REG1.
	codeGenObj->genComment("Copying return address to register 3");
	codeGenObj->genLD(REG1, -1, FP);

	// Once we have saved this. We have to load the return value
	// on AC1. TODO: Will do this later.

	// Remove the AR and set the BP and FP correctly.
	ProcObject* procObj = scopeStackObj->getProcObject(currentProcName);
	if(procObj == NULL)
	{
		codeGenPrintf("\nWe are in trouble. Check popARAndReturnFromProc(). The proc name is %s. EXITING.", currentProcName.c_str());
		return -1;
	}

	int numberOfSymbols = procObj->PST->getNumberOfSymbols();
	int actualDisplacement = numberOfSymbols + 1;

	codeGenObj->genComment("Setting the FP to BP. (While quitting the function)");
	codeGenObj->genLDA(FP, 0, BP);

	// Pop the base pointer. Hopefully this should be on top of the stack.
	codeGenObj->genComment("Pop the old BP from the stack and store it in BP");
	codeGenObj->genPop();
	// Now the value of the old BP is in AC2. Store it in BP.
	codeGenObj->genLDA(BP, 0, AC2);
	codeGenObj->genComment("BP has been set.");

	// For DEBUG purposes
	codeGenObj->genComment("FOR DEBUG");
	codeGenObj->outRegister(FP);
	codeGenObj->outRegister(BP);
	codeGenObj->outRegister(SP);
	codeGenObj->outRegister(REG1);
	codeGenObj->genComment("FOR DEBUG");
	// Now jump unconditionally to the return address which is stored in REG1.
	codeGenObj->genComment("Jump unconditionally out of the proc to the return address");
	codeGenObj->genJEQToRegister(ZERO, REG1);
}

int functionCall(string procName)
{
	// Push the return address.
	codeGenPrintf("\nIn Function call.");
	pushReturnAddress();
	ProcObject* procObj = scopeStackObj->getProcObject(procName);
	if(procObj == NULL)
	{
		codeGenPrintf("\nWe are in trouble. Check out what is happening. The proc name is %s. EXITING.", procName.c_str());
		return -1;
	}
	int lineToJumpTo = procObj->lineWhereIWasDeclared;
	codeGenObj->genComment("From function call: Unconditional jump to the beginning of the proc.");
	codeGenObj->genJEQ(ZERO, lineToJumpTo);
}

int handleReturn()
{
	if(procFlag == 0)
	{
		// This will have to backpatched. So that we know the last line of the program
		// and then we can go there.
		codeGenPrintf("\nReturn NOT USED INSIDE a proc. We simply need to exit the code.");

		// TODO:
	}
	else if(procFlag == 1)
	{
		codeGenPrintf("\nRETURN USED INSIDE A PROC.");
		// Since we are inside a proc we should first check two things.
		// If the proc has a return type, then we have to put the return value in
		// the AC1 register and then call popAndReturnFromProc(). If the proc
		// has no return value, simple call popAndReturnFromProc() and we are done.

		ProcObject* procObj = scopeStackObj->getProcObject(currentProcName);
		if(procObj == NULL)
		{
			codeGenPrintf("\n\tSomething is wrong inside handleReturn(). Proc Obj is NULL for proc name %s.", currentProcName.c_str());
		}
		else
		{
			if(procObj->returnType == "")
			{
				codeGenPrintf("\n\tThe proc %s has no return type. Just return to caller.", currentProcName.c_str());
			}
			else
			{
				codeGenPrintf("\n\tThe proc has a return type. Loading the value of the return variable to AC1");
				// Load the value of the return variable(same as the proc name) in AC1.
				string comment = "Loading the return value for ";
				comment += currentProcName;
				codeGenObj->genComment(comment);
				loadLocalID(currentProcName);
			}
		}

		// Now return from the proc call. 
		popARAndReturnFromProc();

	}

	else
	{
		codeGenPrintf("\nSomething is wrong in handleReturn(). Proc flag is not 0 or 1. It is %d", procFlag);
		return -1;
	}
}

int implicitlyLoadReturnValueInAC1WhenQutting()
{
	if(procFlag == 1)
	{
		codeGenPrintf("\nImplicitly setting the return value to be in AC1");
		// Since we are inside a proc we should first check two things.
		// If the proc has a return type, then we have to put the return value in
		// the AC1 register and then call popAndReturnFromProc(). If the proc
		// has no return value, simple call popAndReturnFromProc() and we are done.

		ProcObject* procObj = scopeStackObj->getProcObject(currentProcName);
		if(procObj == NULL)
		{
			codeGenPrintf("\n\tSomething is wrong inside implicitlyLoadReturnValueInAC1WhenQutting(). Proc Obj is NULL for proc name %s.", currentProcName.c_str());
		}
		else
		{
			if(procObj->returnType == "")
			{
				codeGenPrintf("\n\tThe proc %s has no return type. We do not need to load anything.", currentProcName.c_str());
			}
			else
			{
				codeGenPrintf("\n\tThe proc has a return type. Loading the value of the return variable to AC1");
				// Load the value of the return variable(same as the proc name) in AC1.
				string comment = "Loading the return value for ";
				comment += currentProcName;
				codeGenObj->genComment(comment);
				loadLocalID(currentProcName);
			}
		}
	}

	else
	{
		codeGenPrintf("\nSomething is wrong in implicitlyLoadReturnValueInAC1WhenQutting(). Proc flag is not 1. It is %d", procFlag);
		return -1;
	}
}

int handleFa1(string nameOfVariable)
{
	// Here we will first be assigning the value of the loop
	// variable to the value of expression 1.
	// Currently expression 1 has been evaluated and sits in AC1.
	// Store the value of AC1 in the loop variable in the memory.

	// Now we will be handling this as two conditions. One for procs
	// and one without procs. Like an idiot, I went and added the PST
	// and as a result I will need to hack the code here. 
	if(procFlag == 0)
	{
		codeGenObj->pushToBreakStack();
		codeGenPrintf("\nFa used globally.");
		SymbolObject* tmpSymbolObject = scopeStackObj->getSymbolObject(nameOfVariable);
		if(tmpSymbolObject == NULL)
		{
			codeGenPrintf("\n\nTROUBLE, TROUBLE!: Loop variable symbol object is NULL. Check HandleFa1().\n");
			return 0;
		}
		int addressOfLoopVar = tmpSymbolObject->addressOfVariable;
		// Now we have the address/displacement. So just store the value
		// in AC1 to that memory location.
		string comment = "Initializing the loop variable ";
		comment += nameOfVariable;
		codeGenObj->genComment(comment);
		codeGenObj->genStore(AC1, addressOfLoopVar, ZERO);
		// Push the name of the loop variable to a stack.
		codeGenObj->pushToFaVarStack(nameOfVariable);

		// We need to have a loop condition here so that we can return to the beginning
		// of the loop. We need to remember this line number is the true stack.
		codeGenPrintf("\nPushing the line number %d to the FA true stack.", codeGenObj->getAssemblyLineNumber());
		codeGenObj->pushInFaTrueStack();

		return 1;
	}
	else if (procFlag == 1)
	{
		ProcObject* procObj = scopeStackObj->getProcObject(currentProcName);
		codeGenPrintf("\nFa used inside a proc %s.", currentProcName.c_str());
		if(procObj == NULL)
		{
			codeGenPrintf("\n\nTROUBLE, TROUBLE!: Proc Object is NULL. Check HandleFa1().\n");
			return -1;
		}
		codeGenObj->pushToBreakStack();
		codeGenPrintf("\nFa used globally.");
		string tempName = appendToVariable(nameOfVariable, codeGenObj->faConst, faFlag);
		SymbolObject* tmpSymbolObject = procObj->PST->lookUpSymbolInProc(tempName);
		if(tmpSymbolObject == NULL)
		{
			codeGenPrintf("\n\nTROUBLE, TROUBLE!: Loop variable symbol object is NULL. Check HandleFa1().\n");
			return 0;
		}
		int addressOfLoopVar = tmpSymbolObject->addressOfVariable;
		// Now we have the address/displacement. So just store the value
		// in AC1 to that memory location.
		string comment = "Initializing the loop variable ";
		comment += tempName;
		codeGenObj->genComment(comment);
		codeGenObj->genStore(AC1, addressOfLoopVar, BP);
		// Push the name of the loop variable to a stack.
		codeGenObj->pushToFaVarStack(tempName);

		// We need to have a loop condition here so that we can return to the beginning
		// of the loop. We need to remember this line number is the true stack.
		codeGenPrintf("\nPushing the line number %d to the FA true stack.", codeGenObj->getAssemblyLineNumber());
		codeGenObj->pushInFaTrueStack();
	}
	else
	{
		codeGenPrintf("\n\nTROUBLE, TROUBLE! procflag is %d. It can only be 0 or 1", procFlag);
	}
}

int handleFa2()
{
	if(procFlag == 0)
	{
		// The value of the loop variable has been assigned. Now that we have that
		// we need to ensure that the value of the loop variable is lesser than 
		// the value given in exp2. 

		// Now that the label has been created, we need to check if the value of
		// loop variable is lesser than the expression. If it is, we proceed, else
		// we jump to the end of the body loop. However, we do not know where to jump
		// yet. So we simply push the upcoming line number to the false stack so that
		// it can be back patched later. 

		// The expression 1 has been evaluated and sits in AC1. Now lets load the value
		// of the loop variable to AC2.
		string loopVar = codeGenObj->getTopFaVarStack();
		SymbolObject* tmpSymbolObject = scopeStackObj->getSymbolObject(loopVar);
		if(tmpSymbolObject == NULL)
		{
			codeGenPrintf("\n\nTROUBLE, TROUBLE!: Loop variable symbol object is NULL. Check HandleFa2().\n");
			return 0;
		}
		int address = tmpSymbolObject->addressOfVariable;
		string comment = "Loading the value of loop variable ";
		comment += loopVar;
		codeGenObj->genComment(comment);
		codeGenObj->genLD(AC2, address, ZERO);
		codeGenObj->genOperation("-", AC1, AC2, AC1);

		// Now that we have the calculated the necessary details, we save the next line
		// number in case the expression evaluates to false so that we can backpatch
		// later. 
		codeGenPrintf("\nPushing the line number %d to the FA False stack.", codeGenObj->getAssemblyLineNumber());
		codeGenObj->genComment("**Saving line to backpatch false condition for fa **");
		codeGenObj->pushInFaFalseStack();
	}
	else if (procFlag == 1)
	{
		ProcObject* procObj = scopeStackObj->getProcObject(currentProcName);
		codeGenPrintf("\nFa used inside a proc %s.", currentProcName.c_str());
		if(procObj == NULL)
		{
			codeGenPrintf("\n\nTROUBLE, TROUBLE!: Proc Object is NULL. Check HandleFa2().\n");
			return -1;
		}
		string loopVar = codeGenObj->getTopFaVarStack();
		SymbolObject* tmpSymbolObject = procObj->PST->lookUpSymbolInProc(loopVar);
		if(tmpSymbolObject == NULL)
		{
			codeGenPrintf("\n\nTROUBLE, TROUBLE!: Loop variable symbol object is NULL. Check HandleFa2().\n");
			return 0;
		}
		int address = tmpSymbolObject->addressOfVariable;
		string comment = "Loading the value of loop variable ";
		comment += loopVar;
		codeGenObj->genComment(comment);
		codeGenObj->genLD(AC2, address, BP);
		codeGenObj->genOperation("-", AC1, AC2, AC1);

		// Now that we have the calculated the necessary details, we save the next line
		// number in case the expression evaluates to false so that we can backpatch
		// later. 
		codeGenPrintf("\nPushing the line number %d to the FA False stack.", codeGenObj->getAssemblyLineNumber());
		codeGenObj->genComment("**Saving line to backpatch false condition for fa **");
		codeGenObj->pushInFaFalseStack();
	}
	else
	{
		codeGenPrintf("\n\nTROUBLE, TROUBLE! procflag is %d. It can only be 0 or 1", procFlag);
	}

}

int handleFa3()
{
	if(procFlag == 0)
	{
		// First thing we do after all the statements is that we will be explicitly 
		// incrementing the loop variable. So lets do that first. 
		string loopVar = codeGenObj->getTopFaVarStack();
		SymbolObject* tmpSymbolObject = scopeStackObj->getSymbolObject(loopVar);
		if(tmpSymbolObject == NULL)
		{
			codeGenPrintf("\n\nTROUBLE, TROUBLE!: Loop variable symbol object is NULL. Check HandleFa3().\n");
			return 0;
		}
		int address = tmpSymbolObject->addressOfVariable;
		string comment = "Loading the value of loop variable ";
		comment += loopVar;
		codeGenObj->genComment(comment);
		codeGenObj->genLD(AC1, address, ZERO);
		codeGenObj->genComment("Incrementing the loop variable.");
		codeGenObj->genLDA(AC1, 1, AC1);

		// After incrementing, we need to once again store it back to memory.
		codeGenObj->genStore(AC1, address, ZERO);

		// Next thing that we will be doing is jumping unconditionally back to the beginning.
		int beginning = codeGenObj->popFromFaTrueStack();
		codeGenObj->genComment("Jump back to the beginning of fa");
		codeGenObj->genJEQ(ZERO, beginning);

		// Now we know exactly how many lines Fa body took. We now will need to backpatch
		// the false condition. 
		int lineNumberToBackPatch = codeGenObj->popFromFaFalseStack();
		// Since with FA we only go lo - hi our result from should always be -ve or 0.
		// So we can just say, if equal to 0, jump to current line number. 
		codeGenObj->genComment("The subtracted value is in AC1. If AC1 > 0, then jump to the end.");
		codeGenObj->genJGT(lineNumberToBackPatch, AC1, codeGenObj->getAssemblyLineNumber());

		// Handle Break
		codeGenObj->genBreak(loopCount);
		// Now we remove the variable from the loopVarStack.

		codeGenObj->popFromFaVarStack();
	}
	else if (procFlag == 1)
	{
		ProcObject* procObj = scopeStackObj->getProcObject(currentProcName);
		codeGenPrintf("\nFa used inside a proc %s.", currentProcName.c_str());
		if(procObj == NULL)
		{
			codeGenPrintf("\n\nTROUBLE, TROUBLE!: Proc Object is NULL. Check HandleFa3().\n");
			return -1;
		}
		string loopVar = codeGenObj->getTopFaVarStack();
		SymbolObject* tmpSymbolObject = procObj->PST->lookUpSymbolInProc(loopVar);
		if(tmpSymbolObject == NULL)
		{
			codeGenPrintf("\n\nTROUBLE, TROUBLE!: Loop variable symbol object is NULL. Check HandleFa3().\n");
			return 0;
		}
		int address = tmpSymbolObject->addressOfVariable;
		string comment = "Loading the value of loop variable ";
		comment += loopVar;
		codeGenObj->genComment(comment);
		codeGenObj->genLD(AC1, address, BP);
		codeGenObj->genComment("Incrementing the loop variable.");
		codeGenObj->genLDA(AC1, 1, AC1);

		// After incrementing, we need to once again store it back to memory.
		codeGenObj->genStore(AC1, address, BP);

		// Next thing that we will be doing is jumping unconditionally back to the beginning.
		int beginning = codeGenObj->popFromFaTrueStack();
		codeGenObj->genComment("Jump back to the beginning of fa");
		codeGenObj->genJEQ(ZERO, beginning);

		// Now we know exactly how many lines Fa body took. We now will need to backpatch
		// the false condition. 
		int lineNumberToBackPatch = codeGenObj->popFromFaFalseStack();
		// Since with FA we only go lo - hi our result from should always be -ve or 0.
		// So we can just say, if equal to 0, jump to current line number. 
		codeGenObj->genComment("The subtracted value is in AC1. If AC1 > 0, then jump to the end.");
		codeGenObj->genJGT(lineNumberToBackPatch, AC1, codeGenObj->getAssemblyLineNumber());

		// Handle Break
		codeGenObj->genBreak(loopCount);
		// Now we remove the variable from the loopVarStack.
		codeGenObj->popFromFaVarStack();
	}
	else
	{
		codeGenPrintf("\n\nTROUBLE, TROUBLE! procflag is %d. It can only be 0 or 1", procFlag);
	}
}