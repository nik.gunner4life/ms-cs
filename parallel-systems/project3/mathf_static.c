/*
Single Author info: 
nsrivat Nikhil Srivatsan Srinivasan
*/

#include<stdio.h>
#include<stdlib.h>
#include<math.h>
#include<omp.h>

#define PRINT 0

#define N 100000
#define MATHF_NUM 4
#define SELECTION_NUM 16

int selection[] = { 2015, 11028, 3042, 9031, 1017, 8063, 3098, 9004, 7025, 2036, 12074, 4051, 9083, 3002, 10029, 6007};
int parameter[] = { 37, 18, 22, 51, 12, 33, 45, 3, 19, 82, 62, 32, 27, 43, 36, 14};

double Recursive(int n) 
{
  if (n == 0)
	  return 0;
  return (double)(Recursive(n-1) + parameter[n % SELECTION_NUM]);
}

int main(int argc, char ** argv)
{
    int i, mathf, num;
	double x, y, sum;

	struct timeval start_tv, end_tv;
	if (argc != 2){
		printf("Please use the format below to run the program\n");
		printf("mathf <numThreads>\n");
		return 1;
	}
	int numThreads;
	sum = 0.0f;
	gettimeofday(&start_tv, NULL);

	numThreads = (int)atoi(argv[1]);
	printf("nthreads= %d\n", numThreads);
	/* MATH LOOP */
	#pragma omp parallel for schedule(static) private(mathf, num, i, x, y) reduction(+:sum) num_threads(numThreads)
	for(i=0;i<N;i++)
	{
		mathf = i % MATHF_NUM;
		num   = i % SELECTION_NUM;

		x = Recursive (selection[num] + (i % 2000));

		switch(mathf)
		{
			case 0:
				y = log10 (x);
#if PRINT
				printf ("[%d]: (%f) = %f\n", i, x, y);
#endif
			break;

			case 1:
				y = sqrt (x);
#if PRINT
				printf ("[%d]: (%f) = %f\n", i, x, y);
#endif
			break;

			case 2:
				y = cbrt (x);
#if PRINT
				printf ("[%d]: (%f) = %f\n", i, x, y);
#endif
			break;

			case 3:
				y = log (x);
#if PRINT
				printf ("[%d]: (%f) = %f\n", i, x, y);
#endif
			break;

			default:
				printf ("Not possible case\n");
				return -1;
		}

		sum += y;
	}

	gettimeofday(&end_tv, NULL);

	printf("Average: %f\n", sum / N);
	printf("Elapsed time: %f sec\n", 
		(double)( (double)(end_tv.tv_sec - start_tv.tv_sec) + ( (double)(end_tv.tv_usec - start_tv.tv_usec)/1000000)) );

	return 0;
}