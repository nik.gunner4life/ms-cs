/*
Single Author info: 
nsrivat Nikhil Srivatsan Srinivasan
*/

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "mpi.h"

#define ROOT 0

//Global variables that will hold the number of processes
//the rank of the process and the length. 
int   numproc, rank, len;
long *packetSizeArray;
int *countArray;

int MPI_Init(int *argc, char **argv[])
{
	PMPI_Init(argc, argv);
	//Initialize the arrays that will be used for counting.
	MPI_Comm_size(MPI_COMM_WORLD, &numproc);
	MPI_Comm_rank(MPI_COMM_WORLD, &rank);
	packetSizeArray = (long*)malloc(sizeof(long*) * numproc);
	countArray = (int*)malloc(sizeof(int*) * numproc);
	int i = 0;
	for(i = 0; i < numproc; i++)
	{
		packetSizeArray[i] = 0;
		countArray[i] = 0;
	}
}

int MPI_Send(void *buffer, int size, MPI_Datatype datatype, int destinationNode, int tag, MPI_Comm comm)
{
	int datatypeSize = 0;
	PMPI_Type_size(datatype, &datatypeSize);
	packetSizeArray[destinationNode] = packetSizeArray[destinationNode] + (datatypeSize * size);
	countArray[destinationNode]++;
	PMPI_Send(buffer, size, datatype, destinationNode, tag, comm);
}

int MPI_Isend(void *buf, int size, MPI_Datatype datatype, int destinationNode, int tag, MPI_Comm comm, MPI_Request *request)
{
	int datatypeSize = 0;
	PMPI_Type_size(datatype, &datatypeSize);
	packetSizeArray[destinationNode] = packetSizeArray[destinationNode] + (datatypeSize * size);
	countArray[destinationNode]++;
	PMPI_Isend(buf, size, datatype, destinationNode, tag, comm, request);
}
int MPI_Finalize()
{
	//Send all the data to the root to print
	MPI_Status status;
	if(rank != ROOT)
	{
		PMPI_Send(packetSizeArray, numproc, MPI_LONG, ROOT, 50, MPI_COMM_WORLD);
		PMPI_Send(countArray, numproc, MPI_INT, ROOT, 50, MPI_COMM_WORLD);
	}
	else
	{
		int i = 0;
		int j = 0;
		FILE *fp = fopen("matrix.data", "w");

		//First print ROOT's contents. 
		for(i = 0; i < numproc; i++)
		{
			//Avoid dividing by 0
			if(countArray[i] != 0)
				fprintf(fp, "%d %d %d %f\n", ROOT, i, countArray[i], packetSizeArray[i] * 1.0/countArray[i]);
			else
				fprintf(fp, "%d %d %d %f\n", ROOT, i, 0, 0.0);
		}

		//Get contents from all other nodes and print them. 
		for(i = 1; i < numproc; i++)
		{
			long *packetArrayOfOtherNodes;
			int *countArrayOfOtherNodes;
			packetArrayOfOtherNodes = (long*)malloc(sizeof(long*) * numproc);
			countArrayOfOtherNodes = (int*)malloc(sizeof(int*) * numproc);

			PMPI_Recv(packetArrayOfOtherNodes, numproc, MPI_LONG, i, 50, MPI_COMM_WORLD, &status);
			PMPI_Recv(countArrayOfOtherNodes, numproc, MPI_INT, i, 50, MPI_COMM_WORLD, &status);

			for(j = 0; j < numproc; j++)
			{
				if(countArrayOfOtherNodes[j] != 0)
					fprintf(fp, "%d %d %d %f\n", i, j, countArrayOfOtherNodes[j], packetArrayOfOtherNodes[j] * 1.0/countArrayOfOtherNodes[j]);
				else
					fprintf(fp, "%d %d %d %f\n", i, j, 0, 0.0);
			}
		}
		fclose(fp);
	}
	PMPI_Finalize();
}