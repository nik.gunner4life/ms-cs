#!/bin/bash
clear
rm output.log
rm outputMPI.log
rm -f interp_gpu
rm -f interp_gpu_nsrivat.*
make clean && make && clear && qsub interp_gpu.qsub && watch -n 1 qstat
