/***************************************/
// Single Author Info:
// nsrivat Nikhil Srivatsan Srinivasan
/***************************************/

#include<stdio.h>
#include<stdlib.h>
#include <math.h>
#include "mpi.h"
#include <sys/time.h>

#define TPB 500
#define ROOT 0


__global__ void kernel( void ) 
{
} 
__global__ void interpolatePoints(double *M, double *X, double *Y, int n, int points, double x)
{
	int index, j, k;
	double mul = 1; 
	double sum = 0; 
	index = threadIdx.x + (blockIdx.x * blockDim.x);
	if(index < n)
	{
		for(j = 0; j < points; j++)
		{
			mul = 1;
			for(k = 0; k < points; k++)
			{
				if(X[j] == X[k])
					continue;
				mul*= ((x - X[k])/(X[j] - X[k]));
			}
			sum+= mul * M[(index * points) + j];
		}
		Y[index] = sum; 
	}
	
}

/* Helper functions */

// This routine initializes the M Array. 
int input_generator(int n, int dim, double **M)
{
	int i, j;
	//Generating the X points
	for(i = 0; i < n; i++)
	{
		M[i][0] = 0;
		for(j = 0; j < dim-1; j++)
		{
			M[i][j+1] = ((i+1)/100000.0)*(j+1); 
		}
	}
	return 0;
}

//This routine creates a file called output.log
//and enters all the values in that file. 
//This routine creates a file called output.log
//and enters all the values in that file. 
void print_Y(int n, double* Y, int m, double *Z, double x)
{
	int   i;
	FILE *fp = fopen("/home/temp549/HW2/CUDA_PROGRAMMING/output.log", "w");
	for(i = 0; i < n; i++)
	{
		fprintf(fp, "\nF:%d, (%f)=%f", i, x, Y[i]);
	}
	for(i = 0; i < m; i++)
	{
		fprintf(fp, "\nF:%d, (%f)=%f", n + i, x, Z[i]);
	}
	fclose(fp);
}

int main(int argc, char ** argv)
{
	// MPI Initializations
	int   numproc, rank, len, ratioOfData0, ratioOfData1;
	char  hostname[MPI_MAX_PROCESSOR_NAME];
	MPI_Init(&argc, &argv);
	MPI_Comm_size(MPI_COMM_WORLD, &numproc);
	MPI_Comm_rank(MPI_COMM_WORLD, &rank);
	MPI_Get_processor_name(hostname, &len);
	MPI_Status status; 

	double t1, t2; 
	FILE *fp;
	int points, n;
	double x;
	double *Y, *devY, *X, **M, *devM, *devX, *ratioMatrix1, *oneDimArray, *Z;
	int i;
	t1 = MPI_Wtime(); 
	if (argc != 3)
	{
		printf("\nPlease use the format below to run the program");
		printf("\ninterp <filename> <x>");
		return 1;
	}
	if(rank != ROOT)
		printf("\nInput File: %s", argv[1]);

	/* Opening the input file */
	fp = fopen(argv[1], "r");
	if (fp == NULL) 
	{
		printf("Error in opening a file: %s", argv[1]);
		return 0;
	}

	x = (double)atof(argv[2]);
	if(rank != ROOT)
		printf("\nx = %f\n", x);

	/* Reading the maxtrix dimension */
	if(rank == ROOT)
	{
		fscanf(fp, "%d %d\n", &points, &n);
		printf("\npoints = %d, n = %d", points, n);
	}
	//Broadcast the matrix dimensions to the other process. 
	if(rank == ROOT)
	{
		MPI_Send(&n, 1, MPI_INT, 1, 50, MPI_COMM_WORLD);
		MPI_Send(&points, 1, MPI_INT, 1, 50, MPI_COMM_WORLD);
	}
	else
	{
		MPI_Recv(&n, 1, MPI_INT, ROOT, 50, MPI_COMM_WORLD, &status);
		MPI_Recv(&points, 1, MPI_INT, ROOT, 50, MPI_COMM_WORLD, &status);
	}

	//Ratio of Data is a variable that stores how many rows each of
	//the processes will be working with. 
	ratioOfData0 = floor(n/numproc); 
	ratioOfData1 = n - ratioOfData0; 
	if(rank != ROOT)
		ratioMatrix1 = (double*) malloc(ratioOfData1 * points * sizeof(double));
	if(rank == ROOT)
		oneDimArray = (double*) malloc(n * points * sizeof(double));
	
	//Allocate memory for X, Y and finalOutput. 
	X = (double*) malloc(points * sizeof(double));
	
	/* Set the X points and send it to Process 1 */
	if(rank == ROOT)
	{
		for(i = 0; i < points; i++)
		{
			X[i] = i;
		}
	}
	if(rank == ROOT) 
		MPI_Send(X, points, MPI_DOUBLE, 1, 50, MPI_COMM_WORLD);
	else
		MPI_Recv(X, points, MPI_DOUBLE, ROOT, 50, MPI_COMM_WORLD, &status);
	if(rank == ROOT)
	{
		//Allocate space for Matrix M. The input generator is a routine
		//that will initialize M. This could be done either using a file
		//or simply assigning values to M. Since it could be done even
		//by reading a file, we cannot have multiple processes doing this.
		//That is inefficient. It would be better to let the ROOT read the 
		//file and send the other processes the data they need. 
		M = (double**) malloc(n * sizeof(double*));
		for (i = 0; i < n; i++)
			M[i]= oneDimArray + (i * points);
		input_generator(n, points, M);
	}

	//Send one part of the matrix to the other node and keep the other half
	//of the matrix here. 
	if(rank == ROOT) 
		MPI_Send(M[ratioOfData0], ratioOfData1 * points, MPI_DOUBLE, 1, 50, MPI_COMM_WORLD);
	else
		MPI_Recv(ratioMatrix1, ratioOfData1 * points, MPI_DOUBLE, ROOT, 50, MPI_COMM_WORLD, &status);
	

	//Allocate space on the device. Call the kernel routine to execute
	//the code. Once the kernel is done, the output from Process 1 is 
	//transferred to the ROOT, which will use this to print the output. 
	cudaMalloc((void**)&devX, points * sizeof(double));
	cudaMemcpy(devX, X, points * sizeof(double), cudaMemcpyHostToDevice);
	Z = (double*) malloc(ratioOfData1 * sizeof(double));
	if(rank == ROOT)
	{
		Y = (double*) malloc(ratioOfData0 * sizeof(double));
		cudaMalloc((void**)&devM, ratioOfData0 * points * sizeof(double));
		cudaMalloc((void**)&devY, ratioOfData0 * sizeof(double));
		cudaMemcpy(devY, Y, ratioOfData0 * sizeof(double), cudaMemcpyHostToDevice);
		cudaMemcpy(devM, M[0], ratioOfData0 * points * sizeof(double), cudaMemcpyHostToDevice);
		interpolatePoints<<< floor(ratioOfData0/TPB) + 1, TPB >>>(devM, devX, devY, ratioOfData0, points, x);
		cudaMemcpy(Y, devY, ratioOfData0 * sizeof(double), cudaMemcpyDeviceToHost);
		MPI_Recv(Z, ratioOfData1, MPI_DOUBLE, 1, 50, MPI_COMM_WORLD, &status);
	}
	else
	{
		Y = (double*) malloc(ratioOfData1 * sizeof(double));
		cudaMalloc((void**)&devM, ratioOfData1 * points * sizeof(double));
		cudaMalloc((void**)&devY, ratioOfData1 * sizeof(double));
		cudaMemcpy(devY, Y, ratioOfData1 * sizeof(double), cudaMemcpyHostToDevice);
		cudaMemcpy(devM, ratioMatrix1, ratioOfData1 * points * sizeof(double), cudaMemcpyHostToDevice);
		interpolatePoints<<< floor(ratioOfData1/TPB) + 1, TPB >>>(devM, devX, devY, ratioOfData1, points, x);
		cudaMemcpy(Y, devY, ratioOfData1 * sizeof(double), cudaMemcpyDeviceToHost);
		MPI_Send(Y, ratioOfData1, MPI_DOUBLE, ROOT, 50, MPI_COMM_WORLD);
	}

	//Print the output to the output.log file. This file will exist in the
	//root directory. 
	if(rank == ROOT)
	{
		/*for(i = 0; i < n; i++)
		{
			if(i < ratioOfData0)
				printf("\nF:%d, (%f)=%f", i, x, Y[i]); 
			else
				printf("\nF:%d, (%f)=%f", i, x, Z[i - ratioOfData0]); 
		}*/
		print_Y(ratioOfData0, Y, ratioOfData1, Z, x);
	}
	t2 = MPI_Wtime(); 
	if(rank == ROOT)
		printf("\n\nTime difference = %f\n", t2 - t1 );
	//Free all the allocated memory on host. 
	if(rank == ROOT)
	{
		for (i = 0; i < n; i++)
			free(M[i]);
		free(M);
	}
	if(rank != ROOT)
		free(ratioMatrix1);
	free(X);
	free(Y);
	free(Z); 
	if(rank == ROOT)
		free(oneDimArray); 
	
	//Free all allocated memory on device. 
	cudaFree(devM);
	cudaFree(devX);
	cudaFree(devY);
	
	printf("\n");
	MPI_Finalize(); 
	return 1; 
}