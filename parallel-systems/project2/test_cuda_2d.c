#include<stdio.h>
#include<stdlib.h>
#include <math.h>
#include "mpi.h"
#include <sys/time.h>

#define TPB 500
#define ROOT 0


__global__ void kernel( void ) 
{
} 
__global__ void interpolatePoints(double *M, double *X, double *Y, int n, int points, double x)
{
	int index, j, k;
	double mul = 1; 
	double sum = 0; 
	index = threadIdx.x + (blockIdx.x * blockDim.x);
	for(j = 0; j < points; j++)
	{
		mul = 1;
		for(k = 0; k < points; k++)
		{
			if(X[j] == X[k])
				continue;
			mul*= ((x - X[k])/(X[j] - X[k]));
		}
		sum+= mul * M[(index * points) + j];
	}
	Y[index] = sum; 
}

/* Helper functions */

// This routine initializes the M Array. 
int input_generator(int n, int dim, double **M)
{
	int i, j;
	//Generating the X points
	for(i = 0; i < n; i++)
	{
		M[i][0] = 0;
		for(j = 0; j < dim-1; j++)
		{
			M[i][j+1] = ((i+1)/100000.0)*(j+1); 
		}
	}
	return 0;
}

int main(int argc, char ** argv)
{
	// MPI Initializations
	int   numproc, rank, len, ratioOfData;
	char  hostname[MPI_MAX_PROCESSOR_NAME];
	MPI_Init(&argc, &argv);
	MPI_Comm_size(MPI_COMM_WORLD, &numproc);
	MPI_Comm_rank(MPI_COMM_WORLD, &rank);
	MPI_Get_processor_name(hostname, &len);
	MPI_Status status; 

	double t1, t2; 
	FILE *fp;
	int points, n;
	double x;
	double *Y, *devY, *X, **M, *devM, *devX, *finalOutput, *ratioMatrix0, *oneDimArray;
	int i;
	t1 = MPI_Wtime(); 
	if (argc != 3)
	{
		printf("\nPlease use the format below to run the program");
		printf("\ninterp <filename> <x>");
		return 1;
	}
	if(rank != ROOT)
		printf("\nInput File: %s", argv[1]);

	/* Opening the input file */
	fp = fopen(argv[1], "r");
	if (fp == NULL) 
	{
		printf("Error in opening a file: %s", argv[1]);
		return 0;
	}

	x = (double)atof(argv[2]);
	if(rank != ROOT)
		printf("\nx = %f\n", x);

	/* Reading the maxtrix dimension */
	if(rank == ROOT)
	{
		fscanf(fp, "%d %d\n", &points, &n);
		printf("\npoints = %d, n = %d", points, n);
	}
	//Broadcast the matrix dimensions to the other process. 
	if(rank == ROOT)
	{
		MPI_Send(&n, 1, MPI_INT, 1, 50, MPI_COMM_WORLD);
		MPI_Send(&points, 1, MPI_INT, 1, 50, MPI_COMM_WORLD);
	}
	else
	{
		MPI_Recv(&n, 1, MPI_INT, ROOT, 50, MPI_COMM_WORLD, &status);
		MPI_Recv(&points, 1, MPI_INT, ROOT, 50, MPI_COMM_WORLD, &status);
	}

	//Ratio of Data is a variable that stores how many rows each of
	//the processes will be working with. 
	ratioOfData = n/numproc; 
	if(rank != ROOT)
		ratioMatrix0 = (double*) malloc(ratioOfData * points * sizeof(double));
	if(rank == ROOT)
		oneDimArray = (double*) malloc(n * points * sizeof(double));
	
	//Allocate memory for X, Y and finalOutput. 
	X = (double*) malloc(points * sizeof(double));
	finalOutput = (double*) malloc(n * sizeof(double));
	Y = (double*) malloc(ratioOfData * sizeof(double));
	
	/* Set the X points and send it to Process 1 */
	if(rank == ROOT)
	{
		for(i = 0; i < points; i++)
		{
			X[i] = i;
		}
	}
	if(rank == ROOT) 
		MPI_Send(X, points, MPI_DOUBLE, 1, 50, MPI_COMM_WORLD);
	else
		MPI_Recv(X, points, MPI_DOUBLE, ROOT, 50, MPI_COMM_WORLD, &status);
	if(rank == ROOT)
	{
		//Allocate space for Matrix M. The input generator is a routine
		//that will initialize M. This could be done either using a file
		//or simply assigning values to M. Since it could be done even
		//by reading a file, we cannot have multiple processes doing this.
		//That is inefficient. It would be better to let the ROOT read the 
		//file and send the other processes the data they need. 
		M = (double**) malloc(n * sizeof(double*));
		for (i = 0; i < n; i++)
			M[i]= oneDimArray + (i * points);
		input_generator(n, points, M);
	}

	//This initializes the matrix ratioMatrix0 and ratioMatrix1. ratioMatrix0
	//is the data that each of the processes will be working with. The matrix 
	//in this case is split into two parts of n/2 rows each (there are only 
	//going to be two processes. Given). The first n/2 is assigned to the ROOT 
	//process and the other half(ratioMatrix1) is sent to the other process. 
	if(rank == ROOT) 
		MPI_Send(M[ratioOfData], ratioOfData * points, MPI_DOUBLE, 1, 50, MPI_COMM_WORLD);
	else
		MPI_Recv(ratioMatrix0, ratioOfData * points, MPI_DOUBLE, ROOT, 50, MPI_COMM_WORLD, &status);
	
	//Allocate space on the device. 
	cudaMalloc((void**)&devM, ratioOfData * points * sizeof(double));
	cudaMalloc((void**)&devX, points * sizeof(double));
	cudaMalloc((void**)&devY, ratioOfData * sizeof(double));
	
	//Move Arrays X, Y and M in host to devX and devY and devM in device
	//For devM, since it is a dynamically allocated array, we need to copy
	//this row by row. Also the device can hold only 1D arrays and hence
	//we will need to convert this 2D array to a 1D array and then copy
	//it on the devive. 
	cudaMemcpy(devX, X, points * sizeof(double), cudaMemcpyHostToDevice);
	cudaMemcpy(devY, Y, ratioOfData * sizeof(double), cudaMemcpyHostToDevice);
	if(rank == ROOT)
		cudaMemcpy(devM, M[0], ratioOfData * points * sizeof(double), cudaMemcpyHostToDevice);
	else
		cudaMemcpy(devM, ratioMatrix0, ratioOfData * points * sizeof(double), cudaMemcpyHostToDevice);
	
	//Call the kernel routine. The kernel routine simply allocates a thread
	//for every row. Each thread is then responsible for generating one o/p value.
	//Once that is done, the output of every device is moved back to the host
	//and aggregated by the ROOT in finalOutput. 
	interpolatePoints<<< ratioOfData/TPB, TPB >>>(devM, devX, devY, n, points, x);
	cudaMemcpy(Y, devY, ratioOfData * sizeof(double), cudaMemcpyDeviceToHost);
	MPI_Gather(Y, ratioOfData, MPI_DOUBLE, finalOutput, ratioOfData, MPI_DOUBLE, ROOT, MPI_COMM_WORLD);

	//Print the output to the output.log file. This file will exist in the
	//root directory. 
	if(rank == ROOT)
	{
		for(i = 0; i < n; i++)
		{
			printf("\nF:%d, (%f)=%f", i, x, finalOutput[i]); 
		}
	}
	t2 = MPI_Wtime(); 
	if(rank == ROOT)
		printf("\n\nTime difference = %f\n", t2 - t1 );
	//Free all the allocated memory on host. 
	if(rank == ROOT)
	{
		for (i = 0; i < n; i++)
			free(M[i]);
		free(M);
	}
	if(rank != ROOT)
		free(ratioMatrix0);
	free(X);
	free(Y);
	free(finalOutput); 
	if(rank == ROOT)
		free(oneDimArray); 
	
	//Free all allocated memory on device. 
	cudaFree(devM);
	cudaFree(devX);
	cudaFree(devY);
	
	printf("\n");
	MPI_Finalize(); 
	return 1; 
}