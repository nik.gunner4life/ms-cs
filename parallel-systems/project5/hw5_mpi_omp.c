/********************************
Group info:
(hpjoshi) Harshvardhan Joshi
(nmahade) Nandish Mahadevaiah
(nsrivat) Nikhil Srivatsan Srinivasan
Date: 11/27/2013
Project: Homework 5
********************************/

#include <math.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "mpi.h"
#include <omp.h>
#include <sys/time.h>
#include "jemalloc/jemalloc.h"

#define MMAP_SIZE ((size_t)1 << 30)
#define READSIZE 200
#define ROOT 0
#define NODE_1 1
#define OK 100
#define SYSERR -100
#define nsrPrintf(...) if(nsrDebugPrintf) { fflush(stdout); printf(__VA_ARGS__); fflush(stdout);}
int nsrDebugPrintf;
#define flushPrintf(...) if(flushValuePrintf) { fflush(stdout); printf(__VA_ARGS__); fflush(stdout);}
int flushValuePrintf = 1;

int main (int argc, char *argv[])
{
	char MMAP_FILE[READSIZE];
	char BACK_FILE[READSIZE];
	int do_restore = 0;
	int cycle = 10000;
	char *permaDirectory;
	char *mode;
	int   numproc, rank, len;
	char  hostname[MPI_MAX_PROCESSOR_NAME];
	double *diff;  /* change in value */
	int    i, j;
	double mean;
	double *oneDimArrayU, *oneDimArrayW, **u, **w, EPSILON;
	int N, count, threadID, numThreads;
	MPI_Init(&argc, &argv);
	MPI_Request sendRequest[8];
	MPI_Comm_size(MPI_COMM_WORLD, &numproc);
	MPI_Comm_rank(MPI_COMM_WORLD, &rank);
	MPI_Get_processor_name(hostname, &len);
	MPI_Status status;
	omp_set_dynamic(0);  
	struct timeval start_tv, end_tv;
	
	// Identify what arguments have been passed
	for(i = 1; i < argc; i++)
	{
		if(strcmp(argv[i], "-n") == 0)
		{
			if(i == argc)
			{
				flushPrintf("\nArguments are wrong. -n should be succeeded by an integer/long value.");
				return SYSERR;
			}
			N = atoi(argv[i + 1]);
			flushPrintf("\nValue of N: %d", N);
		}
		else if(strcmp(argv[i], "-e") == 0)
		{
			if(i == argc)
			{
				flushPrintf("\nArguments are wrong. -n should be succeeded by an float/double value.");
				return SYSERR;
			}
			EPSILON = atof(argv[i + 1]);
			flushPrintf("\nValue of EPSILON: %f", EPSILON);
		}
		else if(strcmp(argv[i], "-c") == 0)
		{
			cycle = atoi(argv[i + 1]);
			flushPrintf("\nFrequency: %d", cycle);
		}
		else if(strcmp(argv[i], "-r") == 0)
		{
			do_restore = 1;
		}
		else if(strcmp(argv[i], "-p") == 0)
		{
			permaDirectory = argv[i + 1];
			strcpy(MMAP_FILE, permaDirectory);
			strcpy(BACK_FILE, permaDirectory);
		}
	}
	if(rank == ROOT)
	{
		strcat(MMAP_FILE, "temp549_ROOT.mmap");
		strcat(BACK_FILE, "temp549_ROOT.bak");
	}
	else if (rank == NODE_1)
	{
		strcat(MMAP_FILE, "temp549_NODE_1.mmap");
		strcat(BACK_FILE, "temp549_NODE_1.bak");
	}
	oneDimArrayU = (double*) malloc(N * N * sizeof(double));
	perm(oneDimArrayU, N * N * sizeof(double));
	perm(&count, sizeof(int));
	nsrPrintf("\nPerMa Memory Allocated");
	oneDimArrayW = (double*) malloc(N * N * sizeof(double));
	nsrPrintf("\nArray W Allocated");
	u = (double**) malloc(N * sizeof(double*));
	w = (double**) malloc(N * sizeof(double*));
	for (i = 0; i < N; i++)
	{
		u[i] = oneDimArrayU + (i * N);
		w[i] = oneDimArrayW + (i * N);

	}
	nsrPrintf("\nMemory Allocated");
	if(do_restore)
		mode = "r+";
	else
		mode = "w+";
	mopen(MMAP_FILE, mode, MMAP_SIZE);
	bopen(BACK_FILE, mode);

#pragma region ONLY_ROOT
	if(numproc == 1)
	{
		numThreads = 16;
		omp_set_num_threads(numThreads);
		diff = (double*) malloc(numThreads * sizeof(double));
		if(do_restore)
		{
			mflush();
			restore();
		}
		else
		{
			mean = 0.0;
			for(i = 0; i < N; i++)
			{
				u[i][0] = u[i][N-1] = u[0][i] = 100.0;
				u[N-1][i] = 0.0;
				mean += u[i][0] + u[i][N-1] + u[0][i] + u[N-1][i];
			}
			mean /= (4.0 *N);

			nsrPrintf("\nMean Initialized");

			/* Initialize interior values */
			for (i =1; i<N-1; i++)
				for (j=1; j<N-1; j++)
					u[i][j] = mean;
			nsrPrintf("\nArray U Initialized");
			count = 0;
		}
		gettimeofday(&start_tv, NULL);
		/* Compute steady-state solution - Parallelize this loop with MPI_OMP */
		mflush();
		while(1)
		{
			flushPrintf("\nCycle: %d", count);
			if(count % cycle == 0)
			{
				mflush();
				backup();
				flushPrintf("\nTaken Backup at %d.", count);
			}
			count++;
			for(i = 0; i < numThreads; i++)
				diff[i] = 0.0;
			#pragma omp parallel for private(i,j) num_threads(numThreads)
			for (i=1; i<N-1; i++)
			{
				threadID = omp_get_thread_num();
				for (j=1; j<N-1; j++)
				{
					w[i][j] = (u[i-1][j] + u[i+1][j] + u[i][j-1] + u[i][j+1])/4.0;
					if (fabs (w[i][j] - u[i][j]) > diff[threadID])
						diff[threadID] = fabs(w[i][j] - u[i][j]);
				}
			}
			double maxDiff = 0.0;
			for(i = 0; i < numThreads; i++)
			{
				if(diff[i] > maxDiff)
					maxDiff = diff[i];
			}
			if (maxDiff <= EPSILON)
				break;

			#pragma omp parallel for private(i,j) num_threads(numThreads)
			for (i=1; i<N-1; i++)
				for (j=1; j<N-1; j++)
					u[i][j] = w[i][j];
		}
		/* End of parallelization */
		gettimeofday(&end_tv, NULL);
		
		flushPrintf("\nElapsed time: %f sec", (double)( (double)(end_tv.tv_sec - start_tv.tv_sec) + ( (double)(end_tv.tv_usec - start_tv.tv_usec)/1000000)) );
		flushPrintf("\nCount: %d", count);
		/* Print Solution */
		FILE *fp = fopen("output.dat", "w");
		for (i=0; i<N; i++)
		{
			for (j=0; j<N; j++)
			{
				fprintf(fp, "%6.2f ", u[i][j]);
			}
			fprintf(fp, "\n");
		}

		// Clear all the allocated memory. No need to free u and w explicitly. 
		// TODO Need to change all memory free(s) to PerMa free calls. Have to 
		// figure out how to do this. 
		free(oneDimArrayU);
		free(oneDimArrayW);
		fclose(fp);

		flushPrintf("\n");
	}
#pragma endregion

#pragma region 2_NODES
	else if(numproc == 2)
	{
		
		numThreads = 8;
		omp_set_num_threads(numThreads);
		diff = (double*) malloc(numThreads * sizeof(double));
		int M = N/2 - 1;
		if(do_restore)
		{
			mflush();
			restore();
		}
		else
		{
			if(rank == ROOT)
			{
				mean = 0.0;
				for(i = 0; i < N; i++)
				{
					u[i][0] = u[i][N-1] = u[0][i] = 100.0;
					u[N-1][i] = 0.0;
					mean += u[i][0] + u[i][N-1] + u[0][i] + u[N-1][i];
				}
				mean /= (4.0 *N);

				nsrPrintf("\nMean Initialized");

				/* Initialize interior values */
				for (i =1; i<N-1; i++)
					for (j=1; j<N-1; j++)
						u[i][j] = mean;
				nsrPrintf("\nArray U Initialized");
			}
			// Let both nodes have the entire matrix. 
			if(rank == ROOT)
				MPI_Send(oneDimArrayU, N * N, MPI_DOUBLE, NODE_1, 50, MPI_COMM_WORLD);
			else if(rank == NODE_1)
				MPI_Recv(oneDimArrayU, N * N, MPI_DOUBLE, ROOT, 50, MPI_COMM_WORLD, &status);
			count = 0;
		}
		
		// Start Timer
		gettimeofday(&start_tv, NULL);
		mflush();
		while(1)
		{
			if(rank == ROOT)
				flushPrintf("\nCycle: %d", count);
			if(count % cycle == 0)
			{
				MPI_Barrier(MPI_COMM_WORLD);
				mflush();
				backup();
				MPI_Barrier(MPI_COMM_WORLD);
				if(rank == ROOT)
					flushPrintf("\nAll processes have taken backup at %d.", count);
			}
			count++;
			for(i = 0; i < numThreads; i++)
				diff[i] = 0.0;
			MPI_Barrier(MPI_COMM_WORLD);
			if(rank == ROOT)
			{
				#pragma omp parallel for private(i,j) num_threads(numThreads)
				for (i = 1; i < N/2; i++)
				{
					threadID = omp_get_thread_num();
					for (j = 1; j < (N - 1); j++)
					{
						w[i][j] = (u[i-1][j] + u[i+1][j] + u[i][j-1] + u[i][j+1])/4.0;
						if (fabs (w[i][j] - u[i][j]) > diff[threadID])
							diff[threadID] = fabs(w[i][j] - u[i][j]);
					}
				}
			}
			else if(rank == NODE_1)
			{
				#pragma omp parallel for private(i,j) num_threads(numThreads)
				for (i = N/2; i < (N - 1); i++)
				{
					threadID = omp_get_thread_num();
					for (j = 1; j < (N - 1); j++)
					{
						w[i][j] = (u[i-1][j] + u[i+1][j] + u[i][j-1] + u[i][j+1])/4.0;
						if (fabs (w[i][j] - u[i][j]) > diff[threadID])
							diff[threadID] = fabs(w[i][j] - u[i][j]);
					}
				}
			}
			double maxDiff = 0.0;
			for(i = 0; i < numThreads; i++)
			{
				if(diff[i] > maxDiff)
					maxDiff = diff[i];
			}

			// Get the max difference and use this to see if it is lesser
			// than EPSILON. 
			MPI_Allreduce(&maxDiff, &maxDiff, 1, MPI_DOUBLE, MPI_MAX, MPI_COMM_WORLD );

			if (maxDiff <= EPSILON)
				break;

			// The boundaries have to be shared. Imagine a 12 * 12 Matrix. 
			// ROOT will work on the first 6 rows and NODE_1 will work on the
			// next 6. However there is a dependency. For the next iteration
			// NODE_1 will need the updated Row 6 and similarly ROOT will need
			// the updated Row 7. Hence we need to communicate these. We cannot
			// simply get each node to work on the boundary row since once again
			// we will need the row that precedes/succeeds that. Communication
			// is our salvation. 
			if(rank == ROOT)
			{
				#pragma omp parallel for private(i,j) num_threads(numThreads)
				for (i = 1; i < N/2; i++)
					for (j = 1; j < (N - 1); j++)
						u[i][j] = w[i][j];
				MPI_Send(u[M], N, MPI_DOUBLE, NODE_1, 50, MPI_COMM_WORLD);
				MPI_Recv(u[N/2], N, MPI_DOUBLE, NODE_1, 50, MPI_COMM_WORLD, &status);

			}
			else if(rank == NODE_1)
			{
				#pragma omp parallel for private(i,j) num_threads(numThreads)
				for (i = N/2; i < (N - 1); i++)
					for (j = 1; j < (N - 1); j++)
						u[i][j] = w[i][j];
				MPI_Send(u[N/2], N, MPI_DOUBLE, ROOT, 50, MPI_COMM_WORLD);
				MPI_Recv(u[M], N, MPI_DOUBLE, ROOT, 50, MPI_COMM_WORLD, &status);
			}
			
		}
		// Stop timer. 
		gettimeofday(&end_tv, NULL);

		if(rank == NODE_1)
		{
			for(i = N/2; i < N; i++)
			{
				MPI_Send(u[i], N, MPI_DOUBLE, ROOT, 50, MPI_COMM_WORLD);
			}
			nsrPrintf("\nNode 1 has transmitted its half of the array back to ROOT");
		}
		else if(rank == ROOT)
		{
			for(i = N/2; i < N; i++)
			{
				MPI_Recv(u[i], N, MPI_DOUBLE, NODE_1, 50, MPI_COMM_WORLD, &status);
			}
			nsrPrintf("\nROOT has received NODE 1's half");
		}

		if(rank == ROOT)
		{
			FILE *fp = fopen("output.dat", "w");
			flushPrintf("\nElapsed time: %f sec", (double)( (double)(end_tv.tv_sec - start_tv.tv_sec) + ( (double)(end_tv.tv_usec - start_tv.tv_usec)/1000000)) );
			flushPrintf("\nCount: %d", count);
			/* Print Solution */
			for (i=0; i<N; i++)
			{
				for (j=0; j<N; j++)
				{
					fprintf(fp, "%6.2f ", u[i][j]);
				}
				fprintf(fp, "\n");
			}
			fclose(fp);
		}
		

		// Clear all the allocated memory. No need to free u and w explicitly. 
		// TODO Need to change all memory free(s) to PerMa free calls. Have to
		// figure out how to do this. 
		free(oneDimArrayU);
		free(oneDimArrayW);
			
		flushPrintf("\n");
	}
#pragma endregion

#pragma region MORE THAN 2 NODES
	else
	{
		flushPrintf("\nThis code has been designed to run for only 2 nodes.");
	}
#pragma endregion
	MPI_Finalize();
	return 0;
}
