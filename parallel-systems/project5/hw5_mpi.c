/********************************
Group info:
(nmahade) Nandish Mahadevaiah
(hpjoshi) Harshvardhan Joshi
(nsrivat) Nikhil Srivatsan Srinivasan
Date: 11/27/2013
Project: Homework 5
********************************/

#include <math.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "mpi.h"
#include <sys/time.h>
#include "jemalloc/jemalloc.h"

#define MMAP_SIZE ((size_t)1 << 30)
#define READSIZE 200
#define ROOT 0
#define NODE_1 1
#define OK 100
#define SYSERR -100
#define nsrPrintf(...) if(nsrDebugPrintf) { fflush(stdout); printf(__VA_ARGS__); fflush(stdout);}
int nsrDebugPrintf;
#define flushPrintf(...) if(flushValuePrintf) { fflush(stdout); printf(__VA_ARGS__); fflush(stdout);}
int flushValuePrintf = 1;

int main (int argc, char *argv[])
{
	// PERMA Variables
	char MMAP_FILE[READSIZE];
	char BACK_FILE[READSIZE];
	int do_restore = 0;
	int cycle = 10000;
	char *mode;
	char *permaDirectory;

	int   numproc, rank, len;
	char  hostname[MPI_MAX_PROCESSOR_NAME];
	double diff;  /* change in value */
	int    i, j;
	double mean;
	double *oneDimArrayU, *oneDimArrayW, **u, **w, EPSILON;
	int N, count;
	MPI_Init(&argc, &argv);
	MPI_Request sendRequest[8];
	MPI_Comm_size(MPI_COMM_WORLD, &numproc);
	MPI_Comm_rank(MPI_COMM_WORLD, &rank);
	MPI_Get_processor_name(hostname, &len);
	MPI_Status status;

	if(rank == ROOT)
		flushPrintf("Number of Processes: %d", numproc);
	// Identify what arguments have been passed
	for(i = 1; i < argc; i++)
	{
		if(strcmp(argv[i], "-n") == 0)
		{
			if(i == argc)
			{
				flushPrintf("\nArguments are wrong. -n should be succeeded by an integer/long value.");
				return SYSERR;
			}
			N = atoi(argv[i + 1]);
			if(rank == ROOT)
				flushPrintf("\nValue of N: %d", N);
		}
		else if(strcmp(argv[i], "-e") == 0)
		{
			if(i == argc)
			{
				flushPrintf("\nArguments are wrong. -n should be succeeded by an float/double value.");
				return SYSERR;
			}
			EPSILON = atof(argv[i + 1]);
			if(rank == ROOT)
				flushPrintf("\nValue of EPSILON: %f", EPSILON);
		}
		else if(strcmp(argv[i], "-c") == 0)
		{
			cycle = atoi(argv[i + 1]);
			if(rank == ROOT)
				flushPrintf("\nFrequency: %d", cycle);
		}
		else if(strcmp(argv[i], "-r") == 0)
		{
			do_restore = 1;
		}
		else if(strcmp(argv[i], "-p") == 0)
		{
			permaDirectory = argv[i + 1];
			strcpy(MMAP_FILE, permaDirectory);
			strcpy(BACK_FILE, permaDirectory);
		}
	}
	struct timeval start_tv, end_tv;
	nsrPrintf("\nNumproc: %d", numproc);
	// Allocating two dimensional memory on a 1 dimensional memory
	oneDimArrayU = (double*) malloc(N * N * sizeof(double));
	perm(oneDimArrayU, N * N * sizeof(double));
	perm(&count, sizeof(int));
	nsrPrintf("\nArray U Allocated");
	oneDimArrayW = (double*) malloc(N * N * sizeof(double));
	nsrPrintf("\nArray W Allocated");
	u = (double**) malloc(N * sizeof(double*));
	w = (double**) malloc(N * sizeof(double*));
	for (i = 0; i < N; i++)
	{
		u[i] = oneDimArrayU + (i * N);
		w[i] = oneDimArrayW + (i * N);

	}
	nsrPrintf("\nMemory Allocated");
	switch(rank)
	{
	case 0:
		{
			strcat(MMAP_FILE, "temp549_0.mmap");
			strcat(BACK_FILE, "temp549_0.bak");
			break;
		}
	case 1:
		{
			strcat(MMAP_FILE, "temp549_1.mmap");
			strcat(BACK_FILE, "temp549_1.bak");
			break;
		}
	case 2:
		{
			strcat(MMAP_FILE, "temp549_2.mmap");
			strcat(BACK_FILE, "temp549_2.bak");
			break;
		}
	case 3:
		{
			strcat(MMAP_FILE, "temp549_3.mmap");
			strcat(BACK_FILE, "temp549_3.bak");
			break;
		}
	case 4:
		{
			strcat(MMAP_FILE, "temp549_4.mmap");
			strcat(BACK_FILE, "temp549_4.bak");
			break;
		}
	case 5:
		{
			strcat(MMAP_FILE, "temp549_5.mmap");
			strcat(BACK_FILE, "temp549_5.bak");
			break;
		}
	case 6:
		{
			strcat(MMAP_FILE, "temp549_6.mmap");
			strcat(BACK_FILE, "temp549_6.bak");
			break;
		}
	case 7:
		{
			strcat(MMAP_FILE, "temp549_7.mmap");
			strcat(BACK_FILE, "temp549_7.bak");
			break;
		}
	case 8:
		{
			strcat(MMAP_FILE, "temp549_8.mmap");
			strcat(BACK_FILE, "temp549_8.bak");
			break;
		}
	case 9:
		{
			strcat(MMAP_FILE, "temp549_9.mmap");
			strcat(BACK_FILE, "temp549_9.bak");
			break;
		}
	case 10:
		{
			strcat(MMAP_FILE, "temp549_10.mmap");
			strcat(BACK_FILE, "temp549_10.bak");
			break;
		}
	case 11:
		{
			strcat(MMAP_FILE, "temp549_11.mmap");
			strcat(BACK_FILE, "temp549_11.bak");
			break;
		}
	case 12:
		{
			strcat(MMAP_FILE, "temp549_12.mmap");
			strcat(BACK_FILE, "temp549_12.bak");
			break;
		}
	case 13:
		{
			strcat(MMAP_FILE, "temp549_13.mmap");
			strcat(BACK_FILE, "temp549_13.bak");
			break;
		}
	case 14:
		{
			strcat(MMAP_FILE, "temp549_14.mmap");
			strcat(BACK_FILE, "temp549_14.bak");
			break;
		}
	case 15:
		{
			strcat(MMAP_FILE, "temp549_15.mmap");
			strcat(BACK_FILE, "temp549_15.bak");
			break;
		}
	}
	if(do_restore)
		mode = "r+";
	else
		mode = "w+";
	mopen(MMAP_FILE, mode, MMAP_SIZE);
	bopen(BACK_FILE, mode);

#pragma region ONLY_ROOT
	if(numproc == 1)
	{
		if(do_restore)
		{
			mflush();
			restore();
		}
		else
		{
			mean = 0.0;
			for(i = 0; i < N; i++)
			{
				u[i][0] = u[i][N-1] = u[0][i] = 100.0;
				u[N-1][i] = 0.0;
				mean += u[i][0] + u[i][N-1] + u[0][i] + u[N-1][i];
			}
			mean /= (4.0 *N);

			nsrPrintf("\nMean Initialized");

			/* Initialize interior values */
			for (i =1; i<N-1; i++)
				for (j=1; j<N-1; j++)
					u[i][j] = mean;
			nsrPrintf("\nArray U Initialized");
			count = 0;
		}

		gettimeofday(&start_tv, NULL);
		/* Compute steady-state solution - Parallelize this loop with OpenAcc */
		mflush();
		while(1)
		{
			flushPrintf("\nCycle: %d", count);
			if(count % cycle == 0)
			{
				mflush();
				backup();
				flushPrintf("\nTaken Backup at Cycle: %d.", count);
			}
			count++;
			diff = 0.0;
			for (i=1; i<N-1; i++)
			{
				for (j=1; j<N-1; j++)
				{
					w[i][j] = (u[i-1][j] + u[i+1][j] + u[i][j-1] + u[i][j+1])/4.0;
					if (fabs (w[i][j] - u[i][j]) > diff)
						diff = fabs(w[i][j] - u[i][j]);
				}
			}

			if (diff <= EPSILON)
				break;

			for (i=1; i<N-1; i++)
				for (j=1; j<N-1; j++)
					u[i][j] = w[i][j];
		}
		/* End of parallelization */

		gettimeofday(&end_tv, NULL);
		flushPrintf("\nElapsed time: %f sec", (double)( (double)(end_tv.tv_sec - start_tv.tv_sec) + ( (double)(end_tv.tv_usec - start_tv.tv_usec)/1000000)) );
		flushPrintf("\nCount: %d", count);
		/* Print Solution */
		FILE *fp = fopen("output.dat", "w");
		for (i=0; i<N; i++)
		{
			for (j=0; j<N; j++)
			{
				fprintf(fp, "%6.2f ", u[i][j]);
			}
			fprintf(fp, "\n");
		}

		// Clear all the allocated memory. No need to u and w explicitly. 
		// TODO Need to change all memory free(s) to PerMa free calls. 
		// Have to figure out how to do this. 
		free(oneDimArrayU);
		free(oneDimArrayW);
		fclose(fp);

		flushPrintf("\n");
	}
#pragma endregion

#pragma region 2_NODES
	else if(numproc > 1)
	{
		struct timeval start_tv, end_tv;
		int M = N/2 - 1;
		nsrPrintf("\nNumproc: %d", numproc);
		if(do_restore)
		{
			mflush();
			restore();
		}
		else
		{
			mean = 0.0;
			for(i = 0; i < N; i++)
			{
				u[i][0] = u[i][N-1] = u[0][i] = 100.0;
				u[N-1][i] = 0.0;
				mean += u[i][0] + u[i][N-1] + u[0][i] + u[N-1][i];
			}
			mean /= (4.0 *N);
			//nsrPrintf("\nMean Initialized");

			/* Initialize interior values */
			for (i =1; i<N-1; i++)
				for (j=1; j<N-1; j++)
					u[i][j] = mean;
			//nsrPrintf("\nArray U Initialized");
			count = 0;
		}
		

		int startIter, endIter;
		if(rank == ROOT)
		{
			startIter = 0;
			endIter = startIter + (N/numproc);
		}
		else if(rank == numproc - 1)
		{
			startIter = (rank * (N/numproc)) + 1;
			endIter = N - 1;
		}
		else
		{
			startIter = rank * (N/numproc) + 1;
			endIter = startIter + (N/numproc) - 1;
		}
		for(i = 0; i < numproc; i++)
		{
			if(rank == i)
				nsrPrintf("\nRank %d: %d -> %d", rank, startIter, endIter);
		}
		MPI_Barrier(MPI_COMM_WORLD);
		// Start Timer
		gettimeofday(&start_tv, NULL);
		mflush();
		while(1)
		{
			if(rank == ROOT)
				flushPrintf("\nCycle: %d", count);
			if(count % cycle == 0)
			{
				MPI_Barrier(MPI_COMM_WORLD);
				backup();
				MPI_Barrier(MPI_COMM_WORLD);
				if(rank == ROOT)
					flushPrintf("\nAll processes have taken a backup at %d.", count);
			}
			count++;
			diff = 0.0;
			if(rank == ROOT)
			{
				for (i = 1; i <= endIter; i++)
				{
					for (j = 1; j < (N - 1); j++)
					{
						w[i][j] = (u[i-1][j] + u[i+1][j] + u[i][j-1] + u[i][j+1])/4.0;
						if (fabs (w[i][j] - u[i][j]) > diff)
							diff = fabs(w[i][j] - u[i][j]);
					}
				}
			}
			else if(rank == numproc - 1)
			{
				for (i = startIter; i < (N - 1); i++)
				{
					for (j = 1; j < (N - 1); j++)
					{
						w[i][j] = (u[i-1][j] + u[i+1][j] + u[i][j-1] + u[i][j+1])/4.0;
						if (fabs (w[i][j] - u[i][j]) > diff)
							diff = fabs(w[i][j] - u[i][j]);
					}
				}
			}
			else
			{
				for (i = startIter; i <= endIter; i++)
				{
					for (j = 1; j < (N - 1); j++)
					{
						w[i][j] = (u[i-1][j] + u[i+1][j] + u[i][j-1] + u[i][j+1])/4.0;
						if (fabs (w[i][j] - u[i][j]) > diff)
							diff = fabs(w[i][j] - u[i][j]);
					}
				}
			}

			// Get the max difference and use this to see if it is lesser
			// than EPSILON. 
			MPI_Allreduce(&diff, &diff, 1, MPI_DOUBLE, MPI_MAX, MPI_COMM_WORLD );

			if (diff <= EPSILON)
				break;

			// The boundaries have to be shared. Imagine a 12 * 12 Matrix. 
			// ROOT will work on the first 6 rows and NODE_1 will work on the
			// next 6. However there is a dependency. For the next iteration
			// NODE_1 will need the updated Row 6 and similarly ROOT will need
			// the updated Row 7. Hence we need to communicate these. 
			int nextNode = rank + 1;
			int prevNode = rank - 1;
			if(rank == ROOT)
			{
				for (i = 1; i <= endIter; i++)
					for (j = 1; j < (N - 1); j++)
						u[i][j] = w[i][j];
				MPI_Send(u[endIter], N, MPI_DOUBLE, nextNode, 50, MPI_COMM_WORLD);
				MPI_Recv(u[endIter + 1], N, MPI_DOUBLE, nextNode, 50, MPI_COMM_WORLD, &status);

			}
			else if(rank == numproc - 1)
			{
				for (i = startIter; i < (N - 1); i++)
					for (j = 1; j < (N - 1); j++)
						u[i][j] = w[i][j];
				MPI_Send(u[startIter], N, MPI_DOUBLE, prevNode, 50, MPI_COMM_WORLD);
				MPI_Recv(u[startIter - 1], N, MPI_DOUBLE, prevNode, 50, MPI_COMM_WORLD, &status);
			}
			else
			{
				for (i = startIter; i <= endIter; i++)
					for (j = 1; j < (N - 1); j++)
						u[i][j] = w[i][j];
				MPI_Send(u[startIter], N, MPI_DOUBLE, prevNode, 50, MPI_COMM_WORLD);
				MPI_Send(u[endIter], N, MPI_DOUBLE, nextNode, 50, MPI_COMM_WORLD);
				MPI_Recv(u[startIter - 1], N, MPI_DOUBLE, prevNode, 50, MPI_COMM_WORLD, &status);
				MPI_Recv(u[endIter + 1], N, MPI_DOUBLE, nextNode, 50, MPI_COMM_WORLD, &status);
			}

		}
		// Stop timer. 
		gettimeofday(&end_tv, NULL);

		for(i = 1; i < numproc; i++)
		{
			if(rank == i)
			{
				MPI_Send(&startIter, 1, MPI_DOUBLE, ROOT, 49, MPI_COMM_WORLD);
				MPI_Send(&endIter, 1, MPI_DOUBLE, ROOT, 50, MPI_COMM_WORLD);
				for(j = startIter; j <= endIter; j++)
					MPI_Send(u[j], N, MPI_DOUBLE, ROOT, 50, MPI_COMM_WORLD);
			}
			else if(rank == ROOT)
			{
				int otherStartIter, otherEndIter;
				MPI_Recv(&otherStartIter, 1, MPI_DOUBLE, i, 49, MPI_COMM_WORLD, &status);
				MPI_Recv(&otherEndIter, 1, MPI_DOUBLE, i, 50, MPI_COMM_WORLD, &status);
				nsrPrintf("\nReceived %d and %d from rank %d", otherStartIter, otherEndIter, i);
				for(j = otherStartIter; j <= otherEndIter; j++)
					MPI_Recv(u[j], N, MPI_DOUBLE, i, 50, MPI_COMM_WORLD, &status);
			}
			MPI_Barrier(MPI_COMM_WORLD);
		}

		if(rank == ROOT)
		{
			FILE *fp = fopen("output.dat", "w");
			flushPrintf("\nElapsed time: %f sec", (double)( (double)(end_tv.tv_sec - start_tv.tv_sec) + ( (double)(end_tv.tv_usec - start_tv.tv_usec)/1000000)) );
			flushPrintf("\nCount: %d", count);
			/* Print Solution */
			for (i=0; i<N; i++)
			{
				for (j=0; j<N; j++)
				{
					fprintf(fp, "%6.2f ", u[i][j]);
				}
				fprintf(fp, "\n");
			}
			fclose(fp);
		}


		// Clear all the allocated memory. No need to free u and w explicitly. 
		// TODO Need to change all memory free(s) to PerMa free calls. Have to
		// figure out how to do this. 
		free(oneDimArrayU);
		free(oneDimArrayW);
		if(rank == ROOT)
			flushPrintf("\n");
	}
#pragma endregion

	MPI_Finalize();
	return 0;
}
