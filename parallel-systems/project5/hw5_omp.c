/********************************
Group info:
(hpjoshi) Harshvardhan Joshi
(nmahade) Nandish Mahadevaiah
(nsrivat) Nikhil Srivatsan Srinivasan
Date: 11/27/2013
Project: Homework 5
********************************/

#include <math.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <omp.h>
#include <sys/time.h>
#include "jemalloc/jemalloc.h"

#define READSIZE 200
#define MMAP_SIZE ((size_t)1 << 30)

#define OK 100
#define SYSERR -100
#define nsrPrintf(...) if(nsrDebugPrintf) { fflush(stdout); printf(__VA_ARGS__); fflush(stdout);}
int nsrDebugPrintf;
#define flushPrintf(...) if(flushValuePrintf) { fflush(stdout); printf(__VA_ARGS__); fflush(stdout);}
int flushValuePrintf = 1;

int main (int argc, char *argv[])
{
	int do_restore = 0;
	char MMAP_FILE[READSIZE];
	char BACK_FILE[READSIZE];
	int cycle = 10000;
	char *permaDirectory;
	int    i, j;
	double mean;
	char *mode;
	double *oneDimArrayU, *oneDimArrayW, *diff, **u, **w, EPSILON;
	int N, count, threadID, numThreads;
	numThreads = 16;
	
	
	// Identify what arguments have been passed
	for(i = 1; i < argc; i++)
	{
		if(strcmp(argv[i], "-n") == 0)
		{
			if(i == argc)
			{
				flushPrintf("\nArguments are wrong. -n should be succeeded by an integer/long value.");
				return SYSERR;
			}
			N = atoi(argv[i + 1]);
			flushPrintf("\nN: %d", N);
		}
		else if(strcmp(argv[i], "-e") == 0)
		{
			if(i == argc)
			{
				flushPrintf("\nArguments are wrong. -n should be succeeded by an float/double value.");
				return SYSERR;
			}
			EPSILON = atof(argv[i + 1]);
			flushPrintf("\nEPSILON: %f", EPSILON);
		}
		else if(strcmp(argv[i], "-c") == 0)
		{
			cycle = atoi(argv[i + 1]);
			flushPrintf("\nFrequency: %d", cycle);
		}
		else if(strcmp(argv[i], "-r") == 0)
		{
			do_restore = 1;
		}
		else if(strcmp(argv[i], "-p") == 0)
		{
			permaDirectory = argv[i + 1];
			strcpy(MMAP_FILE, permaDirectory);
			strcpy(BACK_FILE, permaDirectory);
		}
		else if(strcmp(argv[i], "-nthreads") == 0)
		{
			numThreads = atoi(argv[i + 1]);
		}
	}
	omp_set_dynamic(0);     
	omp_set_num_threads(numThreads); 
	strcat(MMAP_FILE, "temp549_omp.mmap");
	strcat(BACK_FILE, "temp549_omp.bak");
	struct timeval start_tv, end_tv;
	diff = (double*) malloc(numThreads * sizeof(double));
	// Allocating two dimensional memory on a 1 dimensional memory
	oneDimArrayU = (double*) malloc(N * N * sizeof(double));
	perm(oneDimArrayU, N * N * sizeof(double));
	perm(&count, sizeof(int));
	nsrPrintf("\nArray U Allocated");
	oneDimArrayW = (double*) malloc(N * N * sizeof(double));
	nsrPrintf("\nArray W Allocated");
	u = (double**) malloc(N * sizeof(double*));
	w = (double**) malloc(N * sizeof(double*));
	for (i = 0; i < N; i++)
	{
		u[i] = oneDimArrayU + (i * N);
		w[i] = oneDimArrayW + (i * N);

	}
	nsrPrintf("\nMemory Allocated");
	
	if(do_restore)
		mode = "r+";
	else
		mode = "w+";
	mopen(MMAP_FILE, mode, MMAP_SIZE);
	bopen(BACK_FILE, mode);
	if(do_restore)
	{
		mflush();
		restore();
	}
	else
	{
		mean = 0.0;
		for(i = 0; i < N; i++)
		{
			u[i][0] = u[i][N-1] = u[0][i] = 100.0;
			u[N-1][i] = 0.0;
			mean += u[i][0] + u[i][N-1] + u[0][i] + u[N-1][i];
		}
		mean /= (4.0 *N);

		nsrPrintf("\nMean Initialized");

		/* Initialize interior values */
		for (i = 1; i < N-1; i++)
			for (j = 1; j < N-1; j++)
				u[i][j] = mean;
		nsrPrintf("\nArray U Initialized");

		count = 0;
	}

	gettimeofday(&start_tv, NULL);
	/* Compute steady-state solution - Parallelize this loop with OpenAcc */
	mflush();
	while(1)
	{
		if(count % cycle == 0)
		{
			mflush();
			backup();
			flushPrintf("\nTaken Backup at %d.", count);
		}
		flushPrintf("\nCycle: %d", count);
		count++;
		// Initialize Difference Array
		for(i = 0; i < numThreads; i++)
			diff[i] = 0.0;
		#pragma omp parallel for private(i,j) num_threads(numThreads)
		for (i = 1; i < N-1; i++)
		{
			threadID = omp_get_thread_num();
			for (j = 1; j < N-1; j++)
			{
				w[i][j] = (u[i-1][j] + u[i+1][j] + u[i][j-1] + u[i][j+1])/4.0;
				if (fabs (w[i][j] - u[i][j]) > diff[threadID])
					diff[threadID] = fabs(w[i][j] - u[i][j]);
			}
		}
		double maxDiff = 0.0;
		// Calculate the maximum difference amongst all the differences
		// collected from all the threads. 
		for(i = 0; i < numThreads; i++)
		{
			if(diff[i] > maxDiff)
				maxDiff = diff[i];
		}
		if (maxDiff <= EPSILON)
			break;

		#pragma omp parallel for schedule(static) private(i,j) num_threads(16)
		for (i = 1; i < N-1; i++)
			for (j = 1; j < N-1; j++)
				u[i][j] = w[i][j];
	}
	/* End of parallelization */
	gettimeofday(&end_tv, NULL);

	flushPrintf("\nElapsed time: %f sec", (double)( (double)(end_tv.tv_sec - start_tv.tv_sec) + ( (double)(end_tv.tv_usec - start_tv.tv_usec)/1000000)) );
	flushPrintf("\nCount: %d", count);
	/* Print Solution */
	FILE *fp = fopen("output.dat", "w");
	for (i = 0; i < N; i++)
	{
		for (j = 0; j < N; j++)
		{
			fprintf(fp, "%6.2f ", u[i][j]);
		}
		fprintf(fp, "\n");
	}

	// Clear all the allocated memory. No need to u and w explicitly. 
	// TODO Need to change all memory free(s) to PerMa free calls. 
	// Have to figure out how to do this. 
	free(oneDimArrayU);
	free(oneDimArrayW);
	fclose(fp);

	flushPrintf("\n");
	return 0;
}
