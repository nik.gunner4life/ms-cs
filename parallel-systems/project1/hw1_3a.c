/* CSC548 HW #1-3 */

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "mpi.h"

#define DEBUG 0

//prints out the function and its derivative to a file
void print_x(int iter, int n, double* x)
{
	int   i;
	FILE *fp = fopen("x.dat", "w");

	fprintf(fp, "itereation: %d, ", iter);
	fprintf(fp, "x=[");
	for(i= 0; i < n; i++){
		fprintf(fp, " %f", x[i]);
	}
	fprintf(fp, " ]");
	fprintf(fp, "\n");

	fclose(fp);
}

int check_matrix(double** mat,int n)
{
	int i,j;
	double sum;

	for(i=0;i<n;i++){
		sum=0;
		for(j=0;j<n;j++){
			if(i!=j)
				sum += fabs(mat[i][j]);
		}

		if(fabs(mat[i][i]) < sum)
			return 0;
	}
	return 1;
}

int main(int argc, char *argv[])
{
	int   numproc, rank, len;
    /* current process hostname */
	char  hostname[MPI_MAX_PROCESSOR_NAME];
	MPI_Init(&argc, &argv);
	MPI_Request sendRequest[8];
	MPI_Comm_size(MPI_COMM_WORLD, &numproc);
	MPI_Comm_rank(MPI_COMM_WORLD, &rank);
	MPI_Get_processor_name(hostname, &len);
	MPI_Status status;
	FILE *fp;
    int i,j,n;
	double **mat, *x;
    double sum,temp,diff,bb;
	double e;
	int iter=0;

	if (argc != 3){
		printf("Usage: hw1 <filename> <error>\n");
		return 1;
	}
	printf("\nInput File: %s\n", argv[1]);

	/* Opening the input file */
	fp = fopen(argv[1], "r");
	if (fp == NULL) {
		printf("Error in opening a file: %s", argv[1]);
		return 0;
	}

	e = (double)atof(argv[2]);
	printf("error= %f\n", e);

	/* Reading the maxtrix dimension */
	fscanf(fp, "%d",&n);
	printf("n= %d\n", n);

	mat = (double**) malloc(n * sizeof(double*)); 
	for (i= 0; i<n; i++)
		mat[i]= (double*) malloc((n+1) * sizeof(double));
	x = (double*) malloc(n * sizeof(double));

	j=1;
	/* Reading the input matrix */
    for(i=0;i<n;i++){
        for(j=0;j<n+1;j++){
			fscanf(fp, "%lf", &mat[i][j]);
        }
    }

#if DEBUG
    for(i=0;i<n;i++){
        for(j=0;j<n+1;j++){
			printf("%lf ", mat[i][j]);
        }
		printf("\n");
    }
#endif

#if DEBUG
	if(!check_matrix(mat,n)){
		printf("The matrix is not valid.\n");
		return 0;
	}
#endif

	/* Initializing x */
    for(i=0;i<n;i++){
        x[i]=0;
    }
	double t1 = MPI_Wtime();
	iter=1;
	/* Solving the given matrix iteratively */
    do {
        bb=0;
        for(i=0;i<n;i++){
            sum=0;
            for(j=0;j<n;j++){
                if(j!=i){
                    sum=sum+mat[i][j]*x[j];
                }
            }
			temp=(mat[i][n]-sum) / mat[i][i];
            diff=fabs(x[i]-temp);
            if(diff>bb){
				bb=diff;
            }
            x[i]=temp;
#if DEBUG
            printf("\nx[%d] =%f", i, x[i]);
#endif
        }
#if DEBUG
        printf("\n");
#endif
		iter++;
    }
    while(bb>=e);
	t1 = MPI_Wtime() - t1;
#if DEBUG
	/* prints the solution */
    printf("\nAnswer >>", i, x[i]);
    for(i=0; i<n; i++){
        printf("\nx[%d]=%f", i, x[i]);
    }
    printf("\n");
#endif

	print_x(iter, n, x);
    printf("\nTime: %fdone\n", t1);

	for (i=0; i<n; i++){
		free(mat[i]);
	}
	free(mat);
	free(x);

	fclose(fp);
	MPI_Finalize();
	return 1;
}