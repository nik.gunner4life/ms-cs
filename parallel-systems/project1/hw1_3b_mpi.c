/***************************************/
// Group info:
// nsrivat Nikhil Srivatsan Srinivasan
// hpjoshi Harshvardhan Joshi
// nmahade Nandish Mahadevaiah
/***************************************/

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "mpi.h"

#define DEBUG 0
#define ROOT 0

//prints out the function and its derivative to a file
void print_x(int iter, int n, double* x)
{
	int   i;
	FILE *fp = fopen("x.dat", "w");

	fprintf(fp, "itereation: %d, ", iter);
	fprintf(fp, "x=[");
	for(i= 0; i < n; i++){
		fprintf(fp, " %f", x[i]);
	}
	fprintf(fp, " ]");
	fprintf(fp, "\n");

	fclose(fp);
}

int check_matrix(double** mat,int n)
{
	int i,j;
	double sum;
	for(i=0;i<n;i++){
		sum=0;
		for(j=0;j<n;j++){
			if(i!=j)
				sum += fabs(mat[i][j]);
		}

		if(fabs(mat[i][i]) < sum)
			return 0;
	}
	return 1;
}

int main(int argc, char *argv[])
{
	int   numproc, rank, len;
    /* current process hostname */
	char  hostname[MPI_MAX_PROCESSOR_NAME];
	MPI_Init(&argc, &argv);
	MPI_Request sendRequest[8];
	MPI_Comm_size(MPI_COMM_WORLD, &numproc);
	MPI_Comm_rank(MPI_COMM_WORLD, &rank);
	MPI_Get_processor_name(hostname, &len);
	MPI_Status status;
	FILE *fp;
    int i,j,n, looper;
	double **mat, *x;
    double sum,temp,diff,bb, reducedSum = 0.0, valueToUpdate = 0.0;
	double e;
	int iter=0;
	double tAlgoStart, tAlgoEnd, tAlgoDiff, tOverallStart, tOverallEnd, tOverallDiff;

	if (argc != 3)
	{
		if(rank == ROOT)
			printf("Usage: hw1 <filename> <error>\n");
		return 1;
	}
	if(rank == ROOT)
		printf("\nInput File: %s\n", argv[1]);

	// Opening the input file. This is done to ensure
	// all the arguments are correct. If they were wrong
	// Only the root process would come out, leaving the 
	// other processes dangling. 
	fp = fopen(argv[1], "r");
	
	if (fp == NULL) 
	{
		printf("Error in opening a file: %s", argv[1]);
		return 0;
	}
	
	// Can be broadcasted, but since this is SPMD, this does not have to be changed.
	e = (double)atof(argv[2]);
	if(rank == ROOT)
		printf("error= %f\n", e);

	/* Reading the maxtrix dimension */
	if(rank == ROOT)
	{
		fscanf(fp, "%d",&n);
		printf("n= %d\n", n);
	}

	//Broadcast the matrix Dimensions to all processes. 
	MPI_Bcast(&n, 1, MPI_DOUBLE, ROOT, MPI_COMM_WORLD); 

	//All processes create the matrix. 
	mat = (double**) malloc(n * sizeof(double*)); 
	for (i= 0; i<n; i++)
		mat[i]= (double*) malloc((n+1) * sizeof(double));
	x = (double*) malloc(n * sizeof(double));
	MPI_Barrier(MPI_COMM_WORLD);
	
	/* Initializing x. Done by all processes. */
    for(i=0;i<n;i++)
	{
        x[i]=0;
    }
	iter=0;
	j=1;
	// Reading the input matrix. Done by the ROOT process
	// Every row is then broadcasted to every process.
    for(i=0;i<n;i++)
	{
		if(rank == ROOT)
		{
			for(j=0;j<n+1;j++)
			{
				fscanf(fp, "%lf", &mat[i][j]);
			}
		}
    }

	// Start the timing from here. This is where the actual
	// communication begins. 
	if(rank == ROOT)
	{
		tOverallStart = MPI_Wtime();
	}

	//Broadcast the entire matrix to all the processes. 
	if(numproc != 1)
	{
		for(i = 0; i < n; i++)
		{
			MPI_Bcast(mat[i], n+1, MPI_DOUBLE, ROOT, MPI_COMM_WORLD); 
		}
	}
	
	/* Solving the given matrix iteratively */
	MPI_Barrier(MPI_COMM_WORLD);
	if(rank == ROOT)
	{
		tAlgoStart = MPI_Wtime();
	}
    do {
        bb=0;
        for(i=0;i<n;i++)
		{
            sum=0;
			int startIterValue, endIterValue;

			//This ensures that every process works only on certain
			// column (equally assigned). A point to note here is
			// that numproc must be chosen in such a way so that
			// it is the product of factors of 625 and 16. 
			startIterValue = (n/numproc) * rank;
			endIterValue = startIterValue + (n/numproc); 
            for(j = startIterValue; j < endIterValue; j++)
			{
                if(j!=i)
				{
                    sum=sum+mat[i][j]*x[j];
                }
            }
			MPI_Barrier(MPI_COMM_WORLD); 
			// Receive the local sum from every process in reducedSum.
			// Sum them all up and store the overall result in sum. 
			if(rank != ROOT)
			{
				MPI_Send(&sum, 1, MPI_DOUBLE, ROOT, 50, MPI_COMM_WORLD);
			}
			else
			{
				if(numproc > 1)
				{
					for(looper = 1; looper < numproc; looper++)
					{
						MPI_Recv(&reducedSum, 1, MPI_DOUBLE, looper, 50, MPI_COMM_WORLD, &status);
						sum = sum + reducedSum; 
					}
				}
			}
			if(rank == ROOT)
			{
				temp=(mat[i][n]-sum) / mat[i][i];
				diff=fabs(x[i]-temp);
				if(diff>bb)
				{
					bb=diff;
				}
				x[i]=temp;
			}
			MPI_Barrier(MPI_COMM_WORLD);
			// Here I am simply communicating the updated value to the
			// required process. Remember all processes will not be using
			// all parts of the x vector. They all use only the required
			// columns as is evident from the code and the algorithm.
			// This saves us time as we now do not have to broadcast the 
			// entire x vector leading to better scaling and overall speedup. 
			if(numproc == 4)
			{
				if(i < 2500)
				{
					//MPI_Send(&x[i], 1, MPI_DOUBLE, 0, 50, MPI_COMM_WORLD);
				}
				else if(i >= 2500 && i < 5000)
				{
					if(rank == ROOT)
						MPI_Send(&x[i], 1, MPI_DOUBLE, 1, 50, MPI_COMM_WORLD);
					if(rank == 1)
					{
						MPI_Recv(&valueToUpdate, 1, MPI_DOUBLE, ROOT, 50, MPI_COMM_WORLD, &status);
						x[i] = valueToUpdate;
					}
				}
				else if(i >= 5000 && i < 7500)
				{
					if(rank == ROOT)
						MPI_Send(&x[i], 1, MPI_DOUBLE, 2, 50, MPI_COMM_WORLD);
					if(rank == 2)
					{
						MPI_Recv(&valueToUpdate, 1, MPI_DOUBLE, ROOT, 50, MPI_COMM_WORLD, &status);
						x[i] = valueToUpdate;
					}
				}
				else
				{
					if(rank == ROOT)
						MPI_Send(&x[i], 1, MPI_DOUBLE, 3, 50, MPI_COMM_WORLD);
					if(rank == 3)
					{
						MPI_Recv(&valueToUpdate, 1, MPI_DOUBLE, ROOT, 50, MPI_COMM_WORLD, &status);
						x[i] = valueToUpdate;
					}
				}
			}
			else if (numproc == 2)
			{
				if(i < n/2)
				{
					//MPI_Send(&x[i], 1, MPI_DOUBLE, 0, 50, MPI_COMM_WORLD);
				}
				else if(i >= n/2)
				{
					if(rank == ROOT)
						MPI_Send(&x[i], 1, MPI_DOUBLE, 1, 50, MPI_COMM_WORLD);
					if(rank == 1)
					{
						MPI_Recv(&valueToUpdate, 1, MPI_DOUBLE, ROOT, 50, MPI_COMM_WORLD, &status);
						x[i] = valueToUpdate;
					}
				}
			}
			// This is to ensure that the same code can be used
			// for both serial and parallelized code. All MPI calls
			// have been placed in a similar if statement. This
			// potentially ensures that we do not need to submit two
			// codes for the same functionality. 
			if(numproc != 1)
				MPI_Bcast(&bb, 1, MPI_DOUBLE, ROOT, MPI_COMM_WORLD); 
        }
		iter++;
    }
    while(bb>=e);

	// Measure the end time for both the running time
	// of the algorithm as well as the overall running
	// time which includes the broadcasting of the matrix
	// as well. 
	if(rank == ROOT)
	{
		tAlgoEnd = MPI_Wtime();
		tAlgoDiff = tAlgoEnd - tAlgoStart; 
		tOverallEnd = MPI_Wtime();
		tOverallDiff = tOverallEnd - tOverallStart; 
		printf("\n\n----- Overall Time Diff: %f, Algorithm Time Diff: %f, Iterations: %d\n\n", tOverallDiff, tAlgoDiff, iter);
	}
	
	if(rank == ROOT)
	{
		printf("\nAnswer >>", i, x[i]);
		for(i=0; i<n; i++)
		{
			printf("\nx[%d] = %f", i, x[i]);
		}
	}
	print_x(iter, n, x);
	for (i=0; i<n; i++)
	{
		free(mat[i]);
	}
	free(mat);
	free(x);

	fclose(fp);
	MPI_Finalize(); 
	return 1;
}