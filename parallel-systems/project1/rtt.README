
Single Author: Nikhil Srivatsan Srinivasan
Username: temp549
Unity ID: nsrivat
Homework: Homework 1: Problem 2a and 2b. 

IMPORTANT NOTE: How to compile the program?
Along with the submission, I have also attached a simple Makefile, which will clean and compile the code (make clean && make). The code takes an argument. The argument states
which experiment will be conducted RTTA or RTTB. It is 1 for RTTA and 2 for RTTB. I have also attached the qsub file for your benefit. 

1) When calculating average rtt, skip the first message exchange -- why?
We skip the first message because, the first message will usually involve a primary handshake which will then be followed by packet transfer. Hence the first message is usually
ignored as this does not really give us an indication of the latency. 

2) Exp a: Ensure that only two nodes are exchanging messages at any one time -- why? / Exp b: Ensure that all 4 nodes are exchanging messages at any one time -- why?
For Exp a: we notice that the ROOT sends packets and receives packets. However if we have more than 2 processes i.e ROOT communicating with two other processes, we
might get into a situation where we do not know who the packet is received from. Further, we may receive the packet from one (if using MPI_ANY_SOURCE) and we might
use that to calculate the RTT where as in fact it might be from another process. Another possible issue that we might have it, if packet from another process has 
already arrived, but in the code we use blocking calls, it might be queued till that call is processed and satisfied and this once again does not gives us an indication
of latency. 

For Exp b: We can ensure 2 messages are flowing at the same time. This is because communication between a pair of processes does not interfere with communication between
the other pair. Further this can be done simultaneously thereby leading to overall better throughput. Hence all 4 nodes can be exchanging messages at any one time. 

3) The graphs titled rtta.jpg and rttb.jpg have been attached. 

4) The topology file has been explained above and need not be repeated here. 
Topology:
---------
RTTA
----

For RTTA we can see that the 4 nodes are 20, 50, 47 and 66 with 20 as the ROOT. All packets are transmitted and received from the ROOT to the various processors. 
Let us look at the topology file to get a better understanding of the underlying cluster, which will help us understand the latency even better. 

We can see that node 20 is connected to the switch S-0002c90200423e98 which is lid 7. Now let us take the process with rank 1. The process in this case lies on
the node 50. Node 50 is connected to the switch S-0002c90200423e78 which is lid 5. In this case, the message would probably have 4 hop (From node 20 -> lid 7 ->
lid 15 -> lid 5 -> node 50). 

Similarly this is the case for node 47 (connected to switch S-0002c90200423ed8 and lid 14). Hence in this case the packet might probably have hops (From node 20 -> lid 7 -> lid 15
-> lid 14 -> node 47). From the graph rtta.jpg, we can see that the times are more or less the same which further justifies that the proposed topology might be
correct. 

For node 66 (connected to switch S-0002c90200423e70/lid 3), we might have the following path/hops (node 20 -> lid 7 -> lid 6 -> lid 3 -> node 66). Here once again we see that
there might be 4 hops and hence the overall time must be very close to the previous ones which is captured in the graph. 

RTTB
----
In this experiment we communicate between node 4 and 82, node 7 and 66. Node 4 is connected to switch S-0002c90200423b78/lid 4. Similarly node 82 is connected to switch S-0002c90200423e78/
lid 5. Now a possible way of communication would be node 4 -> lid 4 -> lid 6 -> lid 5 -> node 82 and overall of 4 hops.  

Similarly we have node 7 and node 66. node 7 is connected to the switch S-0002c90200423b78/lid 4 and node 66 is connected to S-0002c90200423e70/lid 3. Hence a possible way of communication 
would be from node 7 -> lid 4 -> lid 6 -> lid 3 -> node 66. Once again we have 4 hops and hence the time should be more or less the same as the previous one. 

Another thing we can note from here is that all communications happen over 4 hops. This means that all nodes were not connected to same switch. Hence by inference we can probably say that time
taken for communication in RTTA should be more or less the same as in RTTB. When we see the times and we observe the graph, we notice that this more or less holds. 