/***************************************/
// Group info:
// nsrivat Nikhil Srivatsan Srinivasan
// hpjoshi Harshvardhan Joshi
// nmahade Nandish Mahadevaiah
/***************************************/

#include <stdio.h>
#include <stdlib.h>
#include "mpi.h"
#include <math.h>

#define DEBUG 1
#define	DOPRINT 0
#define	ROOT 0

//prints out the function and its derivative to a file
void print_x(int iter, int n, double* x)
{
	int   i;
	FILE *fp = fopen("x.dat", "w");

	fprintf(fp, "itereation: %d, ", iter);
	fprintf(fp, "x=[");
	for(i= 0; i < n; i++){
		fprintf(fp, " %f", x[i]);
	}
	fprintf(fp, " ]");
	fprintf(fp, "\n");

	fclose(fp);
}

int check_matrix(double** mat,int n)
{
	int i,j;
	double sum;

	for(i=0;i<n;i++)
	{
		sum=0;
		for(j=0;j<n;j++)
		{
			if(i!=j)
				sum += fabs(mat[i][j]);
		}

		if(fabs(mat[i][i]) < sum)
			return 0;
	}
	return 1;
}

int main(int argc, char *argv[])
{
	int   numproc, rank, len;
    /* current process hostname */
	char  hostname[MPI_MAX_PROCESSOR_NAME];
	MPI_Init(&argc, &argv);
	MPI_Request sendRequest[8];
	MPI_Comm_size(MPI_COMM_WORLD, &numproc);
	MPI_Comm_rank(MPI_COMM_WORLD, &rank);
	MPI_Get_processor_name(hostname, &len);
	MPI_Status status;
	FILE *fp;
    int i,j,k,n;
	double **mat, *x, *y;
    double sum,temp,diff,bb, reducedSum;
	double e;
	int iter=0;
	double valueToUpdate = 0.0; 
	double tAlgoStart, tAlgoEnd, tAlgoDiff, tOverallStart, tOverallEnd, tOverallDiff;
	
	if (argc != 3)
	{
		if(rank == ROOT)
			printf("Usage: hw1 <filename> <error>\n");
		return 1;
	}
	if(rank == ROOT)
		printf("\nInput File: %s\n", argv[1]);

	// Opening the input file. This is done to ensure
	// all the arguments are correct. If they were wrong
	// Only the root process would come out, leaving the 
	// other processes dangling. 
	fp = fopen(argv[1], "r");
	
	if (fp == NULL) 
	{
		printf("Error in opening a file: %s", argv[1]);
		return 0;
	}
	
	// Can be broadcasted, but since this is SPMD, I have kept it. 
	e = (double)atof(argv[2]);
	if(rank == ROOT)
		printf("error= %f\n", e);

	/* Reading the maxtrix dimension */
	if(rank == ROOT)
	{
		fscanf(fp, "%d",&n);
		printf("n= %d\n", n);
	}
	//Broadcast the matrix Dimensions to all processes. 
	MPI_Bcast(&n, 1, MPI_DOUBLE, ROOT, MPI_COMM_WORLD); 
	mat = (double**) malloc(n * sizeof(double*)); 
	for (i= 0; i<n; i++)
		mat[i]= (double*) malloc((n+1) * sizeof(double));
	
	/* Initializing x */
	//Let the ROOT initialize X and Y and broadcast it to other processes. 
	//The broadcasting happens within the alogorithm.
	x = (double*) malloc(n * sizeof(double));
	y = (double*) malloc((n/numproc) * sizeof(double));

	j=1;
	iter = 0; 
	
	// Reading the input matrix. Done by the ROOT process
	// Every row is then broadcasted to every process.
    for(i=0;i<n;i++)
	{
		if(rank == ROOT)
		{
			for(j=0;j<n+1;j++)
			{
				fscanf(fp, "%lf", &mat[i][j]);
			}
		}
    }
	for(i=0;i<n;i++)
	{
		x[i] = 0;
		if(i < n/numproc)
			y[i] = 0;
	}
	if(rank == ROOT)
	{
		tOverallStart = MPI_Wtime();
	}
	
	// For this algorithm we are going to be using only the rows.
	// There are no dependencies between rows and hence every
	// process will have its own rows. However since the rows
	// may not be in contiguous memory locations, they need to be
	// sent one at a time and cannot be sent to gether. The rows 
	// are equally divided and the respective rows are sent to the 
	// different processes. 
	for(i = 0; i < n; i++)
	{
		if(numproc == 4)
		{
			if(i < 2500)
			{
				//Do nothing. 
			}
			else if(i >= 2500 && i < 5000)
			{
				if(rank == ROOT)
					MPI_Send(mat[i], n+1, MPI_DOUBLE, 1, 50, MPI_COMM_WORLD);
				if(rank == 1)
					MPI_Recv(mat[i], n+1, MPI_DOUBLE, ROOT, 50, MPI_COMM_WORLD, &status);
			}
			else if(i >= 5000 && i < 7500)
			{
				if(rank == ROOT)
					MPI_Send(mat[i], n+1, MPI_DOUBLE, 2, 50, MPI_COMM_WORLD);
				if(rank == 2)
					MPI_Recv(mat[i], n+1, MPI_DOUBLE, ROOT, 50, MPI_COMM_WORLD, &status);
			}
			else
			{
				if(rank == ROOT)
					MPI_Send(mat[i], n+1, MPI_DOUBLE, 3, 50, MPI_COMM_WORLD);
				if(rank == 3)
					MPI_Recv(mat[i], n+1, MPI_DOUBLE, ROOT, 50, MPI_COMM_WORLD, &status);
			}
		}
		else if (numproc == 2)
		{
			if(i < n/2)
			{
				//Do Nothing.
			}
			else if(i >= n/2)
			{
				if(rank == ROOT)
					MPI_Send(mat[i], n+1, MPI_DOUBLE, 1, 50, MPI_COMM_WORLD);
				if(rank == 1)
					MPI_Recv(mat[i], n+1, MPI_DOUBLE, ROOT, 50, MPI_COMM_WORLD, &status);
			}
		}
		else
		{
			// It does not need to send to different processes.
			// Just come out of the loop. 
			break;
		}
	}
	
	if(rank == ROOT)
	{
		tAlgoStart = MPI_Wtime();
	}
	int index = 0;

    do 
	{
		// Here we parallelize across the I loop. This is 
		// done primarily because we don't have dependencies
		// anymore. The only thing that is done here is that
		// for every iteration (iter) the x vetor has to be
		// broadcasted again. The values are very close to
		// the original. Some match, some are of by 0.00001. 
		// This is because we have changed the equation for
		// this code as mentioned in the HW. 
        bb=0;
		int startIterValue, endIterValue;
		startIterValue = (n/numproc) * rank;
		endIterValue = startIterValue + (n/numproc); 
        for(i = startIterValue; i < endIterValue; i++)
		{
            sum=0;
            for(j=0; j < n; j++)
			{
                if(j!=i)
				{
                    sum=sum+mat[i][j]*x[j];
                }
            }
			temp=(mat[i][n]-sum) / mat[i][i];
			diff=fabs(x[i]-temp);
			if(diff>bb)
			{
				bb=diff;
			}
			// Here the values are saved in a Y vector as well. 
			// Because when the numproc is more than 1, we need
			// to combine the different values from the different
			// processes into a single X vector. However the size
			// of the X vector has to remain the same. Another
			// possible way of doing this would be to let X vector
			// be transmitted, but we would require another vector
			// to collect the different values into a single vector.
			// Overall, it is exactly the same. 
			index = i % (n/numproc);
			y[index] = temp;
			x[i] = temp; 
        }
		//Gather all y vectors and combine it into x vector
		if(numproc > 1)
		{
			MPI_Allgather(y, n/numproc, MPI_DOUBLE, x, n/numproc, MPI_DOUBLE, MPI_COMM_WORLD);
			MPI_Allreduce(&bb, &bb, 1, MPI_DOUBLE, MPI_MAX, MPI_COMM_WORLD );
		}
		iter++;
    }
    while(bb>=e);
	if(rank == ROOT)
	{
		tAlgoEnd = MPI_Wtime();
		tAlgoDiff = tAlgoEnd - tAlgoStart; 
	}
	if(rank == ROOT)
	{
		tOverallEnd = MPI_Wtime();
		tOverallDiff = tOverallEnd - tOverallStart; 
		printf("\n\n----- Overall Time Diff: %f, Algorithm Time Diff: %f, Iterations: %d\n\n", tOverallDiff, tAlgoDiff, iter);
	}
	
	// Print all the values of the X Vector. 
	if(rank == ROOT)
	{
		for(i=0; i<n; i++)
		{
		    printf("\nx[%d] = %f", i, x[i]);
		}
		printf("\n");
	}

	print_x(iter, n, x);
	for (i=0; i<n; i++)
	{
		free(mat[i]);
	}
	free(mat);
	free(x);
	free(y);

	fclose(fp);
	MPI_Finalize();
	return 1;
}