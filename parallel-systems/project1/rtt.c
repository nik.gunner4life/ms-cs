/***************************************/
// Single Author Info:
// nsrivat Nikhil Srivatsan Srinivasan
/***************************************/

#include <stdio.h>
#include <stdlib.h>
#include "mpi.h"
#include <math.h>

/* This is the root process */
#define  ROOT		0

int main (int argc, char *argv[])
{

  /* process information */
  int   numproc, rank, len;
  char  hostname[MPI_MAX_PROCESSOR_NAME];
  int i = 0;
  MPI_Init(&argc, &argv);
  MPI_Request sendRequest, recvRequest;
  MPI_Comm_size(MPI_COMM_WORLD, &numproc);
  MPI_Comm_rank(MPI_COMM_WORLD, &rank);
  MPI_Get_processor_name(hostname, &len);
  MPI_Status status;
  printf ("\nHello from task %d on %s!\n", rank, hostname);
  MPI_Barrier(MPI_COMM_WORLD);
  
  // The same code can be used for both RTT and RTT4.
  // The difference is identified by the choice variable.
  // 1 indicates simple RTT and 2 will be RTT4. 
  int choice = (int)atoi(argv[1]);
  
  if(choice == 1)
  {
	  double t1, t2, tAvg, tTotal, tMax, tMin; 
	  int numTurns = 20;
	  double timeArray[numTurns];
	  int index = 1;
	  int packetCount = 0;
	  int j = 0;
	  int tag = 0;

	  if(rank == ROOT)
		  printf("\n ----------------------------------RTT 4 A-----------------------------------------------\n");
	
	  // This is how the below algorithm works. We first create a
	  // packet(specified size) and that packet is used for communication
	  // between the ROOT and all other processes, one(process) at a time. 
	  // Each of these iterations is performed numTurns number of times
	  // to get a good average value. Once this is done the packet size
	  // is increased and entire process is repeated till all packet
	  // sizes have been completed. 
	  for(index = 2; index < 22; index++)
	  {
			long packetSize = pow(2, index);
			char* packet = (char*)malloc(sizeof(char*)*packetSize);
			if(rank != ROOT)
			{
				// We know the number of packets each process is going
				// to receive. Calculating that in advance we can simply
				// let the process collect the packets. We know it will
				// be collecting the correct packet because, the packetsize
				// will remain the same in every iteration (index). 
				for(packetCount = 0; packetCount < numTurns; packetCount++)
				{
					MPI_Recv(packet, packetSize, MPI_CHAR, ROOT, MPI_ANY_TAG, MPI_COMM_WORLD, &status);
					MPI_Send(packet, packetSize, MPI_CHAR, ROOT, 50, MPI_COMM_WORLD);
				}
			}
			else
			{
				for(i = 1; i < numproc; i++)
				{
					for(j = 0; j < numTurns; j++)
					{
						t1 = MPI_Wtime(); 
						MPI_Send(packet, packetSize, MPI_CHAR, i, 50, MPI_COMM_WORLD);
						MPI_Recv(packet, packetSize, MPI_CHAR, i, MPI_ANY_TAG, MPI_COMM_WORLD, &status);
						t2 = MPI_Wtime();
						// All the time differences are stored in this time array.
						timeArray[j] = 0.0;
						timeArray[j] = t2 - t1;
					}
					// Finding the average of all turns after excluding
					// the first one (j starts from 1 and not 0). 
					tMax = 0.0;
					tMin = 100.0;
					tTotal = 0.0;
					for(j = 1; j < numTurns; j++)
					{
						if(timeArray[j] > tMax)
							tMax = timeArray[j];
						tTotal = tTotal + timeArray[j];
					}
					for(j = 1; j < numTurns; j++)
					{
						if(timeArray[j] < tMin)
							tMin = timeArray[j];
					}
					tAvg = tTotal/(numTurns-1);
					printf("\n>>>>Max: %f, Min: %f, Average: %f for packet size %ld bytes and for Process %d", tMax, tMin, tAvg, packetSize, i);
				}
			}
		}
	}

  // RTT4. Even senders and Odd receivers. 
  else if(choice == 2)
  {
	  double t1, t2, tAvg, tTotal = 0, tMax = 0, tMin = 100; 
	  int numTurns = 10, index = 1, packetCount = 0, j = 0, maxIndex = 22, procID, k = 0;
	  int procArray[numproc/2];
	  double timeArray[numTurns];
	  char proceed = 'Y'; 
	  long packetSize; 
	  char* packet; 
	  if(rank == ROOT)
		  printf("\n ----------------------------------RTT 4 B-----------------------------------------------\n");
	  for(index = 2; index < maxIndex; index++)
	  {
		  // This is how the below algorithms works. A packet is created and
		  // that packet is used by the even ranked processes. These processes
		  // send these packets to all the odd ranked processes for numTurns
		  // number of times so that we get a good indication of the RTT.
		  // Once this is done, the packet size is increased and the process
		  // is repeated till all the packet sizes have been completed. 
		  packetSize = pow(2, index);
	      packet = (char*)malloc(sizeof(char*)*packetSize);
		  for(j = 0; j < numTurns; j++)
		  {
			  
			  if(rank % 2 == 0)
			  {
				  procID = (rank + 1); 
				  t1 = MPI_Wtime();
				  MPI_Send(packet, packetSize, MPI_CHAR, procID, 50, MPI_COMM_WORLD);
			  }
			  else
			  {
				  procID = (rank - 1); 
				  MPI_Recv(packet, packetSize, MPI_CHAR, procID, 50, MPI_COMM_WORLD, &status);
			  }
			  // The barrier is used here because we do not want the 
			  // processes to communicate together. This ensures
			  // that the packets are sent and received together
			  // at more or less around the same time. 
			  MPI_Barrier(MPI_COMM_WORLD); 
			  if(rank % 2 == 0)
			  {
				  procID = (rank + 1); 
				  MPI_Recv(packet, packetSize, MPI_CHAR, procID, 50, MPI_COMM_WORLD, &status);
				  t2 = MPI_Wtime();
				  timeArray[j] = 0.0;
				  timeArray[j] = t2 - t1;
			  }
			  else
			  {
				  procID = (rank - 1); 
				  MPI_Send(packet, packetSize, MPI_CHAR, procID, 50, MPI_COMM_WORLD);
			  }
			  // The barrier is used here because we do not want the 
			  // processes to communicate together. This ensures
			  // that the packets are sent and received together
			  // at more or less around the same time. 
			  MPI_Barrier(MPI_COMM_WORLD);
		  }
		  if(rank % 2 == 0)
		  {
			  tMax = 0.0;
			  tMin = 100.0;
			  tTotal = 0.0;
			  for(j = 1; j < numTurns; j++)
			  {
				  if(timeArray[j] > tMax)
						tMax = timeArray[j];
				  tTotal = tTotal + timeArray[j];
			  }
			  for(j = 1; j < numTurns; j++)
			  {
				  if(timeArray[j] < tMin)
						tMin = timeArray[j];
			  }
			  tAvg = tTotal/(numTurns-1);
			  printf("\n>>>>Max: %f, Min: %f, Average: %f for packet size %ld bytes and for Process %d to Process %d", tMax, tMin, tAvg, packetSize, rank, procID);
		  }
		  MPI_Barrier(MPI_COMM_WORLD);
		  sleep(1);
	  }
  }
  /* graceful exit */
  MPI_Finalize();
}