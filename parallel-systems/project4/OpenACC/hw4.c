
#include <math.h>
#include<stdlib.h>
#include <stdio.h>

#define N 1000
#define EPSILON 0.01

int main (int argc, char *argv[])
{
	double diff;  /* change in value */
	int    i, j;
	double mean;
	double u[N][N];
	double w[N][N];

	struct timeval start_tv, end_tv;
	FILE *fp = fopen("output.dat", "w");

	/* Set boundary values and compute mean boundary value */
	mean = 0.0;
	for (i=0; i<N; i++){
		u[i][0] = u[i][N-1] = u[0][i] = 100.0;
		u[N-1][i] = 0.0;
		mean += u[i][0] + u[i][N-1] + u[0][i] + u[N-1][i];
	}
	mean /= (4.0 *N);

	/* Initialize interior values */
	for (i =1; i<N-1; i++)
		for (j=1; j<N-1; j++)
			u[i][j] = mean;

	gettimeofday(&start_tv, NULL);

	/* Compute steady-state solution - Parallelize this loop with OpenAcc */
	while(1)
	{
		diff = 0.0;
		for (i=1; i<N-1; i++){
			for (j=1; j<N-1; j++){
				w[i][j] = (u[i-1][j] + u[i+1][j] + u[i][j-1] + u[i][j+1])/4.0;
				if (fabs (w[i][j] - u[i][j]) > diff)
					diff = fabs(w[i][j] - u[i][j]);
			}
		}

		if (diff <= EPSILON)
			break;
		for (i=1; i<N-1; i++)
			for (j=1; j<N-1; j++)
				u[i][j] = w[i][j];
	}
	/* End of parallelization */

	gettimeofday(&end_tv, NULL);
	printf("Elapsed time: %f sec\n", 
		(double)( (double)(end_tv.tv_sec - start_tv.tv_sec) + ( (double)(end_tv.tv_usec - start_tv.tv_usec)/1000000)) );

	/* Print Solution */
	for (i=0; i<N; i++){
		for (j=0; j<N; j++){
			fprintf(fp, "%6.2f ", u[i][j]);
		}
		fprintf(fp, "\n");
	}
	fclose(fp);
	return 0;
}