package bat;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.HashMap;

import oracle.jdbc.OracleTypes;

public class CustomerHandler {

	/**
	 * The connection that will be used by this CustomerHandler.
	 * This is usually generated outside and set using the constructor.
	 */
	private Connection con;
	
	/**
	 * The constructor. Sets the connection that will be used.
	 * @param con 	The connection that will be used. Constructed outside this object.
	 */
	public CustomerHandler(Connection con) {
		this.con = con;
	}

	/**
	 * Gives you the SSN for a specific customer. The SSN is used to identify someone
	 * in the `people` table.
	 * 
	 * Use {@link #getSSN(int)} to obtain the SSN of a specific customer.
	 * StaffHandlers have a similar method.
	 * 
	 * @see StaffHandler
	 * @param customerID 	The ID of the customer. This is the primary key of the `customer` table.
	 * @return				Returns SSN of the customer if the customer exists, null otherwise.
	 * @throws SQLException A failure in the process must be dealt with outside this function.
	 */
	private String getSSN(int customerID) throws SQLException {
		String SSN = null;

		Statement getSSNStatement = con.createStatement();
		ResultSet getSSNs = getSSNStatement
				.executeQuery("SELECT SSN FROM customer WHERE customerID="
						+ customerID);
		if (getSSNs.next()) {
			SSN = getSSNs.getString("SSN");
		}
		return SSN;
	}
	
	/**
	 * Gives you all the personal data for a given customer.
	 * 
	 * @param customerID		The ID of the customer. This is the primary key of the `customer` table.
	 * @return					A HashMap<String,String> containing customerID, SSN, firstName, lastName, gender, phone, email, dob. The dob is in dd/MM/yyyy.
	 * @throws SQLException
	 */
	public HashMap<String,String> getCustomerData(int customerID) throws SQLException {
		String SSN;
		
		PreparedStatement ps = con.prepareStatement("SELECT customerID, SSN FROM customer WHERE customerID=?");
		ResultSet rs = null;
		ps.setInt(1, customerID);
		rs = ps.executeQuery();
		
		HashMap<String,String> data = new HashMap<String,String>();
		
		while(rs.next()) {
			SSN = rs.getString(2);
			PreparedStatement infoStatement = con.prepareStatement("SELECT ssn, firstname, lastname, gender, phone, email, dob FROM people WHERE SSN=?");
			infoStatement.setString(1, SSN);
			ResultSet info = infoStatement.executeQuery();
			
			DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
			
			if (info.next()) {
				data.put("customerID", String.valueOf(rs.getInt(1)));
				data.put("SSN", SSN);
				data.put("firstName", info.getString("firstname"));
				data.put("lastName", info.getString("lastname"));
				data.put("gender", info.getString("gender"));
				data.put("phone", info.getString("phone"));
				data.put("email", info.getString("email"));
				data.put("dob", df.format(info.getDate("dob")));
			}
			else {
				data.put("customerID", "null");
			}
		}
		
		return data;
	}

	/**
	 * Creates a new customer in the `customer` table using the information given. All the information is compulsory.
	 * In the process of creating a customer, it also creates a record for the person in the `people` table using PeopleHandler.
	 * In the process of PeopleHandler creating a person record, it creates a phone-address pair in `people_home`.
	 * 
	 * @see PeopleHandler   	Most of the data is passed to a {@link PeopleHandler} to create a person.
	 * @param SSN 				The SSN of the customer.
	 * @param firstName 		The customer's first name. Any arbitrary string is allowed.
	 * @param lastName 			The customer's last name. Any arbitrary string is allowed.
	 * @param DOB 				The customer's date of birth as a java.sql.Date object.
	 * @param gender 			The gender of the customer. Must be 'M' or 'F'.
	 * @param phone 			The phone number of the customer. No validation is performed.
	 * @param email 			The email of the customer. No validation is performed.
	 * @param address 			The address of the customer.
	 * @return					A customer ID if all went well, -1 otherwise.
	 * @throws SQLException		A failure in this function must be dealt with outside.
	 */
	public int newCustomer(String SSN, String firstName, String lastName,
			Date DOB, String gender, String phone, String email, String address)
			throws SQLException {

		int customerID = -1;
		CallableStatement insertCustomer = null;
		String insertCustomerString = "BEGIN INSERT INTO customer VALUES(?, ?) returning customerid into ?; END;";

		PeopleHandler personAdder = new PeopleHandler(con);
		personAdder.newPerson(SSN, firstName, lastName, gender, phone, email,
				DOB, address);

		insertCustomer = con.prepareCall(insertCustomerString);
		insertCustomer.setInt(1, customerID);
		insertCustomer.setString(2, SSN);
		insertCustomer.registerOutParameter(3, OracleTypes.NUMBER);
		insertCustomer.execute();
		customerID = insertCustomer.getInt(3);
		return customerID;
	}

	/**
	 * Changes the customer's name.
	 * 
	 * @see PeopleHandler 		The {@link PeopleHandler} does most of the work changing the name in the `people` table.
	 * @param customerID 		The ID of the customer who is changing his name.
	 * @param firstName 		The new first name of the customer.
	 * @param lastName 			The new last name of the customer.
	 * @return 					True if the update was successful, false otherwise.
	 * @throws SQLException 	
	 */
	public Boolean updateCustomerName(int customerID, String firstName,
			String lastName) throws SQLException {
		PeopleHandler person = new PeopleHandler(con);
		String SSN = getSSN(customerID);

		if (person.updatePersonName(SSN, firstName, lastName)) {
			return true;
		}
		return false;
	}

	/**
	 * Changes the customer's address.
	 * 
	 * @see PeopleHandler 		The {@link PeopleHandler} changes the address in the `people_home` table.
	 * @param customerID 		The ID of the customer whose address is being changed.
	 * @param address 			The new address of the customer.
	 * @return 					True, if the update was successful, false otherwise
	 * @throws SQLException
	 */
	public Boolean updateCustomerAddress(int customerID, String address)
			throws SQLException {
		PeopleHandler person = new PeopleHandler(con);
		String SSN = getSSN(customerID);

		if (person.updatePersonAddress(SSN, address)) {
			return true;
		}
		return false;

	}

	/**
	 * Changes the phone number of the customer.
	 * 
	 * @see PeopleHandler		The {@link PeopleHandler} changes the phone in `people` and `people_home`.
	 * @param customerID 		The ID of the customer whose phone is being changed.
	 * @param phone				The new phone number of the customer.
	 * @return 					True if the update was successful, false otherwise.
	 * @throws SQLException
	 */
	public Boolean updatePhone(int customerID, String phone)
			throws SQLException {
		PeopleHandler person = new PeopleHandler(con);
		String SSN = getSSN(customerID);

		if (person.updatePersonPhone(SSN, phone)) {
			return true;
		}
		return false;
	}

	/**
	 * Changes the email of the customer.
	 * 
	 * @see PeopleHandler		The {@link PeopleHandler} changes the email in `people`.
	 * @param customerID		The ID of the customer whose phone is being changed.
	 * @param email				The new email of the customer.
	 * @return					True if the update was successful, false otherwise.
	 * @throws SQLException
	 */
	public Boolean updateEmail(int customerID, String email)
			throws SQLException {
		PeopleHandler person = new PeopleHandler(con);
		String SSN = getSSN(customerID);

		if (person.updatePersonEmail(SSN, email)) {
			return true;
		}
		return false;
	}
	
	/** 
	 * Deletes a customer from the database. If the customer was not also a staff member, deletes his person record.
	 *  
	 * @param customerID		The ID of the customer being deleted. 		
	 * @return					True if the deletion was successful. If unsuccessful, an exception must have been thrown.
	 * @throws SQLException
	 */
	public Boolean deleteCustomer(int customerID) throws SQLException {
		PeopleHandler people = new PeopleHandler(con);
		Boolean isAlsoStaff = false;

		String getSSNString = "SELECT SSN FROM customer WHERE customerID="
				+ customerID;

		Statement stmt;
		stmt = con.createStatement();
		ResultSet SSNs = stmt.executeQuery(getSSNString);
		String SSN = null;

		if (SSNs.next()) {
			SSN = SSNs.getString("SSN");
		}

		String isAlsoStaffString = "SELECT staff.SSN FROM staff WHERE staff.SSN="
				+ SSN;

		try {
			ResultSet isAlsoStaffResultSet = stmt
					.executeQuery(isAlsoStaffString);
			if (isAlsoStaffResultSet.next()) {
				isAlsoStaff = true;
				System.out.println("Is also a member of staff!");
			}
		} catch (SQLException e1) {
			e1.printStackTrace();
		}

		String delCustomerString = "DELETE FROM customer WHERE customerID = ?";
		PreparedStatement ps;

		ps = con.prepareStatement(delCustomerString);
		ps.setInt(1, customerID);
		ps.executeUpdate();

		if (!isAlsoStaff) {
			people.deletePerson(SSN);
		}

		return true;

	}
}
