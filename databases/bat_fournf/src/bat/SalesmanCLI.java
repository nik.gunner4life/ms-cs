package bat;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.sql.Connection;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashMap;

public class SalesmanCLI {

	private Connection con;

	/**
	 * SalesmanCLI.staffID is the staffID of the Salesman accessing the system
	 * through this object.
	 */
	@SuppressWarnings("unused")
	private int staffID;

	public SalesmanCLI(Connection con, int staffID) {
		this.con = con;
		this.staffID = staffID;
	}

	/**
	 * The CLI menu function for adding a new customer.
	 * 
	 * @throws Exception
	 */
	private void newCustomer() throws Exception {
		CustomerHandler customers = new CustomerHandler(con);
		String SSN = null;
		String firstName = null;
		String lastName = null;

		Calendar DOBc = null;
		Date DOB = null;
		int day = 1;
		int month = 1;
		int year = 1970;

		String gender = null;
		String phone = null;
		String email = null;
		String address = null;

		int customerID = 0;

		BufferedReader input = new BufferedReader(new InputStreamReader(
				System.in));

		System.out.print("Enter the SSN of the customer: ");
		SSN = input.readLine();

		System.out.print("Enter the first name of the customer: ");
		firstName = input.readLine();

		System.out.print("Enter the last name of the customer: ");
		lastName = input.readLine();

		System.out.println("Enter the date of birth:");
		System.out.print("Day: ");
		day = Integer.parseInt(input.readLine());
		System.out.print("Month: ");
		month = Integer.parseInt(input.readLine());
		System.out.print("Year: ");
		year = Integer.parseInt(input.readLine());
		DOBc = new GregorianCalendar(year, month - 1, day);
		DOB = new Date(DOBc.getTimeInMillis());

		do {
			System.out
					.println("Enter M for male customer, F for female customer: ");
			gender = input.readLine();
		} while (!gender.equals("M") && !gender.equals("F"));

		System.out.println("Enter the phone number of the customer: ");
		phone = input.readLine();

		System.out.println("Enter the email address of the customer: ");
		email = input.readLine();

		System.out.println("Enter the address of the customer: ");
		address = input.readLine();

		customerID = customers.newCustomer(SSN, firstName, lastName, DOB,
				gender, phone, email, address);
		System.out.println("The new customer's ID is :" + customerID);
	}

	/**
	 * The CLI menu function to handle new orders.
	 * 
	 * @throws SQLException
	 * @throws IOException
	 */
	private void newOrder() throws SQLException, IOException {
		Orders orders = new Orders(con);
		int qty = 0;
		float price = 0;
		String isbn = null;
		int custID = 0;
		Date orderdate = null;
		String payStatus = null;
		String status = null;

		BufferedReader input = new BufferedReader(new InputStreamReader(
				System.in));

		System.out.print("Enter the ISBN of the order: ");
		isbn = input.readLine();

		System.out.print("Enter the quantity of the order: ");
		qty = Integer.parseInt(input.readLine());

		System.out.print("Enter the price");
		price = Integer.parseInt(input.readLine());

		System.out.print("Enter the ID of the customer: ");
		custID = Integer.parseInt(input.readLine());

		java.util.Date today = new java.util.Date();
		orderdate = new Date(today.getTime());

		payStatus = "NOT PAID";
		status = "PENDING";

		int oid = orders.newOrder(qty, price, isbn, custID, staffID, orderdate,
				payStatus, status);
		if (oid == -1) {
			System.out.println("Could not place the order");
		} else {
			System.out.println("The order ID is " + oid);
		}
	}

	/**
	 * The CLI menu function to add a new restock.
	 * 
	 * @throws SQLException
	 * @throws IOException
	 */
	private void newRestock() throws SQLException, IOException {
		Orders orders = new Orders(con);
		int qty = 0;
		String status = null;
		String isbn = null;

		BufferedReader input = new BufferedReader(new InputStreamReader(
				System.in));

		System.out.print("Enter the ISBN of the book that needs restocking: ");
		isbn = input.readLine();

		System.out.println("Enter the quantity: ");
		qty = Integer.parseInt(input.readLine());

		status = "PENDING";

		int rrid = orders.newRestockRequest(qty, status, staffID, isbn);
		if (rrid == -1) {
			System.out.println("Could not ask for a restock");
		} else {
			System.out.println("Restock ID" + rrid);
		}
	}

	/**
	 * The CLI menu function change a customer in some way.
	 * 
	 * This CLI menu function changes a customer in some way. It offers a menu
	 * to pick the changes to be made.
	 * 
	 * @throws IOException
	 * @throws SQLException
	 */
	private void updateCustomer() throws IOException, SQLException {
		CustomerHandler customers = new CustomerHandler(con);
		System.out.println("Enter the customer ID: ");
		BufferedReader input = new BufferedReader(new InputStreamReader(
				System.in));
		int customerID = Integer.parseInt(input.readLine());

		int looper = 1;

		do {
			System.out.println("What do you want to update?");
			System.out.println("1. Name");
			System.out.println("2. Address");
			System.out.println("3. Phone");
			System.out.println("4. Email");
			System.out.println("0. Go back");

			BufferedReader reader = new BufferedReader(new InputStreamReader(
					System.in));
			try {
				looper = Integer.parseInt(reader.readLine());
			} catch (NumberFormatException e) {
				looper = -1;
			}

			switch (looper) {
			case 1:
				System.out.println("Enter new first name:");
				String firstName = input.readLine();
				System.out.println("Enter new last name:");
				String lastName = input.readLine();
				customers.updateCustomerName(customerID, firstName, lastName);
				break;
			case 2:
				System.out.println("Enter new address:");
				String address = input.readLine();
				customers.updateCustomerAddress(customerID, address);
				break;
			case 3:
				System.out.println("Enter new phone:");
				String phone = input.readLine();
				customers.updatePhone(customerID, phone);
				break;
			case 4:
				System.out.println("Enter new email address:");
				String email = input.readLine();
				customers.updateEmail(customerID, email);
			case 0:
				break;
			default:
				System.out.println("Pick from the available options:");
			}

		} while (looper != 0);
	}

	/**
	 * Update an order
	 * 
	 * Update an order to mark it shipped, completed, or cancelled.
	 * 
	 * @throws NumberFormatException
	 * @throws IOException
	 * @throws SQLException
	 */
	private void updateOrder() throws NumberFormatException, IOException,
			SQLException {
		Orders orders = new Orders(con);
		System.out.println("Enter the Order ID:");
		BufferedReader input = new BufferedReader(new InputStreamReader(
				System.in));
		int orderID = Integer.parseInt(input.readLine());

		String orderstatus = null;

		int looper = 1;

		do {
			System.out.println("Options:");
			System.out.println("1. Mark order shipped");
			System.out.println("2. Mark order completed");
			System.out.println("3. Cancel order");
			System.out.println("0. Go back");

			BufferedReader reader = new BufferedReader(new InputStreamReader(
					System.in));
			try {
				looper = Integer.parseInt(reader.readLine());
			} catch (NumberFormatException e) {
				looper = -1;
			}

			switch (looper) {
			case 1:
				orderstatus = "SHIPPED";
				orders.updateOrder(orderID, orderstatus);
				break;
			case 2:
				orderstatus = "COMPLETED";
				orders.updateOrder(orderID, orderstatus);
				break;
			case 3:
				orders.cancelOrder(orderID);
			case 0:
				break;
			default:
				System.out.println("Pick from the available options:");
			}

		} while (looper != 0);
	}

	/**
	 * Update a restock
	 * 
	 * Update a restock to mark it completed or cancelled.
	 * 
	 * @throws NumberFormatException
	 * @throws IOException
	 * @throws SQLException
	 */
	private void updateRestock() throws NumberFormatException, IOException,
			SQLException {
		Orders restocks = new Orders(con);
		System.out.println("Enter the Restock ID:");
		BufferedReader input = new BufferedReader(new InputStreamReader(
				System.in));
		int restockorderID = Integer.parseInt(input.readLine());

		String restockstatus = null;

		int looper = 1;

		do {
			System.out.println("Options:");
			System.out.println("1. Mark restock completed");
			System.out.println("2. Cancel restock");
			System.out.println("0. Go back");

			BufferedReader reader = new BufferedReader(new InputStreamReader(
					System.in));
			try {
				looper = Integer.parseInt(reader.readLine());
			} catch (NumberFormatException e) {
				looper = -1;
			}

			switch (looper) {
			case 1:
				restockstatus = "COMPLETED";
				if (restocks.completeRestock(restockorderID) == false) {
					System.out.println("Restocking not completed");
				} else {
					System.out.println("Restocking Complete");
				}
				break;
			case 2:
				if (restocks.cancelRestockRequest(restockorderID) == false) {
					System.out.println("Restocking not cancelled");
				} else {
					System.out.println("Restocking cancelled");
				}
				break;
			case 0:
				break;
			default:
				System.out.println("Pick from the available options:");
			}

		} while (looper != 0);
	}

	/**
	 * Print all information about a customer.
	 * 
	 * Prints all information about a customer.
	 * 
	 * @throws NumberFormatException
	 * @throws IOException
	 * @throws SQLException
	 */
	private void getCustomer() throws NumberFormatException, IOException,
			SQLException {
		CustomerHandler customers = new CustomerHandler(con);
		System.out.println("Enter the customer ID: ");
		BufferedReader input = new BufferedReader(new InputStreamReader(
				System.in));
		int customerID = Integer.parseInt(input.readLine());

		HashMap<String, String> data = customers.getCustomerData(customerID);

		if (data.get("customerID") == null) {
			System.out.println("ERROR: Customer not found");
			return;
		}

		System.out
				.println("Customer ID \t SSN \t First Name \t Last Name \t Gender \t  Phone \t Email \t Date of Birth");
		System.out.println(data.get("customerID") + "\t" + data.get("SSN")
				+ "\t" + data.get("firstName") + "\t" + data.get("lastName")
				+ "\t" + data.get("gender") + "\t" + data.get("phone") + "\t"
				+ data.get("email") + "\t" + data.get("dob"));

	}

	/**
	 * This generates the list of functions that the salesperson can perform.
	 * 
	 * @throws Exception
	 */
	public void showCLI() throws Exception {
		int looper = 1;
		Report objReport = new Report(con);

		do {
			System.out.println("Enter your choice:");
			System.out.println("1. New Customer");
			System.out.println("2. New Order");
			System.out.println("3. New Restock");
			System.out.println("4. Update Customer details");
			System.out.println("5. Update Order");
			System.out.println("6. Update Restock");
			System.out.println("7. Get Customer Information");
			System.out.println("8. List Books and their Current Prices");
			System.out.println("9. Check Availibility");
			System.out.println("0. Exit");

			BufferedReader reader = new BufferedReader(new InputStreamReader(
					System.in));
			try {
				looper = Integer.parseInt(reader.readLine());
			} catch (NumberFormatException e) {
				looper = -1;
			}

			switch (looper) {
			case 1:
				newCustomer();
				break;
			case 2:
				newOrder();
				break;
			case 3:
				newRestock();
				break;
			case 4:
				updateCustomer();
				break;
			case 5:
				updateOrder();
				break;
			case 6:
				updateRestock();
				break;
			case 7:
				getCustomer();
				break;
			case 8:
				ResultSet rs1 = null;
				rs1 = objReport.listPricesOfBooks();
				System.out.println("ISBN   Title   Author");
				System.out.println("---------------------");
				while (rs1.next()) {
					System.out.println(rs1.getString("ISBN") + ", "
							+ rs1.getString("Title") + ", "
							+ rs1.getString("Author"));
				}
				break;
			case 9:
				System.out.println("Enter Book Title:");
				String bookTitle = reader.readLine();
				System.out.println("Enter Book Author:");
				String bookAuthor = reader.readLine();
				bookTitle = "'" + bookTitle + "'";
				bookAuthor = "'" + bookAuthor + "'";
				objReport = new Report(con);
				rs1 = objReport.checkAvailibilityOfBook(bookTitle, bookAuthor,
						staffID);
				int count = 0;
				while (rs1.next()) {
					count++;
				}
				if (count > 0) {
					rs1.beforeFirst();
					System.out
							.println("ISBN   Title   Author");
					System.out
							.println("---------------------");
					while (rs1.next()) {
						System.out.println(rs1.getString("ISBN") + ", "
								+ rs1.getString("Title") + ", "
								+ rs1.getString("Author"));
					}
				} else {
					System.out.println("We currently don't have this book.");
				}
				break;
			case 0:
				System.out.println("Exiting...");
				break;
			default:
				System.out.println("Choose from the available choices.");
			}

		} while (looper != 0);
	}
}
