package bat;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.SQLIntegrityConstraintViolationException;
import java.sql.Statement;

public class PeopleHandler {

	private Connection con;

	/**
	 * 
	 * Set the connection to be used when creating the object.
	 * 
	 * @param con	The connection that will be used
	 */

	public PeopleHandler(Connection con) {
		this.con = con;
	}
	
	/**
	 * 
	 * Add a new person to `people`. We will also add their phone-address pair to `people_home`.
	 * 
	 * @param SSN				The SSN of the person to be added.
	 * @param firstName			The first name of the person to be added.
	 * @param lastName			The last name of the person to be added.
	 * @param gender			The gender of the person to be added. Can be 'M' or 'F'.
	 * @param phone				The phone number of the person to be added.
	 * @param email				The email address of the person to be added.
	 * @param dob				The date of birth of the person to be added.
	 * @param address			The address of the person to be added.
	 * @return					True if the person is added, false otherwise.
	 * @throws SQLException
	 */
	public boolean newPerson(String SSN, String firstName, String lastName,
			String gender, String phone, String email, Date dob, String address)
			throws SQLException {

		String insertContactStatement = "INSERT INTO people_home VALUES(?, ?)";
		PreparedStatement insertContact = con
				.prepareStatement(insertContactStatement);

		insertContact.setString(1, phone);
		insertContact.setString(2, address);

		Statement stmt = con.createStatement();
		ResultSet checkUnique = stmt
				.executeQuery("SELECT address FROM people_home WHERE phone="
						+ phone);
		if (checkUnique.next()) {
			String oldAdd = checkUnique.getString("address");
			if (!oldAdd.equals(address)) {
				return false;
			}
		} else {
			insertContact.executeUpdate();
		}

		String insertPeopleStatement = "INSERT INTO people VALUES(?, ?, ?, ?, ?, ?, ?)";
		PreparedStatement insertPeople = con
				.prepareStatement(insertPeopleStatement);
		insertPeople.setString(1, SSN);
		insertPeople.setString(2, firstName);
		insertPeople.setString(3, lastName);
		insertPeople.setString(4, gender);
		insertPeople.setString(5, phone);
		insertPeople.setString(6, email);
		insertPeople.setDate(7, dob);

		checkUnique = stmt
				.executeQuery("SELECT * FROM people WHERE SSN=" + SSN);
		while (checkUnique.next()) {
			String oldFirstName = checkUnique.getString("firstname");
			String oldLastName = checkUnique.getString("lastname");
			String oldGender = checkUnique.getString("gender");
			String oldPhone = checkUnique.getString("phone");
			String oldEmail = checkUnique.getString("email");
			Date oldDOB = checkUnique.getDate("dob");
			if (!oldFirstName.equals(firstName)
					|| !oldLastName.equals(lastName)
					|| !oldGender.equals(gender) || !oldPhone.equals(phone)
					|| !oldEmail.equals(email) || !oldDOB.equals(dob)) {
				return false;
			}
		}

		insertPeople.executeUpdate();

		return true;
	}

	/**
	 * 
	 * Change the name of a person.
	 * 
	 * @param SSN				The SSN of the person whose name is to be changed.
	 * @param firstName			The new first name.
	 * @param lastName			The new last name.
	 * @return					True if the name is changed, exception thrown if the process fails.
	 * @throws SQLException
	 */
	public boolean updatePersonName(String SSN, String firstName,
			String lastName) throws SQLException {
		PreparedStatement ps = con
				.prepareStatement("UPDATE people SET firstname=?, lastname=? WHERE SSN=?");
		ps.setString(1, firstName);
		ps.setString(2, lastName);
		ps.setString(3, SSN);
		ps.executeUpdate();

		return true;
	}

	/**
	 * 
	 * Change the address of a person.
	 * 
	 * @param SSN				The SSN of the person whose address is to be changed.
	 * @param address			The new address.
	 * @return					True if the address is changed, false otherwise.
	 * @throws SQLException
	 */
	public boolean updatePersonAddress(String SSN, String address)
			throws SQLException {
		Statement stmt = con.createStatement();
		ResultSet rs = stmt
				.executeQuery("SELECT phone FROM customer WHERE SSN=" + SSN);
		String phone = null;
		if (rs.next()) {
			phone = rs.getString("phone");
		} else {
			return false;
		}
		
		PreparedStatement ps = con
				.prepareStatement("UPDATE people_home SET address=? WHERE phone=?");
		ps.setString(1, address);
		ps.setString(2, phone);
		ps.executeUpdate();

		return true;
	}

	/**
	 * 
	 * Change the phone number of a person.
	 * 
	 * @param SSN				The SSN of the person whose phone is to be changed.
	 * @param phone				The new phone number.
	 * @return					True if the person's phone is changed, false otherwise.
	 * @throws SQLException
	 */
	public boolean updatePersonPhone(String SSN, String phone)
			throws SQLException {
		Statement stmt = con.createStatement();
		ResultSet rs = stmt
				.executeQuery("SELECT address FROM people_home WHERE phone="
						+ phone);
		String address = null;

		if (rs.next()) {
			address = rs.getString("address");
		} else {
			return false;
		}

		PreparedStatement ps = con
				.prepareStatement("INSERT INTO people_home VALUES(?,?");
		ps.setString(1, phone);
		ps.setString(2, address);
		ps.executeUpdate();

		PreparedStatement personPhone = con
				.prepareStatement("UPDATE people SET phone=? WHERE SSN=?");
		personPhone.setString(1, phone);
		personPhone.setString(2, SSN);
		personPhone.executeUpdate();

		rs = stmt.executeQuery("SELECT * FROM people WHERE phone=?");
		if (rs.next()) {
			return true;
		} else {
			PreparedStatement clearPhone = con
					.prepareStatement("DELETE FROM people_home WHERE phone=?");
			clearPhone.setString(1, phone);
			clearPhone.executeUpdate();
		}
		return true;
	}

	/**
	 * 
	 * Change a person's email.
	 * 
	 * @param SSN				The SSN of the person whose email address is being changed.
	 * @param email				The new email address.
	 * @return					True. Exception thrown if it fails.
	 * @throws SQLException
	 */
	public boolean updatePersonEmail(String SSN, String email)
			throws SQLException {
		PreparedStatement ps = con
				.prepareStatement("UPDATE people SET email=? WHERE SSN=?");
		ps.setString(1, email);
		ps.setString(2, SSN);
		ps.executeUpdate();

		return true;
	}

	/**
	 * 
	 * Delete a person from `people`. Their details are also removed from `people_home`.
	 * 
	 * @param SSN				The SSN of the person to be deleted.
	 * @return					True if the person is deleted, false otherwise.
	 * @throws SQLException
	 */
	public boolean deletePerson(String SSN) throws SQLException {
		String getHomeString = "SELECT people.phone FROM people_home,people WHERE people_home.phone=people.phone AND SSN="
				+ SSN;
		Statement getHomeStatement = con.createStatement();
		ResultSet getHome = getHomeStatement.executeQuery(getHomeString);
		String phone = null;
		if (getHome.next()) {
			phone = getHome.getString("phone");
		}
		
		String delWorksatString = "DELETE FROM worksat WHERE staffid = (Select StaffID from staff where SSN=?)";
		PreparedStatement delworksatPreparedStatement = con
				.prepareStatement(delWorksatString);
		delworksatPreparedStatement.setString(1, SSN);
		delworksatPreparedStatement.executeUpdate();
		
		String delStaffString = "DELETE FROM staff WHERE SSN = ?";
		PreparedStatement delStaffPreparedStatement = con
				.prepareStatement(delStaffString);
		delStaffPreparedStatement.setString(1, SSN);
		delStaffPreparedStatement.executeUpdate();


		String delPersonString = "DELETE FROM people WHERE SSN = ?";
		PreparedStatement delPersonPreparedStatement = con
				.prepareStatement(delPersonString);
		delPersonPreparedStatement.setString(1, SSN);
		delPersonPreparedStatement.executeUpdate();

		String delHomeString = "DELETE FROM people_home WHERE phone = ?";
		PreparedStatement delHomePreparedStatement = con
				.prepareStatement(delHomeString);
		delHomePreparedStatement.setString(1, phone);
		delHomePreparedStatement.executeUpdate();

		return false;

	}
}
