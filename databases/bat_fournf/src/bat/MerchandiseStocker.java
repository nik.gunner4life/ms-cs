package bat;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

public class MerchandiseStocker {
	/**
	 * Set the connection to be used.
	 * 
	 * @param con
	 */
	private Connection dbmsCon = null;
	public MerchandiseStocker(Connection con)
	{
		dbmsCon = con;
	}
	
	/**
	 * This generates the list of functions that the Merchandise Stocker is allowed to perform.
	 *  
	 * @throws IOException
	 * @throws SQLException
	 */
	public void showCLI() throws IOException, SQLException {
		int looper = 1;
		ResultSet rs = null;
		do {
			System.out.println("Enter your choice:");
			System.out.println("1. Request Books from Vendor");
			System.out.println("2. Update Request Status to shipped");
			System.out.println("3. Update Request Status to Completed");
			System.out.println("4. Cancel Request");
			System.out.println("5. Update Restock as shipped");
			System.out.println("6. Add a book");
			System.out.println("0. Exit");
			BufferedReader reader = new BufferedReader(new InputStreamReader(
					System.in));
			looper = Integer.parseInt(reader.readLine());
			
			Orders objOrders = new Orders(dbmsCon);

			switch (looper)
			{
				case 1:
					System.out.println("Enter ISBN : ");
					String ISBN = reader.readLine();
					
					System.out.println("Enter VendorID : ");
					int vendorID = Integer.parseInt(reader.readLine());
					
					System.out.println("Enter Cost : ");
					double cost = Double.parseDouble(reader.readLine());
					
					System.out.println("Enter Quantity : ");
					int qty = Integer.parseInt(reader.readLine());
					
					int orderID =objOrders.addRequest(ISBN, vendorID, qty,cost); 
					if(orderID==-1)
					{
						System.out.println("Request Failed");
					}
					else
					{
						System.out.println("Request ID : " + orderID);
					}
					break;
				case 2:
					System.out.println("Enter RequestID : ");
					int requestID = Integer.parseInt(reader.readLine());
										
					if(objOrders.updateRequestStatus(requestID,"SHIPPED")==true)
					{
						System.out.println("Status updated to SHIPPED");
					}
					else
					{
						System.out.println("Status Updating Failed!");
					}
					break;
				case 3:
					System.out.println("Enter RequestID : ");
					int requestID3 = Integer.parseInt(reader.readLine());
										
					if(objOrders.completeRequest(requestID3)==true)
					{
						System.out.println("Status updated to completed and warehouse quantity updated");
					}
					else
					{
						System.out.println("Status Updating Failed!");
					}
					break;
				case 4:
					System.out.println("Enter RequestID : ");
					int requestID2 = Integer.parseInt(reader.readLine());
					
					if(objOrders.cancelRequest(requestID2)==true)
					{
						System.out.println("Request Cancelled");
					}
					else
					{
						System.out.println("Could Not Cancel Request !");
					}
					break;
				case 5:
					System.out.println("Enter RestockID : ");
					int restockID = Integer.parseInt(reader.readLine());
					
					if(objOrders.updateRestockRequest(restockID)==true)
					{
						System.out.println("Restock Updated");
					}
					else
					{
						System.out.println("Could not update Status");
					}
					break;
				case 6:
					System.out.println("Enter ISBN : ");
					String  newISBN = reader.readLine();
					
					System.out.println("Enter Title : ");
					String newTitle = reader.readLine();
					
					System.out.println("Enter Publisher : ");
					String newPublisher = reader.readLine();
					
					System.out.println("Enter Author : ");
					String newAuthor = reader.readLine();
					
					System.out.println("Enter warehouse qty : ");
					int newWarehouseQty = Integer.parseInt(reader.readLine());
					
					System.out.println("Enter Price : ");
					double newPrice = Double.parseDouble(reader.readLine());
					
					Vendor vbookadd = new Vendor(dbmsCon);
					if(vbookadd.addBook(newISBN, newTitle, newPublisher, newWarehouseQty, newAuthor, newPrice)==false)
					{
						System.out.println("Failed to add a book ");
					}
					else
					{
						System.out.println("Book added successfully");
					}
				case 0:
					break;
				default:
					System.out.println("Please enter a choice between 0 to 5");
					break;
			}

		} while (looper != 0);
	}
}
