/*
 * Created by 4NF on 28th November 2012
 * Modified by 4NF on 28th November 2012
 */
package bat;

/**
 * @author 4NF
 *
 */
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.Calendar;

public class BranchManagerCLI {
	// Global Variables
	private Connection con;
	int bmStaffID = 0;

	public BranchManagerCLI(Connection con, int staffID) {
		this.con = con;
		bmStaffID = staffID;
	}

	ResultSet rs = null;
	
	/**
	 * This generates the list of all functions that the Branch Manager is allowed to perform. 
	 * 
	 * @throws IOException
	 * @throws SQLException
	 */
	public void showCLI() throws IOException, SQLException {
		int looper = 1;

		do {
			System.out.println("Enter your choice:");
			System.out.println("1. Get Store Purchase History");
			System.out.println("2. Get Staff Information");
			System.out.println("3. Get Vendor Information");
			System.out.println("4. Get Customer Purchase History");
			System.out.println("5. Get Customer Current Outstanding");
			System.out.println("6. Get Individual Staff Sales");
			System.out.println("7. Get Store Sales");
			System.out.println("0. Exit");
			BufferedReader reader = new BufferedReader(new InputStreamReader(
					System.in));
			looper = Integer.parseInt(reader.readLine());

			Billing objBilling = new Billing(con);
			Report objReport = new Report(con);
			Calendar dtcal = Calendar.getInstance();
			SimpleDateFormat dtFormat = new SimpleDateFormat("MMM/dd/yyyy");
			String dtString = dtFormat.format(dtcal.getTime());
			int customerID = 0;
			String startDate = "";
			String endDate = "";
			int storeID = 0;
			int staffID = 0;
			
			switch (looper) {
			case 1:
				storeID = objReport.getStoreID(bmStaffID);
				rs = objReport.getStoreRequestHistory(storeID);
				System.out.println("StoreID RestockOrderID  StaffID  BookTitle  BookISBN   Quantity   RestockStatus");
				System.out.println("-------------------------------------------------------------------------------");
				while (rs.next()) {
					System.out.println(rs.getInt("StoreID") + ", "
							+ rs.getInt("RestockOrderID") + ", "
							+ rs.getInt("StaffIdent") + ", "
							+ rs.getString("BookTitle") + ", "
							+ rs.getString("BookISBN") + ", "
							+ rs.getInt("Qty") + ", "
							+ rs.getString("RestockStatus"));
				}
				break;

			case 2:
				storeID = objReport.getStoreID(bmStaffID);
				rs = objReport.getStaffInformationOfAStore(storeID);
				System.out.println("Jobtitle   StaffID  FirstName  LastName  Gender   Phone   Address    Email    DOB      Department     Salary    JoinedDate");
				System.out.println("--------------------------------------------------------------------------------------------------------------------------");
				while (rs.next()) {
					System.out.println(rs.getString("JobTitle") + ", "
							+ rs.getInt("StaffID") + ", "
							+ rs.getString("FirstName") + ", "
							+ rs.getString("LastName") + ", "
							+ rs.getString("Gender") + ", "
							+ rs.getString("Phone") + ", "
							+ rs.getString("Address") + ", "
							+ rs.getString("Email") + ", " + rs.getDate("DOB")
							+ ", " + rs.getString("Department") + ", "
							+ rs.getString("Salary") + ", "
							+ rs.getDate("JoinedDate"));
				}
				break;

			case 3:
				rs = objReport.getVendorInformation();
				System.out.println("VendorID   Name    Address    Phone");
				System.out.println("-----------------------------------");
				while (rs.next()) {
					System.out.println(rs.getString("VendorID") + ", "
							+ rs.getString("Name") + ", "
							+ rs.getString("Address") + ", "
							+ rs.getString("Phone"));
				}
				break;

			case 4:
				System.out.println("Enter Customer ID:");
				customerID = Integer.parseInt(reader.readLine());
				System.out.println("Enter Start Date: (dd-MMM-YY):");
				startDate = reader.readLine();
				startDate = "'" + startDate + "'";
				System.out.println("Enter End Date: (dd-MMM-YY):");
				endDate = reader.readLine();
				endDate = "'" + endDate + "'";
				rs = objReport.getPurchaseHistory(startDate, endDate,
						customerID);
				while (rs.next()) {
					System.out.println(rs.getString("CustomerFName") + " "
							+ rs.getString("CustomerLName"));
					break;
				}
				System.out.println("Purchase History:");
				rs.beforeFirst();
				System.out.println("OrderNumber   BookTitle   Price   Qty   Amount   PaymentStatus");
				System.out.println("--------------------------------------------------------------");
				while (rs.next()) {
					System.out.println(rs.getString("OrderNumber") + ", "
							+ rs.getString("BookTitle") + ","
							+ rs.getInt("Price") + "," + rs.getInt("Qty") + ","
							+ rs.getInt("Amount") + ","
							+ rs.getString("PaymentStatus"));
				}
				break;

			case 5:
				System.out.println("Enter Customer ID:");
				customerID = Integer.parseInt(reader.readLine());
				rs = objBilling.getCurrentOutstanding(dtString, customerID);
				int count = 0;
				while (rs.next()) {
					System.out.println("For customer with ID " + customerID + " the current Oustanding amount is "+ rs.getInt("Amount"));
					count++;
				}
				if (count == 0)
					System.out.println("For customer with ID " + customerID + " the current outstanding amount is 0");
				break;
				
			case 6:
				int staffSalesLooper = 1;
				while(staffSalesLooper == 1)
				{
					System.out.println("Enter Staff ID:");
					staffID = Integer.parseInt(reader.readLine());
					int staff_StoreID = objReport.getStoreID(staffID);
					int manager_StoreID = objReport.getStoreID(bmStaffID);
					if(staff_StoreID == manager_StoreID)
					{
						staffSalesLooper = 0;
						break;
					}
					else
					{
						System.out.println("This Staff Member does not belong to your store. Enter a Staff ID of a person of your store.");
					}
				}
				System.out.println("Enter Start Date: (dd-MMM-YY):");
				startDate = reader.readLine();
				startDate = "'" + startDate + "'";
				System.out.println("Enter End Date: (dd-MMM-YY):");
				endDate = reader.readLine();
				endDate = "'" + endDate + "'";
				rs = objReport.getStaffSales(startDate, endDate, staffID);
				System.out.println("StaffID   TotalSales");
				System.out.println("--------------------");
				while (rs.next()) {
					System.out.println(rs.getString("StaffID") + ", "
							+ rs.getString("TotalSales"));
				}
				break;
				
			case 7:
				System.out.println("Enter Start Date: (dd-MMM-YY):");
				startDate = reader.readLine();
				startDate = "'" + startDate + "'";
				System.out.println("Enter End Date: (dd-MMM-YY):");
				endDate = reader.readLine();
				endDate = "'" + endDate + "'";
				int sum = 0;
				storeID = objReport.getStoreID(bmStaffID);
				rs = objReport.getStoreSalesOfAStore(startDate, endDate, storeID);
				System.out.println("StoreID   Amount");
				System.out.println("----------------1");
				while (rs.next()) {
					System.out.println(rs.getString("StoreID") + ", "
							+ rs.getString("Amount"));
					sum = sum + Integer.parseInt(rs.getString("Amount"));
				}
				System.out.println("Total Sales: " + sum);
				break;


			case 0:
				System.out.println("Good Bye");
				break;
			default:
				System.out.println("Enter a number from the given choices.");
			}
		} while (looper != 0);
	}
	
}
