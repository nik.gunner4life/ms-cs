/*
 * Created by 4NF on 28th November 2012
 * Modified by 4NF on 28th November 2012
 */
package bat;

/**
 * @author 4NF
 *
 */
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.Calendar;

public class BillingCLI {
	/**
	 * Set the connection to be used.
	 * 
	 * @param con
	 */
	private Connection con;

	public BillingCLI(Connection con) {
		this.con = con;
	}

	/**
	 * This generates the list of all functions that Billing Members are allowed
	 * to perform.
	 */
	public void showCLI() throws IOException, SQLException {
		int looper = 1;
		ResultSet rs = null;
		do {
			System.out.println("Enter your choice:");
			System.out.println("1. Get Customer Current Outstanding");
			System.out.println("2. Update Customer Bill");
			System.out.println("3. Generate Customer Bill");
			System.out.println("4. Get Customer Purchase History");
			System.out.println("5. Generate Customer Billing Cycle");
			System.out.println("0. Exit");
			BufferedReader reader = new BufferedReader(new InputStreamReader(
					System.in));
			looper = Integer.parseInt(reader.readLine());

			Billing objBilling = new Billing(con);
			Report objReport = new Report(con);
			Calendar dtcal = Calendar.getInstance();
			SimpleDateFormat dtFormat = new SimpleDateFormat("MMM/dd/yyyy");
			String dtString = dtFormat.format(dtcal.getTime());
			int customerID = 0;
			int orderID = 0;
			String startDate = "";
			String endDate = "";

			switch (looper) {
			case 1:
				System.out.println("Enter Customer ID:");
				customerID = Integer.parseInt(reader.readLine());
				rs = objBilling.getCurrentOutstanding(dtString, customerID);
				int count = 0;
				while (rs.next()) {
					System.out.println("For customer with ID " + customerID
							+ " the current Oustanding amount is "
							+ rs.getInt("Amount"));
					count++;
				}
				if (count == 0)
					System.out.println("For customer with ID " + customerID
							+ " the current outstanding amount is 0");
				break;

			case 2:
				System.out.println("Enter Customer ID:");
				customerID = Integer.parseInt(reader.readLine());
				String paymentStatus = objBilling.payBill(customerID);
				if (paymentStatus == "success")
					System.out.println(paymentStatus);
				break;

			case 3:
				System.out.println("Enter Customer ID:");
				customerID = Integer.parseInt(reader.readLine());
				rs = objBilling.getBillsToSend(dtString, customerID);
				while (rs.next()) {
					System.out.println(rs.getString("CustomerFName") + " "
							+ rs.getString("CustomerLName"));
					break;
				}

				System.out.println("Your bill is:");
				rs.beforeFirst();
				int sum = 0;
				while (rs.next()) {
					System.out.println(rs.getString("OrderNumber") + ", "
							+ rs.getString("BookTitle") + ","
							+ rs.getInt("Price") + "," + rs.getInt("Qty") + ","
							+ rs.getInt("Amount"));
					sum = sum + rs.getInt("Amount");
				}
				System.out.println("Total Amount: " + sum);
				break;

			case 4:
				System.out.println("Enter Customer ID:");
				customerID = Integer.parseInt(reader.readLine());
				System.out.println("Enter Start Date: (dd-MMM-YY):");
				startDate = reader.readLine();
				startDate = "'" + startDate + "'";
				System.out.println("Enter End Date: (dd-MMM-YY):");
				endDate = reader.readLine();
				endDate = "'" + endDate + "'";
				rs = objReport.getPurchaseHistory(startDate, endDate,
						customerID);
				while (rs.next()) {
					System.out.println(rs.getString("CustomerFName") + " "
							+ rs.getString("CustomerLName"));
					break;
				}
				System.out.println("Purchase History:");
				rs.beforeFirst();
				while (rs.next()) {
					System.out.println(rs.getString("OrderNumber") + ", "
							+ rs.getString("BookTitle") + ","
							+ rs.getInt("Price") + "," + rs.getInt("Qty") + ","
							+ rs.getInt("Amount") + ","
							+ rs.getString("PaymentStatus"));
				}
				break;
				
			case 5:
				System.out.println("Enter Customer ID:");
				customerID = Integer.parseInt(reader.readLine());
				System.out.println("Enter Start Date: (MMM-YY):");
				startDate = reader.readLine();
				startDate = "01-" + startDate;
				rs = objBilling.getBillingCycle(startDate, customerID);
				count = 0;
				while (rs.next()) {
					count++;
				}
				if (count > 0) {
					rs.beforeFirst();
					while (rs.next()) {
						System.out.println(rs.getString("CustomerFName") + " "
								+ rs.getString("CustomerLName"));
						break;
					}
					System.out.println("Billing Cycle:");
					rs.beforeFirst();
					while (rs.next()) {
						System.out.println(rs.getString("OrderNumber") + ", "
								+ rs.getString("BookTitle") + ","
								+ rs.getInt("Price") + "," + rs.getInt("Qty")
								+ "," + rs.getInt("Amount") + ","
								+ rs.getString("PaymentStatus"));
					}
				}
				else
				{
					System.out.println("No Purchases were made.");
				}
				break;

			case 0:
				System.out.println("Good Bye");
				break;
			default:
				System.out.println("Enter a number from the given choices.");
			}
		} while (looper != 0);
	}

}
