/*
 * Created by 4NF on 28th November 2012
 * Modified by 4NF on 28th November 2012
 */
package bat;

/**
 * @author 4NF
 *
 */
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Locale;
import java.sql.Date;

public class ChainManagerCLI {
	/**
	 * Set the connection to be used.
	 * 
	 * @param con
	 */
	private Connection con;

	public ChainManagerCLI(Connection con) {
		this.con = con;
	}

	ResultSet rs = null;

	/**
	 * This generates the list of all the functions that the Chain Manager is
	 * allowed to perform.
	 * 
	 * @throws IOException
	 * @throws SQLException
	 */
	public void showCLI() throws IOException, SQLException, Exception {
		int looper = 1;

		do {
			System.out.println("Enter your choice:");
			System.out.println("1. Get Individual Store Purchase History");
			System.out.println("2. Get Overall Store Purchase Information");
			System.out.println("3. Get Vendor Information");
			System.out.println("4. Get All Staff Information");
			System.out
					.println("5. Get Life time achievement Award Winners List");
			System.out.println("6. Get Store Request History");
			System.out.println("7. Get Vendor Request History");
			System.out.println("8. Get All Vendors Request History");
			System.out.println("9. Generate Vendor Bills");
			System.out.println("10. Pay A vendor");
			System.out.println("11. Add a staff");
			System.out.println("12. Add a Vendor");
			System.out.println("13. Add a Contract");
			System.out.println("14. Delete a Contract");
			System.out.println("15. Fire a staff member");
			System.out.println("16. Get Contract Information");
			System.out.println("17. Get Vendor Billing Cycle");

			System.out
					.println("18. Get Salary Details of Staff Grouped by Jobtitle");
			System.out.println("19. Add a store");
			System.out.println("0. Exit");                                                                                                                    
			BufferedReader reader = new BufferedReader(new InputStreamReader(
					System.in));
			looper = Integer.parseInt(reader.readLine());

			Billing objBilling = new Billing(con);
			Report objReport = new Report(con);
			Calendar dtcal = Calendar.getInstance();
			SimpleDateFormat dtFormat = new SimpleDateFormat("MMM/dd/yyyy");
			String dtString = dtFormat.format(dtcal.getTime());
			int customerID = 0;
			String startDate = "";
			String endDate = "";
			int storeID = 0;
			int vendorID = 0;
			int sum = 0;
			float totalSales = 0;
			int count = 0;
			int date, month, year;
			Calendar tempC;

			switch (looper) {
			case 1:
				System.out.println("Enter Store ID:");
				storeID = Integer.parseInt(reader.readLine());
				rs = objReport.getStoreRequestHistory(storeID);
				System.out
						.println("StoreID   RestockOrderID   StaffID   BookTitle    BookISBN   Qty   RestockStatus");
				System.out
						.println("--------------------------------------------------------------------------------");
				while (rs.next()) {
					System.out.println(rs.getInt("StoreID") + ", "
							+ rs.getInt("RestockOrderID") + ", "
							+ rs.getInt("StaffIdent") + ", "
							+ rs.getString("BookTitle") + ", "
							+ rs.getString("BookISBN") + ", "
							+ rs.getInt("Qty") + ", "
							+ rs.getString("RestockStatus"));
				}
				break;

			case 2:
				System.out.println("Enter Start Date: (dd-MMM-YY):");
				startDate = reader.readLine();
				startDate = "'" + startDate + "'";
				System.out.println("Enter End Date: (dd-MMM-YY):");
				endDate = reader.readLine();
				endDate = "'" + endDate + "'";
				rs = objReport.getAllStoresSales(startDate, endDate);
				System.out.println("StoreID   Amount");
				System.out.println("----------------");
				while (rs.next()) {
					System.out.println(rs.getString("StoreID") + ", "
							+ rs.getString("Amount"));
					sum = sum + Integer.parseInt(rs.getString("Amount"));
				}
				System.out.println("Total Sales: " + sum);
				break;

			case 3:
				rs = objReport.getVendorInformation();
				System.out.println("VendorID   Name   Address   Phone");
				System.out.println("---------------------------------");
				while (rs.next()) {
					System.out.println(rs.getString("VendorID") + ", "
							+ rs.getString("Name") + ", "
							+ rs.getString("Address") + ", "
							+ rs.getString("Phone"));
				}
				break;

			case 4:
				rs = objReport.getStaffInformation();
				System.out
						.println("JobTitle  StaffID  FirstName  LastName  Gender  Phone  Address  Email  DOB  Department  Salary   JoinedeDate");
				System.out
						.println("------------------------------------------------------------------------------------------------------------");
				while (rs.next()) {
					System.out.println(rs.getString("JobTitle") + ", "
							+ rs.getInt("StaffID") + ", "
							+ rs.getString("FirstName") + ", "
							+ rs.getString("LastName") + ", "
							+ rs.getString("Gender") + ", "
							+ rs.getString("Phone") + ", "
							+ rs.getString("Address") + ", "
							+ rs.getString("Email") + ", " + rs.getDate("DOB")
							+ ", " + rs.getString("Department") + ", "
							+ rs.getString("Salary") + ", "
							+ rs.getDate("JoinedDate"));
				}
				break;

			case 5:
				rs = objReport.getLifeTimeAward();
				System.out.println("Department  StaffID");
				System.out.println("-------------------");
				while (rs.next()) {
					System.out.println(rs.getString("Department") + ", "
							+ rs.getInt("StaffID"));
				}
				break;

			case 6:
				System.out.println("Enter Store ID:");
				storeID = Integer.parseInt(reader.readLine());
				rs = objReport.getStoreRequestHistory(storeID);
				System.out
						.println("StoreID   RestockOrderID   StaffID   BookTitle   BookISBN   Qty   RestockStatus");
				System.out
						.println("-------------------------------------------------------------------------------");
				while (rs.next()) {
					System.out.println(rs.getInt("StoreID") + ", "
							+ rs.getInt("RestockOrderID") + ", "
							+ rs.getInt("StaffIdent") + ", "
							+ rs.getString("BookTitle") + ", "
							+ rs.getString("BookISBN") + ", "
							+ rs.getInt("Qty") + ", "
							+ rs.getString("RestockStatus"));
				}
				break;

			case 7:
				System.out.println("Enter the Vendor ID:");
				vendorID = Integer.parseInt(reader.readLine());
				System.out.println("Enter Start Date: (dd-MMM-YY):");
				startDate = reader.readLine();
				startDate = "'" + startDate + "'";
				System.out.println("Enter End Date: (dd-MMM-YY):");
				endDate = reader.readLine();
				endDate = "'" + endDate + "'";
				rs = objReport.getVendorRequestHistory(startDate, endDate,
						vendorID);
				totalSales = 0;
				System.out
						.println("RequestID   ISBN   Cost   Qty   Amount   RequestStatus");
				System.out
						.println("------------------------------------------------------");
				while (rs.next()) {
					System.out.println(rs.getString("RequestID") + ", "
							+ rs.getString("ISBN") + ", "
							+ rs.getString("Cost") + ", " + rs.getString("Qty")
							+ ", " + rs.getString("Amount") + ", "
							+ rs.getString("RequestStatus"));
					totalSales = totalSales
							+ Float.parseFloat(rs.getString("Amount"));
				}
				System.out.println("Total Vendor Acquisition: " + totalSales);
				break;

			case 8:
				System.out.println("Enter Start Date: (dd-MMM-YY):");
				startDate = reader.readLine();
				startDate = "'" + startDate + "'";
				System.out.println("Enter End Date: (dd-MMM-YY):");
				endDate = reader.readLine();
				endDate = "'" + endDate + "'";
				rs = objReport.getAllVendorsRequestHistory(startDate, endDate);
				totalSales = 0;
				System.out
						.println("RequestID   ISBN   Cost   Qty   Amount   RequestStatus");
				System.out
						.println("------------------------------------------------------");
				while (rs.next()) {
					System.out.println(rs.getString("RequestID") + ", "
							+ rs.getString("ISBN") + ", "
							+ rs.getString("Cost") + ", " + rs.getString("Qty")
							+ ", " + rs.getString("Amount") + ", "
							+ rs.getString("RequestStatus"));
					totalSales = totalSales
							+ Float.parseFloat(rs.getString("Amount"));
				}
				System.out.println("Total Vendor Acquisition: " + totalSales);
				break;

			case 9:
				ResultSet vendorList = null;
				vendorList = objBilling.getVendorList();
				while (vendorList.next()) {
					totalSales = 0;
					count = 0;
					rs = objBilling.generateVendorBill(dtString,
							vendorList.getInt("VendorID"));
					while (rs.next()) {
						count++;
					}
					if (count != 0) {
						System.out.println("The bill for Vendor with ID "
								+ vendorList.getInt("VendorID"));
						rs.beforeFirst();
						System.out
								.println("RequestID   ISBN   Cost   Qty   Amount   RequestDate");
						System.out
								.println("----------------------------------------------------");
						while (rs.next()) {
							System.out.println(rs.getString("RequestID") + ", "
									+ rs.getString("ISBN") + ", "
									+ rs.getString("Cost") + ", "
									+ rs.getString("Qty") + ", "
									+ rs.getString("Amount") + ", "
									+ rs.getString("RequestDate"));
							totalSales = totalSales
									+ Float.parseFloat(rs.getString("Amount"));
							count++;
						}
						System.out.println("The total expenditure is "
								+ totalSales);
					}

				}
				break;

			case 10:
				boolean paymentStatus = false;
				System.out.println("Enter a vendor ID:");
				vendorID = Integer.parseInt(reader.readLine());
				paymentStatus = objBilling.payAVendor(vendorID);
				if (paymentStatus) {
					System.out.println("Payment completed successfully");
				} else {
					System.out
							.println("There was some error. Please try again later ... ");
				}
				break;
			case 11:
				System.out.println("Enter the SSN:");
				String SSN = reader.readLine();

				System.out.println("Enter the first Name:");
				String firstName = reader.readLine();

				System.out.println("Enter the last Name:");
				String lastName = reader.readLine();

				System.out.println("Enter the gender:");
				String gender = reader.readLine();

				System.out.println("Enter the date of birth:");
				System.out.print("Day: ");
				date = Integer.parseInt(reader.readLine());
				System.out.print("Month: ");
				month = Integer.parseInt(reader.readLine());
				System.out.print("Year: ");
				year = Integer.parseInt(reader.readLine());
				tempC = new GregorianCalendar(year, month - 1, date);
				Date DOB = new Date(tempC.getTimeInMillis());

				System.out.println("Enter the store ID:");
				storeID = Integer.parseInt(reader.readLine());

				System.out.println("Enter the Join Date:");
				System.out.print("Day: ");
				date = Integer.parseInt(reader.readLine());
				System.out.print("Month: ");
				month = Integer.parseInt(reader.readLine());
				System.out.print("Year: ");
				year = Integer.parseInt(reader.readLine());
				tempC = new GregorianCalendar(year, month - 1, date);
				Date joinDate = new Date(tempC.getTimeInMillis());

				System.out.println("Enter the job Title:");
				String jobTitle = reader.readLine();

				System.out.println("Enter the department:");
				String department = reader.readLine();

				System.out.println("Enter the salary(integer):");
				int salary = Integer.parseInt(reader.readLine());

				System.out.println("Enter the phone:");
				String phone = reader.readLine();

				System.out.println("Enter the address:");
				String address = reader.readLine();

				System.out.println("Enter the email:");
				String email = reader.readLine();

				StaffHandler sh = new StaffHandler(con);
				int newStaffID = sh.newStaff(SSN, firstName, lastName, gender,
						DOB, storeID, joinDate, jobTitle, department, salary,
						phone, address, email);

				if (newStaffID == -1) {
					System.out.println("Failed to add new Staff");
				} else {
					System.out
							.println("New Staff successfully added with ID = "
									+ newStaffID);
				}
				break;
			case 12:
				System.out.println("Enter the Vendor Name:");
				String vendorName = reader.readLine();

				System.out.println("Enter the Address:");
				String vendorAddress = reader.readLine();

				System.out.println("Enter the Phone:");
				String vendorPhone = reader.readLine();

				Vendor vadd = new Vendor(con);
				int newVendorID = vadd.addVendor(vendorName, vendorAddress,
						vendorPhone);
				if (newVendorID == -1) {
					System.out.println("The Vendor could not be added");
				} else {
					System.out
							.println("Vendor added successfully with VendorID="
									+ newVendorID);
				}
				break;

			case 13:
				System.out.println("Enter the Vendor ID:");
				int addContractVendorID = Integer.parseInt(reader.readLine());

				System.out.println("Enter the Start Date:");
				System.out.print("Day: ");
				date = Integer.parseInt(reader.readLine());
				System.out.print("Month: ");
				month = Integer.parseInt(reader.readLine());
				System.out.print("Year: ");
				year = Integer.parseInt(reader.readLine());
				tempC = new GregorianCalendar(year, month - 1, date);
				Date contractStartDate = new Date(tempC.getTimeInMillis());

				System.out.println("Enter the end Date(DD-MMM-YYYY):");
				System.out.print("Day: ");
				date = Integer.parseInt(reader.readLine());
				System.out.print("Month: ");
				month = Integer.parseInt(reader.readLine());
				System.out.print("Year: ");
				year = Integer.parseInt(reader.readLine());
				tempC = new GregorianCalendar(year, month - 1, date);
				Date contractEndDate = new Date(tempC.getTimeInMillis());

				System.out.println("Enter the ISBN:");
				String contractISBN = reader.readLine();

				Vendor contractVendorObj = new Vendor(con);
				int contractSuccess = contractVendorObj.addContract(
						addContractVendorID, contractStartDate,
						contractEndDate, contractISBN);
				if (contractSuccess == -1) {
					System.out.println("Contract Was not Added Successfully");
				} else {
					System.out.println("Contract added Successfully with ID="
							+ contractSuccess);
				}
				break;
			case 14:
				System.out.println("Enter the Contract ID:");
				int delContractID = Integer.parseInt(reader.readLine());

				Vendor vdelContract = new Vendor(con);
				if (vdelContract.deleteContract(delContractID) == false) {
					System.out.println("The Contract could not be deleted");
				} else {
					System.out.println("Contract deleted successfully");
				}
				break;
			case 15:
				System.out.println("Enter the Staff ID:");
				int delStaffID = Integer.parseInt(reader.readLine());

				StaffHandler sdel = new StaffHandler(con);
				if (sdel.deleteStaff(delStaffID) == false) {
					System.out.println("The Staff could not be deleted");
				} else {
					System.out.println("Staff deleted successfully");
				}
				break;

			case 16:
				rs = objReport.getContractInformation();
				System.out
						.println("VendorID   ContractID   StartDate   EndDate   ISBN");
				System.out
						.println("------------------------------------------------");
				while (rs.next()) {
					System.out.println(rs.getString("VendorID") + ", "
							+ rs.getString("ContractID") + ", "
							+ rs.getString("StartDate") + ", "
							+ rs.getString("EndDate") + ", "
							+ rs.getString("ISBN"));
				}
				break;
			case 17:
				System.out.println("Enter the Vendor ID:");
				vendorID = Integer.parseInt(reader.readLine());
				System.out.println("Enter Start Date: (MMM-YY):");
				startDate = reader.readLine();
				startDate = "01-" + startDate;
				rs = objReport.getVendorBillingInformation(startDate, vendorID);
				totalSales = 0;
				System.out
						.println("RequestID   ISBN   Cost   Qty   Amount   RequestStatus");
				System.out
						.println("------------------------------------------------------");
				while (rs.next()) {
					System.out.println(rs.getString("RequestID") + ", "
							+ rs.getString("ISBN") + ", "
							+ rs.getString("Cost") + ", " + rs.getString("Qty")
							+ ", " + rs.getString("Amount") + ", "
							+ rs.getString("RequestStatus"));
					totalSales = totalSales
							+ Float.parseFloat(rs.getString("Amount"));
				}
				System.out.println("Total Vendor Acquisition: " + totalSales);
				break;

			case 18:
				rs = objReport.getAverageSalaryofStaff();
				System.out
						.println("Jobtitle   MaximumSalary   MinSalary   AvgSalary   CountOfEmployees");
				System.out.println("------------------------");
				while (rs.next()) {
					System.out.println(rs.getString("Jobtitle") + ", "
							+ rs.getString("MaxSalary") + ", "
							+ rs.getString("MinSalary") + ", "
							+ rs.getString("AvgSalary") + ", "
							+ rs.getString("NoOfEmployees"));
				}
				break;
			case 19:
				System.out.println("Enter the address of the store:");
				String storeAddress = reader.readLine();

				Stores str = new Stores(con);
				int newstoreID = str.addStore(storeAddress);
				if (newstoreID == -1) {
					System.out.println("The Store could not be added");
				} else {
					System.out
							.println("Store added successfully with store ID "
									+ newstoreID);
				}
				break;
			case 0:
				System.out.println("Good Bye");
				break;

			default:
				System.out.println("Enter a number from the given choices.");
			}
		} while (looper != 0);
	}
}
