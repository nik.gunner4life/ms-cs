package bat;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;

import oracle.jdbc.internal.OracleTypes;

public class Stores {
	
	private Connection con;
	
	/**
	 * 
	 * Set the connection to be used.
	 * 
	 * @param cnn
	 */
	public Stores(Connection cnn)
	{
		con = cnn;
	}
	
	/**
	 * 
	 * Adds a store to the database.
	 * 
	 * @param address			The address of the store to be added.
	 * @return					The ID of the store that has been added.
	 * @throws SQLException
	 */
	public int addStore(String address) throws SQLException{
		
		int storeid=-1;
		CallableStatement addStore = null;
		String addStoreQuery = "BEGIN INSERT INTO store(address) VALUES(?) returning storeid into ?;END;";
		
		addStore= con.prepareCall(addStoreQuery);
		addStore.setString(1, address);
		addStore.registerOutParameter(2,OracleTypes.NUMBER);
		addStore.execute();
		storeid = addStore.getInt(2);
		return storeid;
	}
}
