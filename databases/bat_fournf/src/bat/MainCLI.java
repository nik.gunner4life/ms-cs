/*
 * Created by 4NF on 28th November 2012
 * Modified by 4NF on 28th November 2012
 */
package bat;

/**
 * @author 4NF
 *
 */
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.Calendar;

public class MainCLI {
	/**
	 * Set the connection to be used.
	 * 
	 * @param con
	 */
	private Connection con;
	public static int staffID = 0;

	public MainCLI(Connection con) {
		this.con = con;
	}
	
	/**
	 * This is the Actual Interface which handled the Login and calls the Relevant CLI based on the staffID.
	 *  
	 * @param args			Command-line arguments. None of them will be used.
	 * @throws Exception
	 */
	public static void main(String args[]) throws Exception {
		String jdbcURL = "jdbc:oracle:thin:@ora.csc.ncsu.edu:1521:orcl";
		String user = "pswamy";
		String password = "001085721";

		try {

			Class.forName("oracle.jdbc.driver.OracleDriver");
			Connection con = DriverManager.getConnection(jdbcURL, user,
					password);
			MainCLI k = new MainCLI(con);
			BufferedReader reader = new BufferedReader(new InputStreamReader(
					System.in));
			int looper = 0;
			String jobTitle = "";
			int loginLooper = 1;
			int staff_ID=-1;
			do {
				System.out.println("Enter 1 to login and 0 to exit");
				loginLooper = Integer.parseInt(reader.readLine());
				if (loginLooper == 1) {
					do {
						System.out.println("Enter your StaffID:");
						staffID = Integer.parseInt(reader.readLine());
						System.out.println("Enter your Password:");
						String staffPassword = reader.readLine();
						Billing objBilling = new Billing(con);
						ResultSet rs = objBilling.getLoginDetails();
						while (rs.next()) {
							if (rs.getInt("StaffID") == staffID
									&& rs.getString("Password")
											.equalsIgnoreCase(staffPassword)) {
								staff_ID = rs.getInt("StaffID");
								jobTitle = objBilling.getJobTitle(staff_ID);
								break;
							}
						}

						// Call the Job Title Specific ShowCLI
						if (jobTitle.length() > 0) {
							looper = 1;
						} else {
							System.out.println("Incorrect ID or Password");
						}

					} while (looper == 0);

					if (jobTitle.equalsIgnoreCase("billing staff")) {
						BillingCLI objBillingCLI = new BillingCLI(con);
						objBillingCLI.showCLI();
					} else if (jobTitle.equalsIgnoreCase("salesperson")) {
						SalesmanCLI sman = new SalesmanCLI(con,staff_ID);
						sman.showCLI();
					} else if (jobTitle.equalsIgnoreCase("chairman")) {
						ChainManagerCLI objChairManagerCLI = new ChainManagerCLI(
								con);
						objChairManagerCLI.showCLI();
					} else if (jobTitle.equalsIgnoreCase("merchandise stocker")) {
						MerchandiseStocker objMerchadiseStocker = new MerchandiseStocker(
								con);
						objMerchadiseStocker.showCLI();
					} else if (jobTitle.equalsIgnoreCase("branch manager")) {
						BranchManagerCLI objBranchManagerCLI = new BranchManagerCLI(
								con, staffID);
						objBranchManagerCLI.showCLI();
					} else
						System.out
								.println("Something went wrong. Please Restart Code ... ");

				} else {
					System.out.println("Good Bye");
				}
			} while (loginLooper == 1);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
