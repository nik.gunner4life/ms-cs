/*
 * Created by 4NF on 22nd November 2012
 * Modified by 4NF on 29th November 2012
 */
package bat;

/**
 * @author 4NF
 *
 */
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.Calendar;

public class Report {
	private Connection con;

	/**
	 * 
	 * Set the connection to be used.
	 * 
	 * @param con
	 */
	public Report(Connection con) {
		this.con = con;
	}

	/**
	 * 
	 * Gets the Purchase History of a customer
	 * 
	 * @param startDate
	 *            The startDate for purchase history is passed
	 * @param endDate
	 *            The end date for purchase history is passed
	 * @param customerID
	 *            The customer ID for purchase history is passed
	 * @return This returns a resultset which contains all the tuples.
	 */
	public ResultSet getPurchaseHistory(String startDate, String endDate,
			int customerID) {
		ResultSet rs = null;
		try {

			Statement stmt = con.createStatement(
					ResultSet.TYPE_SCROLL_INSENSITIVE,
					ResultSet.CONCUR_UPDATABLE);
			rs = stmt
					.executeQuery("select distinct S1.FirstName as CustomerFname, S1.LastName as CustomerLName, Orders.OrderID as OrderNumber, S2.Title as BookTitle, Orders.Price, Orders.Qty, (Price*Qty) as Amount, PaymentStatus, Orders.StaffID from orders, (select FirstName, LastName, CustomerID from people, customer where people.SSN = customer.SSN) S1, (select Title, Book.ISBN as ISBNo from Book, Orders where Orders.ISBN = Book.ISBN) S2  where Orders.CustomerID = "
							+ customerID
							+ " and S1.CustomerID = Orders.CustomerID and S2.ISBNo = Orders.ISBN and OrderDate between "
							+ startDate + " and " + endDate);

		} catch (Exception e) {
			System.out.println(e);
		}
		return rs;
	}

	/**
	 * 
	 * Gets the Store Restock History of a Particular store
	 * 
	 * @param storeID
	 *            The store ID for Store Restock history is passed
	 * @return This returns a resultset which contains all the tuples.
	 */

	public ResultSet getStoreRequestHistory(int storeID) {
		ResultSet rs = null;
		try {

			Statement stmt = con.createStatement(
					ResultSet.TYPE_SCROLL_INSENSITIVE,
					ResultSet.CONCUR_UPDATABLE);
			rs = stmt
					.executeQuery("select distinct RestockOrderID, S1.StoreID, S1.StaffIdent, S2.Title as BookTitle, S2.ISBNo as BookISBN, Qty, RestockStatus from storerestock, (select distinct storerestock.StaffID as StaffIdent, StoreID from storerestock, worksat where storerestock.staffID = worksat.staffID) S1, (select Title, Book.ISBN as ISBNo from Book, StoreRestock where StoreRestock.ISBN = Book.ISBN) S2 where S1.StoreID = "
							+ storeID
							+ " and S2.ISBNo = storerestock.ISBN and storerestock.StaffID = S1.StaffIdent order by RestockOrderID");
		} catch (Exception e) {
			System.out.println(e);
		}
		return rs;
	}

	/**
	 * 
	 * Gets the Staff Information
	 * 
	 * @return This returns a resultset which contains all the tuples.
	 */

	public ResultSet getStaffInformation() throws SQLException {
		ResultSet rs = null;
		try {

			Statement stmt = con.createStatement(
					ResultSet.TYPE_SCROLL_INSENSITIVE,
					ResultSet.CONCUR_UPDATABLE);
			rs = stmt
					.executeQuery("select staff.jobtitle, staff.staffID, S1.Firstname, S1.lastname, S1.gender, S1.phone, S1.address, S1.email, S1.DOB, staff.department,  staff.salary, staff.joineddate from (select people.SSN, people.firstname, people.lastname, people.gender, people.phone, people_home.address, people.email, people.DOB from people, people_home where people.phone = people_home.phone) S1, staff  where staff.SSN = S1.SSN and staff.employed = 'YES' order by staff.jobtitle ");
		} catch (Exception e) {
			System.out.println(e);
		}
		return rs;
	}

	/**
	 * 
	 * Get Average Salary of the staff members.
	 * 
	 * @return This returns a resultset with the Average Salary of the staff
	 *         members grouped by salary
	 * @throws SQLException
	 */
	public ResultSet getAverageSalaryofStaff() throws SQLException {
		ResultSet rs = null;
		try {

			Statement stmt = con.createStatement(
					ResultSet.TYPE_SCROLL_INSENSITIVE,
					ResultSet.CONCUR_UPDATABLE);
			rs = stmt
					.executeQuery("select jobtitle, Max(salary) as MaxSalary, Min(salary) as MinSalary, Avg(salary) as AvgSalary, Count(jobtitle) as NoOfEmployees from staff group by jobtitle");
		} catch (Exception e) {
			System.out.println(e);
		}
		return rs;
	}

	/**
	 * 
	 * Gets the Staff Information of a Particular Store
	 * 
	 * @return This returns a resultset which contains all the tuples.
	 */

	public ResultSet getStaffInformationOfAStore(int storeID)
			throws SQLException {

		ResultSet rs = null;
		try {

			Statement stmt = con.createStatement(
					ResultSet.TYPE_SCROLL_INSENSITIVE,
					ResultSet.CONCUR_UPDATABLE);
			rs = stmt
					.executeQuery("select S2.storeID, S2.staffID, S2.jobtitle, S1.Firstname, S1.lastname, S1.gender, S1.phone, S1.address, S1.email, S1.DOB, S2.department,  S2.salary, S2.joineddate from (select people.SSN, people.firstname, people.lastname, people.gender, people.phone, people_home.address, people.email, people.DOB from people, people_home where people.phone = people_home.phone)S1, (select Staff.SSN, staff.jobtitle, staff.staffID, staff.department, staff.joinedDate, staff.salary, worksat.storeID from staff, worksat where staff.staffID = worksat.staffID and staff.employed='YES')S2 where S1.SSN = S2.SSN and S2.StoreID = "
							+ storeID);
		} catch (Exception e) {
			System.out.println(e);
		}
		return rs;
	}

	/**
	 * 
	 * Gets the Vendor Information
	 * 
	 * @return This returns a resultset which contains all the Vendors
	 *         information.
	 */

	public ResultSet getVendorInformation() throws SQLException {
		ResultSet rs = null;
		try {

			Statement stmt = con.createStatement(
					ResultSet.TYPE_SCROLL_INSENSITIVE,
					ResultSet.CONCUR_UPDATABLE);
			rs = stmt.executeQuery("select * from vendors");
		} catch (Exception e) {
			System.out.println(e);
		}
		return rs;
	}

	/**
	 * 
	 * Gets the Contract Information
	 * 
	 * @return This returns a resultset which contains all the contract
	 *         information.
	 */

	public ResultSet getContractInformation() throws SQLException {
		ResultSet rs = null;
		try {

			Statement stmt = con.createStatement(
					ResultSet.TYPE_SCROLL_INSENSITIVE,
					ResultSet.CONCUR_UPDATABLE);
			rs = stmt
					.executeQuery("select VendorID, ContractID, StartDate, EndDate, ISBN from contract order by vendorID");
		} catch (Exception e) {
			System.out.println(e);
		}
		return rs;
	}

	/**
	 * 
	 * Gets the Store Sales for specific stores
	 * 
	 * @param startDate
	 *            The startDate for store Sales is passed
	 * @param endDate
	 *            The end date for store Sales is passed
	 * @param storeID
	 *            The store ID for store Sales is passed
	 * @return This returns a result set which with sales of individual stores.
	 */

	public ResultSet getStoreSales(String startDate, String endDate, int storeID)
			throws SQLException {

		ResultSet rs = null;
		try {

			Statement stmt = con.createStatement(
					ResultSet.TYPE_SCROLL_INSENSITIVE,
					ResultSet.CONCUR_UPDATABLE);
			rs = stmt
					.executeQuery("select worksat.storeID, SUM(Orders.price * orders.qty) as amount from worksat, orders where worksat.staffID = orders.staffID and StoreID = "
							+ storeID
							+ " and orders.orderdate between "
							+ startDate
							+ " and "
							+ endDate
							+ " group by worksat.storeID");

		} catch (Exception e) {
			System.out.println(e);
		}
		return rs;
	}

	/**
	 * 
	 * Gets the Sales of all Stores
	 * 
	 * @param startDate
	 *            The startDate for sales of all stores is passed
	 * @param endDate
	 *            The end date for sales of all stores is passed
	 * @param customerID
	 *            The customer ID for sales of all stores is passed
	 * @return This returns a resultset which with the individual store Sales
	 *         and overall sales.
	 */

	public ResultSet getAllStoresSales(String startDate, String endDate)
			throws SQLException {

		ResultSet rs = null;
		try {

			Statement stmt = con.createStatement(
					ResultSet.TYPE_SCROLL_INSENSITIVE,
					ResultSet.CONCUR_UPDATABLE);
			rs = stmt
					.executeQuery("select worksat.storeID, SUM(Orders.price * orders.qty) as amount from worksat, orders where worksat.staffID = orders.staffID"
							+ " and orders.orderdate between "
							+ startDate
							+ " and " + endDate + " group by worksat.storeID");

		} catch (Exception e) {
			System.out.println(e);
		}
		return rs;
	}

	/**
	 * 
	 * Gets the Sales of all Stores
	 * 
	 * @param startDate
	 *            The startDate for sales of all stores is passed
	 * @param endDate
	 *            The end date for sales of all stores is passed
	 * @param storeID
	 *            The store ID for sales of the stores is passed
	 * @return This returns a resultset which with the individual store Sales
	 *         and overall sales.
	 */

	public ResultSet getStoreSalesOfAStore(String startDate, String endDate,
			int storeID) throws SQLException {

		ResultSet rs = null;
		try {

			Statement stmt = con.createStatement(
					ResultSet.TYPE_SCROLL_INSENSITIVE,
					ResultSet.CONCUR_UPDATABLE);
			rs = stmt
					.executeQuery("select worksat.storeID, SUM(Orders.price * orders.qty) as amount from worksat, orders where worksat.staffID = orders.staffID"
							+ " and orders.orderdate between "
							+ startDate
							+ " and "
							+ endDate
							+ " and storeID = "
							+ storeID
							+ "group by worksat.storeID");

		} catch (Exception e) {
			System.out.println(e);
		}
		return rs;
	}

	/**
	 * 
	 * Gets the Sales of individual Staff members
	 * 
	 * @param startDate
	 *            The startDate to get sales of specific is passed
	 * @param endDate
	 *            The end date to get sales of specific is passed
	 * @param staffID
	 *            The staff ID to get the individual sales of the staff is
	 *            passed
	 * @return This returns a result set which contains all the tuples.
	 */
	public ResultSet getStaffSales(String startDate, String endDate, int staffID)
			throws SQLException {

		ResultSet rs = null;
		try {

			Statement stmt = con.createStatement(
					ResultSet.TYPE_SCROLL_INSENSITIVE,
					ResultSet.CONCUR_UPDATABLE);
			rs = stmt
					.executeQuery("select staffID, SUM(QTY*PRICE) as TotalSales from orders where StaffID = "
							+ staffID
							+ " and orderdate between "
							+ startDate
							+ " and " + endDate + "group by staffID");

		} catch (Exception e) {
			System.out.println(e);
		}
		return rs;
	}

	/**
	 * 
	 * Gets the Life time achievement awards
	 * 
	 * @return This returns a result set which contains all the staff who have
	 *         been working for the longest time in every department.
	 */
	public ResultSet getLifeTimeAward() throws SQLException {

		ResultSet rs = null;
		try {

			Statement stmt = con.createStatement(
					ResultSet.TYPE_SCROLL_INSENSITIVE,
					ResultSet.CONCUR_UPDATABLE);
			rs = stmt
					.executeQuery("select Department, StaffID from Staff where JoinedDate in (select MIN(JoinedDate) from Staff group by Department) order by Department");

		} catch (Exception e) {
			System.out.println(e);
		}
		return rs;
	}

	/**
	 * 
	 * Gets the Request Made by the Warehouse to Individual Vendors
	 * 
	 * @param startDate
	 *            The startDate for store Sales is passed
	 * @param endDate
	 *            The end date for store Sales is passed
	 * @param storeID
	 *            The store ID for store Sales is passed
	 * @return This returns a result set which with sales of individual stores.
	 */

	public ResultSet getVendorRequestHistory(String startDate, String endDate,
			int vendorID) throws SQLException {

		ResultSet rs = null;
		try {

			Statement stmt = con.createStatement(
					ResultSet.TYPE_SCROLL_INSENSITIVE,
					ResultSet.CONCUR_UPDATABLE);
			String queryStatement = "select RequestID, ISBN, Cost, Qty, (Cost*Qty) As amount, requestStatus from request where VendorID = "
					+ vendorID
					+ " and RequestDate between to_date("
					+ startDate
					+ ", 'DD-MM-YY') and to_date("
					+ endDate + ", 'DD-MM-YY') + 1";
			rs = stmt
					.executeQuery(queryStatement);

		} catch (Exception e) {
			System.out.println(e);
		}
		return rs;
	}

	/**
	 * Get billing information about vendors
	 * 
	 * Get a list of requestID, ISBN, unit cost, quantity, and request cost for
	 * a given vendor ID and cycle.
	 * 
	 * @param startDate
	 *            Specifies the cycle that will be measured.
	 * @param vendorID
	 *            The ID of the vendor about whose Vendor Requests information
	 *            is required.
	 * @return
	 * @throws SQLException
	 */
	public ResultSet getVendorBillingInformation(String startDate, int vendorID)
			throws SQLException {

		ResultSet rs = null;
		try {

			Statement stmt = con.createStatement(
					ResultSet.TYPE_SCROLL_INSENSITIVE,
					ResultSet.CONCUR_UPDATABLE);
			Billing objBilling = new Billing(con);
			String dtstringArray[] = startDate.split("-");
			String theDate = dtstringArray[1] + "/" + dtstringArray[0] + "/"
					+ dtstringArray[2];
			startDate = objBilling.generateDate(theDate, 1);
			String endDate = objBilling.generateDate(theDate, 2);
			rs = stmt
					.executeQuery("select RequestID, ISBN, Cost, Qty, (Cost*Qty) As amount, requestStatus from request where VendorID = "
							+ vendorID
							+ " and RequestDate between to_date("
							+ startDate
							+ ", 'DD-MM-YY') and to_date("
							+ endDate + ", 'DD-MM-YY') + 1");

		} catch (Exception e) {
			System.out.println(e);
		}
		return rs;
	}

	/**
	 * 
	 * Gets the Request Made by the Warehouse to All Vendors
	 * 
	 * @param startDate
	 *            The startDate for store Sales is passed
	 * @param endDate
	 *            The end date for store Sales is passed
	 * @param storeID
	 *            The store ID for store Sales is passed
	 * @return This returns a result set which with sales of individual stores.
	 */

	public ResultSet getAllVendorsRequestHistory(String startDate,
			String endDate) throws SQLException {

		ResultSet rs = null;
		try {

			Statement stmt = con.createStatement(
					ResultSet.TYPE_SCROLL_INSENSITIVE,
					ResultSet.CONCUR_UPDATABLE);
			String queryStatement = "select RequestID, ISBN, Cost, Qty, (Cost*Qty) As amount, requestStatus from request where RequestDate between to_date("
					+ startDate
					+ ", 'DD-MM-YY') and to_date("
					+ endDate + ", 'DD-MM-YY') + 1";
			rs = stmt
					.executeQuery(queryStatement);

		} catch (Exception e) {
			System.out.println(e);
		}
		return rs;
	}

	/**
	 * 
	 * Gets the StoreID of the given StaffID
	 * 
	 * @param staffID
	 *            The staff ID for store Sales is passed
	 * @return This returns a storeID
	 */

	public int getStoreID(int staffID) throws SQLException {

		int storeID = 0;
		ResultSet rs = null;
		try {

			Statement stmt = con.createStatement(
					ResultSet.TYPE_SCROLL_INSENSITIVE,
					ResultSet.CONCUR_UPDATABLE);
			rs = stmt
					.executeQuery("select storeID from worksat where staffID = "
							+ staffID);
			while (rs.next()) {
				storeID = rs.getInt("StoreID");
			}

		} catch (Exception e) {
			System.out.println(e);
		}
		return storeID;
	}

	/**
	 * Lists all books and their prices.
	 * 
	 * @return Returns the result Set containing the books and their prices.
	 * @throws SQLException
	 */
	public ResultSet listPricesOfBooks() throws SQLException {

		int storeID = 0;
		ResultSet rs = null;
		try {

			Statement stmt = con.createStatement(
					ResultSet.TYPE_SCROLL_INSENSITIVE,
					ResultSet.CONCUR_UPDATABLE);
			rs = stmt.executeQuery("select ISBN, Title, Author from Book");

		} catch (Exception e) {
			System.out.println(e);
		}
		return rs;
	}

	/**
	 * Checks if a particular book is available.
	 * 
	 * @return Returns a ResultSet containing the information of the book.
	 * @throws SQLException
	 */
	public ResultSet checkAvailibilityOfBook(String bookTitle,
			String bookAuthor, int staffID) throws SQLException {
		int storeID = getStoreID(staffID);
		ResultSet rs = null;
		try {

			Statement stmt = con.createStatement(
					ResultSet.TYPE_SCROLL_INSENSITIVE,
					ResultSet.CONCUR_UPDATABLE);
			rs = stmt
					.executeQuery("select Book.ISBN, Title, Author from Book where Title = "
							+ bookTitle + " and Author = " + bookAuthor);

		} catch (Exception e) {
			System.out.println(e);
		}
		return rs;
	}

}
