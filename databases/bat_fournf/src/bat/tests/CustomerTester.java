/*
 * 
 */
package bat.tests;

/**
 * @author Roshan George
 *
 */
import java.sql.Connection;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.SQLIntegrityConstraintViolationException;
import java.sql.Statement;
import java.util.Calendar;
import java.util.GregorianCalendar;

import bat.CustomerHandler;

public class CustomerTester {

	private static final String jdbcURL = "jdbc:oracle:thin:@ora.csc.ncsu.edu:1521:orcl";
	private static final String user = "rgeorge2";
	private static final String password = "r0racle";

	/**
	 * @param args
	 */
	public static void main(String[] args) {

		try {
			
			Class.forName("oracle.jdbc.driver.OracleDriver");
			Connection con1 = DriverManager.getConnection(jdbcURL, user, password);
			
			
			if(testInsert(con1)) {
				System.out.println("Adding a customer member went fine.");
			} else {
				System.out.println("Adding a customer member failed.");
			}
			if (testDelete(con1)) {
				System.out.println("Removing the customer member went fine.");
			} else {
				System.out.println("Removing the customer member failed.");
			}
		} catch (Exception e) {
			System.out.println("CustomerTester: " + e);
		}

	}

	public static Boolean testInsert(Connection con) {

		Calendar DOB = new GregorianCalendar(1988, 01, 28);
		Date DOBd = new Date(DOB.getTimeInMillis());

		CustomerHandler customerList = new CustomerHandler(con);

		int customerID = -1;
		try {
			customerID = customerList.newCustomer("1110001110", "Test",
					"Customer", DOBd, "F", "5558675309",
					"customer@example.com", "N16 Crest");
		} catch (SQLIntegrityConstraintViolationException e) {
			e.printStackTrace();
			return false;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}

		try {
			Statement stmt = con.createStatement();
			ResultSet rs = stmt
					.executeQuery("SELECT customerID FROM customer WHERE SSN=1110001110");
			if (rs.next()) {
				System.out.println("Inserted customer with customer ID: "
						+ rs.getString("customerID"));
			} else {
				System.out.println("Failed to insert customer member.");
				return false;
			}
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}

		return true;
	}

	public static Boolean testDelete(Connection con) {
		CustomerHandler customerList = new CustomerHandler(con);
		try {
			Statement stmt = con.createStatement();
			ResultSet rs = stmt
					.executeQuery("SELECT customerID FROM customer WHERE SSN=1110001110");
			while (rs.next()) {
				customerList.deleteCustomer(rs.getInt("customerID"));
				con.commit();
			}
		} catch (SQLException e) {
			System.out.println("Failed to delete test entry.");
			e.printStackTrace();
			return false;
		}

		try {
			Statement stmt = con.createStatement();
			ResultSet rs = stmt
					.executeQuery("SELECT customerID FROM customer WHERE SSN=1110001110");
			if (rs.next()) {
				System.out.println("Failed to delete customer member.");
				return false;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return true;
	}
}
