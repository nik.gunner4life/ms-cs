/*
 * 
 */
package bat.tests;

/**
 * @author Roshan George
 *
 */
import java.sql.Connection;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.SQLIntegrityConstraintViolationException;
import java.sql.Statement;
import java.util.Calendar;
import java.util.GregorianCalendar;

import java.util.logging.Level;
import java.util.logging.Logger;

import bat.StaffHandler;

public class StaffTester extends PeopleTester{

	private static final String jdbcURL = "jdbc:oracle:thin:@ora.csc.ncsu.edu:1521:orcl";
	private static final String user = "rgeorge2";
	private static final String password = "r0racle";
	
	private static final Logger logger = Logger.getLogger(StaffTester.class.getName());

	/**
	 * @param args
	 */
	public static void main(String[] args) {


		try {
			Class.forName("oracle.jdbc.driver.OracleDriver");
			Connection con = DriverManager.getConnection(jdbcURL, user,
					password);
			
			if (testInserts(con)) {
				logger.log(Level.INFO, "Staff Insertion Tests OK.");
			} else {
				logger.log(Level.SEVERE, "Staff Insertion Test FAILURE.");
			}
			if (testDeletes(con)) {
				logger.log(Level.INFO, "Staff Deletion Tests OK.");
			} else {
				logger.log(Level.SEVERE, "Staff Deletion Test FAILURE.");
			}
		} catch (Exception e) {
			System.out.println("StaffTester: " + e);
			logger.log(Level.SEVERE, "Testing mechanism FAILURE.");
		}

	}

	public static Boolean testInsert(Connection con, int i) {

		Calendar joinDate = new GregorianCalendar(2000, 04, 14);
		Date joinDated = new Date(joinDate.getTimeInMillis());

		StaffHandler staffList = new StaffHandler(con);

		int staffID = -1;
		try {
			staffID = staffList.newStaff(
						testSSNs[i], testFirstNames[i], testLastNames[i], testGenders[i],
						testDOBs[i], 4, joinDated, "Billing Manager", "Billing", 10001,
						testPhones[i], testAddresses[i], testEmails[i]
					);
			logger.log(Level.FINE, "Attempted insertion of staff {0} OK", staffID);
		} catch (SQLIntegrityConstraintViolationException e) {
			e.printStackTrace();
			return false;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}

		try {
			Statement stmt = con.createStatement();
			ResultSet rs = stmt
					.executeQuery("SELECT staffID FROM staff WHERE SSN="+testSSNs[i]);
			if (rs.next()) {
				logger.log(Level.FINE, "Checked presence of staff {0} OK.",rs.getInt("staffID"));
			} else {
				logger.log(Level.SEVERE, "No staff present in database with test SSN {0} FAILURE", testSSNs[i]);
				return false;
			}
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}

		return true;
	}

	public static Boolean testDelete(Connection con, int i) {
		StaffHandler staffList = new StaffHandler(con);
		try {
			Statement stmt = con.createStatement();
			ResultSet rs = stmt
					.executeQuery("SELECT staffID FROM staff WHERE SSN="+testSSNs[i]);
			int staffID = -1;
			while (rs.next()) {
				staffID = rs.getInt("staffID");
				logger.log(Level.FINE, "Attempting deletion of staff {0} with SSN {1} OK", new Object[] {staffID, testSSNs[i]});
				staffList.deleteStaff(staffID);
				con.commit();
			}
		} catch (SQLException e) {
			logger.log(Level.SEVERE,"Deletion FAILURE.");
			e.printStackTrace();
			return false;
		}

		try {
			Statement stmt = con.createStatement();
			ResultSet rs = stmt
					.executeQuery("SELECT staffID FROM staff WHERE SSN="+testSSNs[i]);
			if (rs.next()) {
				logger.log(Level.SEVERE, "Failed to delete staff member FAILURE.");
				return false;
			}
			else {
				logger.log(Level.FINE, "Checked absence of staff with test SSN {0} OK", testSSNs[i]);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return true;
	}
	
	public static Boolean testInserts(Connection con) {
		int i;
		for(i=0;i<1;i++) {
			if(!testInsert(con, i)) {
				return false;
			}
		}
		return true;
	}
	
	public static Boolean testDeletes(Connection con) {
		int i;
		for(i=0;i<5;i++) {
			if(!testDelete(con, i)) {
				return false;
			}
		}
		return true;
	}
}
