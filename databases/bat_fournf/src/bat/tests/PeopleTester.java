package bat.tests;

import java.sql.Date;
import java.util.Calendar;
import java.util.GregorianCalendar;

public class PeopleTester {
	protected static String testSSNs[]			=	{
														"0001110010", "0001110002",
														"0001110003", "0001110004",
														"0001110005"
													};
	protected static String testFirstNames[] 	= 	{
														"TestFirst1", "TestFirst2",
														"TestFirst3","TestFirst4", "TestFirst5"
													};
	protected static String testLastNames[] 	= 	{
														"TestLast1", "TestLast2",
														"TestLast3", "TestLast4", "TestLast5"
													};
	protected static String testGenders[] 		=	{"M", "F", "M", "F", "M"};
	private static Calendar testDOBCalendars[] 	= 	{
														new GregorianCalendar(1988,01,01),
														new GregorianCalendar(1998,02,02),
														new GregorianCalendar(1584,11,03),
														new GregorianCalendar(1992,12,11),
														new GregorianCalendar(1929,04,30)
													};
	protected static Date testDOBs[] 			=	{
														new Date(testDOBCalendars[0].getTimeInMillis()),
														new Date(testDOBCalendars[1].getTimeInMillis()),
														new Date(testDOBCalendars[2].getTimeInMillis()),
														new Date(testDOBCalendars[3].getTimeInMillis()),
														new Date(testDOBCalendars[4].getTimeInMillis())
													};
	protected static String testPhones[] 		= 	{
														"5550001110", "5550001111",
														"5550001112", "5550001113",
														"5550001114"
													};
	protected static String testAddresses[] 		= {
														"#1, 2nd Street, 11100",
														"#1, 2nd Street, 11100",
														"#2, 3rd Street, 11101", 
														"#3, 4th Street, 11110",
														"#2, 3rd Street, 11101"
													};
	protected static String testEmails[] 		=	{
														"test1@example.com", "test2@example.com",
														"test3@example.com", "test4@example.com",
														"test5@example.com"
													};
}
