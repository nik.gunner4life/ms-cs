package bat;


import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.SQLIntegrityConstraintViolationException;
import java.sql.Statement;

import oracle.jdbc.OracleTypes;

public class StaffHandler {

	private Connection con;

	/**
	 * Set the connection to be used.
	 * 
	 * @param con
	 */
	public StaffHandler(Connection con) {
		this.con = con;
	}
	
	/**
	 * Get the SSN of a staff member given their ID.
	 * 
	 * @param staffID			The ID of the staff member whose SSN is needed.
	 * @return					The SSN of the staff member if he exists, null otherwise
	 * @throws SQLException
	 */
	private String getSSN(int staffID) throws SQLException {
		String SSN = null;

		Statement getSSNStatement = con.createStatement();
		ResultSet getSSNs = getSSNStatement
				.executeQuery("SELECT SSN FROM staff WHERE staffID=" + staffID);
		if (getSSNs.next()) {
			SSN = getSSNs.getString("SSN");
		}
		return SSN;
	}

	/**
	 * 
	 * Add a new staff member.
	 * 
	 * Adds a new staff member. Staff members who were fired will never be rehired, even at other stores of Books-A-Thousand. 
	 * 
	 * @see PeopleHandler		PeopleHandler adds the person's information to `people` and `people_home`.
	 * @param SSN				SSN of the new staff member.
	 * @param firstName			First name of the new staff member.
	 * @param lastName			Last name of the new staff member.
	 * @param gender			Gender of the new staff member, must be 'M' or 'F'.
	 * @param dob				Date of Birth of the new staff member.
	 * @param storeID			ID of the store where the staff member is assigned to work.
	 * @param joinDate			The date that the staff member joined.
	 * @param jobTitle			The job title that the staff member holds.
	 * @param department		The department the staff member is assigned to.
	 * @param salary			The salary the staff member is paid.
	 * @param phone				The phone number of the staff member.
	 * @param address			The address of the staff member.
	 * @param email				The email address of the staff member.
	 * @return					Staff ID of the new staff member if successful, -1 otherwise
	 * @throws Exception
	 */
	public int newStaff(String SSN, String firstName, String lastName,
			String gender, Date dob, int storeID, Date joinDate,
			String jobTitle, String department, int salary, String phone,
			String address, String email) throws Exception {
		
		int staffID = -1;
		CallableStatement insertStaff = null;
		String insertStaffStatement = "BEGIN INSERT INTO staff(SSN,department,jobtitle,salary,joineddate) VALUES(?, ?, ?, ?, ?) returning staffID into ?; END;";

		try {
			con.setAutoCommit(false);
			PeopleHandler personAdder = new PeopleHandler(con);
			if(personAdder.newPerson(SSN, firstName, lastName, gender, phone,email, dob, address)==false)
			{
				con.rollback();
				con.setAutoCommit(true);
				return -1;
			}

			insertStaff = con.prepareCall(insertStaffStatement);
			insertStaff.setString(1, SSN);
			insertStaff.setString(2, department);
			insertStaff.setString(3, jobTitle);
			insertStaff.setInt(4, salary);
			insertStaff.setDate(5, joinDate);
			insertStaff.registerOutParameter(6, OracleTypes.NUMBER);
			insertStaff.executeUpdate();

			staffID = insertStaff.getInt(6);
			
			if(changeStaffWork(staffID, storeID)==false)
			{
				con.rollback();
				return -1;
			}
			
			con.commit();
		} catch (SQLException e) {
			con.rollback();
			throw e;
		} catch (Exception e) {
			con.rollback();
		} finally {
			con.setAutoCommit(true);
		}
				
		return staffID;
	}

	/**
	 * Change the name of the staff member.
	 * 
	 * @param staffID			The ID of the staff member whose name is being changed.
	 * @param firstName			The new first name of the staff member.
	 * @param lastName			The new last name of the staff member.
	 * @return					True if the name was changed, false otherwise.
	 * @throws SQLException
	 */
	public Boolean updateStaffName(int staffID, String firstName, String lastName) throws SQLException {
		PeopleHandler person = new PeopleHandler(con);
		String SSN = getSSN(staffID);
		
		if(person.updatePersonName(SSN, firstName, lastName)) {
			return true;
		}
		return false;
	}
	
	/**
	 * Change the address of the staff member.
	 * 
	 * @param staffID			The ID of the staff member whose address is being changed.
	 * @param address			The new address of the staff member.
	 * @return					True if the name was changed, false otherwise.
	 * @throws SQLException
	 */
	public Boolean updateStaffAddress(int staffID, String address) throws SQLException {
		PeopleHandler person = new PeopleHandler(con);
		String SSN = getSSN(staffID);
		
		if(person.updatePersonAddress(SSN, address)) {
			return true;
		}
		return false;
		
	}
	
	/**
	 * Change the phone number of the staff member.
	 * @param staffID			The ID of the staff member whose phone is being changed.
	 * @param phone				The new phone number of the staff member.
	 * @return					True if the number was changed, false otherwise.
	 * @throws SQLException
	 */
	public Boolean updateStaffPhone(int staffID, String phone) throws SQLException {
		PeopleHandler person = new PeopleHandler(con);
		String SSN = getSSN(staffID);
		
		if(person.updatePersonPhone(SSN, phone)) {
			return true;
		}
		return false;
	}
	
	/**
	 * Change the email address of a staff member.
	 * 
	 * @param staffID			The ID of the staff member whose email address is being changed.
	 * @param email				The new email address.
	 * @return					True if the number was changed, false otherwise.
	 * @throws SQLException
	 */
	public Boolean updateStaffEmail(int staffID, String email) throws SQLException {
		PeopleHandler person = new PeopleHandler(con);
		String SSN = getSSN(staffID);
		
		if(person.updatePersonEmail(SSN, email)) {
			return true;
		}
		return false;
	}
	
	/**
	 * Change the department a staff member is assigned to work in.
	 * 
	 * @param staffID			The ID of the staff member who is being assigned a department.
	 * @param department		The new department of the staff member.
	 * @return					True if the department was successfully assigned, false otherwise.
	 * @throws SQLException
	 */
	public Boolean updateStaffDepartment(int staffID, String department) throws SQLException {
		PreparedStatement ps = con.prepareStatement("UPDATE staff SET department=? WHERE staffID=?");
		ps.setString(1, department);
		ps.setInt(2, staffID);
		ps.executeUpdate();
		
		return true;
	}
	
	/**
	 * Change the job title a staff member holds.
	 * 
	 * @param staffID			The ID of the staff member whose job title is being changed.
	 * @param jobTitle			The new job title.
	 * @return					True if the job title was changed, false otherwise.
	 * @throws SQLException
	 */
	public Boolean updateStaffJobTitle(int staffID, String jobTitle) throws SQLException {
		PreparedStatement ps = con.prepareStatement("UPDATE staff SET jobtitle=? WHERE staffID=?");
		ps.setString(1, jobTitle);
		ps.setInt(2, staffID);
		ps.executeUpdate();
		
		return true;
	}
	
	/**
	 * Change the salary a staff member earns.
	 * 
	 * @param staffID			The ID of the staff member whose salary is being modified.
	 * @param salary			The new salary of the staff member.
	 * @return					True if the salary was changed, false otherwise.
	 * @throws SQLException
	 */
	public Boolean updateStaffSalary(int staffID, int salary) throws SQLException {
		PreparedStatement ps = con.prepareStatement("UPDATE staff SET salary=? WHERE staffID=?");
		ps.setInt(1, salary);
		ps.setInt(2, staffID);
		ps.executeUpdate();
		
		return true;	
	}
	
	/**
	 * Change the store at which a staff member is assigned to work.
	 * 
	 * @param staffID			The ID of the staff member who is being assigned to a store.
	 * @param storeID			The ID of the store to which the staff member is being assigned.
	 * @return					True if the staff member was assigned, false otherwise
	 * @throws SQLException
	 */
	public Boolean changeStaffWork(int staffID, int storeID) throws SQLException {
		
		deleteWorks(staffID);
		
		Statement findStore = con.createStatement();
		ResultSet stores = findStore.executeQuery("SELECT storeID FROM store WHERE storeID="+storeID);
		if(!stores.next()) {
			throw new SQLIntegrityConstraintViolationException("The store given does not exist!" + storeID);
		}
		
		PreparedStatement addNew = con.prepareStatement("INSERT INTO worksat VALUES(?,?)");
		addNew.setInt(1, storeID);
		addNew.setInt(2, staffID);
		addNew.executeUpdate();
		
		PreparedStatement addNewLogin = con.prepareStatement("INSERT INTO logins VALUES(?,?)");
		addNewLogin.setInt(1, staffID);
		addNewLogin.setInt(2, staffID);
		addNewLogin.executeUpdate();
		
		return true;
	}
	
	/**
	 * Remove all assignments of a staff member.
	 * 
	 * Removes any work assignments of the staff member. The staff member can then be assigned a new store to work at using {@link #changeStaffWork(int, int)}.
	 * 
	 * @param staffID			The	ID of the staff member who is being removed from all stores.
	 * @return
	 * @throws SQLException
	 */
	public Boolean deleteWorks(int staffID) throws SQLException {
		PreparedStatement delOld = con.prepareStatement("DELETE FROM worksat WHERE staffID=?");
		delOld.setInt(1, staffID);
		delOld.executeUpdate();
		
		return true;
	}
	
	/**
	 * Fire the staff member. They will remain in the database so that they are not rehired.
	 * 
	 * @param staffID			The ID of the staff member who is being deleted.
	 * @return					True if the staff was correctly deleted, false otherwise.
	 * @throws SQLException
	 */
	public Boolean deleteStaff(int staffID) throws SQLException {
	
		String delStaffString = "UPDATE staff SET employed='NO' WHERE staffid=?";
		String delLoginString = "DELETE FROM logins WHERE staffid=?";
		PreparedStatement ps;
		PreparedStatement ls;
		try {
			con.setAutoCommit(false);
			ps = con.prepareStatement(delStaffString);
			ps.setInt(1, staffID);
			ps.executeUpdate();
			ls = con.prepareStatement(delLoginString);
			ls.setInt(1, staffID);
			ls.executeUpdate();
			deleteWorks(staffID);
			con.commit();
		}
		catch(Exception e) {
			con.rollback();
			return false;
		} finally {
			con.setAutoCommit(true);
		}
		return true;
	}
}
