/*
 * Created by 4NF on 17th November 2012
 * Modified by 4NF on 29th November 2012
 */
package bat;

/**
 * @author 4NF
 *
 */
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.Calendar;

public class Billing {
	/**
	 * Set the connection to be used.
	 * 
	 * @param con
	 */
	private Connection con;

	public Billing(Connection con) {
		this.con = con;
	}

	/**
	 * Generate the date in the required SQL Format.
	 * 
	 * @param theDate
	 *            the Date is the system generated date for that particular day
	 * @param firstorLast
	 *            This specifies whether the date required is the first or last
	 *            of the month
	 * @return This returns the date in the required SQL format.
	 */
	public String generateDate(String theDate, int firstOrLast) {
		String newDate = "";
		String dtStringArray[] = theDate.split("/");

		if (dtStringArray[0].toLowerCase().equalsIgnoreCase("jan")) {
			if (firstOrLast == 1) {
				newDate = "'01-Jan-" + dtStringArray[2] + "'";
			} else {
				newDate = "'31-Jan-" + dtStringArray[2] + "'";
			}
		} else if (dtStringArray[0].toLowerCase().equalsIgnoreCase("feb")) {
			if (firstOrLast == 1) {
				newDate = "'01-Jan-" + dtStringArray[2] + "'";
			} else {
				if (Integer.parseInt(dtStringArray[2]) % 4 == 0) {
					newDate = "'29-Feb-" + dtStringArray[2] + "'";
				} else {
					newDate = "'28-Feb-" + dtStringArray[2] + "'";
				}

			}
		} else if (dtStringArray[0].toLowerCase().equalsIgnoreCase("mar")) {
			if (firstOrLast == 1) {
				newDate = "'01-Mar-" + dtStringArray[2] + "'";
			} else {
				newDate = "'31-Mar-" + dtStringArray[2] + "'";
			}
		} else if (dtStringArray[0].toLowerCase().equalsIgnoreCase("apr")) {
			if (firstOrLast == 1) {
				newDate = "'01-Apr-" + dtStringArray[2] + "'";
			} else {
				newDate = "'30-Apr-" + dtStringArray[2] + "'";
			}
		} else if (dtStringArray[0].toLowerCase().equalsIgnoreCase("may")) {
			if (firstOrLast == 1) {
				newDate = "'01-May-" + dtStringArray[2] + "'";
			} else {
				newDate = "'31-May-" + dtStringArray[2] + "'";
			}
		} else if (dtStringArray[0].toLowerCase().equalsIgnoreCase("jun")) {
			if (firstOrLast == 1) {
				newDate = "'01-Jun-" + dtStringArray[2] + "'";
			} else {
				newDate = "'30-Jun-" + dtStringArray[2] + "'";
			}
		} else if (dtStringArray[0].toLowerCase().equalsIgnoreCase("jul")) {
			if (firstOrLast == 1) {
				newDate = "'01-Jul-" + dtStringArray[2] + "'";
			} else {
				newDate = "'31-Jul-" + dtStringArray[2] + "'";
			}
		} else if (dtStringArray[0].toLowerCase().equalsIgnoreCase("aug")) {
			if (firstOrLast == 1) {
				newDate = "'01-Aug-" + dtStringArray[2] + "'";
			} else {
				newDate = "'31-Aug-" + dtStringArray[2] + "'";
			}
		} else if (dtStringArray[0].toLowerCase().equalsIgnoreCase("sep")) {
			if (firstOrLast == 1) {
				newDate = "'01-Sep-" + dtStringArray[2] + "'";
			} else {
				newDate = "'30-Sep-" + dtStringArray[2] + "'";
			}
		} else if (dtStringArray[0].toLowerCase().equalsIgnoreCase("oct")) {
			if (firstOrLast == 1) {
				newDate = "'01-Oct-" + dtStringArray[2] + "'";
			} else {
				newDate = "'31-Oct-" + dtStringArray[2] + "'";
			}
		} else if (dtStringArray[0].toLowerCase().equalsIgnoreCase("nov")) {
			if (firstOrLast == 1) {
				newDate = "'01-Nov-" + dtStringArray[2] + "'";
			} else {
				newDate = "'30-Nov-" + dtStringArray[2] + "'";
			}
		} else {
			if (firstOrLast == 1) {
				newDate = "'01-Dec-" + dtStringArray[2] + "'";
			} else {
				newDate = "'31-Dec-" + dtStringArray[2] + "'";
			}
		}
		return newDate;
	}

	/**
	 * 
	 * Gets the Bills for every customer
	 * 
	 * @param theDate
	 *            The system generated date of that particular day
	 * @param customerID
	 *            The customer ID for purchase history is passed
	 * @return This returns a resultset which contains the orders and bill of
	 *         that customer.
	 */
	public ResultSet getBillsToSend(String theDate, int customerID) {
		ResultSet rs = null;
		try {
			Statement stmt = con.createStatement(
					ResultSet.TYPE_SCROLL_INSENSITIVE,
					ResultSet.CONCUR_UPDATABLE);
			
			String startDate = generateDate("Jan/01/1971", 1);
			String endDate = generateDate(theDate, 2);
			rs = stmt
					.executeQuery("select distinct S1.FirstName as CustomerFname, S1.LastName as CustomerLName, Orders.OrderID as OrderNumber, S2.Title as BookTitle, Orders.Price, Orders.Qty, (Price*Qty) as Amount from orders, (select FirstName, LastName, CustomerID from people, customer where people.SSN = customer.SSN) S1, (select Title, Book.ISBN as ISBNo from Book, Orders where Orders.ISBN = Book.ISBN) S2  where Orders.CustomerID = "
							+ customerID
							+ " and S1.CustomerID = Orders.CustomerID and S2.ISBNo = Orders.ISBN and OrderDate between "
							+ startDate
							+ " and "
							+ endDate
							+ " and PaymentStatus = 'NOT PAID'");

		} catch (Exception e) {
			System.out.println(e);
		}
		return rs;
	}

	/**
	 * 
	 * Pay the bill / Update Payment Status to Paid
	 * 
	 * @param orderID
	 *            The order ID corresponding of the payment.
	 * @param customerID
	 *            The customer ID of the person paying the bill
	 * @return This returns a string which states whether the payment was a
	 *         success or a failure.
	 */
	public String payBill(int customerID) throws SQLException {
		String paymentStatus = "failure";
		try {
			Statement stmt = con.createStatement();
			stmt.executeUpdate("update Orders Set PaymentStatus = 'PAID' where CustomerID = "
					+ customerID);
			paymentStatus = "success";

		} catch (Exception e) {
			System.out.println(e);
		}
		return paymentStatus;
	}

	/**
	 * 
	 * Gets the Current Outstanding of the Amount of the customer
	 * 
	 * @param theDate
	 *            The system generated date of the particular day
	 * @param customerID
	 *            The customer ID whose current outstanding is required.
	 * @return This returns a resultset which contains Customer's Current
	 *         Outstanding.
	 */

	public ResultSet getCurrentOutstanding(String theDate, int customerID) {
		ResultSet rs = null;
		try {
			Statement stmt = con.createStatement();
			String startDate = generateDate("Jan/01/1971", 1);
			String endDate = generateDate(theDate, 2);
			rs = stmt
					.executeQuery("select CustomerID, Sum(Price*Qty) As Amount from orders where CustomerID = "
							+ customerID
							+ " and orderdate between"
							+ startDate
							+ " and "
							+ endDate
							+ " and PaymentStatus = 'NOT PAID' group by customerID");

		} catch (Exception e) {
			System.out.println(e);
		}

		return rs;
	}

	/**
	 * 
	 * Gets the Current Outstanding of the Amount of the customer
	 * 
	 * @param theDate
	 *            The system generated date of the particular day
	 * @param customerID
	 *            The customer ID whose current outstanding is required.
	 * @return This returns a resultset which contains Customer's Current
	 *         Outstanding.
	 */

	public ResultSet generateVendorBill(String theDate, int vendorID) {
		ResultSet rs = null;
		try {
			Statement stmt = con.createStatement(
					ResultSet.TYPE_SCROLL_INSENSITIVE,
					ResultSet.CONCUR_UPDATABLE);
			String startDate = generateDate("Jan/01/1971", 1);
			String endDate = generateDate(theDate, 2);
			rs = stmt
					.executeQuery("select RequestID, ISBN, Cost, Qty, (Cost*Qty) as Amount, RequestDate, VendorID from Request where VendorID = "
							+ vendorID
							+ " and requestDate between"
							+ startDate
							+ " and "
							+ endDate
							+ " and RequestStatus = 'COMPLETED'");

		} catch (Exception e) {
			System.out.println(e);
		}

		return rs;
	}

	/**
	 * 
	 * Gets the Current Outstanding of the Amount of the customer
	 * 
	 * @return This returns a resultset which contains the list of vendors
	 */
	
	public ResultSet getVendorList() throws SQLException {
		ResultSet rs = null;
		try {
			Statement stmt = con.createStatement();
			rs = stmt.executeQuery("select VendorID from Vendors");

		} catch (Exception e) {
			System.out.println(e);
		}
		return rs;

	}
	
	/**
	 * 
	 * Update the payment status of the vendor
	 * 
	 * @return This returns a boolean value which indicates if the payment has been successful or not. 
	 */
	public boolean payAVendor(int vendorID) throws SQLException {
		boolean payStatus = false;
		try {
			Statement stmt = con.createStatement();
			stmt.executeUpdate("update Request Set RequestStatus = 'PAID' where VendorID = "
					+ vendorID + " and RequestStatus = 'COMPLETED'");
			payStatus = true;

		} catch (Exception e) {
			System.out.println(e);
		}

		return payStatus;
	}
	
	/** 
	 * Gets the Login Details of the Staff
	 * 
	 * @return This returns a resultset which contains Login Information
	 */

	public ResultSet getLoginDetails() {
		ResultSet rs = null;
		try {
			Statement stmt = con.createStatement();
			rs = stmt.executeQuery("select StaffID, Password from Logins");

		} catch (Exception e) {
			System.out.println(e);
		}

		return rs;
	}
	
	/**
	 * 
	 * @param startDate
	 * @param customerID
	 * @return
	 */
	public ResultSet getBillingCycle(String startDate, int customerID) {
		ResultSet rs = null;
		try {
			
			Statement stmt = con.createStatement(
					ResultSet.TYPE_SCROLL_INSENSITIVE,
					ResultSet.CONCUR_UPDATABLE);
			String dtstringArray[] = startDate.split("-");
			String theDate = dtstringArray[1]+"/" + dtstringArray[0] + "/" + dtstringArray[2];
			startDate = generateDate(theDate, 1);
			String endDate = generateDate(theDate, 2);
			rs = stmt
					.executeQuery("select distinct S1.FirstName as CustomerFname, S1.LastName as CustomerLName, Orders.OrderID as OrderNumber, S2.Title as BookTitle, Orders.Price, Orders.Qty, (Price*Qty) as Amount, PaymentStatus, Orders.StaffID from orders, (select FirstName, LastName, CustomerID from people, customer where people.SSN = customer.SSN) S1, (select Title, Book.ISBN as ISBNo from Book, Orders where Orders.ISBN = Book.ISBN) S2  where Orders.CustomerID = "
							+ customerID
							+ " and S1.CustomerID = Orders.CustomerID and S2.ISBNo = Orders.ISBN and OrderDate between "
							+ startDate + " and " + endDate);

		} catch (Exception e) {
			System.out.println(e);
		}
		return rs;
	}

	/**************************************
	 * 
	 * Gets the Job Title of the Staff
	 * 
	 * @param staffID
	 *            The staff ID is passed to get the corresponding Job title
	 * @return This returns a string which contains Login Information
	 */

	public String getJobTitle(int staffID) {
		ResultSet rs = null;
		String jobTitle = "";
		try {
			Statement stmt = con.createStatement();
			rs = stmt
					.executeQuery("select jobtitle from staff where staffID = "
							+ staffID);
			while (rs.next()) {
				jobTitle = rs.getString("JobTitle");
				break;
			}

		} catch (Exception e) {
			System.out.println(e);
		}
		return jobTitle;
	}

}
