package bat;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

import oracle.jdbc.OracleTypes;

/**
 * 
 * class Orders contains methods to manipulate orders, restocks and vendor requisition
 * 
 * @param orders		These are orders that a customer asks for. They are entered by a Salesperson.
 * @param restocks		These are books that a store needs. They are entered by a Salesperson and fulfilled by a Merchandise stocker.
 * @param requests		These are vendor requisitions to get books from a vendor when they are unavailable at the warehouse. They are entered by a Merchandise Stocker and marked as completed by a Merchandise Stocker. 
 */
public class Orders {
	/**
	 * Set the connection to be used.
	 * 
	 * @param con
	 */
	private Connection conn;
	
	//constructor
	public Orders(Connection conn)
	{
		this.conn = conn;
	}
	
	/**
	 * Inserts a new order placed by the customer. 
	 * 
	 * @param qty			The quantity for that order			
	 * @param price			The price of that order
	 * @param isbn			The ISBN for that order.
	 * @param custID		The customerID of the person who places the order.
	 * @param staffID		The Staff ID of the person who places the order.
	 * @param orderdate		The order date of that order.
	 * @param payStatus		The payment status of that particular order. 
	 * @param status		The delivery status of that particular order. 
	 * @return
	 * @throws SQLException
	 */
	public int newOrder( int qty, float price,String isbn, int custID,int staffID, Date orderdate, String payStatus,String status) 
									throws SQLException
	{
		
		int orderID = -1;
		CallableStatement neworder = null;
		PreparedStatement checkorder = null;
		PreparedStatement checkStore = null;
		try
		{
			conn.setAutoCommit(false);
			
			String checkQuery = "SELECT COUNT(*) AS CNT FROM BOOK WHERE ISBN = ?";
			
			checkorder = conn.prepareCall(checkQuery);
			checkorder.setString(1, isbn);
			ResultSet rs = checkorder.executeQuery();
			
			if(rs!=null && rs.next() && rs.getInt(1)>0)
			{	
				String neworderQuery = "BEGIN INSERT INTO ORDERS"+
									"(QTY,PRICE,ISBN,CUSTOMERID,STAFFID,ORDERDATE,"+
					"PAYMENTSTATUS,ORDERSTATUS) VALUES(?,?,?,?,?,?,?,?) RETURNING ORDERID INTO ?;END;";
				neworder = conn.prepareCall(neworderQuery);
				neworder.setInt(1, qty);
				neworder.setFloat(2,price);
				neworder.setString(3, isbn);
				neworder.setInt(4,custID);
				neworder.setInt(5,staffID);
				neworder.setDate(6,orderdate);
				neworder.setString(7,payStatus);
				neworder.setString(8,status);
				neworder.registerOutParameter(9, OracleTypes.INTEGER);
				neworder.execute();
				String getstoreQuery = "select storeid from worksat where staffid =? ";
				checkStore = conn.prepareCall(getstoreQuery);
				checkStore.setInt(1, staffID);
				ResultSet rs1 = checkStore.executeQuery();
				if(rs1.next() && rs1!= null)
				{
					int rem_storeid = rs1.getInt(1);
				
					Vendor v = new Vendor(conn);

					if(v.removeBookQtyStore(rem_storeid, isbn, qty) == true)
					{
						orderID = neworder.getInt(9);
						conn.commit();
						conn.setAutoCommit(true);
						System.out.println("\n Order placed successfully");
						
					}
					else
					{
						conn.rollback();
						conn.setAutoCommit(true);
						System.out.println("\n Order cannot be placed");
						
					}
				}
			}
			else
			{
				System.out.println("ISBN not available");
			}
		}
		catch(Exception e)
		{
			return -1;
		}
		return orderID;
	}
	
	/**
	 * Updates the Customer Orders.
	 * 
	 * @param orderID			The order ID of that order.
	 * @param orderstatus		The order status of that order. 
	 * @return					Returns a boolean value to indicate if the update was successful or not.
	 * @throws SQLException
	 */
	public boolean updateOrder(int orderID, String orderstatus) throws SQLException
	{
	
		int count=0;
		boolean success=false;
		PreparedStatement updateCheck = null;
		PreparedStatement updateorder = null;
		try
		{
			String updateOrderCnt = "SELECT count(*) as cnt from orders where orderid=?";
			String updateOrderQuery = "UPDATE orders set orderstatus=? where orderid=?";
			updateCheck = conn.prepareStatement(updateOrderCnt);
			updateCheck.setInt(1, orderID);
			ResultSet rs = updateCheck.executeQuery();
			if(rs!=null && rs.next() && rs.getInt(1)>0)
			{	
					updateorder = conn.prepareStatement(updateOrderQuery);
					updateorder.setString(1, orderstatus);
					updateorder.setInt(2,orderID);
					count = updateorder.executeUpdate();
					if(count>0)
						success=true;
				
			}
		}
		catch(Exception e)
		{
			System.out.println("Exception in update order"+e);
		}
			return success;
	}
	
	/**
	 * Cancels an already placed order.
	 * 
	 * @param orderID			The order ID of that particular Order
	 * @return					Returns a boolean value to indicate if the order has been successfully cancelled. 
	 * @throws SQLException
	 * @throws IOException
	 */
	public boolean cancelOrder(int orderID) throws SQLException,IOException
	{
		int count=0;
		boolean success=false;
		PreparedStatement cancelorder = null;
		PreparedStatement cancelCheck = null;
		PreparedStatement checkStore = null;
		//PreparedStatement inquireBookBack = null;
		try
		{
			conn.setAutoCommit(false);
			
			String cancelOrderCheck = "SELECT isbn,qty,staffid from orders where orderid=?";
			
			String cancelOrderQuery = "DELETE from orders where orderid=?";
			
			cancelCheck = conn.prepareStatement(cancelOrderCheck);
			cancelCheck.setInt(1, orderID);
			ResultSet rs = cancelCheck.executeQuery();
				
				if(rs!=null && rs.next())
				{	
						//
						String ISBN = rs.getString(1);
						int qty = rs.getInt(2);
						int staffID = rs.getInt(3);
						Vendor v = new Vendor(conn);
						
						cancelorder = conn.prepareStatement(cancelOrderQuery);
						cancelorder.setInt(1, orderID);
						count = cancelorder.executeUpdate();
						String getstoreQuery = "select storeid from worksat where staffid = ?" ;
						checkStore = conn.prepareCall(getstoreQuery);
						checkStore.setInt(1, staffID);
						ResultSet rs1 = checkStore.executeQuery();
						if(rs1.next() && rs1!= null)
						{
							
							int storeid = rs.getInt(1);
							System.out.println("Do you want to confirm the cancellation?");
							BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
							String confirmation = br.readLine();
							//System.out.println("isbn" + ISBN+ "intial qty available" + v.getBookQtyStore(storeid, ISBN));
							if(v.updateBookQtyStore(storeid, ISBN, qty) == true && confirmation.equalsIgnoreCase("yes"))
							{
								if(count>0)
								success=true;
								
								
								conn.commit();
								conn.setAutoCommit(true);
								System.out.println("Book order cancelled and warehouse updated ");
								//System.out.println("isbn" + ISBN+ "final qty available" + v.getBookQtyStore(storeid, ISBN));
							}
							else
							{
								
								conn.rollback();
								conn.setAutoCommit(true);
								System.out.println("Cancel order rolled back");
								//System.out.println("isbn" + ISBN+ "final qty available" + v.getBookQtyStore(storeid, ISBN));
							}
						}
				}
				else
					System.out.println("order id not available");
		}
		catch(SQLException e)
		{
			System.out.println("Exception in cancel Order"+e);
			//e.printStackTrace();
		}
		return success;
	}
	
	
	/**
	 * Places a new Restock order with the Warehouse.
	 *  
	 * @param qty				The quantity for that order. 
	 * @param status			The status of that restock order.
	 * @param staffID			The Staff ID of the person who places the order.
	 * @param isbn				The ISBN of the book for that order.  
	 * @return					Returns the integer Restock ID.
	 * @throws SQLException
	 */
	public int newRestockRequest(int qty, String status, int staffID,String isbn ) throws SQLException
	{
		int restockID = -1;
		CallableStatement newrestock = null;
		PreparedStatement checkrestock = null;
		String checkQuery = "SELECT COUNT(*) AS CNT FROM STAFF WHERE STAFFID = ?";
		checkrestock = conn.prepareCall(checkQuery);
		checkrestock.setInt(1, staffID);
		ResultSet rs = checkrestock.executeQuery();
		
		if(rs!=null && rs.next() && rs.getInt(1)>0)
		{
			String restockQuery = "BEGIN INSERT INTO STORERESTOCK (QTY,RESTOCKSTATUS,STAFFID,ISBN) VALUES (?,?,?,?)"
									+"RETURNING RESTOCKORDERID INTO ?;END;";
			newrestock = conn.prepareCall(restockQuery);
			newrestock.setInt(1, qty);
			newrestock.setString(2, status);
			newrestock.setInt(3,staffID);
			newrestock.setString(4,isbn);
			
			newrestock.registerOutParameter(5, OracleTypes.INTEGER);
			newrestock.execute();
			restockID = newrestock.getInt(5);	
		}
		else
		{
			System.out.println("invalid staff");
		}
			return restockID;
		
	}
		
	/**
	 * 
	 * Update a restock request to SHIPPED if PENDING
	 * 
	 * @param restockorderID		The restock Order ID of that restock order. 
	 * @return						Returns a boolean value to indicate if the update was successful. 
	 * @throws SQLException
	 */
	public boolean updateRestockRequest(int restockorderID) throws SQLException 
	{
	
		int count=0;
		boolean success=false;
		PreparedStatement updateCheck = null;
		PreparedStatement updaterestock = null;
		PreparedStatement updatewarehouse=null;
		try
		{
			conn.setAutoCommit(false);
			
			String updaterestockCnt = "SELECT ISBN,qty,staffid from storerestock where restockorderid=? and restockstatus='PENDING'";
			String updateRestockrQuery = "UPDATE storerestock set restockstatus=? where restockorderid=?";
			updateCheck = conn.prepareStatement(updaterestockCnt);
			updateCheck.setInt(1, restockorderID);
			ResultSet rs = updateCheck.executeQuery();
			if(rs!=null && rs.next())
			{	
				int qty = rs.getInt(2);
				String ISBN = rs.getString(1);
				int staffID = rs.getInt(3);
				updaterestock = conn.prepareStatement(updateRestockrQuery);
				
				updaterestock.setString(1, "SHIPPED");
				updaterestock.setInt(2,restockorderID);
				count = updaterestock.executeUpdate();
				if(count<=0)
				{
					conn.rollback();
					conn.setAutoCommit(true);
					return false;
				}
					
				
				Vendor checkobj = new Vendor(conn);
				int whqty = checkobj.getBookQty(ISBN);
				if(whqty>qty)
				{
					updatewarehouse = conn.prepareStatement("UPDATE book set warehouseqty=warehouseqty-? where isbn=?");
					updatewarehouse.setInt(1, qty);
					updatewarehouse.setString(2, ISBN);
					count = updatewarehouse.executeUpdate();
					
					if(count<=0)
					{
						conn.rollback();
						conn.setAutoCommit(true);
						return false;
					}
					else
					{
						conn.commit();
						conn.setAutoCommit(true);
						return true;
					}
				}
				else
				{
					conn.rollback();
					conn.setAutoCommit(true);
					return false;
				}
				
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		finally
		{
			conn.setAutoCommit(true);
		}
		return success;
	}
	
	/**
	 * Cancels the order placed with the warehouse.
	 *  
	 * @param restockorderID		The Restock Order ID
	 * @return						Returns a boolean value to indicate if the restock order has been successfully cancelled. 
	 * @throws SQLException
	 */
	public boolean cancelRestockRequest(int restockorderID) throws SQLException
	{
		int count=0;
		boolean success=false;
		PreparedStatement cancelrestock = null;
		PreparedStatement cancelCheck = null;
		
		String cancelRestockCheck = "SELECT count(*) as cnt from storerestock where restockorderid=? and restockstatus='PENDING'";
		String cancelRestockQuery = "DELETE from storerestock where restockorderid=?";
		cancelCheck = conn.prepareStatement(cancelRestockCheck);
		cancelCheck.setInt(1, restockorderID);
		ResultSet rs = cancelCheck.executeQuery();
		if(rs!=null && rs.next() && rs.getInt(1)>0)
		{	
			cancelrestock = conn.prepareStatement(cancelRestockQuery);
			cancelrestock.setInt(1, restockorderID);
			count = cancelrestock.executeUpdate();
			if(count>0)
				success=true;
			
		}
		return success;
	}
	
	/**
	 * Adds Request from the warehouse to the vendor.
	 * 
	 * @param isbn			The ISBN of the book requested from the vendor.
	 * @param vendorID		The Vendor ID from whom the request is being made. 
	 * @param qty			The Quantity for the request
	 * @param cost			The Cost for that particular request
	 * @return				Returns an integer which is the RequestID.
	 * @throws SQLException
	 */
		public int addRequest(String isbn, int vendorID, int qty,double cost ) throws SQLException
		{
			int requestID = -1;
			CallableStatement newrequest = null;
			PreparedStatement checkrequest = null;
			String checkQuery = "select (SELECT COUNT(*) AS CNT FROM vendors WHERE vendorid = ?)+(SELECT COUNT(*) AS CNT FROM book WHERE ISBN = ?)+(select count(*) as cnt from contract where vendorid=? and isbn=?) as totalcnt from dual";
			checkrequest = conn.prepareCall(checkQuery);
			checkrequest.setInt(1, vendorID);
			checkrequest.setString(2, isbn);
			checkrequest.setInt(3, vendorID);
			checkrequest.setString(4, isbn);
			ResultSet rs = checkrequest.executeQuery();
			
			if(rs!=null && rs.next())
			{
			
				if(rs.getInt(1)==3)
				{
					String restockQuery = "BEGIN INSERT INTO REQUEST (ISBN,VENDORID,QTY,cost) VALUES (?,?,?,?)"
											+"RETURNING REQUESTID INTO ?;END;";
					newrequest = conn.prepareCall(restockQuery);
					newrequest.setString(1, isbn);
					newrequest.setInt(2, vendorID);
					newrequest.setInt(3,qty);
					newrequest.setDouble(4,cost);
					newrequest.registerOutParameter(5, OracleTypes.INTEGER);
					newrequest.execute();
					requestID = newrequest.getInt(5);
				}
			}
			else
			{
				System.out.println("invalid staff/isbn/vendor");
			}
			return requestID;
		}
		
		/**
		 * Updates the request made by the warehouse to the vendor. 
		 * 
		 * @param requestID 		The requestID of the request whose status is to be changed.
		 * @param status			The status of the particular request. The request status can be PENDING, SHIPPED, COMPLETED.
		 * @return					Returns a boolean value indicating whether the update has been successful or not. 
		 * @throws SQLException
		 */
		public boolean updateRequestStatus(int requestID,String status) throws SQLException
		{
			int count=0;
			boolean success=false;
			PreparedStatement updaterequest = null;
			PreparedStatement updateCheck = null;
			
			String updateRequestCheck = "SELECT count(*) as cnt from request where requestid=?";
			String updateRequestQuery = "update request set requeststatus=? where requestid=?";
			updateCheck = conn.prepareStatement(updateRequestCheck);
			updateCheck.setInt(1, requestID);
			ResultSet rs = updateCheck.executeQuery();
			if(rs!=null && rs.next())
			{	
				if(rs.getInt(1)>0)
				{
					updaterequest = conn.prepareStatement(updateRequestQuery);
					updaterequest.setString(1, status);
					updaterequest.setInt(2, requestID);
					count = updaterequest.executeUpdate();
					if(count>0)
						success=true;
				}
				
			}
			return success;
		}
		
		/**
		 * Cancels a Request made by the warehouse to the vendor. 
		 * 
		 * @param requestID				The Request ID for the request that is to be cancelled. 
		 * @return						Returns a boolean value which indicates whether the request has been successfully cancelled or not. 
		 * @throws SQLException
		 */
		public boolean cancelRequest(int requestID) throws SQLException
		{
			int count=0;
			boolean success=false;
			PreparedStatement cancelrequest = null;
			PreparedStatement cancelCheck = null;
			
			String cancelRequestCheck = "SELECT count(*) as cnt from request where requestid=?";
			String cancelRequestQuery = "DELETE from request where requestid=?";
			cancelCheck = conn.prepareStatement(cancelRequestCheck);
			cancelCheck.setInt(1, requestID);
			ResultSet rs = cancelCheck.executeQuery();
			if(rs!=null && rs.next())
			{	
				if(rs.getInt(1)>0)
				{
					cancelrequest = conn.prepareStatement(cancelRequestQuery);
					cancelrequest.setInt(1, requestID);
						count = cancelrequest.executeUpdate();
						if(count>0)
							success=true;
				}
				
			}
			return success;
		}
		
		/**
		 * Changes the Status to completed when the Vendor has delivered to the warehouse. 
		 * 
		 * @param requestID			The RequestID of the request that is to be marked completed.
		 * @return					Returns a boolean value to indicate if that request has been marked completed or not.
		 * @throws SQLException
		 */
		public boolean completeRequest(int requestID) throws SQLException
		{
			int count=-1;
			boolean success=false;
			PreparedStatement completeRequest = null;
			PreparedStatement completeCheck = null;
			conn.setAutoCommit(false);
			
			String changeRequestCheck = "SELECT ISBN,qty from request where requestID=?";
			completeCheck = conn.prepareStatement(changeRequestCheck);
			completeCheck.setInt(1, requestID);
			ResultSet rs = completeCheck.executeQuery();
			if(rs!=null && rs.next())
			{
				int qty = rs.getInt(2);
				String ISBN = rs.getString(1);
				String completeRequestQuery = "update request set requeststatus='COMPLETED' where requestid=?";
				completeRequest = conn.prepareStatement(completeRequestQuery);
				completeRequest.setInt(1, requestID);
				count = completeRequest.executeUpdate();
				
				Vendor v=new Vendor(conn);
				if(v.addBookQty(ISBN, qty)==true)
				{
					conn.commit();
					conn.setAutoCommit(false);
					return true;
				}
				else
				{
					conn.rollback();
					conn.setAutoCommit(false);
					return false;
				}
				
				
			}
			
			return success;
		}
		
		/**
		 * 
		 * Updates warehouse and store inventory when books are delivered from warehouse to store and sets restock to complete.
		 * 
		 * @param restockID			The ID of the restock that will be completed
		 * @return					True, if the restock is successfully marked completed. False, otherwise.
		 * @throws SQLException
		 */
		public boolean completeRestock(int restockID) throws SQLException
		{
			int count=-1;
			boolean success=false;
			PreparedStatement completeRequest = null;
			PreparedStatement completeCheck = null;
			PreparedStatement checkstore=null;
			PreparedStatement updaterestock=null;
			PreparedStatement updatewarehouse=null;
			try
			{
				conn.setAutoCommit(false);
				
				String changeRequestCheck = "SELECT ISBN,qty,staffid from storerestock where restockorderID=? and restockstatus='SHIPPED'";
				completeCheck = conn.prepareStatement(changeRequestCheck);
				completeCheck.setInt(1, restockID);
				ResultSet rs = completeCheck.executeQuery();
				if(rs!=null && rs.next())
				{
					int qty = rs.getInt(2);
					String ISBN = rs.getString(1);
					int staffID = rs.getInt(3);
					Report obj = new Report(conn);
					int storeID = obj.getStoreID(staffID);
					
					String checkstoreQ = "Select count(*) as cnt from storeinventory where ISBN=? and storeID=?";
					checkstore = conn.prepareStatement(checkstoreQ);
					checkstore.setString(1, ISBN);
					checkstore.setInt(2, storeID);
					ResultSet rs2 = checkstore.executeQuery();
					int cnt=0;
					if(rs2!=null && rs2.next())
					{
						cnt = rs2.getInt(1);
					}
					String updatequant;
					if(cnt==0)
					{
						completeRequest = conn.prepareStatement("INSERT INTO storeinventory(storeid,isbn,qty) values(?,?,?)");
						completeRequest.setInt(1, storeID);
						completeRequest.setString(2, ISBN);
						completeRequest.setInt(3, qty);
					}
					else
					{
						completeRequest = conn.prepareStatement("UPDATE storeinventory set qty=qty + ? where storeid=? and isbn=?");
						completeRequest.setInt(1, qty);
						completeRequest.setInt(2, storeID);
						completeRequest.setString(3, ISBN);
					}
					String updateRestockrQuery = "UPDATE storerestock set restockstatus=? where restockorderid=?";
					updaterestock = conn.prepareStatement(updateRestockrQuery);
					
					updaterestock.setString(1, "COMPLETED");
					updaterestock.setInt(2,restockID);
					count = updaterestock.executeUpdate();
					if(count<=0)
					{
						conn.rollback();
						conn.setAutoCommit(true);
						return false;
					}
					
					count = completeRequest.executeUpdate();
					
					if(count<=0)
					{
						conn.rollback();
						conn.setAutoCommit(true);
						return false;
					}
					else
					{
						conn.commit();
						conn.setAutoCommit(true);
						return true;
					}
					
					
					
				}
			}
			finally
			{
				conn.setAutoCommit(true);
			}
			return false;
		}
}


