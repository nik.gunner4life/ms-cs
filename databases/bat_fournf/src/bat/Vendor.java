/**
 * 
 */
package bat;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import oracle.jdbc.internal.OracleTypes;

/**
 * @author Roopak Venkatakrishnan
 *
 */
public class Vendor {
	
	private Connection con;
	
	/**
	 * 
	 * Set the connection to be used.
	 * 
	 * @param con
	 */
	public Vendor(Connection con) {
		this.con = con;
	}
	
	/**************************************
	 * 
	 * Adds a vendor.
	 * 
	 * @param name 				The name of the vendor to be added.
	 * @param address			The address of the vendor to be added.
	 * @param phone				The phone of the vendor to be added.
	 * @return					Vendor ID if vendor is successfully added, -1 if failed
	 * @throws SQLException
	 */
	public int addVendor(String name,String address,String phone) throws SQLException{
		
		int venid=-1;
		CallableStatement addVendor = null;
		String addVendorQuery = "BEGIN INSERT INTO vendors(name,address,phone) VALUES(?, ?, ?) returning vendorid into ?;END;";
		
		addVendor = con.prepareCall(addVendorQuery);
		addVendor.setString(1, name);
		addVendor.setString(2, address);
		addVendor.setString(3, phone);
		addVendor.registerOutParameter(4,OracleTypes.NUMBER);
		addVendor.execute();
		venid = addVendor.getInt(4);
		return venid;
	}
	
	/**
	 * Update a vendor to change his address and phone
	 * 
	 * @param VendorID			The ID of the vendor whose address and phone are being changed.
	 * @param address			The new address of the vendor.
	 * @param phone				The new phone of the vendor.
	 * @return					True if successful, false otherwise.
	 * @throws SQLException
	 */
	public boolean updateVendor(int VendorID,String address,String phone) throws SQLException{
		
		int count=0;
		boolean success=false;
		PreparedStatement updateVendor = null;
		PreparedStatement updateCheck = null;
		
		String updateVendorCheck = "SELECT count(*) as cnt from vendors where vendorid=?";
		String updateVendorQuery = "UPDATE vendors set address=?,phone=? where vendorid=?";
		updateCheck = con.prepareStatement(updateVendorCheck);
		updateCheck.setInt(1, VendorID);
		ResultSet rs = updateCheck.executeQuery();
		if(rs!=null && rs.next())
		{
			if(rs.getInt(1)>0)
			{
				updateVendor = con.prepareStatement(updateVendorQuery);
				updateVendor.setString(1, address);
				updateVendor.setString(2, phone);
				updateVendor.setInt(3, VendorID);
				count = updateVendor.executeUpdate();
				if(count>0)
					success=true;
			}
		}
		return success;
	}
	
	/**
	 * Delete a vendor from the database.
	 * 
	 * @param VendorID			The ID of the vendor to be deleted.
	 * @return					True if successful, false otherwise.
	 * @throws SQLException
	 */
	public boolean deleteVendor(int VendorID) throws SQLException
	{
		int count=0;
		boolean success=false;
		PreparedStatement deleteVendor = null;
		PreparedStatement deleteCheck = null;
		
		String deleteVendorCheck = "SELECT count(*) as cnt from vendors where vendorid=?";
		String deleteVendorQuery = "DELETE from vendors where vendorid=?";
		deleteCheck = con.prepareStatement(deleteVendorCheck);
		deleteCheck.setInt(1, VendorID);
		ResultSet rs = deleteCheck.executeQuery();
		if(rs!=null && rs.next())
		{
			if(rs.getInt(1)>0)
			{
				deleteVendor = con.prepareStatement(deleteVendorQuery);
				deleteVendor.setInt(1, VendorID);
				count = deleteVendor.executeUpdate();
				if(count>0)
					success=true;
			}
		}
		return success;
	}
	
	/**
	 * Add a contract to the database.
	 * 
	 * @param VendorID			The vendor with whom the contract is.
	 * @param startdate			The date at which the contract is in effect.
	 * @param enddate			The date at which the contract expires.
	 * @param ISBN				The ISBN of the effort.
	 * @return					Contract ID if true, -1 otherwise.
	 * @throws SQLException
	 */
	public int addContract(int VendorID,Date startdate,Date enddate,String ISBN) throws SQLException
	{
		int count=0;
		int contractid=-1;
		PreparedStatement addContractCheck= null;
		CallableStatement addContract = null;
		
		String addContractCheckQuery = "select (SELECT count(*) as cnt from vendors where vendorid=?)+(SELECT count(*) as cnt from book where ISBN=?) as totalcnt from dual";
		String addContractQuery = "BEGIN INSERT INTO contract(startdate,enddate,vendorid,ISBN) VALUES(?, ?, ?, ?) returning contractid into ?;END;";
		addContractCheck = con.prepareStatement(addContractCheckQuery);
		addContractCheck.setInt(1, VendorID);
		addContractCheck.setString(2,ISBN);
		ResultSet rs = addContractCheck.executeQuery();
		if(rs!=null && rs.next())
		{
			if(rs.getInt(1)>=2)
			{
				addContract = con.prepareCall(addContractQuery);
				addContract.setDate(1, startdate);
				addContract.setDate(2, enddate);
				addContract.setInt(3, VendorID);
				addContract.setString(4, ISBN);
				addContract.registerOutParameter(5,OracleTypes.NUMBER);
				addContract.execute();
				contractid = addContract.getInt(5);
			}
		}
		return contractid;
	}
	
	/**
	 * Delete a contract from the database.
	 * 
	 * @param ContractID		The ID of the contract to be deleted
	 * @return					True if the deletion was successful, false otherwise.
	 * @throws SQLException
	 */
	public boolean deleteContract(int ContractID) throws SQLException
	{
		int count=0;
		boolean success=false;
		PreparedStatement deleteContract = null;
		PreparedStatement deleteCheck = null;
		
		String deleteContractCheck = "SELECT count(*) as cnt from contract where contractid=?";
		String deleteContractQuery = "DELETE from contract where contractid=?";
		deleteCheck = con.prepareStatement(deleteContractCheck);
		deleteCheck.setInt(1, ContractID);
		ResultSet rs = deleteCheck.executeQuery();
		if(rs!=null && rs.next())
		{
			if(rs.getInt(1)>0)
			{
				deleteContract = con.prepareStatement(deleteContractQuery);
				deleteContract.setInt(1, ContractID);
				count = deleteContract.executeUpdate();
				if(count>0)
					success=true;
			}
		}
		return success;
	}
	
	/**
	 * Adds a new book to the warehouse. This specifically adds a new kind of book.
	 * 
	 * Do not use {@link #addBook(String, String, String, int, String)} to add quantities of the book to the warehouse.
	 * 
	 * @param ISBN				The ISBN of the book to be added.
	 * @param title				The title of the book to be added.
	 * @param publisher			The publisher of the book to be added.
	 * @param warehouseqty		The quantity of the book that will be immediately stocked in the warehouse.
	 * @param author			The author of the book to be added.
	 * @param price				The price of the book to be added.
	 * @return					True if book is successfully added, false otherwise.
	 * @throws SQLException
	 */
	public boolean addBook(String ISBN,String title,String publisher,int warehouseqty,String author, double price) throws SQLException{
		
		boolean success=false;
		int count=0;
		PreparedStatement addCheck = null;
		CallableStatement addBook = null;
		
		String addBookQuery = "INSERT INTO book(ISBN,title,publisher,warehouseqty,author,price) VALUES(?, ?, ?, ?, ?, ?)";
		String addCheckQuery = "SELECT count(*) as cnt from book where ISBN=?";
		
		addCheck = con.prepareStatement(addCheckQuery);
		addCheck.setString(1, ISBN);
		ResultSet rs = addCheck.executeQuery();
		if(rs!=null && rs.next())
		{
			if(rs.getInt(1)==0)
			{
				addBook = con.prepareCall(addBookQuery);
				addBook.setString(1, ISBN);
				addBook.setString(2, title);
				addBook.setString(3, publisher);
				addBook.setInt(4, warehouseqty);
				addBook.setString(5, author);
				addBook.setDouble(6, price);
				count = addBook.executeUpdate();
				if(count>0)
					success=true;
			}
		}
		return success;
	}
	
	/**
	 * Add a quantity of books to the warehouse.
	 * 
	 * @param ISBN				The ISBN of the book that you're adding quantities of.
	 * @param newNo				The number of books you are adding.
	 * @return					True if quantity successfully added, false otherwise.
	 * @throws SQLException
	 */
	public boolean addBookQty(String ISBN,int newNo) throws SQLException{
		
		int count=0;
		boolean success=false;
		PreparedStatement addBookQty = null;
		PreparedStatement addBookCheck = null;
		
		String addBookCheckQuery = "SELECT count(*) as cnt from Book where ISBN=?";
		String addBookQtyQuery = "UPDATE Book set warehouseqty=warehouseqty+? where ISBN=?";
		addBookCheck = con.prepareStatement(addBookCheckQuery);
		addBookCheck.setString(1, ISBN);
		ResultSet rs = addBookCheck.executeQuery();
		if(rs!=null && rs.next())
		{
			if(rs.getInt(1)>0)
			{
				addBookQty = con.prepareStatement(addBookQtyQuery);
				addBookQty.setInt(1, newNo);
				addBookQty.setString(2, ISBN);
				count = addBookQty.executeUpdate();
				if(count>0)
					success=true;
			}
		}
		return success;
	}
	
	/**
	 * Remove a quantity of books from the warehouse 
	 * 
	 * @param ISBN				The ISBN of the books that are being removed.
	 * @param newNo				The number of books that are being removed.
	 * @return					True if the books were successfully removed, false otherwise.
	 * @throws SQLException
	 */
	public boolean removeBookQty(String ISBN,int newNo) throws SQLException{
		
		int count=0;
		boolean success=false;
		PreparedStatement removeBookQty = null;
		PreparedStatement removeBookCheck = null;
		
		String removeBookCheckQuery = "SELECT count(*) as cnt,warehouseqty from Book where ISBN=? group by warehouseqty";
		String removeBookQtyQuery = "UPDATE Book set warehouseqty=warehouseqty-? where ISBN=?";
		removeBookCheck = con.prepareStatement(removeBookCheckQuery);
		removeBookCheck.setString(1, ISBN);
		ResultSet rs = removeBookCheck.executeQuery();
		if(rs!=null && rs.next())
		{
			if(rs.getInt(1)>0)
			{
				if(rs.getInt(2)>newNo)
				{
					removeBookQty = con.prepareStatement(removeBookQtyQuery);
					removeBookQty.setInt(1, newNo);
					removeBookQty.setString(2, ISBN);
					count = removeBookQty.executeUpdate();
					if(count>0)
						success=true;
				}
			}
		}
		return success;
	}
	
	/**
	 * Get the number of books of a specific ISBN that are stored in the warehouse.
	 * 
	 * @param ISBN				The ISBN of the books to count
	 * @return					The number of books if the book is known to us, -1 otherwise
	 * @throws SQLException
	 */
	public int getBookQty(String ISBN) throws SQLException{
		
		int count=-1;
		boolean success=false;
		PreparedStatement getBookQty = null;
		PreparedStatement getBookCheck = null;
		
		String removeBookCheckQuery = "SELECT count(*) as cnt,warehouseqty from Book where ISBN=? group by warehouseqty";
		getBookCheck = con.prepareStatement(removeBookCheckQuery);
		getBookCheck.setString(1, ISBN);
		ResultSet rs = getBookCheck.executeQuery();
		if(rs!=null && rs.next())
		{
			if(rs.getInt(1)>0)
			{
				return rs.getInt(2);
			}
		}
		return count;
	}
	
	/**
	 * Add a new type of book to the store.
	 * 
	 * @param storeID			The ID of the store that we are adding the book to.
	 * @param ISBN				The ISBN of the book that we are adding.
	 * @param qty				The quantity of these books that we are adding.
	 * @return					True if the books were successfully added, false otherwise.
	 * @throws SQLException
	 */
	public boolean insertBooktoStore(int storeID,String ISBN,int qty) throws SQLException
	{
		boolean success=false;
		int count=0;
		PreparedStatement addCheck = null;
		CallableStatement addBook = null;
		
		String insertBookQuery = "INSERT INTO storeinventory(storeID,ISBN,qty) VALUES(?, ?, ?)";
		String insertCheckQuery = "select (SELECT count(*) as cnt from store where storeid=?)+(SELECT count(*) as cnt from book where ISBN=?)+(SELECT count(*) as cnt from storeinventory where ISBN=? and storeID=?) as totalcnt from dual";
		
		addCheck = con.prepareStatement(insertCheckQuery);
		addCheck.setInt(1, storeID);
		addCheck.setString(2, ISBN);
		addCheck.setString(3, ISBN);
		addCheck.setInt(4, storeID);
		
		
		ResultSet rs = addCheck.executeQuery();
		if(rs!=null && rs.next())
		{
			if(rs.getInt(1)==2)
			{
				addBook = con.prepareCall(insertBookQuery);
				addBook.setInt(1, storeID);
				addBook.setString(2, ISBN);
				addBook.setInt(3, qty);
				count = addBook.executeUpdate();
				if(count>0)
					success=true;
			}
		}
		return success;
	}
	
	/**
	 * Change the quantity of a specific book in a specific store.
	 * 
	 * @param storeID			The ID of the store where the book's quantity is being modified.
	 * @param ISBN				The ISBN of the book whose quantity is being modified.
	 * @param newqty			The new quantity of this book at this store.
	 * @return					True if the number of books was correctly set, false otherwise.
	 * @throws SQLException
	 */
	public boolean updateBookQtyStore(int storeID,String ISBN,int newqty) throws SQLException
	{
		boolean success=false;
		int count=0;
		PreparedStatement addCheck = null;
		CallableStatement addBook = null;
		
		String insertBookQuery = "update storeinventory set qty=qty+? where storeID=? and ISBN=?";
		String insertCheckQuery = "select (SELECT count(*) as cnt from store where storeid=?)+(SELECT count(*) as cnt from book where ISBN=?)+(SELECT count(*) as cnt from storeinventory where ISBN=? and storeID=?) as totalcnt from dual";
		
		addCheck = con.prepareStatement(insertCheckQuery);
		addCheck.setInt(1, storeID);
		addCheck.setString(2, ISBN);
		addCheck.setString(3, ISBN);
		addCheck.setInt(4, storeID);
		
		
		ResultSet rs = addCheck.executeQuery();
		if(rs!=null && rs.next())
		{
			if(rs.getInt(1)==3)
			{
				addBook = con.prepareCall(insertBookQuery);
				addBook.setInt(1, newqty);
				addBook.setInt(2, storeID);
				addBook.setString(3, ISBN);
				count = addBook.executeUpdate();
				if(count>0)
					success=true;
			}
		}
		return success;
	}
	
	/**
	 * Removes a number of books of a specific ISBN from a specific store.
	 * 
	 * @param storeID			The ID of the store from which books are being removed.
	 * @param ISBN				The ISBN of the books which are being removed.
	 * @param newqty			The quantity of books being removed.
	 * @return					True if the books are successfully removed, false otherwise.
	 * @throws SQLException
	 */
	public boolean removeBookQtyStore(int storeID,String ISBN,int newqty) throws SQLException
	{
		boolean success=false;
		int count=0;
		PreparedStatement addCheck = null;
		CallableStatement addBook = null;
		
		String insertBookQuery = "update storeinventory set qty=qty-? where storeID=? and ISBN=?";
		String insertCheckQuery = "select (SELECT count(*) as cnt from store where storeid=?)+(SELECT count(*) as cnt from book where ISBN=?)+(SELECT count(*) as cnt from storeinventory where ISBN=? and storeID=? and qty>=?) as totalcnt from dual";
		
		addCheck = con.prepareStatement(insertCheckQuery);
		addCheck.setInt(1, storeID);
		addCheck.setString(2, ISBN);
		addCheck.setString(3, ISBN);
		addCheck.setInt(4, storeID);
		addCheck.setInt(5, newqty);
		
		ResultSet rs = addCheck.executeQuery();
		if(rs!=null && rs.next())
		{
			if(rs.getInt(1)==3)
			{
				addBook = con.prepareCall(insertBookQuery);
				addBook.setInt(1, newqty);
				addBook.setInt(2, storeID);
				addBook.setString(3, ISBN);
				count = addBook.executeUpdate();
				if(count>0)
					success=true;
			}
		}
		return success;
	}
	
	/**
	 * Returns the number of books of a specific ISBN present in a specific store.
	 * 
	 * @param storeID			The ID of the store which we want information about.
	 * @param ISBN				The ISBN of the book that we want the quantity of.
	 * @return					The number of books with ISBN present in store storeID.
	 * @throws SQLException
	 */
	public int getBookQtyStore(int storeID,String ISBN) throws SQLException
	{
		boolean success=false;
		int count=-1;
		PreparedStatement addCheck = null;
		CallableStatement addBook = null;
		
		String insertBookQuery = "select qty from storeinventory where storeID=? and ISBN=?";
				
		addCheck = con.prepareStatement(insertBookQuery);
		addCheck.setInt(1, storeID);
		addCheck.setString(2, ISBN);
		
		ResultSet rs = addCheck.executeQuery();
		if(rs!=null && rs.next())
		{
			count = rs.getInt(1);
		}
		return count;
	}
}
